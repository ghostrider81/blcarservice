<?php include "php/functions.php"; ?>

<!DOCTYPE html>
<html lang="de">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon.ico">

    <title>INSPINIA | Main view</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="profile-element">
                        <img src="img/logo.png" class="img-responsive logo-large center-block" />
                    </div>
                    <div class="logo-element">
                        <img src="img/logo.png" class="img-responsive logo-small" />
                    </div>
                </li>
                <li>
                    <a href="#" id="dashboard" class="active" data-navcomponent="true">
                        <i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span>
                    </a>
                    <ul class="nav nav-second-level collapse hidden">
                        <li><a href="#" data-navcomponent="true">dummy</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-user"></i> <span class="nav-label">Kontakte</span> <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#" id="contactNew" data-navcomponent="true">Neuen Kontakt erstellen</a></li>
                        <li><a href="#" id="contactList" data-navcomponent="true">Alle Kontakte anzeigen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-sticky-note"></i> <span class="nav-label">Aufträge</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#" id="assignmentNew" data-navcomponent="true">Neuen Auftrag anlegen</a></li>
                        <li><a href="#" id="assignmentList" data-navcomponent="true">Alle Aufträge anzeigen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-sticky-note"></i> <span class="nav-label">Rechnungen</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#" id="invoiceNew" data-navcomponent="true">Neue Rechnung anlegen</a></li>
                        <li><a href="#" id="invoiceListAll" data-navcomponent="true">Alle Rechnungen anzeigen</a></li>

                        <ul class="nav nav-third-level collapse in" aria-expanded="true" style="">
                            <li>
                                <a href="#" id="invoiceListDraft" data-navcomponent="true">Entwürfe <span class="label label-info counter-badge" data-state="0"></span></a>
                            </li>
                            <li>
                                <a href="#" id="invoiceListOpen" data-navcomponent="true">Offen <span class="label label-info counter-badge" data-state="1"></span></a>
                            </li>
                            <li>
                                <a href="#" id="invoiceListDue" data-navcomponent="true">Fällig <span class="label label-info counter-badge" data-state="2"></span></a>
                            </li>
                            <li>
                                <a href="#" id="invoiceListPaid" data-navcomponent="true">Bezahlt <span class="label label-info counter-badge" data-state="3"></span></a>
                            </li>

                        </ul>


                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-archive"></i> <span class="nav-label">Reifenlager</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#" id="racksList" data-navcomponent="true">Regale einsehen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Einstellungen</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#" id="settingsGeneral" data-navcomponent="true">Allgemein</a></li>
                        <li><a href="#" id="settingsCompany" data-navcomponent="true">Firmendaten</a></li>
                        <li><a href="#" id="settingsParts" data-navcomponent="true">Teileverwaltung</a></li>
                        <li><a href="#" id="settingsAw" data-navcomponent="true">AW Verwaltung</a></li>
                        <li><a href="#" id="settingsUser" data-navcomponent="true">User Verwaltung</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" placeholder="Suche..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <div class="dropdown head-dropdown pull-right user-box">
                    <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="fa fa-user" aria-hidden="true"></i> Angemeldet als Sven
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i> Ausloggen</a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- Dynamic Area -->
        <div class="row main-container" id="dynamicContent">



        </div>
        <!-- /Dynamic Area -->

        <div class="footer">
            <div class="pull-right">
                <strong>&copy;</strong> B&L Carservice &copy; 2014-2017
            </div>
        </div>

    </div>
</div>

<!-- FRAMEWORKS MAINSCRIPT -->
<script src="js/frameworks/jquery-3.3.1.min.js"></script>
<script src="js/frameworks/jquery-migrate-3.0.1.min.js"></script>
<script src="js/frameworks/jquery-ui-1.10.4.min.js"></script>
<script src="js/frameworks/decimal.js"></script>
<script src="js/frameworks/bootstrap.min.js"></script>
<script src="js/frameworks/datatables.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- FRAMEWORKS -->
<script src="js/frameworks/framework.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<!-- CUSTOM  -->
<script src="js/config.js"></script>
<script src="js/functions.js"></script>
<script src="js/modules/assignment.js"></script>
<script src="js/modules/invoice.js"></script>
<script src="js/modules/listener.js"></script>
<script src="js/modules/wheels.js"></script>
<script src="js/modules/contacts.js"></script>



</body>

</html>
