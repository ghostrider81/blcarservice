<?php

// Alle Kontakte ausgeben
function contactsGetAll($pdo) {

    // MySQL Query via PDO prepared Statement
    $stmt = $pdo->prepare("SELECT contact_id, kdnnr, name, vorname, strasse, plz, ort, telefon_1 FROM kontakte");

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo "Keine Kontakte vorhanden";
    }
}

// Einzelnen Kontakt nach contactId ausgeben
function contactsGetDetail($pdo, $contactId) {

    // MySQL Query via PDO prepared Statement
    $stmt = $pdo->prepare("SELECT kontakte.*, fahrzeuge.*
                                FROM kontakte
                                LEFT JOIN fahrzeuge ON fahrzeuge.contact_id=kontakte.contact_id WHERE kontakte.contact_id = :contactId");
    $stmt->bindParam(':contactId', $contactId);

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        echo PDO::errorInfo();
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo "Es wurde kein Kontakt zu der übertragenen ID gefunden";
    }
}

// Variablen auf Inhalt prüfen
function checkVarContent($replaceString, $source)
    {
        $output = "";
        if(empty($source))
            {
                $output = $replaceString;
            }

        else
            {
                $output = $source;
            }

        return $output;
    }

