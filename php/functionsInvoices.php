<?php
error_reporting(E_ALL);

// Alle Aufträge ausgeben
function invoiceGetAll($pdo, $status) {

    if($status != "all") {
        $additional_query = "WHERE r.status = ".$status;
    } else {
        $additional_query = "";
    }

    $query = "  SELECT 
                  kontakte.name, 
                  kontakte.vorname, 
                  kontakte.kdnnr, 
                SUM(p.anzahl * p.preis) gesamtbetrag,
                  r.billing_id, 
                  r.billing_nr, 
                  r.date_billing, 
                  r.date_target, 
                  r.payment_method, 
                  r.operator,
                  r.barverkauf,
                  r.tuv 
                FROM 
                  rechnung r 
                LEFT JOIN 
                  positionen_rechnung p 
                ON 
                  r.billing_id = p.billing_id 
                LEFT JOIN 
                  kontakte 
                ON 
                  r.contact_id = kontakte.contact_id
                ".$additional_query."
                GROUP BY 
                  r.billing_id";

    $stmt = $pdo->prepare($query);


    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        return false;
    }
}


function check_articles_invoice($pdo, $desc) {

	$stmt = $pdo->prepare("SELECT artikel_id, bezeichnung FROM positionen WHERE LOWER(bezeichnung) = LOWER(:description)");
	$stmt->bindParam(':description', $desc);

	if(!$stmt->execute())
	{
		$pdo = NULL;
		echo "Bei der Abfrage ist ein Fehler unterlaufen";
	}

	else {

		if($stmt->rowCount() > 0) {
			$result = $stmt->fetchAll(PDO::FETCH_OBJ);
			$artikel_id = $result[0]->artikel_id;
			return $artikel_id;

		}

		else {
			// BUILD MYSQL QUERY
			$stmt_art = $pdo->prepare("INSERT INTO positionen
                ( bezeichnung )                  
              VALUES
				( :bezeichnung )");

			// PREPARE MYSQL STATEMENT
			$stmt_art->bindParam(':bezeichnung', $desc);

			if(!$stmt_art->execute())
			{
				$pdo = NULL;
				echo "Bei der Abfrage ist ein Fehler unterlaufen";
			}

			else {
				return $pdo->lastInsertId();
			}
		}
	}
}


/** TODO:  need Operators..  **/
function getOperators($pdo) {
//    $operators = [['operator' => 'Jack O Neil'], ['operator' => 'Daniel Jackson'], ['operator' => 'Rambo']];
//
//    echo json_encode($operators);
}


function get_cat_id($block_id) {
	if(empty($block_id)) {
		return false;
	}

	else {
		switch($block_id) {
			case "4":
				return 0;
				break;
			case "5":
				return 1;
				break;
			case "6":
				return 2;
				break;
			case "7":
				return 3;
				break;
			case "8":
				return 4;
				break;
			case "9":
				return 5;
				break;
		}
	}
}


// Nächste Rechnungsnummer generieren
function getNextInvoiceNumber($pdo) {

    // LETZTE VERGEBENE RECHNUNGSNUMMER UND AKTUELLES JAHR AUSLESEN
    $query_billing_nr = $pdo->prepare("SELECT billing_nr FROM rechnung ORDER BY billing_id DESC LIMIT 1");
    $query_billing_nr->execute();

    $aktuelles_jahr = date("Y");

    if($query_billing_nr->rowCount() > 0) {

        $result_billing_nr = $query_billing_nr->fetchAll(PDO::FETCH_OBJ);

        $letzte_rg = $result_billing_nr[0]->billing_nr;


        // LETZTE VERGEBENE RECHNUNGSNUMMER TEILEN UND ÜBERPRÜFEN
        $parts_rg = explode("/", $letzte_rg);

        // WENN AKTUELLES JAHR MIT DEM LETZTEN NR ÜBEREINSTIMMT, UM EINEN ERHÖHEN
        if ($aktuelles_jahr == $parts_rg[1]) {
            $neue_rg = $parts_rg[0] + 1;
            $neue_rg = $neue_rg . "/" . $aktuelles_jahr;
            return $neue_rg;
        } // WENN NICHT, DANN AKTUELLES JAHR UND LFNDNR AUF 1 SETZEN
        else {
            $neue_rg = "1/" . $aktuelles_jahr;
            return $neue_rg;
        }
    }
    else {
        $neue_rg = "1/" . $aktuelles_jahr;
        return $neue_rg;
    }

}



/*** Action Handler ***/
if(isset($_REQUEST['state']))
{
    switch ($_REQUEST['state']) {
        case 'getInvoiceEdit':
            include "../classes/sqlConnect.php";
            getInvoiceEdit($pdo);
            break;
        case 'setNewInvoice':
            include "../classes/sqlConnect.php";
            setNewInvoice($pdo);
            break;
        case 'updateInvoice':
            include "../classes/sqlConnect.php";
            updateInvoice($pdo);
            break;
        case 'getNextInvoiceNumber':
            include "../classes/sqlConnect.php";
            getNextInvoiceNumber($pdo);
            break;
        case 'getOperators':
            include "../classes/sqlConnect.php";
            getOperators($pdo);
            break;
        case 'getStatusCount':
            include "../classes/sqlConnect.php";
            get_billing_status_qty($pdo);
            break;
    }
}



function getInvoiceEdit($pdo) {
    $billingID = $_REQUEST['billingID'];
    $sqlQuery = "SELECT * FROM rechnung
                 LEFT JOIN fahrzeuge ON rechnung.car_id = fahrzeuge.car_id
           LEFT JOIN positionen_rechnung ON rechnung.billing_id = positionen_rechnung.billing_id
           LEFT JOIN kontakte ON rechnung.contact_id = kontakte.contact_id WHERE rechnung.billing_id = ".$billingID;

    $stmt = $pdo->prepare($sqlQuery);
//    var_dump($sqlQuery);

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Invoice Query failed.";
    }


    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        //var_dump($result);
        foreach($result as $key=>$res)
        {
            //var_dump($res->artikel_id);
            //$result[$res]
            //echo $res[$key];
            //$res[$key] = $res->artikel_id;
            //var_dump($result);
        }
        echo json_encode($result);
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo "Invoice not found.";
    }
}


function array_flatten($array) {
	$return = array();
	foreach ($array as $key => $value) {
		if (is_array($value)){ $return = array_merge($return, array_flatten($value));}
		else {$return[$key] = $value;}
	}
	return $return;
}

function multiKeyExists(array $arr, $key) {

    // is in base array?
    if (array_key_exists($key, $arr)) {
        return true;
    }

    // check arrays contained in this array
    foreach ($arr as $element) {
        if (is_array($element)) {
            if (multiKeyExists($element, $key)) {
                return true;
            }
        }

    }

    return false;
}



function refreshKilometer($pdo, $car_id, $kilometer) {
    // BUILD MYSQL QUERY

    $stmt = $pdo->prepare("UPDATE fahrzeuge SET km = :km WHERE car_id = :car_id");
    $stmt->bindParam(':km', $kilometer);
    $stmt->bindParam(':car_id', $car_id);

    // EXECUTE MYSQL QUERY
    if(!$stmt->execute())
    {
        print_r($stmt->errorInfo());
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    else
    {
        return true;
    }
}


function setNewInvoice($pdo) {
	// GET DATAPACKAGE

    $dataSet = $_REQUEST;


    var_dump($dataSet);
    // BUILD AND FORMAT DATA

    if($dataSet['data'][1]['contact_id'] == "barverkauf") {
        $contact_id = 0;
        $barverkauf = 1;
    }

    else {
        $contact_id = $dataSet['data'][1]['contact_id'];
        $barverkauf = 0;
    }

    $car_id = multiKeyExists($dataSet['data'], "car_id") ? $dataSet['data'][1]['car_id'] : 0;

    $tuv = $dataSet['data'][2]['carInspectionResul1'] != "" ? $tuv = number_format(floatval($dataSet['data'][2]['carInspectionResul1']), 2,".", "") : "";



    $status = $dataSet['data'][1]['status'];
    $invoiceNumber = $dataSet['data'][0]['invoiceNumbe1'];
    $invoiceDate = make_unix_stamp($dataSet['data'][0]['invoiceDat0']);
    $payableDate = make_unix_stamp($dataSet['data'][0]['payableDat4']);
    $kmstand = $dataSet['data'][0]['kmStan3'];
    $paymentMethod = $dataSet['data'][1]['paymentMethod'];
    $operator = $dataSet['data'][1]['sachbearbeiter'];
    $aw1 = $dataSet['data'][1]['mechanik_aw'];
    $aw2 = $dataSet['data'][1]['karosserie_aw'];
    $aw3 = $dataSet['data'][1]['lack_aw'];
    $aw4 = $dataSet['data'][1]['elektrik_aw'];
    $notes = $dataSet['data'][3]['note0'];
    $att_desc = "";
    $att_percent = "";
    $att_price = "";


    // BUILD MYSQL QUERY
    $stmt = $pdo->prepare("INSERT INTO rechnung
        ( contact_id, 
          barverkauf, 
          car_id, 
          status, 
          billing_nr, 
          date_billing, 
          date_target, 
          kilometerstand, 
          payment_method, 
          operator, 
          aw_1, 
          aw_2, 
          aw_3, 
          aw_4, 
          notices, 
          tuv, 
          att_desc, 
          att_percent, 
          att_price )
          
      VALUES
		( :contact_id, 
		  :barverkauf, 
		  :car_id, 
		  :status, 
		  :billing_nr, 
		  :date_billing, 
		  :date_target, 
		  :kilometerstand, 
		  :payment_method, 
		  :operator, 
		  :aw_1, 
		  :aw_2, 
		  :aw_3, 
		  :aw_4, 
		  :notices, 
		  :tuv, 
		  :att_desc, 
		  :att_percent, 
		  :att_price )");


    // PREPARE MYSQL STATEMENT
    $stmt->bindParam(':contact_id', $contact_id);
    $stmt->bindParam(':barverkauf', $barverkauf);
    $stmt->bindParam(':car_id', $car_id);
    $stmt->bindParam(':status', $status);
    $stmt->bindParam(':billing_nr', $invoiceNumber);
    $stmt->bindParam(':date_billing', $invoiceDate);
    $stmt->bindParam(':date_target', $payableDate);
    $stmt->bindParam(':kilometerstand', $kmstand);
    $stmt->bindParam(':payment_method', $paymentMethod);
    $stmt->bindParam(':operator', $operator);
    $stmt->bindParam(':aw_1', $aw1);
    $stmt->bindParam(':aw_2', $aw2);
    $stmt->bindParam(':aw_3', $aw3);
    $stmt->bindParam(':aw_4', $aw4);
    $stmt->bindParam(':notices', $notes);
    $stmt->bindParam(':tuv', $tuv);
    $stmt->bindParam(':att_desc', $att_desc);
    $stmt->bindParam(':att_percent', $att_percent);
    $stmt->bindParam(':att_price', $att_price);


	// EXECUTE MYSQL QUERY -> RETURNS BILLING_ID IN CASE OF SUCCESS
	if(!$stmt->execute())
	{
		print_r($stmt->errorInfo());
		$pdo = NULL;
		echo "Bei der Abfrage ist ein Fehler unterlaufen [exception new_invoice_save]";
	}

	else
	{
		$billing_id = $pdo->lastInsertId();
	}

    // REFRESH KILOMETERS
    refreshKilometer($pdo, $car_id, $kmstand);

    ///////////////////////////////////////////////////////////////////////
    ///
	//     $dataset['data'][4] = Pauschale Arbeitslohn Positionen -> cat0
	//     $dataset['data'][5] = Arbeitslohn Mechanik -> cat1
	//     $dataset['data'][6] = Arbeitslohn Karosserie -> cat2
	//     $dataset['data'][7] = Arbeitslohn Lack -> cat3
	//     $dataset['data'][8] = Arbeitslohn Elekrik -> cat4
	//     $dataset['data'][9] = Material -> cat5
    ///
    ///////////////////////////////////////////////////////////////////////



	for($i=4; $i<=9; $i++) {

        $result = array_flatten($dataSet['data'][$i]);
        $splitted = array_chunk($result, 4);


		///////////////////////////////////////////////////////////////////////
		///
		//     $split[0] -> Position
		//     $split[1] -> Bezeichnung
		//     $split[2] -> Anzahl
		//     $split[3] -> Preis
		///
		///////////////////////////////////////////////////////////////////////


		$cat_id = get_cat_id($i);

        foreach($splitted AS $split) {

            if($split[1] != "" && $split[2] != "" && $split[3] != "")
            {
                $artikel_id = check_articles_invoice($pdo, $split[1]);

                // BUILD MYSQL QUERY
                $stmt = $pdo->prepare("INSERT INTO positionen_rechnung (cat_id, artikel_id, billing_id, anzahl, preis) VALUES (:cat_id, :artikel_id, :billing_id, :anzahl, :preis)");

                // PREPARE MYSQL STATEMENT
                $stmt->bindParam(':cat_id', $cat_id);
                $stmt->bindParam(':artikel_id', $artikel_id);
                $stmt->bindParam(':billing_id', $billing_id);
                $stmt->bindParam(':anzahl', $split[2]);
                $stmt->bindParam(':preis', $split[3]);

                // EXECUTE MYSQL QUERY -> RETURNS BILLING_ID IN CASE OF SUCCESS
                if(!$stmt->execute())
                {
                    echo PDO::errorInfo();
                    $pdo = NULL;
                    echo "Bei der Abfrage ist ein Fehler unterlaufen";
                }
            }
        }
    }
}


function updateInvoice($pdo) {
	// GET DATAPACKAGE

	$dataSet = $_REQUEST;

	// BUILD AND FORMAT DATA

	if($dataSet['data'][1]['contact_id'] == "barverkauf") {
		$contact_id = 0;
		$barverkauf = 1;
		echo "Contact-ID: ".$contact_id;
		echo "Barverkauf: ".$barverkauf;
	}

	else {
		$contact_id = $dataSet['data'][1]['contact_id'];
		$barverkauf = 0;
		echo "Contact-ID: ".$contact_id;
		echo "Barverkauf: ".$barverkauf;
	}

	echo "test 1";

	$car_id = multiKeyExists($dataSet['data'], "car_id") ? $dataSet['data'][1]['car_id'] : 0;

	echo "test 2";

	$tuv = $dataSet['data'][2]['carInspectionResul1'] != "" ? $tuv = number_format(floatval($dataSet['data'][2]['carInspectionResul1']), 2,".", "") : "";

	echo "test 3";

	$invoice_id = $dataSet['data'][1]['billing_id'];
	$status = $dataSet['data'][1]['status'];
	$invoiceNumber = $dataSet['data'][0]['invoiceNumbe1'];
	$invoiceDate = make_unix_stamp($dataSet['data'][0]['invoiceDat0']);
	$payableDate = make_unix_stamp($dataSet['data'][0]['payableDat4']);
	$kmstand = $dataSet['data'][0]['kmStan3'];
	$paymentMethod = $dataSet['data'][1]['paymentMethod'];
	$operator = $dataSet['data'][1]['sachbearbeiter'];
	$aw1 = $dataSet['data'][1]['mechanik_aw'];
	$aw2 = $dataSet['data'][1]['karosserie_aw'];
	$aw3 = $dataSet['data'][1]['lack_aw'];
	$aw4 = $dataSet['data'][1]['elektrik_aw'];
	$notes = $dataSet['data'][3]['note0'];
	$att_desc = "";
	$att_percent = "";
	$att_price = "";

	echo "test 4";

	// BUILD MYSQL QUERY
	$stmt = $pdo->prepare("UPDATE rechnung SET 
					          contact_id = :contact_id, 
					          barverkauf = :barverkauf, 
					          car_id = :car_id, 
					          status = :status, 
					          billing_nr = :billing_nr, 
					          date_billing = :date_billing, 
					          date_target = :date_target, 
					          kilometerstand = :kilometerstand, 
					          payment_method = :payment_method, 
					          operator = :operator, 
					          aw_1 = :aw_1, 
					          aw_2 = :aw_2, 
					          aw_3 = :aw_3, 
					          aw_4 = :aw_4, 
					          notices = :notices, 
					          tuv = :tuv, 
					          att_desc = :att_desc, 
					          att_percent = :att_percent, 
					          att_price = :att_price 
					        WHERE billing_id = :invoice_id");

	echo "test 5";

	// PREPARE MYSQL STATEMENT
	$stmt->bindParam(':contact_id', $contact_id);
	$stmt->bindParam(':barverkauf', $barverkauf);
	$stmt->bindParam(':car_id', $car_id);
	$stmt->bindParam(':status', $status);
	$stmt->bindParam(':billing_nr', $invoiceNumber);
	$stmt->bindParam(':date_billing', $invoiceDate);
	$stmt->bindParam(':date_target', $payableDate);
	$stmt->bindParam(':kilometerstand', $kmstand);
	$stmt->bindParam(':payment_method', $paymentMethod);
	$stmt->bindParam(':operator', $operator);
	$stmt->bindParam(':aw_1', $aw1);
	$stmt->bindParam(':aw_2', $aw2);
	$stmt->bindParam(':aw_3', $aw3);
	$stmt->bindParam(':aw_4', $aw4);
	$stmt->bindParam(':notices', $notes);
	$stmt->bindParam(':tuv', $tuv);
	$stmt->bindParam(':att_desc', $att_desc);
	$stmt->bindParam(':att_percent', $att_percent);
	$stmt->bindParam(':att_price', $att_price);
	$stmt->bindParam(':invoice_id', $invoice_id);

    // REFRESH KILOMETERS
    refreshKilometer($pdo, $car_id, $kmstand);

	// EXECUTE MYSQL QUERY -> RETURNS BILLING_ID IN CASE OF SUCCESS
	if(!$stmt->execute())
	{
		print_r($stmt->errorInfo());
		$pdo = NULL;
		echo "Bei der Abfrage ist ein Fehler unterlaufen";
	}

	else
	{
		$stmt2 = $pdo->prepare("DELETE FROM positionen_rechnung WHERE billing_id = :invoice_id");
		$stmt2->bindParam(':invoice_id', $invoice_id);
		if(!$stmt2->execute())
		{
			print_r($stmt2->errorInfo());
			$pdo = NULL;
			echo "Bei der Abfrage ist ein Fehler unterlaufen";

		}

		else {
			for($i=4; $i<=9; $i++) {

				$result = array_flatten($dataSet['data'][$i]);
				$splitted = array_chunk($result, 4);

				$cat_id = get_cat_id($i);

				foreach($splitted AS $split) {

					if($split[1] != "" && $split[2] != "" && $split[3] != "")
					{
						$artikel_id = check_articles_invoice($pdo, $split[1]);

						// BUILD MYSQL QUERY
						$stmt = $pdo->prepare("INSERT INTO positionen_rechnung (cat_id, artikel_id, billing_id, anzahl, preis) VALUES (:cat_id, :artikel_id, :billing_id, :anzahl, :preis)");

						// PREPARE MYSQL STATEMENT
						$stmt->bindParam(':cat_id', $cat_id);
						$stmt->bindParam(':artikel_id', $artikel_id);
						$stmt->bindParam(':billing_id', $invoice_id);
						$stmt->bindParam(':anzahl', $split[2]);
						$stmt->bindParam(':preis', $split[3]);

						// EXECUTE MYSQL QUERY -> RETURNS BILLING_ID IN CASE OF SUCCESS
						if(!$stmt->execute())
						{
							print_r($stmt->errorInfo());
							$pdo = NULL;
							echo "Bei der Abfrage ist ein Fehler unterlaufen";
						}
					}
				}
			}
		}
	}
}



function get_billing_status_qty($pdo) {

        // MySQL Query via PDO prepared Statement
        $stmt = $pdo->prepare("SELECT SUM(CASE WHEN status='0' THEN 1 ELSE 0 END) AS state_0,
                                      SUM(CASE WHEN status='1' THEN 1 ELSE 0 END) AS state_1,
                                      SUM(CASE WHEN status='2' THEN 1 ELSE 0 END) AS state_2,
                                      SUM(CASE WHEN status='3' THEN 1 ELSE 0 END) AS state_3
                               FROM rechnung");

         // MySQL Query ausführen, bei Error DB Objekt löschen
        if(!$stmt->execute())
        {
            $pdo->errorInfo();
        }

        else {
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            echo json_encode($result);
        }
}


// ERSTELLEN EINES UNIX TIMESTAMPS
function make_unix_stamp($valid_date) {
	if (!empty($valid_date)) {
		$date_cutted = explode (".", $valid_date);
		$tag = $date_cutted[0];
		$monat = $date_cutted[1];
		$jahr = $date_cutted[2];
		return $unix_stamp = mktime(0,0,0,$monat,$tag,$jahr);
	}

	elseif (empty($valid_date)) {
		return $unix_stamp = "";
	}
}