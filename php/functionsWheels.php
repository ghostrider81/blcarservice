<?php

/***************************************
***** BUILD ALL RACKS FOR FRONTEND *****
***************************************/

function wheelsBuildRacks($pdo) {

    $stmt = $pdo->prepare("SELECT 
                                w.*,
                                GROUP_CONCAT(we.subject_id) AS subject_id,
                                GROUP_CONCAT(we.mark) AS kennzeichen,
                                GROUP_CONCAT(we.rack_id) AS rack_id_e,
                                GROUP_CONCAT(we.inform_contact) AS inform,
                                GROUP_CONCAT(k.name) AS name,
                                GROUP_CONCAT(k.vorname) AS vorname
                            FROM 
                                wheelracks w 
                            LEFT JOIN 
                                wheelracks_entrys we
                            ON
                                w.rack_id = we.rack_id
                            LEFT JOIN
                                kontakte k
                            ON 
                                we.contact_id = k.contact_id
                            GROUP BY rack_id
                            ");


    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }


    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        $row_index = 0;

        foreach($result AS $result_row) {

            $entrys['subject_id'][$row_index] = explode(",",$result[$row_index]->subject_id);
            $entrys['kennzeichen'][$row_index] = explode(",",$result[$row_index]->kennzeichen);
            $entrys['name'][$row_index] = explode(",",$result[$row_index]->name);
            $entrys['vorname'][$row_index] = explode(",",$result[$row_index]->vorname);
            $entrys['rack_id_e'][$row_index] = explode(",",$result[$row_index]->rack_id_e);
            $entrys['inform'][$row_index] = explode(",",$result[$row_index]->inform);

            $row_index++;
        }



        $i = 0;
        foreach($result AS $row) {
            $reihen = $row->num_rows;
            $spalten = $row->num_slots_per_row;
            $cell_width = 100 / $row->num_slots_per_row;
            $cells = $reihen * $spalten * 1;

            echo '<table class="table table-condensed table-bordered table-responsive racks" data-rack-id="'.$row->rack_id.'">';
            echo '<thead>';
            echo '<tr>';
            echo '<td  colspan="'.$spalten.'">';
            echo $row->description;
            echo '</td>';
            echo '</tr>';
            echo '</thead>';

            echo '<tbody>';

            $fach_id = 1;

            for($a = 1; $a <= $reihen; $a++) {

                echo '<tr height="100">';

                for($b = 1; $b <= $spalten; $b++) {


                    echo '<td data-subject-id="'.$fach_id.'" width="'.$cell_width.'%" style="position: relative;">';

                        // REGALFÄCHER BEFÜLLEN

                            for($e = 0; $e <= $cells; $e++) {


                                if($entrys['rack_id_e'][$i][$e] == $row->rack_id && $entrys['subject_id'][$i][$e] == $fach_id) {

                                    if($entrys['inform'][$i][$e] == 1) {
                                        echo "<span class='badge' style='float: right; background-color: #C00000; color: #FFFFFF;'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></span>";
                                    }
                                    echo $entrys['name'][$i][$e].'<br />';
                                    echo $entrys['kennzeichen'][$i][$e];

                                    echo '<div class="wheelDataWindowBtn">';
                                    echo '<i class="fa fa-ellipsis-h" aria-hidden="true"></i>';
                                    echo '</div>';
                                }
                                else {

                                }

                            }

                    echo '</td>';

                    $fach_id++;

                }

                echo '</tr>';



            }

            echo '</tbody>';
            $i++;
        }

        echo '</table>';
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo "Keine Reifenregale vorhanden";
    }
}


/***********************************************
******* INSERT NEW OCCUPANCY IN WHEELRACK ******
***********************************************/

function newRackOccupancy($pdo, $jsonObj) {

	// CHECK IF SUBJECT EXISTS
	$stmtCheck = $pdo->prepare("SELECT entry_id FROM wheelracks_entrys WHERE rack_id = :rack_id AND subject_id = :subject_id");
	$stmtCheck->bindParam(':rack_id', $jsonObj->rack_id);
	$stmtCheck->bindParam(':subject_id', $jsonObj->subject_id);
	$stmtCheck->execute();

	if($stmtCheck->rowCount() > 0)
	{
		$test=12345;
		// EINTRAG VORHANDEN
		$resultCheck = $stmtCheck->fetchAll(PDO::FETCH_OBJ);
		$entry_id = $resultCheck[0]->entry_id;

		$stmtDelete = $pdo->prepare("DELETE FROM wheelracks_entrys WHERE entry_id = :entry_id");
		$stmtDelete->bindParam(':entry_id', $entry_id);
		$stmtDelete->execute();
	}


    // INSERT IN DATABASE
    $stmt = $pdo->prepare("
                              INSERT INTO 
                                  wheelracks_entrys 
                                      ( rack_id, subject_id, contact_id, mark, profile_vl, profile_vr, profile_hl, profile_hr, wheel_ok, inform_contact, notes )
                              VALUES
                                      ( :rack_id, :subject_id, :contact_id, :mark, :profile_vl, :profile_vr, :profile_hl, :profile_hr, :wheel_ok, :inform_contact, :notes )
                         ");

    $stmt->bindParam(':rack_id', $jsonObj->rack_id);
    $stmt->bindParam(':subject_id', $jsonObj->subject_id);
    $stmt->bindParam(':contact_id', $jsonObj->contact_id);
    $stmt->bindParam(':mark', $jsonObj->mark);
    $stmt->bindParam(':profile_vl', $jsonObj->profile_vl);
    $stmt->bindParam(':profile_vr', $jsonObj->profile_vr);
    $stmt->bindParam(':profile_hl', $jsonObj->profile_hl);
    $stmt->bindParam(':profile_hr', $jsonObj->profile_hr);
    $stmt->bindParam(':wheel_ok', $jsonObj->wheel_ok);
    $stmt->bindParam(':inform_contact', $jsonObj->inform_contact);
    $stmt->bindParam(':notes', $jsonObj->notes);

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        echo "\nPDO::errorInfo():\n";
        print_r($stmt->errorInfo());
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    else
    {
        echo $pdo->lastInsertId();
    }
}


/***********************************************
******** GET ENTRY DATA FOR INFOWINDOW *********
***********************************************/

function getEntryData($pdo, $jsonObj) {

    $stmt = $pdo->prepare("SELECT * FROM wheelracks_entrys WHERE rack_id = :rack_id AND subject_id = :subject_id");
    $stmt->bindParam(':rack_id', $jsonObj->rack_id);
    $stmt->bindParam(':subject_id', $jsonObj->subject_id);
    $stmt->execute();


    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        echo "\nPDO::errorInfo():\n";
        print_r($stmt->errorInfo());
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }


    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        header('Content-Type: application/json');
        echo json_encode($result);
        exit;

    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        return null;
    }

}



/***************************************************
****** ACTIONHANDLER FOR WHEELRACK OPERATIONS ******
***************************************************/

if(file_get_contents('php://input')) {
    $json = file_get_contents('php://input');
    $jsonObj = json_decode($json);

    $action = $jsonObj->action;
}

if(isset($action))
{
    switch ($action) {

        case 'newOccupancy':
            include "../classes/sqlConnect.php";
                newRackOccupancy($pdo, $jsonObj);
            break;

        case 'getEntryData':
            include "../classes/sqlConnect.php";
                getEntryData($pdo, $jsonObj);
            break;


    }
}