<?php
    include "../classes/sqlConnect.php";

    // Login Parameter annehmen
    if(isset($_POST['loginBenutzername']) && isset($_POST['loginPasswort']))
    {
        $user = $_POST['loginBenutzername'];
        $pass = $_POST['loginPasswort'];
    }

    // MySQL Query via PDO prepared Statement
    $stmt = $pdo->prepare("SELECT password FROM login_data WHERE username = :user");
    $stmt->bindParam(':user', $user);
    var_dump($stmt);

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
        {
            $pdo = NULL;
            echo "Bei der Abfrage ist ein Fehler unterlaufen";
        }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
        {
            // Result ist nicht leer
            $row = $stmt->fetch(PDO::FETCH_OBJ);

            // Überprüfung ob Passwort korrekt ist
            if($pass == $row->password)
                {
                    $pdo = NULL;
                    echo "true";
                }

            else
                {
                    $pdo = NULL;
                    echo "Das eingegebene Passwort ist nicht korrekt";
                }
        }

    else
        {
            // Result ist leer
            $pdo = NULL;
            echo "Benutzername wurde nicht gefunden";
        }