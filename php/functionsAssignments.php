<?php

// Alle Aufträge ausgeben
function assignmentGetAll($pdo) {

    $stmt = $pdo->prepare("SELECT 
                              kontakte.name, 
                              kontakte.vorname, 
                              kontakte.kdnnr, 
                            SUM(p.anzahl * p.preis) gesamtbetrag,
                              a.commission_id, 
                              a.commission_nr, 
                              a.date_acception, 
                              a.date_target, 
                              a.operator
                            FROM 
                              auftrag a 
                            LEFT JOIN 
                              positionen_auftrag p 
                            ON 
                              a.commission_id = p.commission_id 
                            LEFT JOIN 
                              kontakte 
                            ON 
                              a.contact_id = kontakte.contact_id 
                            GROUP BY 
                              a.commission_id ");

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        return false;
    }
}


// Nächste Auftragssnummer generieren
function getNextAssignmentNumber($pdo) {

    // LETZTE VERGEBENE RECHNUNGSNUMMER UND AKTUELLES JAHR AUSLESEN
    $query_assignment_nr = $pdo->prepare("SELECT commission_nr FROM auftrag ORDER BY commission_id DESC LIMIT 1");
    $query_assignment_nr->execute();

    if($query_assignment_nr->rowCount() > 0) {

        $result_assignment_nr = $query_assignment_nr->fetchAll(PDO::FETCH_OBJ);

        $commission_nr = $result_assignment_nr[0]->commission_nr;

        if ($commission_nr == "" or $commission_nr == 1) {
            $new_commission_nr = 400;
        } else {
            $new_commission_nr = $commission_nr + 1;
        }

    }

    else {
        $new_commission_nr = 400;
    }


    return $new_commission_nr;
}


function check_articles_assignment($pdo, $desc) {

    $stmt = $pdo->prepare("SELECT artikel_id, bezeichnung FROM positionen WHERE LOWER(bezeichnung) = LOWER(:description)");
    $stmt->bindParam(':description', $desc);

    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    else {

        if($stmt->rowCount() > 0) {
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            $artikel_id = $result[0]->artikel_id;
            return $artikel_id;

        }

        else {
            // BUILD MYSQL QUERY
            $stmt_art = $pdo->prepare("INSERT INTO positionen
                ( bezeichnung )                  
              VALUES
				( :bezeichnung )");

            // PREPARE MYSQL STATEMENT
            $stmt_art->bindParam(':bezeichnung', $desc);

            if(!$stmt_art->execute())
            {
                $pdo = NULL;
                echo "Bei der Abfrage ist ein Fehler unterlaufen";
            }

            else {
                return $pdo->lastInsertId();
            }
        }
    }
}


/*** Action Handler ***/
if(isset($_REQUEST['state']))
{
    switch ($_REQUEST['state']) {

        case 'getVehicleInformationByFullNameKDNR':
            include "../classes/sqlConnect.php";
            getVehicleInformationByFullNameKDNR($pdo);

            break;

        case 'getAssignmentEdit':
            include "../php/functions.php";
            include "../classes/sqlConnect.php";
            getAssignmentEdit($pdo);

            break;
        case 'getAssignmentToInvoiceINFO':
            include "../classes/sqlConnect.php";
            getAssignmentToInvoiceINFO($pdo);
            break;
            // TODO: rechnung umwandeln
        case 'getAssignmentToInvoiceSubmit':
            include "../classes/sqlConnect.php";
            getAssignmentToInvoiceSubmit($pdo);
            break;

        case 'getArtikelDesc':
            include "../classes/sqlConnect.php";
            getArtikelDesc($pdo);
            break;

        case 'searchForPositions':
            include "../classes/sqlConnect.php";
            searchForPositions($pdo);
            break;

        case 'setNewAssignment':
            include "../classes/sqlConnect.php";
            setNewAssignment($pdo);
            break;

        case 'updateAssignment':
            include "../classes/sqlConnect.php";
            updateAssignment($pdo);
            break;

        case 'checkSignature':
            include "../classes/sqlConnect.php";
            checkSignature($pdo);
            break;

        case 'getAwSets':
            include "../classes/sqlConnect.php";
            include "functions.php";
            getAwSets($pdo);
            break;

    }
}


function searchForPositions($pdo) {
    $searchKey = $_REQUEST["searchKey"];

    if($_REQUEST["searchKey"] != "")
    {
        $searchQuery = $_REQUEST["searchKey"];
        $searchReturn = array();

        $stmt = $pdo->prepare("SELECT * FROM positionen WHERE bezeichnung LIKE '%".$searchQuery."%'");

        if(!$stmt->execute())
        {
            echo PDO::errorInfo();
            $pdo = NULL;
            echo "Bei der Abfrage ist ein Fehler unterlaufen";
        }

        $searchResult = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($searchResult AS $row) {
            $searchReturn[] = $row->bezeichnung;
        }

        echo json_encode($searchReturn);
    }

}
function getArtikelDesc($pdo) {
    $artikelID = $_REQUEST["artikel_id"];

    if($_REQUEST["artikel_id"] != "")
    {
        $artikelID = $_REQUEST["artikel_id"];
        $searchReturn = array();

        $stmt = $pdo->prepare("SELECT * FROM positionen WHERE artikel_id = '".$artikelID."'");

        if(!$stmt->execute())
        {
            echo PDO::errorInfo();
            $pdo = NULL;
            echo "Bei der Abfrage ist ein Fehler unterlaufen";
        }

        $searchResult = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($searchResult AS $row) {
            $searchReturn = $row->bezeichnung;
        }


        echo json_encode($searchReturn);
    }

}

/*** FUNCTIONS  ---------------------------------------------------------------------------------------------- ***/
function getVehicleInformationByFullNameKDNR($pdo) {
    $vorname = $_REQUEST["vorname"];
    $nachname = $_REQUEST["nachname"];
    // TODO : select position where artikel_id... -> description
    $sqlQuery = "SELECT * FROM  kontakte 
                  LEFT JOIN fahrzeuge ON kontakte.contact_id = fahrzeuge.contact_id 
                  WHERE name = '".$nachname."' AND vorname = '".$vorname."'  ";
    $stmt = $pdo->prepare($sqlQuery);
    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo "Keine Fahrzeuginformationen vorhanden";
    }
}


// Nächste Rechnungsnummer generieren
function generateNextInvoiceNumber($pdo) {

	// LETZTE VERGEBENE RECHNUNGSNUMMER UND AKTUELLES JAHR AUSLESEN
	$query_billing_nr = $pdo->prepare("SELECT billing_nr FROM rechnung ORDER BY billing_id DESC LIMIT 1");
	$query_billing_nr->execute();

	$aktuelles_jahr = date("Y");

	if($query_billing_nr->rowCount() > 0) {

		$result_billing_nr = $query_billing_nr->fetchAll(PDO::FETCH_OBJ);

		$letzte_rg = $result_billing_nr[0]->billing_nr;


		// LETZTE VERGEBENE RECHNUNGSNUMMER TEILEN UND ÜBERPRÜFEN
		$parts_rg = explode("/", $letzte_rg);

		// WENN AKTUELLES JAHR MIT DEM LETZTEN NR ÜBEREINSTIMMT, UM EINEN ERHÖHEN
		if ($aktuelles_jahr == $parts_rg[1]) {
			$neue_rg = $parts_rg[0] + 1;
			$neue_rg = $neue_rg . "/" . $aktuelles_jahr;
			return $neue_rg;
		} // WENN NICHT, DANN AKTUELLES JAHR UND LFNDNR AUF 1 SETZEN
		else {
			$neue_rg = "1/" . $aktuelles_jahr;
			return $neue_rg;
		}
	}
	else {
		$neue_rg = "1/" . $aktuelles_jahr;
		return $neue_rg;
	}

}



function get_id_by_number($pdo, $number) {
	$sqlQuery = "SELECT commission_id FROM auftrag WHERE commission_nr = ".$number;
	$stmt = $pdo->prepare($sqlQuery);

	// MySQL Query ausführen, bei Error DB Objekt löschen
	if(!$stmt->execute())
	{
		print_r($stmt->errorInfo());
		$pdo = NULL;
		echo "Bei der Abfrage ist ein Fehler unterlaufen";
	}

// Result ist nicht leer
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$commission_id = $result->commission_id;
		return $commission_id;
}



function getAssignmentToInvoiceINFO($pdo) {
    $commissionID = $_REQUEST['data']["commissionID"];
    $sqlQuery = "SELECT * FROM auftrag LEFT JOIN positionen_auftrag ON auftrag.commission_id = positionen_auftrag.commission_id LEFT JOIN fahrzeuge ON auftrag.car_id = fahrzeuge.car_id LEFT JOIN kontakte ON auftrag.contact_id = kontakte.contact_id WHERE auftrag.commission_id = ".$commissionID;
    $stmt = $pdo->prepare($sqlQuery);

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
    }

    else
    {
        // Result ist leer
	    print_r($stmt->errorInfo());
        $pdo = NULL;
        echo " getAssignmentToInvoiceINFO failed";
    }
}


function getAssignmentEdit($pdo) {
    $commissionID = $_REQUEST["commissionID"];


    $sqlQuery = "SELECT * FROM auftrag 
    LEFT JOIN positionen_auftrag ON auftrag.commission_id = positionen_auftrag.commission_id 
    LEFT JOIN fahrzeuge ON auftrag.car_id = fahrzeuge.car_id 
    LEFT JOIN kontakte ON auftrag.contact_id = kontakte.contact_id 
    WHERE auftrag.commission_id = ".$commissionID;
    $stmt = $pdo->prepare($sqlQuery);
    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);


        echo json_encode($result);
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo " Assignment Details failed";
    }
}



function array_flatten($array) {
    $return = array();
    foreach ($array as $key => $value) {
        if (is_array($value)){ $return = array_merge($return, array_flatten($value));}
        else {$return[$key] = $value;}
    }
    return $return;
}


// ERSTELLEN EINES UNIX TIMESTAMPS
function make_stamp($valid_date) {
	if (!empty($valid_date)) {
		$date_cutted = explode (".", $valid_date);
		$tag = $date_cutted[0];
		$monat = $date_cutted[1];
		$jahr = $date_cutted[2];
		return $unix_stamp = mktime(0,0,0,$monat,$tag,$jahr);
	}

	elseif (empty($valid_date)) {
		return $unix_stamp = "";
	}
}


function setNewAssignment($pdo) {

    // GET DATAPACKAGE
    $dataSet = $_REQUEST;

    // BUILD AND FORMAT DATA
    $commission_nr = $dataSet['data'][0]['commission_i1'];
    $date_acception = make_stamp($dataSet['data'][0]['date_acceptio2']);
    $date_target = make_stamp($dataSet['data'][0]['date_targe3']);
    $km_acception = $dataSet['data'][0]['km_acceptio4'];
    $operator = $dataSet['data'][0]['operator'];
    $employee = $dataSet['data'][0]['selectedEmployee'];
    $car_id = $dataSet['data'][2]['car_id'];
    $contact_id = $dataSet['data'][2]['contact_id'];
    $signature = $dataSet['data'][2]['signature'];
    $notes = $dataSet['data'][4]['notice0'];

    echo $commission_nr."<br />";
    echo $date_acception."<br />";
    echo $date_target."<br />";
    echo $km_acception."<br />";
    echo $operator."<br />";
    echo $employee."<br />";
    echo $car_id."<br />";
    echo $contact_id."<br />";
    echo $signature."<br />";
    echo $notes."<br />";


    // BUILD MYSQL QUERY
    $stmt = $pdo->prepare("INSERT INTO auftrag
        ( contact_id, 
          car_id, 
          commission_nr, 
          date_acception, 
          operator,
          employee,
          date_target,
          km_acception, 
          notices )
          
      VALUES
		( :contact_id, 
          :car_id, 
          :commission_nr, 
          :date_acception, 
          :operator,
          :employee,
          :date_target,
          :km_acception, 
          :notices )");


    // PREPARE MYSQL STATEMENT
    $stmt->bindParam(':contact_id', $contact_id);
    $stmt->bindParam(':car_id', $car_id);
    $stmt->bindParam(':commission_nr', $commission_nr);
    $stmt->bindParam(':date_acception', $date_acception);
    $stmt->bindParam(':operator', $operator);
    $stmt->bindParam(':employee', $employee);
    $stmt->bindParam(':date_target', $date_target);
    $stmt->bindParam(':km_acception', $km_acception);
    $stmt->bindParam(':notices', $notes);


    // EXECUTE MYSQL QUERY -> RETURNS ASSIGNMENT_ID IN CASE OF SUCCESS
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen [execute-auftrag]";
    }

    else
    {
        $assignment_id = $pdo->lastInsertId();

    }


    // HANDLE ARRAY
    $result = array_flatten($dataSet['data'][5]);
    $splitted = array_chunk($result, 4);


    foreach($splitted AS $split) {

        if($split[1] != "" && $split[2] != "" && $split[3] != "")
        {
            $artikel_id = check_articles_assignment($pdo, $split[1]);

            // BUILD MYSQL QUERY
            $stmt = $pdo->prepare("INSERT INTO positionen_auftrag (artikel_id, commission_id, anzahl, preis) VALUES (:artikel_id, :assignment_id, :anzahl, :preis)");

            // PREPARE MYSQL STATEMENT
            $stmt->bindParam(':artikel_id', $artikel_id);
            $stmt->bindParam(':assignment_id', $assignment_id);
            $stmt->bindParam(':anzahl', $split[2]);
            $stmt->bindParam(':preis', $split[3]);


            echo "artikel id: ".$artikel_id."<br />";
            echo "assignmend id: ".$assignment_id."<br />";
            echo "anzahl: ".$split[2]."<br />";
            echo "preis: ".$split[3]."<br /><br />";


            // EXECUTE MYSQL QUERY -> RETURNS BILLING_ID IN CASE OF SUCCESS
            $stmt->execute();
            }
        }

        if($signature == 1) {
            $status = 1;

            // BUILD MYSQL QUERY
            $stmt = $pdo->prepare("INSERT INTO assignment_pending (assignment_id, status) VALUES (:assignment_id, :status)");

            // PREPARE MYSQL STATEMENT
            $stmt->bindParam(':assignment_id', $assignment_id);
            $stmt->bindParam(':status', $status);

            // EXECUTE MYSQL QUERY -> RETURNS BILLING_ID IN CASE OF SUCCESS
            if(!$stmt->execute())
            {
                echo PDO::errorInfo();
                $pdo = NULL;
                echo "Bei der Abfrage ist ein Fehler unterlaufen [execute-pending-signature]";
            }
        }

        else {
            echo "true";
        }

}


function checkSignature($pdo) {

    // CHECK ASSIGNMENT_PENDING IF SIGNATURE TRANSFERED
    $sqlQuery = "SELECT assignment_id FROM assignment_pending WHERE status = 2";
    $stmt = $pdo->prepare($sqlQuery);
    // MySQL Query ausführen, bei Error DB Objekt löschen
    if($stmt->execute())
    {
        if($stmt->rowCount() > 0)
        {
            // SIGNATURE TRANSFERED

            $status = 0;

            $sqlQuery2 = "UPDATE assignment_pending SET status = :status_finish WHERE status = 2";
            $stmt2 = $pdo->prepare($sqlQuery2);

            $stmt2->bindParam(':status_finish', $status);

            $stmt2->execute();

            echo "true";
        }
    }
}



function multiKeyExistsInvoice(array $arr, $key) {

    // is in base array?
    if (array_key_exists($key, $arr)) {
        return true;
    }

    // check arrays contained in this array
    foreach ($arr as $element) {
        if (is_array($element)) {
            if (multiKeyExistsInvoice($element, $key)) {
                return true;
            }
        }

    }

    return false;
}

// TODO - Need commission_id

function updateAssignment($pdo) {

	// GET DATAPACKAGE
	$dataSet = $_REQUEST;

	// BUILD AND FORMAT DATA
	$commission_nr = $dataSet['data'][0]['commission_n1'];
	$commission_id = $dataSet['data'][2]['commission_id'];
	$date_acception = make_stamp($dataSet['data'][0]['date_acceptio2']);
	$date_target = make_stamp($dataSet['data'][0]['date_targe3']);
	$km_acception = $dataSet['data'][0]['km_acceptio4'];

    $operator = multiKeyExistsInvoice($dataSet['data'], "operator") ? $dataSet['data'][0]['operator'] : 0;
    $employee = multiKeyExistsInvoice($dataSet['data'], "selectedEmployee") ? $dataSet['data'][0]['selectedEmployee'] : 0;

	$car_id = $dataSet['data'][2]['car_id'];
	$contact_id = $dataSet['data'][2]['contact_id'];
	$notes = $dataSet['data'][4]['notice0'];

	// BUILD MYSQL QUERY
	$stmt = $pdo->prepare("UPDATE auftrag SET
                              contact_id = :contact_id, 
                              car_id = :car_id, 
                              commission_nr = :commission_nr, 
                              date_acception = :date_acception, 
                              operator = :operator,
                              employee = :employee,
                              date_target = :date_target,
                              km_acception = :km_acception, 
                              notices = :notices
                            WHERE 
                              commission_id = :commission_id");

	// PREPARE MYSQL STATEMENT
	$stmt->bindParam(':commission_id', $commission_id);
	$stmt->bindParam(':contact_id', $contact_id);
	$stmt->bindParam(':car_id', $car_id);
	$stmt->bindParam(':commission_nr', $commission_nr);
	$stmt->bindParam(':date_acception', $date_acception);
	$stmt->bindParam(':operator', $operator);
	$stmt->bindParam(':employee', $employee);
	$stmt->bindParam(':date_target', $date_target);
	$stmt->bindParam(':km_acception', $km_acception);
	$stmt->bindParam(':notices', $notes);

	// EXECUTE MYSQL QUERY -> RETURNS ASSIGNMENT_ID IN CASE OF SUCCESS
	if(!$stmt->execute())
	{
		print_r($stmt->errorInfo());
		$pdo = NULL;
		echo "Bei der Abfrage ist ein Fehler unterlaufen [execute-edit-auftrag]";
	}

	else
	{

		$stmt2 = $pdo->prepare("DELETE FROM positionen_auftrag WHERE commission_id = :commission_id");
		$stmt2->bindParam(':commission_id', $commission_id);
		if(!$stmt2->execute())
		{
				print_r($stmt2->errorInfo());
				$pdo = NULL;
				echo "Bei der Abfrage ist ein Fehler unterlaufen";
        }

        else {
            // HANDLE ARRAY
            $result = array_flatten($dataSet['data'][5]);
            $splitted = array_chunk($result, 4);

            foreach($splitted AS $split) {

                if($split[1] != "" && $split[2] != "" && $split[3] != "")
                {
                    $artikel_id = check_articles_assignment($pdo, $split[1]);

                    // BUILD MYSQL QUERY
                    $stmt = $pdo->prepare("INSERT INTO positionen_auftrag (artikel_id, commission_id, anzahl, preis) VALUES (:artikel_id, :assignment_id, :anzahl, :preis)");

                    // PREPARE MYSQL STATEMENT
                    $stmt->bindParam(':artikel_id', $artikel_id);
                    $stmt->bindParam(':assignment_id', $commission_id);
                    $stmt->bindParam(':anzahl', $split[2]);
                    $stmt->bindParam(':preis', $split[3]);


                    echo "artikel id: ".$artikel_id."<br />";
                    echo "assignmend id: ".$commission_id."<br />";
                    echo "anzahl: ".$split[2]."<br />";
                    echo "preis: ".$split[3]."<br /><br />";


                    // EXECUTE MYSQL QUERY -> RETURNS BILLING_ID IN CASE OF SUCCESS
                    $stmt->execute();
                }
            }
        }
	}
}


// TODO - assignmentEdit.php - assigmentToInvoice Func (Rechnung umwandeln)   [SVEN]
function getAssignmentToInvoiceSubmit($pdo) {
    echo 'getAssignmentToInvoiceSubmit has been called';
    $data = $_REQUEST['data'];
    $carID = $data['car_id'];
    $contactID = $data['contact_id'];
    $assignmentList = $data['assignmentList'];
    foreach($assignmentList as $as) {
        echo $as;
    }
    //echo $carID;
    //echo $contactID;
}

// setNewInvoice - updateInvoice - setNewRechnung - updateRechnung



