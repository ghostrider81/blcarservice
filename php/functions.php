<?php
function makeDateFromTimestamp($timestamp) {
    if($timestamp != "0") {
        $date = date('d.m.Y', $timestamp);
        return $date;
    }

    else {
        return "";
    }
}



function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

function array_search_r($id, $array) {
    foreach ($array as $key => $val) {
        if ($val['subject_id'] === $id) {
            return $key;
        }
    }
    return null;
}


/***** RETURNS THE CURRENT DATE *****/
function getCurrentDate() {
    $date = date("d.m.Y");
    return $date;
}


/***** RETURNS ALL AW SETS *****/
function getAwSets($pdo) {

    // MySQL Query via PDO prepared Statement
    $stmt = $pdo->prepare("SELECT aw_id, anz_aw, stundenlohn, preis_aw, beschreibung FROM aw");

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach($result AS $row)
        {
            $preis_aw = number_format($row->preis_aw, 2, ',', '.');
            echo "<option value=\"".$row->preis_aw."\">".$row->beschreibung." / 1 Stunde / ".$row->anz_aw." AW / ZE ".$preis_aw." €</option>";
        }
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo "Keine AW vorhanden";
    }
}


/***** RETURN AW SETS FOR SETTINGSPAGE *****/
function getAwSetsForSettings($pdo) {
    // MySQL Query via PDO prepared Statement
    $stmt = $pdo->prepare("SELECT * FROM aw_sets WHERE aw_id = 1");

    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo "Keine AW vorhanden";
    }
}



/*** SAVE AW SETS **/
function saveAwSets($pdo) {
    $json = $_REQUEST;

    $anzahl_aw_m = $json['anzahl_m'];
    $stundenlohn_m = number_format($json['stundenlohn_m'], 2,".","");
    $preis_aw_m = number_format($json['preis_m'], 2,".","");
    $anzahl_aw_k = $json['anzahl_k'];
    $stundenlohn_k = number_format($json['stundenlohn_k'], 2,".","");
    $preis_aw_k = number_format($json['preis_k'], 2,".","");
    $anzahl_aw_l = $json['anzahl_l'];
    $stundenlohn_l = number_format($json['stundenlohn_l'], 2,".","");
    $preis_aw_l = number_format($json['preis_l'], 2,".","");
    $anzahl_aw_e = $json['anzahl_e'];
    $stundenlohn_e = number_format($json['stundenlohn_e'], 2,".","");
    $preis_aw_e = number_format($json['preis_e'], 2,".","");

    $stmt = $pdo->prepare("
                    UPDATE aw_sets SET
                      anzahl_aw_m = :anzahl_aw_m,
                      stundenlohn_m = :stundenlohn_m,
                      preis_aw_m = :preis_aw_m,
                      anzahl_aw_k = :anzahl_aw_k,
                      stundenlohn_k = :stundenlohn_k,
                      preis_aw_k= :preis_aw_k,
                      anzahl_aw_l = :anzahl_aw_l,
                      stundenlohn_l = :stundenlohn_l,
                      preis_aw_l = :preis_aw_l,
                      anzahl_aw_e = :anzahl_aw_e,
                      stundenlohn_e = :stundenlohn_e,
                      preis_aw_e = :preis_aw_e
                    WHERE aw_id = 1");

    $stmt->bindParam(':anzahl_aw_m', $anzahl_aw_m);
    $stmt->bindParam(':stundenlohn_m', $stundenlohn_m);
    $stmt->bindParam(':preis_aw_m', $preis_aw_m);
    $stmt->bindParam(':anzahl_aw_k', $anzahl_aw_k);
    $stmt->bindParam(':stundenlohn_k', $stundenlohn_k);
    $stmt->bindParam(':preis_aw_k', $preis_aw_k);
    $stmt->bindParam(':anzahl_aw_l', $anzahl_aw_l);
    $stmt->bindParam(':stundenlohn_l', $stundenlohn_l);
    $stmt->bindParam(':preis_aw_l', $preis_aw_l);
    $stmt->bindParam(':anzahl_aw_e', $anzahl_aw_e);
    $stmt->bindParam(':stundenlohn_e', $stundenlohn_e);
    $stmt->bindParam(':preis_aw_e', $preis_aw_e);

    // EXECUTE MYSQL QUERY
    if(!$stmt->execute())
    {
        print_r($stmt->errorInfo());
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    else
    {
        return true;
    }
}


/*** ACTION HANDLER ***/
if(isset($_REQUEST['state']))
{
    switch ($_REQUEST['state']) {
        case 'saveAwSets':
            include "../classes/sqlConnect.php";
            saveAwSets($pdo);

            break;

    }
}