	<script type="text/javascript">
		$(function() {
			
			// RECHNUNG LÖSCHEN
			$(".billing_delete").live('click', function(){
			var element = $(this);
			var del_id = element.attr("id");
			var info = 'id=' + del_id;
			if(confirm("Soll die Rechnung wirklich gelöscht werden?"))
			{
			 $.ajax({
				   type: "POST",
				   url: "mod_billing/billing_delete.php",
				   data: info,
				   success: function(){}
			   });
			  $(this).parent().parent().animate({ backgroundColor: "#099" }, "slow")
			  .animate({ opacity: "hide" }, "fast");
			 }
			return false;
			});
			
			
			// RECHNUNG STATUS ÄNDERN
			$("select[id^='billing_status_']").change(function() {
			var element = $(this);
			var billing_id = element.attr("name");
			var status_id = $("option:selected", this).val();
			var info = 'billing_id=' + billing_id + '&status_id=' + status_id;

			if(confirm("Soll der Status dieser Rechnung wirklich geändert werden?"))
			{
			 $.ajax({
				   type: "POST",
				   url: "mod_billing/billing_changestatus.php",
				   data: info,
				   success: function(){}
			   });
			  $(this).parent().parent().animate({ backgroundColor: "#099" }, "slow")
			  .animate({ opacity: "hide" }, "fast");
			  
			 }
			return false;
			});
		});
	</script>

			<table border="0" id="billinglist" cellpadding="0" cellspacing="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; text-align: center;">RG-Nr.:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Name:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Betrag Netto:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Betrag Brutto:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">F&auml;llig am:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Zahlungsmethode:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Sachbearbeiter:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;" width="150">Optionen:</th>
				</tr>

<?php

	include "../config/config.inc";
	include "../includes/database_connection.php";
	include "../php/math_functions.php";
	
	$date = time();// - (60 * 60 * 24 * 14);
				
				$status = $_GET['status'];
				
				if($status == 0) // Entwürfe
					{
						$query = "
							SELECT billing_id, contact_id, car_id, billing_nr, payment_method, date_billing, operator, date_target, barverkauf, status
							FROM rechnung 
							WHERE status =".$status." 
							ORDER BY billing_id DESC";
						//$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"mod_billing/billing_delete.php\" target=\"_blank\" title=\"DIESE RECHNUNG L&Ouml;SCHEN\" class=\"billing_delete\" id=\"{billing_id}\"><img src=\"img/icon_delete.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=billing&action=billing_edit&billing_id={billing_id}\" title=\"DIESE RECHNUNG BEARBEITEN\"><img src=\"img/icon_edit.png\" width=\"16\" height=\"16\" border=\"0\" /></a><br /><select name=\"{billing_id}\" id=\"billing_status_{billing_id}\" style=\"border: 1px #009999 solid; width: 145px;\"><option>Status &auml;ndern</option><option value=\"1\">Offen</option><option value=\"3\">Bezahlt</option></select>";
					}

				elseif($status == 1) // Offen
					{
						$query = "
							SELECT billing_id, contact_id, car_id, billing_nr, payment_method, date_billing, operator, date_target, barverkauf, status
							FROM rechnung 
							WHERE status =".$status." 
							ORDER BY billing_id DESC";
						//$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;<br /><select name=\"{billing_id}\" id=\"billing_status_{billing_id}\" style=\"border: 1px #009999 solid; width: 145px;\"><option>Status &auml;ndern</option><option value=\"3\">Bezahlt</option></select>";
					}
					
				elseif($status == 3) // Bezahlt
					{
						$query = "
							SELECT billing_id, contact_id, car_id, billing_nr, payment_method, date_billing, operator, date_target, barverkauf, status
							FROM rechnung 
							WHERE status =".$status." 
							ORDER BY billing_id DESC";
						//$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>";
					}					
					
				elseif($status == 2) // Fällig
					{
						$query = "
							SELECT billing_id, contact_id, car_id, billing_nr, payment_method, date_billing, operator, date_target, barverkauf, status
							FROM rechnung 
							WHERE status = 1 AND date_target <= ".$date." 
							ORDER BY billing_id DESC";
						//$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=billing&action=create_reminder&billing_id={billing_id}\" title=\"ZAHLUNGSERINNERUNG GENERIEREN\"><img src=\"img/reminder.png\" width=\"16\" height=\"16\" border=\"0\" /></a><br /><select name=\"{billing_id}\" id=\"billing_status_{billing_id}\" style=\"border: 1px #009999 solid; width: 145px;\"><option>Status &auml;ndern</option><option value=\"3\">Bezahlt</option></select>";
					}
					
				elseif($status == 4)
					{
						$query = "
							SELECT billing_id, contact_id, car_id, billing_nr, payment_method, date_billing, operator, date_target, barverkauf, status 
							FROM rechnung 
							ORDER BY billing_id DESC";
						//$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"mod_billing/billing_delete.php\" target=\"_blank\" title=\"DIESE RECHNUNG L&Ouml;SCHEN\" class=\"billing_delete\" id=\"{billing_id}\"><img src=\"img/icon_delete.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=billing&action=billing_edit&billing_id={billing_id}\" title=\"DIESE RECHNUNG BEARBEITEN\"><img src=\"img/icon_edit.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=billing&action=create_reminder&billing_id={billing_id}\" title=\"ZAHLUNGSERINNERUNG GENERIEREN\"><img src=\"img/reminder.png\" width=\"16\" height=\"16\" border=\"0\" /></a><br /><select name=\"{billing_id}\" id=\"billing_status_{billing_id}\" style=\"border: 1px #009999 solid; width: 145px;\"><option>Status &auml;ndern</option><option value=\"0\">Entw&uuml;rfe</option><option value=\"1\">Offen</option><option value=\"3\">Bezahlt</option></select>";
					}									


						// Mysql Abfrage wird durchgeführt
						$result = mysql_query($query);
						$num_rows = mysql_num_rows($result);				
						
						while($row = mysql_fetch_array($result))
																
							{
							
								$billing_id = $row['billing_id'];
							
								if($row['barverkauf'] == 1)
									{
										$name = "<span style=\"color:red\">BARVERKAUF</span>";
										$kdnr = 0;
									}
								
								else
									{
										$query2 = "SELECT name, vorname, kdnnr FROM kontakte WHERE contact_id =".$row['contact_id'];						
										$result2 = mysql_query($query2);
										$row2 = mysql_fetch_array($result2);
										
										$name = $row2['name'].", ".$row2['vorname'];
										$kdnr = $row2['kdnr'];							
									}
		
		
								if($row['status'] == 0)
									{
										$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"mod_billing/billing_delete.php\" target=\"_blank\" title=\"DIESE RECHNUNG L&Ouml;SCHEN\" class=\"billing_delete\" id=\"{billing_id}\"><img src=\"img/icon_delete.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=billing&action=billing_edit&billing_id={billing_id}\" title=\"DIESE RECHNUNG BEARBEITEN\"><img src=\"img/icon_edit.png\" width=\"16\" height=\"16\" border=\"0\" /></a><br /><select name=\"{billing_id}\" id=\"billing_status_{billing_id}\" style=\"border: 1px #009999 solid; width: 145px;\"><option>Status &auml;ndern</option><option value=\"1\">Offen</option><option value=\"3\">Bezahlt</option></select>";
									}

								elseif($row['status'] == 1)
									{
										if($row['date_target'] <= $date)
											{
												$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=billing&action=reminder_create&billing_id={billing_id}\" title=\"ZAHLUNGSERINNERUNG GENERIEREN\"><img src=\"img/reminder.png\" width=\"16\" height=\"16\" border=\"0\" /></a><br /><select name=\"{billing_id}\" id=\"billing_status_{billing_id}\" style=\"border: 1px #009999 solid; width: 145px;\"><option>Status &auml;ndern</option><option value=\"3\">Bezahlt</option></select>";
											}
											
										else
											{
												$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;<br /><select name=\"{billing_id}\" id=\"billing_status_{billing_id}\" style=\"border: 1px #009999 solid; width: 145px;\"><option>Status &auml;ndern</option><option value=\"3\">Bezahlt</option></select>";
											}
											
									}
									
								elseif($row['status'] == 3)
									{
										$operations = "<a href=\"mod_billing/billing_export.php?PDF=OUT&billing_id={billing_id}\" target=\"_blank\" title=\"PDF VON DIESER RECHNUNG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>";
									}
		
								$rechnungsdatum = make_date($row['date_billing']);
								$faelligkeitsdatum = make_date($row['date_target']);
								
								if($row['payment_method'] == 1)
									{
										$zahlungsmethode = "&Uuml;berweisung";
									}
									
								elseif($row['payment_method'] == 2)
									{
										$zahlungsmethode = "Barzahlung";
									}
									
								
								$summe_netto = calc_sum($row['billing_id'],"rechnung_netto");
								$summe_brutto = calc_sum($row['billing_id'],"rechnung_brutto");
																									    																			    	
								print "<tr>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; text-align: center;\">".$row['billing_nr']."</td>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: left;\">".$name."</td>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: right;\">".$summe_netto."</td>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: right;\">".$summe_brutto."</td>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: center;\">".$faelligkeitsdatum."</td>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: left;\">".$zahlungsmethode."</td>";				
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black;\">".$row['operator']."</td>";
								print str_replace("{billing_id}",$billing_id,"<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black;\">".$operations."</td>");
								print "</tr>";
															
							}

?>
			</table>