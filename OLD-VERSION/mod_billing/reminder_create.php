<!----------------- INCLUDES REMINDER MAIN ----------------->
<?php
	include "php/billing_functions.php";
	$billing_id = $_GET['billing_id'];
	$frist = 5 * 24 * 60 * 60;
	$stamp = make_stamp($date2);
	$ze_date = make_date($frist+$stamp);
?>
		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/reminder.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Neue Zahlungserinnerung erstellen</h1>
			<hr noshade size="1" color="#333333">
			
			<form action="static.php?active_module=billing&action=reminder_set" method="post" id="reminder_form">
				<input type="hidden" name="scdAction" value="save" />
				<table border="0" cellpadding="0" cellpadding="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
					<tr>
						<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;">Anschrift:</td>
						<td width="50%"></td>
					</tr>
					<tr>
						<td rowspan="7" width="50%" valign="top" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><p><b><?php print get_contact($billing_id,"name").", ".get_contact($billing_id,"vorname")."</b></p><p>".get_contact($billing_id,"strasse")."</p><p>".get_contact($billing_id,"plz")." ".get_contact($billing_id,"ort"); ?></p></td>
						<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Kunden-Nr.:</strong></label>&nbsp;<input type="text" name="kdnnr" style="border: 1px #009999 solid; width: 145px;" value="<?php print get_contact($billing_id,"kdnnr"); ?>" /></td>
					</tr>
					<tr>
						<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Rechnungs-Nr.:</strong></label>&nbsp;<input type="text" name="billing_nr" style="border: 1px #009999 solid; width: 145px;" value="<?php print get_billing_detail($billing_id,"billing_nr",0); ?>" readonly /></td>
					</tr>
					<tr>
						<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>F&auml;lliger Betrag Brutto:</strong></label>&nbsp;<input type="text" name="betrag" style="border: 1px #009999 solid; width: 145px;" value="<?php print calc_sum($billing_id,"rechnung_brutto"); ?>" readonly /></td>
					</tr>					
					<tr>
						<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Rechnungs-Datum:</strong></label>&nbsp;<input type="text" name="date_billing" style="border: 1px #009999 solid; width: 145px;" value="<?php print get_billing_detail($billing_id,"date_billing",1); ?>" readonly /></td>
					</tr>								
					<tr>
						<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Rechnung war f&auml;llig zum:</strong></label>&nbsp;<input type="text" id="date_billing_target" style="border: 1px #009999 solid; width: 145px;" value="<?php print get_billing_detail($billing_id,"date_target",1); ?>" readonly /></td>
					</tr>
					<tr>
						<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Datum Zahlungserinnerung:</strong></label>&nbsp;<input type="text" name="date_reminder" style="border: 1px #009999 solid; width: 145px;" value="<?php echo $date2; ?>" readonly /></td>
					</tr>
					<tr>
						<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>F&auml;lligkeit Zahlungserinnerung:</strong></label>&nbsp;<input type="text" name="date_reminder_target" style="border: 1px #009999 solid; width: 145px;" value="<?php echo $ze_date; ?>" readonly /></td>
					</tr>
					<tr>
						<td width="50%" colspan="2" align="center" ><br /><input type="submit" value="Status Zahlungserinnerung setzen" style="background-color: #009999; color: #FFF; width: 100%;" /></td>
					</tr>		
				</table>	
			</form>
			
		</div>