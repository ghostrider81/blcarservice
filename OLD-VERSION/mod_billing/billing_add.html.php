<!----------------- INCLUDES BILLING MAIN ----------------->

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/billing.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Neue Rechnung erstellen</h1>
			<hr noshade size="1" color="#333333">

		<script type="text/javascript">
	        $(document).ready(function() {
	            $("input[id='BtnBarverkauf']").click(function() {
	                $("#BillingAdress").load('mod_billing/billing_cash_payment.php');
	            });
	            
	            $("#TxtNamesuchen").autocomplete({
					source: "mod_billing/contact_search.php",
					minLength: 2,
					select: function(event, ui) {
						$('#BillingAdress').load('mod_billing/billing_load_contact.php?contact_id='+ui.item.id); }
				});
	        });
		</script>
			
			<form action="static.php?active_module=billing&action=billing_insert" method="post" id="billing_form">
			<table border="0" cellpadding="0" cellpadding="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;">Rechnungsanschrift:</td>
					<td width="50%"></td>
				</tr>
				<tr>
					<td rowspan="5" width="50%" valign="top" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><p id="BillingAdress"><input type="button" value="Barverkauf" id="BtnBarverkauf" style="border-radius: 5px; background-color: #009999; border: 1px solid #FFF; color: #FFF; width: 145px;" />&nbsp;&nbsp;oder&nbsp;&nbsp;<input type="text" value="Namen suchen" id="TxtNamesuchen" style="border-radius: 5px; background-color: #009999; border: 1px solid #FFF; color: #FFF; width: 145px; text-align:center;" onfocus="this.value = ''" /></p></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Rechnungs-Datum:</strong></label>&nbsp;<input type="text" name="date_billing" style="border: 1px #009999 solid; width: 145px;" value="<?php echo $date2; ?>" readonly /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Rechnungs-Nr.:</strong></label>&nbsp;<input type="text" name="billing_nr" style="border: 1px #009999 solid; width: 145px;" value="<?php print get_new_billingnr(); ?>" /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Kunden-Nr.:</strong></label>&nbsp;<input type="text" id="kdnnr" style="border: 1px #009999 solid; width: 145px;" readonly /></td>
				</tr>								
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Status:</strong></label>&nbsp;<select name="status" style="border: 1px #009999 solid; width: 145px;"><option value="0">Rechnungsentwurf</option><option value="1">Offene Rechnung</option><option value="3">Bezahlte Rechnung</option></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Sachbearbeiter:</strong></label>&nbsp;<select name="operator" style="border: 1px #009999 solid; width: 145px;"><option value="Eike Lauenstein">Eike Lauenstein</option><option value="Felix Becher">Felix Becher</option></select></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><strong>Notizen:</strong></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Zahlbar bis:</strong></label>&nbsp;<input type="text" name="date_target" style="border: 1px #009999 solid; width: 145px;" id="commission_date" value="<?php print make_date(time() + (60 * 60 * 24 * 14)); ?>" /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><textarea name="notices" style="width:90%; height:15px; border:1px solid #009999;"></textarea></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Zahlungsmethode:</strong></label>&nbsp;<select name="payment_method" style="border: 1px #009999 solid; width: 145px;"><option value="1">Überweisung</option><option value="2">Zahlung bei Abholung</option></select></td>
				</tr>			
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"><hr noshade size="1" color="#009999" /></td>
				</tr>
				<tr>
					<td colspan="2">


						<!-- ANFANG ARBEITSLOHN PAUSCHALEN -->

								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Pauschale Arbeitslohn Positionen:</td>
											<td width="50%" align="right">
												<select id="option_pauschale">
													<option value="0">Pauschale Positionen hinzuf&uuml;gen?</option>
													<option value="0">Inaktiv</option>
													<option value="1">Aktiv</option>									
												</select>&nbsp;<span id="option_pauschale_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
											</td>
										</tr>
									</table>
								</div>
							
								<div id="layer_pauschale">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_pauschale">
										<thead>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
												<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
												<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
											</tr>
										</thead>
										<tfoot style="border-bottom: #FFF;">
											<tr>
												<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; border-bottom: #FFF 15px solid;"><span style="margin-right: 5px;">Summe Pauschale Arbeitslohn Positionen:</span></td>
												<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid; border-bottom: #FFF 15px solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_pauschale" name="total_netto_pauschale" readonly /></td>
												<td style="border-bottom: #FFF 15px solid;">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody>
											<tr style="border-bottom: 1px #FFF solid;">
												<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_pauschale_1" name="row_pauschale[1][desc]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_pauschale_1" name="row_pauschale[1][quant]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="preis_pauschale_1" name="row_pauschale[1][preis]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_pauschale_1" name="total_pauschale[1]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_pauschale_1" name="row_pauschale[1][art_id]" value="" /><input type="button" class="add_row_pauschale" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
											</tr>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ARBEITSLOHN PAUSCHALEN -->
					
						<!-- ANFANG ARBEITSLOHN MECHANIK -->
						
								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Mechanik:</td>
											<td width="50%" align="right">
												<select id="aw_mechanik" name="aw_mechanik_val">
													<option value="">AW Satz ausw&auml;hlen:</option>
													<?php getAW(); ?>
												</select>&nbsp;<span id="aw_mechanik_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
											</td>
										</tr>
									</table>
								</div>

								<div id="layer_mechanik">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_mechanik">
										<thead>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
												<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
												<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; border-bottom: #FFF 15px solid;"><span style="margin-right: 5px;">Summe Mechanik:</span></td>
												<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid; border-bottom: #FFF 15px solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_mechanik" name="total_netto_mechanik" readonly /></td>
												<td style="border-bottom: #FFF 15px solid;">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody>
											<tr style="border-bottom: 1px #FFF solid;">
												<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_mechanik_1" name="row_mechanik[1][desc]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_mechanik_1" name="row_mechanik[1][quant]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="aw_mechanik_1" name="row_mechanik[1][aw]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_mechanik_1" name="total_mechanik[1]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_mechanik_1" name="row_mechanik[1][art_id]" value="" /><input type="button" class="add_row_mechanik" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							
						<!-- ENDE ARBEITSLOHN MECHANIK -->
						

							
						<!-- ANFANG ARBEITSLOHN KAROSSERIE -->

								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Karosserie:</td>
											<td width="50%" align="right">
												<select id="aw_karosserie" name="aw_karosserie_val">
													<option value="">AW Satz ausw&auml;hlen:</option>
													<?php getAW(); ?>
												</select>&nbsp;<span id="aw_karosserie_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
											</td>
										</tr>
									</table>
								</div>

								<div id="layer_karosserie">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_karosserie">
										<thead>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
												<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
												<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; border-bottom: #FFF 15px solid;"><span style="margin-right: 5px;">Summe Karosserie:</span></td>
												<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid; border-bottom: #FFF 15px solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_karosserie" name="total_netto_karosserie" readonly /></td>
												<td style="border-bottom: #FFF 15px solid;">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody>
											<tr style="border-bottom: 1px #FFF solid;">
												<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_karosserie_1" name="row_karosserie[1][desc]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_karosserie_1" name="row_karosserie[1][quant]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="aw_karosserie_1" name="row_karosserie[1][aw]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_karosserie_1" name="total_karosserie[1]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_karosserie_1" name="row_karosserie[1][art_id]" value="" /><input type="button" class="add_row_karosserie" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
											</tr>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ARBEITSLOHN KAROSSERIE -->
						

							
						<!-- ANFANG ARBEITSLOHN LACK -->

								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Lack:</td>
											<td width="50%" align="right">
												<select id="aw_lack" name="aw_lack_val">
													<option value="">AW Satz ausw&auml;hlen:</option>
													<?php getAW(); ?>
												</select>&nbsp;<span id="aw_lack_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
											</td>
										</tr>
									</table>
								</div>

								<div id="layer_lack">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_lack">
										<thead>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
												<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
												<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; border-bottom: #FFF 15px solid;"><span style="margin-right: 5px;">Summe Lack:</span></td>
												<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid; border-bottom: #FFF 15px solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_lack" name="total_netto_lack" readonly /></td>
												<td style="border-bottom: #FFF 15px solid;">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody>
											<tr style="border-bottom: 1px #FFF solid;">
												<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_lack_1" name="row_lack[1][desc]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_lack_1" name="row_lack[1][quant]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="aw_lack_1" name="row_lack[1][aw]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_lack_1" name="total_lack[1]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_lack_1" name="row_lack[1][art_id]" value="" /><input type="button" class="add_row_lack" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
											</tr>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ARBEITSLOHN LACK -->
						
					
						
						<!-- ANFANG ARBEITSLOHN ELEKTRIK -->

								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Elektrik:</td>
											<td width="50%" align="right">
												<select id="aw_elektrik" name="aw_elektrik_val">
													<option value="">AW Satz ausw&auml;hlen:</option>
													<?php getAW(); ?>
												</select>&nbsp;<span id="aw_elektrik_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return;" style="color: #FFF;">Speichern</a></span>
											</td>
										</tr>
									</table>
								</div>

								<div id="layer_elektrik">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_elektrik">
										<thead>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
												<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
												<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; border-bottom: #FFF 15px solid;"><span style="margin-right: 5px;">Summe Elektrik:</span></td>
												<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid; border-bottom: #FFF 15px solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_elektrik" name="total_netto_elektrik" readonly /></td>
												<td style="border-bottom: #FFF 15px solid;">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody>
											<tr style="border-bottom: 1px #FFF solid;">
												<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_elektrik_1" name="row_elektrik[1][desc]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_elektrik_1" name="row_elektrik[1][quant]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="aw_elektrik_1" name="row_elektrik[1][aw]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_elektrik_1" name="total_elektrik[1]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_elektrik_1" name="row_elektrik[1][art_id]" value="" /><input type="button" class="add_row_elektrik" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
											</tr>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ARBEITSLOHN ELEKTRIK -->
						


						<!-- ANFANG ERSATZTEILE -->

								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Material:</td>
											<td width="50%" align="right">
												<select id="option_ersatzteile">
													<option value="0">Material hinzuf&uuml;gen?</option>
													<option value="0">Inaktiv</option>
													<option value="1">Aktiv</option>									
												</select>&nbsp;<span id="option_ersatzteile_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
											</td>
										</tr>
									</table>
								</div>
							
								<div id="layer_ersatzteile">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_ersatzteile">
										<thead>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
												<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
												<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
											</tr>
										</thead>
										<tfoot style="border-bottom: #FFF;">
											<tr>
												<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; border-bottom: #FFF 15px solid;"><span style="margin-right: 5px;">Summe Material:</span></td>
												<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid; border-bottom: #FFF 15px solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_ersatzteile" name="total_netto_ersatzteile" readonly /></td>
												<td style="border-bottom: #FFF 15px solid;">&nbsp;</td>
											</tr>
										</tfoot>
										<tbody>
											<tr style="border-bottom: 1px #FFF solid;">
												<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_ersatzteile_1" name="row_ersatzteile[1][desc]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_ersatzteile_1" name="row_ersatzteile[1][quant]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="preis_ersatzteile_1" name="row_ersatzteile[1][preis]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_ersatzteile_1" name="total_ersatzteile[1]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_ersatzteile_1" name="row_ersatzteile[1][art_id]" value="" /><input type="button" class="add_row_ersatzteile" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
											</tr>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ERSATZTEILE -->

						<!-- ANFANG TÜV -->

								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">T&Uuml;V:</td>
											<td width="50%" align="right">
												<select id="option_tuv">
													<option value="0">T&Uuml;V hinzuf&uuml;gen?</option>
													<option value="0">Inaktiv</option>
													<option value="1">Aktiv</option>									
												</select>&nbsp;<span id="option_tuv_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
											</td>
										</tr>
									</table>
								</div>
							
								<div id="layer_tuv">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_tuv">
										<tbody>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
												<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
												<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
												<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; text-align: right;">Geb&uuml;hr f&uuml;r T&Uuml;V / AU:</td>
												<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_tuv_1" name="total_tuv_1" /></td>
												<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
											</tr>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE TÜV -->

						<!-- ANFANG ATT -->

								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Altteile Pfand:</td>
											<td width="50%" align="right">
												<select id="option_att">
													<option value="0">Altteile Pfand hinzuf&uuml;gen?</option>
													<option value="0">Inaktiv</option>
													<option value="1">Aktiv</option>									
												</select>&nbsp;<span id="option_att_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
											</td>
										</tr>
									</table>
								</div>
							
								<div id="layer_att">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_att">
										<thead>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;"></th>
												<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Satz:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Summe:</th>
												<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;"></th>
											</tr>
										</thead>
										<tbody>
											<tr style="border-bottom: 1px #FFF solid;">
												<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;"></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_att" name="desc_att" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="percent_att" name="percent_att" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="preis_att" name="preis_att" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_att" name="total_att" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"></td>
											</tr>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ATT -->

						<!-- ANFANG ZUSAMMENFASSUNG -->
							
								<div>
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid; text-align: right;">Summe der Positionen:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-top: 15px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_netto" name="total_netto" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">MwSt:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_mwst" name="total_mwst" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">T&Uuml;V / AU:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_tuv" name="total_tuv" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">USt. Altteile Pfand:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_att_1" name="total_att_1" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>										
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">Gesamtsumme:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_brutto" name="total_brutto" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
									</table>
								</div>
								
						<!-- ENDE ZUSAMMENFASSUNG -->
						
					</td>
				</tr>			
			</table>	
			
			<input type="submit" />
			
			</form>
			
		</div>