<!----------------- INCLUDES BILLING LEFTBAR -------------------->

		<div style="background-color: #CCCCCC; border-radius: 10px; width: 180px; margin: 0; padding: 10px;">
			<h1><img src="img/list.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Rechnungen:</h1>
			<hr noshade size="1" color="#333333" />
			<ul id="menu">
				<li style="margin-bottom:15px;"><a href="static.php?active_module=billing&action=billing_add">Neue Rechnung erstellen</a></li>
				<li><a href="static.php?active_module=billing&action=billing_list&status=4">Alle Rechnungen (<?php countBillings(4); ?>)</a></li>
				<li><a href="static.php?active_module=billing&action=billing_list&status=0">Entw&uuml;rfe (<?php countBillings(0); ?>)</a></li>
				<li><a href="static.php?active_module=billing&action=billing_list&status=1">Offen (<?php countBillings(1); ?>)</a></li>
				<li><a href="static.php?active_module=billing&action=billing_list&status=2">F&auml;llig (<?php countBillings(2); ?>)</a></li>
				<li style="margin-bottom:15px;"><a href="static.php?active_module=billing&action=billing_list&status=3">Bezahlt (<?php countBillings(3); ?>)</a></li>
				<li><a href="static.php?active_module=billing&action=billing_list&status=5">Zahlungserinnerungen (<?php countBillings(5); ?>)</a></li>
				<li><a href="static.php?active_module=billing&action=billing_list&status=6">Mahnungen (<?php countBillings(6); ?>)</a></li>
			</ul>
		</div>
		
		<?php if ($_POST['action'] == "billing_from_commission") { ?>
		
		<div style="background-color: #CCCCCC; border-radius: 10px; width: 180px; margin: 10px 0 0 0; padding: 10px;">
			<h1><img src="img/list.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Fahrzeug:</h1>
			<hr noshade size="1" color="#333333" />
			<ul id="menu_2">
				<li><b>Hersteller / Modell:</b></li>
				<li><?php print $hersteller." / ".$modell; ?></li>
				<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
				<li><b>Kennzeichen:</b></li>
				<li><?php print $kennzeichen; ?></li>
				<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
				<li><b>Ident-Nr.:</b></li>
				<li><?php print $identnr; ?></li>
				<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
				<li><b>Erstzulassung:</b></li>
				<li><?php print $datum_erstzulassung; ?></li>
				<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
				<li><b>KBA-Nr.:</b></li>
				<li><?php print $kbanr; ?></li>
				<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
				<li><b>Kilometerstand:</b></li>
				<li><?php print $km; ?></li>
			</ul>		
		</div>	
		
		<?php }
		
		elseif (isset($_GET['contact_id']) && $_GET['contact_id'] != "") { 
			
				$query_fahrzeug = "SELECT car_id, hersteller, modell, kennzeichen, identnr, erstzulassung, kbanr, km FROM fahrzeuge WHERE contact_id =".$_GET['contact_id'];
				$result_fahrzeug = mysql_query($query_fahrzeug);
				$numrows_fahrzeug = mysql_num_rows($result_fahrzeug);
				
				if($numrows_fahrzeug == 1)
					{
						$row_fahrzeug = mysql_fetch_array($result_fahrzeug);
						
						$car_id = $row_fahrzeug['car_id'];
						$hersteller = $row_fahrzeug['hersteller'];
						$modell = $row_fahrzeug['modell'];
						$kennzeichen = $row_fahrzeug['kennzeichen'];
						$identnr = $row_fahrzeug['identnr'];
						$erstzulassung = $row_fahrzeug['erstzulassung'];
						$datum_erstzulassung = date("d.m.Y",$erstzulassung);
						$kbanr = $row_fahrzeug['kbanr'];
						$km = $row_fahrzeug['km'];
					?>
						<script type="text/javascript">
							$(document).ready(function(){
								$("#form_car_id").val("<?php print $car_id; ?>");
							});
						</script>
						
						<div style="background-color: #CCCCCC; border-radius: 10px; width: 180px; margin: 10px 0 0 0; padding: 10px;">
							<h1><img src="img/list.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Fahrzeug:</h1>
							<hr noshade size="1" color="#333333" />
							<ul id="menu_2">
								<li><b>Hersteller / Modell:</b></li>
								<li><?php print $hersteller." / ".$modell; ?></li>
								<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
								<li><b>Kennzeichen:</b></li>
								<li><?php print $kennzeichen; ?></li>
								<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
								<li><b>Ident-Nr.:</b></li>
								<li><?php print $identnr; ?></li>
								<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
								<li><b>Erstzulassung:</b></li>
								<li><?php print $datum_erstzulassung; ?></li>
								<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
								<li><b>KBA-Nr.:</b></li>
								<li><?php print $kbanr; ?></li>
								<li style="margin-left: -50px;"><hr noshade size="1" color="#009999" style="width: 90%;" /></li>
								<li><b>Kilometerstand:</b></li>
								<li><?php print $km; ?></li>
							</ul>		
						</div>	
					
					<?php
					}
				
				else
					{ ?>
					
						<div style="background-color: #CCCCCC; border-radius: 10px; width: 180px; margin: 10px 0 0 0; padding: 10px;">
							<h1><img src="img/list.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Fahrzeug:</h1>
							<hr noshade size="1" color="#333333" />
							<script type="text/javascript">
								$(function() {
									$('#multipleCar').change(function() {
									var x = $(this).val();
									$('#form_car_id').val(x);
									});
								});
							</script>
							<select style="border: 1px #009999 solid; width: 145px;" id="multipleCar">
								<option value="">Fahrzeug w&auml;hlen:</option>
							
							<?php
							
								while($row_fahrzeug = mysql_fetch_array($result_fahrzeug))
														
									{
										print "<option value=\"".$row_fahrzeug['car_id']."\">".$row_fahrzeug['hersteller']." / ".$row_fahrzeug['modell']."</option>";
									}
							
							?>
							</select>
						</div>
					
					<?php }
				

		?>
				
	
				
		<?php } ?>	

<!----------------- END INCLUDES BILLING LEFTBAR -------------------->