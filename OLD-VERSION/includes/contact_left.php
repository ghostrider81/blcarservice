<!----------------- INCLUDES CONTACTS LEFTBAR -------------------->

		<div style="background-color: #CCCCCC; border-radius: 10px; width: 180px; margin: 0; padding: 10px;">
			<h1><img src="img/list.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Menü:</h1>
			<hr noshade size="1" color="#333333">
			<ul id="menu">
				<li><a href="static.php?active_module=contact&action=contact_list">Kontaktliste</a></li>
				<li><a href="static.php?active_module=contact&action=contact_add">Neuen Kontakt erstellen</a></li>
			</ul>
		</div>
		
		<?php if ($_GET['action'] == "contact_list") { ?>
		
			<script lang="text/javascript">
				$(document).ready(function()
				{
						$("#ContactSearch").autocomplete({
						    source: "mod_contact/contact_search.php",
						    minLength: 2,
						    select: function(event, ui) {
						      window.location = ("static.php?active_module=contact&action=contact_detail&contact_id=" + ui.item.id)
						    }
						})
						.data("uiAutocomplete")._renderItem = function( ul, item ) {
						return $( "<li></li>" )
						    .data( "item.autocomplete", item )
						    .append( "<a href='static.php?active_module=contact&action=contact_detail&contact_id="+ item.id + "'>"+ item.label + "</a>" )
						    .appendTo( ul );
						};
				});
			</script>
		
		<div style="background-color: #CCCCCC; border-radius: 10px; width: 180px; margin: 10px 0 0 0; padding: 10px;">
			<h1><img src="img/list.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Suche:</h1>
			<hr noshade size="1" color="#333333">
			<input type="text" id="ContactSearch" name="ContactSearch" style="margin: 5px; border: 1px #009999 solid; width: 165px;" />	
		</div>
		
		<?php } 
		if ($_GET['action'] == "contact_detail") { ?>
		
		<div style="background-color: #CCCCCC; border-radius: 10px; width: 180px; margin: 10px 0 0 0; padding: 10px;">
			<h1><img src="img/list.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Funktionen:</h1>
			<hr noshade size="1" color="#333333">
			<ul id="menu">
				<li><a href="mod_contact/contact_delete.php?action=DeleteContact&contact_id=<?php print $_GET['contact_id']; ?>" onclick="return confirm('Bitte best&auml;tigen Sie, dass die gew&auml;hlte Aktion durchgef&uuml;hrt wird. Sie kann nicht widerrufen werden.')" title="Kontakt l&ouml;schen">Kontakt l&ouml;schen</a></li>
				<li><a href="static.php?active_module=contact&action=car_add&contact_id=<?php print $_GET['contact_id']; ?>">Fahrzeug hinzuf&uuml;gen</a></li>
			</ul>		
		</div>
		
		<?php } ?>	

<!----------------- END INCLUDES CONTACTS LEFTBAR -------------------->