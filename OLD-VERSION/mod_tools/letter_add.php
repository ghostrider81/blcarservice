<!----------------- INCLUDES TOOLS OVERVIEW ----------------->
 <script>
$(function() {
$( document ).tooltip();
});
</script>

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/printjob.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Neuen Serienbrief erstellen</h1>
			<hr noshade size="1" color="#333333">
			<form method="post" action="mod_tools/letter_generate.php?PDF=OUT">
			
			<fieldset style="margin-bottom: 10px; border: 1px solid;">
				<legend style="color: #333; font-size: 12px; font-family: verdana;"><strong>Printjob Einstellungen <a href="#nogo" title="Bei den Printjob Einstellungen legen Sie fest, welche Maße das Druckmedium hat und mit welcher Sortierung die Ausgabe erfolgen soll. Die Angaben erfolgen in mm.">[?]</a></strong></legend>
					<p><label for="letterWidth" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Breite:</label><input type="text" name="letterWidth" style="border: 1px #009999 solid;" /></p>
					<p><label for="letterHeight" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">H&ouml;he:</label><input type="text" name="letterHeight" style="border: 1px #009999 solid;" /></p>
					<p><label for="letterOrder" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Sortierung:</label><select name="letterOrder" style="border: 1px #009999 solid;"><option value="plz" selected>Postleitzahl</option><option value="name">Name</option><option value="ort">Ort</option></select></p>
			</fieldset>

			<fieldset style="margin-bottom: 10px; border: 1px solid;">
				<legend style="color: #333; font-size: 12px; font-family: verdana;"><strong>Block 1 (Absender) <a href="#nogo" title="Block 1 (Absender) bezeichnet den Adressblock mit der Absender Adresse. Neben der zu verwendenen Schriftart und Schriftgröße, wird hier der Startpunkt der Ausgabe definiert. Die Angaben links und oben, bilden den Startpunkt der ersten Reihe der Adresse. Auch hier erfolgen die Angaben in mm.">[?]</a></strong></legend>
					<p><label for="block1Left" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Abstand links:</label><input type="text" name="block1Left" style="border: 1px #009999 solid;" /></p>
					<p><label for="block1Top" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Abstand oben:</label><input type="text" name="block1Top" style="border: 1px #009999 solid;" /></p>
					<p><label for="block1FontType" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Schriftsatz:</label><select name="block1FontType" style="border: 1px #009999 solid;"><option value="Arial" selected>Arial</option><option value="Times">Times</option><option value="Courier">Courier</option></select></p>
					<p><label for="block1FontSize" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Schriftgr&ouml;&szlig;e:</label><select name="block1FontSize" style="border: 1px #009999 solid;"><option value="8" selected>8pt</option><option value="9">9pt</option><option value="10">10pt</option><option value="11">11pt</option><option value="12">12pt</option></select></p>
			</fieldset>

			<fieldset style="border: 1px solid;">
				<legend style="color: #333; font-size: 12px; font-family: verdana;"><strong>Block 2 (Empf&auml;nger) <a href="#nogo" title="Block 2 (Empfänger) bezeichnet den Adressblock mit der Empfänger Adresse. Neben der zu verwendenen Schriftart und Schriftgröße, wird hier der Startpunkt der Ausgabe definiert. Die Angaben links und oben, bilden den Startpunkt der ersten Reihe der Adresse. Auch hier erfolgen die Angaben in mm.">[?]</a></strong></legend>
					<p><label for="block2Left" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Abstand links:</label><input type="text" name="block2Left" style="border: 1px #009999 solid;" /></p>
					<p><label for="block2Top" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Abstand oben:</label><input type="text" name="block2Top" style="border: 1px #009999 solid;" /></p>
					<p><label for="block2FontType" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Schriftsatz:</label><select name="block2FontType" style="border: 1px #009999 solid;"><option value="Arial" selected>Arial</option><option value="Times">Times</option><option value="Courier">Courier</option></select></p>
					<p><label for="block2FontSize" style="float: left; width: 100px; color: #333; font-size: 12px; font-family: verdana;">Schriftgr&ouml;&szlig;e:</label><select name="block2FontSize" style="border: 1px #009999 solid;"><option value="8" selected>8pt</option><option value="9">9pt</option><option value="10">10pt</option><option value="11">11pt</option><option value="12">12pt</option></select></p>
			</fieldset>

			<input type="submit" value="Printjob generieren" style="background-color: #009999; color: #FFF; width: 100%; margin-top: 15px;">
		</div>
<!----------------- END INCLUDES TOOLS OVERVIEW ----------------->