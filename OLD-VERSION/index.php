<?php 
		include ("config/config.inc");

     if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      session_start();

      $username = $_POST['Username'];
      $password = $_POST['Password'];

      $hostname = $_SERVER['HTTP_HOST'];
      $path = dirname($_SERVER['PHP_SELF']);

	if (empty($username))
	{
		header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/index.php?status=empty_username');
		exit;
	}
	
	if (empty($password))
	{
		header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/index.php?status=empty_password');
		exit;
	}

	$link = mysql_pconnect( "$db_host", "$db_user", "$db_pass") ;
	mysql_select_db( "$db_name", $link );

	$result = mysql_query ( "select user_id, username, password, level from login_data where username ='".$username."' and password = '".$password."'") or die( mysql_error ());
	$row = mysql_fetch_array ($result);

	$username_db = $row['username'];
	$passwort_db = $row['password'];

      // Benutzername und Passwort werden ueberprueft
      if ($username == $username_db) {
       $_SESSION['angemeldet'] = true;
       $_SESSION['user_id'] = $row['user_id'];
       $_SESSION['username'] = utf8_encode($row['username']);
       $_SESSION['level'] = $row['level'];


       // Weiterleitung zur gesch?tzten Startseite
       if ($_SERVER['SERVER_PROTOCOL'] == 'HTTP/1.1') {
        if (php_sapi_name() == 'cgi') {
        
         header('Status: 303 See Other');
         }
        else {
  
         header('HTTP/1.1 303 See Other');
         }
        }
		
       header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/static.php?active_module=dashboard&action=index');
       exit;
       }
       
       else { 
       header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/index.php?status=failed');
       exit; }
      }

	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>B&L Carservice</title>
		<link href="css/styles.css" rel="stylesheet" type="text/css" media="all" />
	</head>
	
	<body>
		<div style="width:350px; border-radius:10px; border: 1px solid #333; margin:0 auto; background:#ccc; margin-top: 50px; padding: 10px;">
			<form action="index.php" method="post" name="login">
					
					<p style="font-family: verdana; font-size: 12px; font-weight: bold; color: #333; text-align: center;"><img src="img/logo.png" height="60" width="60" /><br /><br />
					
					<?php 
				
						if(isset($_GET['status']) && $_GET['status'] == "failed")
							{ 
								print "Die Anmeldung ist fehlgeschlagen";
							}
	
						elseif(isset($_GET['status']) && $_GET['status'] == "empty_username")
							{ 
								print "Es wurde kein Benutzername eingegeben";
							}
						
						elseif(isset($_GET['status']) && $_GET['status'] == "empty_password")
							{ 
								print "Es wurde kein Passwort eingegeben";
							}
						else
							{
								print "Bitte melden Sie sich an:";
							}
					
					?>
					
					</p>
					
					<p><label for="username" style="font-family: verdana; font-size: 12px; color: #333; margin-left: 60px; margin-right: 5px;">Benutzer:</label><input type="text" name="Username" id="username" style="border: 1px solid #009999;" /></p>
					<p><label for="password" style="font-family: verdana; font-size: 12px; color: #333; margin-left: 60px; margin-right: 5px;">Passwort:</label><input type="password" name="Password" id="password" style="border: 1px solid #009999;" /></p>
					<p><input type="submit" value="Einloggen" style="background-color: #009999; color: #FFF; width: 100%;"></p>

			</form>
		</div>
	</body>
</html>