////////////////////////////////// DATEPICKER JQUERY UI MIT DEUTSCHER ÜBERSETZUNG ////////////////////////////////////////


        $(function() {
        $( "#commission_date" ).datepicker({
                showWeek: true,
                firstDay: 1,
                beforeShowDay: $.datepicker.noWeekends,
                dateFormat: 'dd.mm.yy'    
        });

        jQuery(function($){
            $.datepicker.regional['de'] = {clearText: 'löschen', clearStatus: 'aktuelles Datum löschen',
                    closeText: 'schließen', closeStatus: 'ohne Änderungen schließen',
                    prevText: '<zurück', prevStatus: 'letzten Monat zeigen',
                    nextText: 'Vor>', nextStatus: 'nächsten Monat zeigen',
                    currentText: 'heute', currentStatus: '',
                    monthNames: ['Januar','Februar','März','April','Mai','Juni',
                    'Juli','August','September','Oktober','November','Dezember'],
                    monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
                    'Jul','Aug','Sep','Okt','Nov','Dez'],
                    monthStatus: 'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
                    weekHeader: 'Wo', weekStatus: 'Woche des Monats',
                    dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                    dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                    dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                    dayStatus: 'Setze DD als ersten Wochentag', dateStatus: 'Wähle D, M d',
                    dateFormat: 'dd.mm.yy', firstDay: 1, 
                    initStatus: 'Wähle ein Datum', isRTL: false};
            $.datepicker.setDefaults($.datepicker.regional['de']);
        });
		});

$(document).ready(function(){

////////////////////////////////// COMMISSION FORM ////////////////////////////////////////

		$('#commission_form').bind('submit', function() {
	        $(this).find('#aw_mechanik').removeAttr('disabled');
	        $(this).find('#aw_karosserie').removeAttr('disabled');
	        $(this).find('#aw_lack').removeAttr('disabled');
	        $(this).find('#aw_elektrik').removeAttr('disabled');
		});

////////////////////////////////// BILLING FORM ////////////////////////////////////////

		$('#billing_form').bind('submit', function() {
	        $(this).find("[name=aw_mechanik_val]").removeAttr('disabled');
	        $(this).find("[name=aw_karosserie_val]").removeAttr('disabled');
	        $(this).find("[name=aw_lack_val]").removeAttr('disabled');
	        $(this).find("[name=aw_elektrik_val]").removeAttr('disabled');
		});

////////////////////////////////// AJAX REQUEST OVERLAY LOADER ////////////////////////////////////////

       $(this).ajaxStart(function()
       {
          $("body").append("<div id='overlay'><img style='margin-top:20px;' src='img/ajax-loader.gif' /></div>");
       });
 
 
       $(this).ajaxStop(function()
       {
          $("#overlay").remove();
       });

////////////////////////////////// FORMEL ////////////////////////////////////////

function kaufm(x) {

	if(isNaN(x)) { x = 0.00; }
	else { x = (Math.round(x * 100) / 100).toFixed(2).toString().replace('.', ','); }
	return x;
}

////////////////////////////////// RECHENOPERATIONEN UND WERTE VERTEILEN ////////////////////////////////////////

function refreshSum() {
		
		    var totalsum_pauschale = 0.0;
		    var totalsum_mechanik = 0.0;
		    var totalsum_karosserie = 0.0;
		    var totalsum_lack = 0.0;
		    var totalsum_elektrik = 0.0;
		    var totalsum_ersatzteile = 0.0;
		    
	        $("#table_pauschale tbody tr").each(function () {
				
				var linesum_pauschale = 0.0;
				var qty_pauschale = $(this).find("[name$='[quant]']");
		        var price_pauschale = $(this).find("[name$='[preis]']");
	
		        // KOMMA DURCH PUNKT ERSETZEN
		        var qty_val_pauschale = qty_pauschale.val().split(',').join('.');
		        var price_val_pauschale = price_pauschale.val().split(',').join('.');
		        var qty_val_pauschale = parseFloat(qty_val_pauschale);
		        var price_val_pauschale = parseFloat(price_val_pauschale);
		        
		        
		        if (!qty_val_pauschale) {
	            	var qty_val_pauschale = 0; }
	            
		        if (!price_val_pauschale) {
		            var price_val_pauschale = 0; }
	
		        if (!qty_val_pauschale || price_pauschale.val() == "") {
		            lineprint_pauschale = 0; }
		            
		        else {
		            lineprint_pauschale = 1; }
	
		        if (lineprint_pauschale == 1) {
		            linesum_pauschale = qty_val_pauschale * price_val_pauschale;
			        totalsum_pauschale = totalsum_pauschale + linesum_pauschale;
					$(this).find("[name^='total_pauschale']").val(kaufm(linesum_pauschale));
					$('#total_netto_pauschale').val(kaufm(totalsum_pauschale)); } 
			        
			    else {
					$(this).find("[name$='total_pauschale']").text("–,–– €"); }

			});

		    $("#table_mechanik tbody tr").each(function () {
				
				var linesum_mechanik = 0.0;
				var qty_mechanik = $(this).find("[name$='[quant]']");
		        var price_mechanik = $(this).find("[name$='[aw]']");
		        
		        // KOMMA DURCH PUNKT ERSETZEN
		        var qty_val_mechanik = qty_mechanik.val().split(',').join('.');
		        var price_val_mechanik = price_mechanik.val().split(',').join('.');
		        var qty_val_mechanik = parseFloat(qty_val_mechanik);
		        var price_val_mechanik = parseFloat(price_val_mechanik);
		        
		        
		        if (!qty_val_mechanik) {
	            	var qty_val_mechanik = 0; }
	            
		        if (!price_val_mechanik) {
		            var price_val_mechanik = 0; }
	
		        if (!qty_val_mechanik || price_mechanik.val() == "") {
		            lineprint_mechanik = 0; }
		            
		        else {
		            lineprint_mechanik = 1; }
	
		        if (lineprint_mechanik == 1) {
		            linesum_mechanik = qty_val_mechanik * price_val_mechanik;
			        totalsum_mechanik = totalsum_mechanik + linesum_mechanik;
			        $(this).find("[name^='total_mechanik']").val(kaufm(linesum_mechanik));
			        $('#total_netto_mechanik').val(kaufm(totalsum_mechanik)); } 
			        
			    else {
					$(this).find("[name^='total_mechanik']").text("–,–– €"); }

	        });


		    $("#table_karosserie tbody tr").each(function () {
				
				var linesum_karosserie = 0.0;
				var qty_karosserie = $(this).find("[name$='[quant]']");
		        var price_karosserie = $(this).find("[name$='[aw]']");
		        
		        // KOMMA DURCH PUNKT ERSETZEN
		        var qty_val_karosserie = qty_karosserie.val().split(',').join('.');
		        var price_val_karosserie = price_karosserie.val().split(',').join('.');
		        var qty_val_karosserie = parseFloat(qty_val_karosserie);
		        var price_val_karosserie = parseFloat(price_val_karosserie);
		        
		        
		        if (!qty_val_karosserie) {
	            	var qty_val_karosserie = 0; }
	            
		        if (!price_val_karosserie) {
		            var price_val_karosserie = 0; }
	
		        if (!qty_val_karosserie || price_karosserie.val() == "") {
		            lineprint_karosserie = 0; }
		            
		        else {
		            lineprint_karosserie = 1; }
	
		        if (lineprint_karosserie == 1) {
		            linesum_karosserie = qty_val_karosserie * price_val_karosserie;
			        totalsum_karosserie = totalsum_karosserie + linesum_karosserie;
			        $(this).find("[name^='total_karosserie']").val(kaufm(linesum_karosserie));
			        $('#total_netto_karosserie').val(kaufm(totalsum_karosserie)); } 
			        
			    else {
					$(this).find("[name^='total_karosserie']").text("–,–– €"); }

	        });


		    $("#table_lack tbody tr").each(function () {
				
				var linesum_lack = 0.0;
				var qty_lack = $(this).find("[name$='[quant]']");
		        var price_lack = $(this).find("[name$='[aw]']");
		        
		        // KOMMA DURCH PUNKT ERSETZEN
		        var qty_val_lack = qty_lack.val().split(',').join('.');
		        var price_val_lack = price_lack.val().split(',').join('.');
		        var qty_val_lack = parseFloat(qty_val_lack);
		        var price_val_lack = parseFloat(price_val_lack);
		        
		        if (!qty_val_lack) {
	            	var qty_val_lack = 0; }
	            
		        if (!price_val_lack) {
		            var price_val_lack = 0; }
	
		        if (!qty_val_lack || price_lack.val() == "") {
		            lineprint_lack = 0; }
		            
		        else {
		            lineprint_lack = 1; }
	
		        if (lineprint_lack == 1) {
		            linesum_lack = qty_val_lack * price_val_lack;
			        totalsum_lack = totalsum_lack + linesum_lack;
			        $(this).find("[name^='total_lack']").val(kaufm(linesum_lack));
			        $('#total_netto_lack').val(kaufm(totalsum_lack)); } 
			        
			    else {
					$(this).find("[name^='total_lack']").text("–,–– €"); }

	        });


		    $("#table_elektrik tbody tr").each(function () {
				
				var linesum_elektrik = 0.0;
				var qty_elektrik = $(this).find("[name$='[quant]']");
		        var price_elektrik = $(this).find("[name$='[aw]']");
		        
		        // KOMMA DURCH PUNKT ERSETZEN
		        var qty_val_elektrik = qty_elektrik.val().split(',').join('.');
		        var price_val_elektrik = price_elektrik.val().split(',').join('.');
		        var qty_val_elektrik = parseFloat(qty_val_elektrik);
		        var price_val_elektrik = parseFloat(price_val_elektrik);
		        
		        if (!qty_val_elektrik) {
	            	var qty_val_elektrik = 0; }
	            
		        if (!price_val_elektrik) {
		            var price_val_elektrik = 0; }
	
		        if (!qty_val_elektrik || price_elektrik.val() == "") {
		            lineprint_elektrik = 0; }
		            
		        else {
		            lineprint_elektrik = 1; }
	
		        if (lineprint_elektrik == 1) {
		            linesum_elektrik = qty_val_elektrik * price_val_elektrik;
			        totalsum_elektrik = totalsum_elektrik + linesum_elektrik;
			        $(this).find("[name^='total_elektrik']").val(kaufm(linesum_elektrik));
			        $('#total_netto_elektrik').val(kaufm(totalsum_elektrik)); } 
			        
			    else {
					$(this).find("[name^='total_elektrik']").text("–,–– €"); }

	        });


	        $("#table_ersatzteile tbody tr").each(function () {
				
				var linesum_ersatzteile = 0.0;
				var qty_ersatzteile = $(this).find("[name$='[quant]']");
		        var price_ersatzteile = $(this).find("[name$='[preis]']");
	
		        // KOMMA DURCH PUNKT ERSETZEN
		        var qty_val_ersatzteile = qty_ersatzteile.val().split(',').join('.');
		        var price_val_ersatzteile = price_ersatzteile.val().split(',').join('.');
		        var qty_val_ersatzteile = parseFloat(qty_val_ersatzteile);
		        var price_val_ersatzteile = parseFloat(price_val_ersatzteile);
		        
		        
		        if (!qty_val_ersatzteile) {
	            	var qty_val_ersatzteile = 0; }
	            
		        if (!price_val_ersatzteile) {
		            var price_val_ersatzteile = 0; }
	
		        if (!qty_val_ersatzteile || price_ersatzteile.val() == "") {
		            lineprint_ersatzteile = 0; }
		            
		        else {
		            lineprint_ersatzteile = 1; }
	
		        if (lineprint_ersatzteile == 1) {
		            linesum_ersatzteile = qty_val_ersatzteile * price_val_ersatzteile;
			        totalsum_ersatzteile = totalsum_ersatzteile + linesum_ersatzteile;
					$(this).find("[name^='total_ersatzteile']").val(kaufm(linesum_ersatzteile));
					$('#total_netto_ersatzteile').val(kaufm(totalsum_ersatzteile)); } 
			        
			    else {
					$(this).find("[name$='total_ersatzteile']").text("–,–– €"); }

			});
			
			var total_tuv = $("#total_tuv_1").val();
			total_tuv = total_tuv.split(',').join('.');
			total_tuv = parseFloat(total_tuv);
			
				if(isNaN(total_tuv)) {
					total_tuv = 0.00;
				}
										
	        var total_netto = totalsum_pauschale + totalsum_mechanik + totalsum_karosserie + totalsum_lack + totalsum_elektrik + totalsum_ersatzteile;
	        var total_brutto = total_netto * 1.19 + total_tuv;
	        var total_mwst = total_netto * 0.19;
	        
			$('#total_netto').val(kaufm(total_netto));
			$('#total_brutto').val(kaufm(total_brutto));
			$('#total_tuv').val(kaufm(total_tuv));
			$('#total_mwst').val(kaufm(total_mwst));
			
        }


////////////////////////////////// FORMULAR VORBEREITEN ////////////////////////////////////////

		$( "#layer_pauschale" ).hide();
		$( "#layer_mechanik" ).hide();
		$( "#layer_karosserie" ).hide();
		$( "#layer_lack" ).hide();
		$( "#layer_elektrik" ).hide();
		$( "#layer_ersatzteile" ).hide();
		$( "#layer_tuv" ).hide();
		$( "#option_pauschale_save" ).hide();
		$( "#aw_mechanik_save" ).hide();
		$( "#aw_karosserie_save" ).hide();
		$( "#aw_lack_save" ).hide();
		$( "#aw_elektrik_save" ).hide();
		$( "#option_ersatzteile_save" ).hide();
		$( "#option_tuv_save" ).hide();		
		
////////////////////////////////// ZEILE LÖSCHEN //////////////////////////////////

		$('.del').live('click',function(){
			$(this).parent().parent().fadeOut()
            .queue(function(nxt) { 
                $(this).remove();
                nxt();
                refreshSum();
            });
		});
		
////////////////////////////////// PAUSCHALE ARBEITSLOHN POSITION ////////////////////////////////////////

		$('select#option_pauschale').change(function(){
		
		    if($(this).val() == 0){
		        $('#option_pauschale_save').show();
				$('#option_pauschale_save').live('click',function(){
				$('#option_pauschale').attr('disabled',true);
				$('#option_pauschale_save').hide();
				
					});		
		        
				}
				
		    else if($(this).val() == 1){
		        $('#option_pauschale_save').show();
				$('#option_pauschale_save').live('click',function(){
				$('#layer_pauschale').slideDown();
				$('#option_pauschale').attr('disabled',true);
				$('#option_pauschale_save').hide();
				
					});		
					
				// ERSTE ZEILE MIT AUTOCOMPLETE BESTÜCKEN
				$("#desc_pauschale_1").autocomplete({
				source: "mod_commission/art_search.php",
				minLength: 2,
				select: function(event, ui) {
					$('#art_pauschale_1').val(ui.item.id); }
					});
				}
		
		});
		
		
		// ZEILE HINZUFÜGEN
		var id_pauschale = 2;		
		$('.add_row_pauschale').live('click',function(){
			$(this).val('Löschen');
			$(this).attr('class','del');
			var appendTxt = "<tr><td align='center' style='color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold; border-bottom: 1px #FFF solid;'>"+id_pauschale+"</td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; solid; height: 20px; margin: 5px 0 5px 0; width: 95%;' type='text' id='desc_pauschale_"+id_pauschale+"' name='row_pauschale["+id_pauschale+"][desc]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;' type='text' size='3' id='quant_pauschale_"+id_pauschale+"' name='row_pauschale["+id_pauschale+"][quant]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' value='0,00' id='preis_pauschale_"+id_pauschale+"' name='row_pauschale["+id_pauschale+"][preis]' /></td><td align='center' style='border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right:1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='total_pauschale_"+id_pauschale+"' name='total_pauschale["+id_pauschale+"]' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='hidden' id='art_pauschale_"+id_pauschale+"' name='row_pauschale["+id_pauschale+"][art_id]' value='' /><input type='button' class='add_row_pauschale' style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value='Hinzuf&uuml;gen' /></td></tr>";
			$("#table_pauschale tr:last").after(appendTxt);
			$("#table_pauschale tr:last").hide().fadeIn();
			var row_id_pauschale = id_pauschale;
			$("#desc_pauschale_"+id_pauschale).autocomplete({
				source: "mod_commission/art_search.php",
				minLength: 2,
				select: function(event, ui) {
					$('#art_pauschale_'+row_id_pauschale).val(ui.item.id); }
			});		
			id_pauschale++;
			
		});
		
		
		// TABLE LISTENER
		$("#table_pauschale").on('blur', "input", refreshSum);
		
		

////////////////////////////////// AW MECHANIK ////////////////////////////////////////

		$('select#aw_mechanik').change(function(){
		    if($(this).val() != ''){
		    	var aw_ze_mechanik_1 = $('#aw_mechanik option:selected').val();
		        $('#aw_mechanik_save').show();
		        $('#aw_mechanik_1').val(aw_ze_mechanik_1);
		    }
		    else if($(this).val() == ''){
		        $('#aw_mechanik_save').hide();
		    }
		});
		
		$('#aw_mechanik_save').live('click',function(){
			$('#layer_mechanik').slideDown();
			$('#aw_mechanik').attr('disabled',true);
			$('#aw_mechanik_save').hide();
		});		
		
		// ERSTE ZEILE MIT AUTOCOMPLETE BESTÜCKEN
		$("#desc_mechanik_1").autocomplete({
			source: "mod_commission/art_search.php",
			minLength: 2,
			select: function(event, ui) {
				$('#art_mechanik_1').val(ui.item.id); }
		});
		
		// ZEILE HINZUFÜGEN
		var id_mechanik = 2;		
		$('.add_row_mechanik').live('click',function(){
			var aw_ze_mechanik = $('#aw_mechanik option:selected').val();
			$(this).val('Löschen');
			$(this).attr('class','del');
			var appendTxt = "<tr><td align='center' style='color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold; border-bottom: 1px #FFF solid;'>"+id_mechanik+"</td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; solid; height: 20px; margin: 5px 0 5px 0; width: 95%;' type='text' id='desc_mechanik_"+id_mechanik+"' name='row_mechanik["+id_mechanik+"][desc]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;' type='text' size='3' id='quant_mechanik_"+id_mechanik+"' name='row_mechanik["+id_mechanik+"][quant]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='aw_mechanik_"+id_mechanik+"' name='row_mechanik["+id_mechanik+"][aw]' value='"+aw_ze_mechanik+"' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right:1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='total_mechanik_"+id_mechanik+"' name='total_mechanik["+id_mechanik+"]' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='hidden' id='art_mechanik_"+id_mechanik+"' name='row_mechanik["+id_mechanik+"][art_id]' value='' /><input type='button' class='add_row_mechanik' style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value='Hinzuf&uuml;gen' /></td></tr>";
			$("#table_mechanik tr:last").after(appendTxt);
			$("#table_mechanik tr:last").hide().fadeIn();
			var row_id_mechanik = id_mechanik;
			$("#desc_mechanik_"+id_mechanik).autocomplete({
				source: "mod_commission/art_search.php",
				minLength: 2,
				select: function(event, ui) {
					$('#art_mechanik_'+row_id_mechanik).val(ui.item.id);  }
			});		
			id_mechanik++;
			
		});
		
		// TABLE LISTENER
		$("#table_mechanik").on('blur', "input", refreshSum);

        
////////////////////////////////// AW KAROSSERIE ////////////////////////////////////////

		$('select#aw_karosserie').change(function(){
		    if($(this).val() != ''){
		    	var aw_ze_karosserie_1 = $('#aw_karosserie option:selected').val();
		        $('#aw_karosserie_save').show();
		        $('#aw_karosserie_1').val(aw_ze_karosserie_1);
		    }
		    else if($(this).val() == ''){
		        $('#aw_karosserie_save').hide();
		    }
		});
		
		$('#aw_karosserie_save').live('click',function(){
			$('#layer_karosserie').slideDown();
			$('#aw_karosserie').attr('disabled',true);
			$('#aw_karosserie_save').hide();
		});		
		
		// ERSTE ZEILE MIT AUTOCOMPLETE BESTÜCKEN
		$("#desc_karosserie_1").autocomplete({
			source: "mod_commission/art_search.php",
			minLength: 2,
			select: function(event, ui) {
				$('#art_karosserie_1').val(ui.item.id); }
		});
		
		// ZEILE HINZUFÜGEN
		var id_karosserie = 2;		
		$('.add_row_karosserie').live('click',function(){
			var aw_ze_karosserie = $('#aw_karosserie option:selected').val();
			$(this).val('Löschen');
			$(this).attr('class','del');
			var appendTxt = "<tr><td align='center' style='color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold; border-bottom: 1px #FFF solid;'>"+id_karosserie+"</td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; solid; height: 20px; margin: 5px 0 5px 0; width: 95%;' type='text' id='desc_karosserie_"+id_karosserie+"' name='row_karosserie["+id_karosserie+"][desc]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;' type='text' size='3' id='quant_karosserie_"+id_karosserie+"' name='row_karosserie["+id_karosserie+"][quant]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='aw_karosserie_"+id_karosserie+"' name='row_karosserie["+id_karosserie+"][aw]' value='"+aw_ze_karosserie+"' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right:1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='total_karosserie_"+id_karosserie+"' name='total_karosserie["+id_karosserie+"]' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='hidden' id='art_karosserie_"+id_karosserie+"' name='row_karosserie["+id_karosserie+"][art_id]' value='' /><input type='button' class='add_row_karosserie' style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value='Hinzuf&uuml;gen' /></td></tr>";
			$("#table_karosserie tr:last").after(appendTxt);
			$("#table_karosserie tr:last").hide().fadeIn();
			var row_id_karosserie = id_karosserie;
			$("#desc_karosserie_"+id_karosserie).autocomplete({
				source: "mod_commission/art_search.php",
				minLength: 2,
				select: function(event, ui) {
					$('#art_karosserie_'+row_id_karosserie).val(ui.item.id); }
			});		
			id_karosserie++;
			
		});
		
		// TABLE LISTENER
		$("#table_karosserie").on('blur', "input", refreshSum);
		

////////////////////////////////// AW LACK ////////////////////////////////////////

		$('select#aw_lack').change(function(){
		    if($(this).val() != ''){
		    	var aw_ze_lack_1 = $('#aw_lack option:selected').val();
		        $('#aw_lack_save').show();
		        $('#aw_lack_1').val(aw_ze_lack_1);
		    }
		    else if($(this).val() == ''){
		        $('#aw_lack_save').hide();
		    }
		});
		
		$('#aw_lack_save').live('click',function(){
			$('#layer_lack').slideDown();
			$('#aw_lack').attr('disabled',true);
			$('#aw_lack_save').hide();
		});		
		
		// ERSTE ZEILE MIT AUTOCOMPLETE BESTÜCKEN
		$("#desc_lack_1").autocomplete({
			source: "mod_commission/art_search.php",
			minLength: 2,
			select: function(event, ui) {
				$('#art_lack_1').val(ui.item.id); }
		});
		
		// ZEILE HINZUFÜGEN
		var id_lack = 2;		
		$('.add_row_lack').live('click',function(){
			var aw_ze_lack = $('#aw_lack option:selected').val();
			$(this).val('Löschen');
			$(this).attr('class','del');
			var appendTxt = "<tr><td align='center' style='color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold; border-bottom: 1px #FFF solid;'>"+id_lack+"</td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; solid; height: 20px; margin: 5px 0 5px 0; width: 95%;' type='text' id='desc_lack_"+id_lack+"' name='row_lack["+id_lack+"][desc]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;' type='text' size='3' id='quant_lack_"+id_lack+"' name='row_lack["+id_lack+"][quant]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='aw_lack_"+id_lack+"' name='row_lack["+id_lack+"][aw]' value='"+aw_ze_lack+"' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right:1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='total_lack_"+id_lack+"' name='total_lack["+id_lack+"]' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='hidden' id='art_lack_"+id_lack+"' name='row_lack["+id_lack+"][art_id]' value='' /><input type='button' class='add_row_lack' style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value='Hinzuf&uuml;gen' /></td></tr>";
			$("#table_lack tr:last").after(appendTxt);
			$("#table_lack tr:last").hide().fadeIn();
			var row_id_lack = id_lack;
			$("#desc_lack_"+id_lack).autocomplete({
				source: "mod_commission/art_search.php",
				minLength: 2,
				select: function(event, ui) {
					$('#art_lack_'+row_id_lack).val(ui.item.id); }
			});		
			id_lack++;
			
		});
		
		// TABLE LISTENER
		$("#table_lack").on('blur', "input", refreshSum);
		

////////////////////////////////// AW ELEKTRIK ////////////////////////////////////////

		$('select#aw_elektrik').change(function(){
		    if($(this).val() != ''){
		    	var aw_ze_elektrik_1 = $('#aw_elektrik option:selected').val();
		        $('#aw_elektrik_save').show();
		        $('#aw_elektrik_1').val(aw_ze_elektrik_1);
		    }
		    else if($(this).val() == ''){
		        $('#aw_elektrik_save').hide();
		    }
		});
		
		$('#aw_elektrik_save').live('click',function(){
			$('#layer_elektrik').slideDown();
			$('#aw_elektrik').attr('disabled',true);
			$('#aw_elektrik_save').hide();
		});		
		
		// ERSTE ZEILE MIT AUTOCOMPLETE BESTÜCKEN
		$("#desc_elektrik_1").autocomplete({
			source: "mod_commission/art_search.php",
			minLength: 2,
			select: function(event, ui) {
				$('#art_elektrik_1').val(ui.item.id); }
		});
		
		// ZEILE HINZUFÜGEN
		var id_elektrik = 2;		
		$('.add_row_elektrik').live('click',function(){
			var aw_ze_elektrik = $('#aw_elektrik option:selected').val();
			$(this).val('Löschen');
			$(this).attr('class','del');
			var appendTxt = "<tr><td align='center' style='color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold; border-bottom: 1px #FFF solid;'>"+id_elektrik+"</td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; solid; height: 20px; margin: 5px 0 5px 0; width: 95%;' type='text' id='desc_elektrik_"+id_elektrik+"' name='row_elektrik["+id_elektrik+"][desc]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;' type='text' size='3' id='quant_elektrik_"+id_elektrik+"' name='row_elektrik["+id_elektrik+"][quant]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='aw_elektrik_"+id_elektrik+"' name='row_elektrik["+id_elektrik+"][aw]' value='"+aw_ze_elektrik+"' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right:1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='total_elektrik_"+id_elektrik+"' name='total_elektrik["+id_elektrik+"]' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='hidden' id='art_elektrik_"+id_elektrik+"' name='row_elektrik["+id_elektrik+"][art_id]' value='' /><input type='button' class='add_row_elektrik' style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value='Hinzuf&uuml;gen' /></td></tr>";
			$("#table_elektrik tr:last").after(appendTxt);
			$("#table_elektrik tr:last").hide().fadeIn();
			var row_id_elektrik = id_elektrik;
			$("#desc_elektrik_"+id_elektrik).autocomplete({
				source: "mod_commission/art_search.php",
				minLength: 2,
				select: function(event, ui) {
					$('#art_elektrik_'+row_id_elektrik).val(ui.item.id); }
			});		
			id_elektrik++;
			
		});
		
		// TABLE LISTENER
		$("#table_elektrik").on('blur', "input", refreshSum);


////////////////////////////////// ERSATZTEILE ////////////////////////////////////////

		$('select#option_ersatzteile').change(function(){
		
		    if($(this).val() == 0){
		        $('#option_ersatzteile_save').show();
				$('#option_ersatzteile_save').live('click',function(){
				$('#option_ersatzteile').attr('disabled',true);
				$('#option_ersatzteile_save').hide();
				
					});		
		        
				}
				
		    else if($(this).val() == 1){
		        $('#option_ersatzteile_save').show();
				$('#option_ersatzteile_save').live('click',function(){
				$('#layer_ersatzteile').slideDown();
				$('#option_ersatzteile').attr('disabled',true);
				$('#option_ersatzteile_save').hide();
				
					});		
					
				// ERSTE ZEILE MIT AUTOCOMPLETE BESTÜCKEN
				$("#desc_ersatzteile_1").autocomplete({
				source: "mod_commission/art_search.php",
				minLength: 2,
				select: function(event, ui) {
					$('#art_ersatzteile_1').val(ui.item.id); }
					});
				}
		
		});
		
		
		// ZEILE HINZUFÜGEN
		var id_ersatzteile = 2;		
		$('.add_row_ersatzteile').live('click',function(){
			$(this).val('Löschen');
			$(this).attr('class','del');
			var appendTxt = "<tr><td align='center' style='color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold; border-bottom: 1px #FFF solid;'>"+id_ersatzteile+"</td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; solid; height: 20px; margin: 5px 0 5px 0; width: 95%;' type='text' id='desc_ersatzteile_"+id_ersatzteile+"' name='row_ersatzteile["+id_ersatzteile+"][desc]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;' type='text' size='3' id='quant_ersatzteile_"+id_ersatzteile+"' name='row_ersatzteile["+id_ersatzteile+"][quant]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' value='0,00' id='preis_ersatzteile_"+id_ersatzteile+"' name='row_ersatzteile["+id_ersatzteile+"][preis]' /></td><td align='center' style='border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right:1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='total_ersatzteile_"+id_ersatzteile+"' name='total_ersatzteile["+id_ersatzteile+"]' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='hidden' id='art_ersatzteile_"+id_ersatzteile+"' name='row_ersatzteile["+id_ersatzteile+"][art_id]' value='' /><input type='button' class='add_row_ersatzteile' style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value='Hinzuf&uuml;gen' /></td></tr>";
			$("#table_ersatzteile tr:last").after(appendTxt);
			$("#table_ersatzteile tr:last").hide().fadeIn();
			var row_id_ersatzteile = id_ersatzteile;
			$("#desc_ersatzteile_"+id_ersatzteile).autocomplete({
				source: "mod_commission/art_search.php",
				minLength: 2,
				select: function(event, ui) {
					$('#art_ersatzteile_'+row_id_ersatzteile).val(ui.item.id); }
			});		
			id_ersatzteile++;
			
		});
		
		
		// TABLE LISTENER
		$("#table_ersatzteile").on('blur', "input", refreshSum);
		
		
////////////////////////////////// TÜV OPTION ////////////////////////////////////////

		$('select#option_tuv').change(function(){
		
		    if($(this).val() == 0){
		        $('#option_tuv_save').show();
				$('#option_tuv_save').live('click',function(){
				$('#option_tuv').attr('disabled',true);
				$('#option_tuv_save').hide();
				
					});		
		        
				}
				
		    else if($(this).val() == 1){
		        $('#option_tuv_save').show();
				$('#option_tuv_save').live('click',function(){
				$('#layer_tuv').slideDown();
				$('#option_tuv').attr('disabled',true);
				$('#option_tuv_save').hide();
				
					});		
				}
		
		});
		
		// TABLE LISTENER
		$("#table_tuv").on('blur', "input", refreshSum);		

});