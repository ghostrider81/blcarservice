<!----------------- INCLUDES CREDIT MAIN ----------------->

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/credit.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Gutschrift bearbeiten</h1>
			<hr noshade size="1" color="#333333">
			
			
			<?php
				// KUNDENDATEN HOLEN
				
				$credit_id = $_GET['credit_id'];
				
				$query_credit = "SELECT car_id, credit_nr, contact_id, operator, date_credit FROM gutschrift WHERE credit_id =".$credit_id;
				$result_credit = mysql_query($query_credit);
				$row_credit = mysql_fetch_array($result_credit);
				
				$contact_id = $row_credit['contact_id'];
				$car_id = $row_credit['car_id'];
				$credit_nr = $row_credit['credit_nr'];
				$date_credit = $row_credit['date_credit'];
				$operator = $row_credit['operator'];
				
				        $query_kunde = "SELECT kdnnr, anrede, titel, strasse, plz, ort, name, vorname FROM kontakte WHERE contact_id =".$contact_id;
						$result_kunde = mysql_query($query_kunde);
						$row_kunde = mysql_fetch_array($result_kunde);
						
						$name = $row_kunde['name']." ,".$row_kunde['vorname'];
						$kdnnr = $row_kunde['kdnnr'];
						$anrede = $row_kunde['anrede'];
						$titel = $row_kunde['titel'];
						$strasse = $row_kunde['strasse'];
						$plz = $row_kunde['plz'];
						$ort = $row_kunde['ort'];						
			
			?>
			
		<script type="text/javascript">
          $(document).ready(function () {
           
                ////////////////////////////////// POSITIONEN GUTSCHRIFT ////////////////////////////////////////
            
                // FORMEL
                function kaufm(x) {

		            if (isNaN(x)) { x = 0.00; }
		            else { x = (Math.round(x * 100) / 100).toFixed(2).toString().replace('.', ','); }
		            return x;
		        }


		        // ZEILE HINZUFÜGEN
		        var id_positionen = 2;
		        $('.add_row_positionen').live('click', function () {
		            $(this).val('Löschen');
		            $(this).attr('class', 'del');
		            var appendTxt = "<tr><td align='center' style='color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold; border-bottom: 1px #FFF solid;'>" + id_positionen + "</td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; solid; height: 20px; margin: 5px 0 5px 0; width: 95%;' type='text' id='desc_positionen_" + id_positionen + "' name='row_positionen[" + id_positionen + "][desc]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;' type='text' size='3' id='quant_positionen_" + id_positionen + "' name='row_positionen[" + id_positionen + "][quant]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' value='0,00' id='preis_positionen_" + id_positionen + "' name='row_positionen[" + id_positionen + "][preis]' /></td><td align='center' style='border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right:1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='total_positionen_" + id_positionen + "' name='total_positionen[" + id_positionen + "]' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='hidden' id='art_positionen_" + id_positionen + "' name='row_positionen[" + id_positionen + "][art_id]' value='' /><input type='button' class='add_row_positionen' style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value='Hinzuf&uuml;gen' /></td></tr>";
		            $("#table_positionen tr:last").after(appendTxt);
		            $("#table_positionen tr:last").hide().fadeIn();
		            var row_id_positionen = id_positionen;
		            $("#desc_positionen_" + id_positionen).autocomplete({
		                source: "mod_commission/art_search.php",
		                minLength: 2,
		                select: function (event, ui) {
		                    $('#art_positionen_' + row_id_positionen).val(ui.item.id);
		                }
		            });
		            id_positionen++;

		        });


		        // TABLE LISTENER
		        $("#table_positionen").on('blur', "input", refreshSumCredit);
              function refreshSumCredit() {

		            var totalsum_positionen = 0.0;

		            $("#table_positionen tbody tr").each(function () {

		                var linesum_positionen = 0.0;
		                var qty_positionen = $(this).find("[name$='[quant]']");
		                var price_positionen = $(this).find("[name$='[preis]']");

		                // KOMMA DURCH PUNKT ERSETZEN
		                var qty_val_positionen = qty_positionen.val().split(',').join('.');
		                var price_val_positionen = price_positionen.val().split(',').join('.');
		                var qty_val_positionen = parseFloat(qty_val_positionen);
		                var price_val_positionen = parseFloat(price_val_positionen);


		                if (!qty_val_positionen) {
		                    var qty_val_positionen = 0;
		                }

		                if (!price_val_positionen) {
		                    var price_val_positionen = 0;
		                }

		                if (!qty_val_positionen || price_positionen.val() == "") {
		                    lineprint_positionen = 0;
		                }

		                else {
		                    lineprint_positionen = 1;
		                }

		                if (lineprint_positionen == 1) {
		                    linesum_positionen = qty_val_positionen * price_val_positionen;
		                    totalsum_positionen = totalsum_positionen + linesum_positionen;
		                    $(this).find("[name^='total_positionen']").val(kaufm(linesum_positionen));
		                    $('#total_netto').val(kaufm(totalsum_positionen));
		                }

		                else {
		                    $(this).find("[name$='total_positionen']").text("–,–– €");
		                }

		            });

                        var total_netto = totalsum_positionen;
		                var total_brutto = total_netto * 1.19;
		                var total_mwst = total_netto * 0.19;

		                $('#total_netto').val(kaufm(total_netto));
		                $('#total_brutto').val(kaufm(total_brutto));
		                $('#total_mwst').val(kaufm(total_mwst));

                    };

              $("#TxtNamesuchen").autocomplete({
                  source: "mod_credit/contact_search.php",
                  minLength: 2,
                  select: function (event, ui) {
                      $('#CreditAdress').load('mod_credit/credit_load_contact.php?contact_id=' + ui.item.id);
                  }
              });
          
              // ERSTE ZEILE MIT AUTOCOMPLETE BESTÜCKEN
                 $("#desc_positionen_1").autocomplete({
		                    source: "mod_commission/art_search.php",
		                    minLength: 2,
		                    select: function (event, ui) {
		                        $('#art_positionen_1').val(ui.item.id);
		                    }
		                });
             });
		</script>			

			<form action="static.php?active_module=credit&action=credit_save" method="post" id="credit_form">
			<input type="hidden" name="credit_id" value="<?php print $credit_id; ?>" />
			<table border="0" cellpadding="0" cellpadding="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;">Gutschriftempf&auml;nger:</td>
					<td width="50%"></td>
				</tr>
				<tr>
					<td rowspan="4" width="50%" valign="top" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><?php print "<b><p>".$name."</p></b><p>".$strasse."</p><p>".$plz." ".$ort."</p>"; ?></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Gutschrift-Datum:</strong></label>&nbsp;<input type="text" name="date_credit" style="border: 1px #009999 solid; width: 145px;" value="<?php echo make_date($date_credit); ?>" readonly /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Gutschrift-Nr.:</strong></label>&nbsp;<input type="text" name="credit_nr" style="border: 1px #009999 solid; width: 145px;" value="<?php echo $credit_nr; ?>" readonly /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Kunden-Nr.:</strong></label>&nbsp;<input type="text" style="border: 1px #009999 solid; width: 145px;" value="<?php echo $kdnnr; ?>" readonly /></td>
				</tr>								
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Sachbearbeiter:</strong></label>&nbsp;<input type="text" style="border: 1px #009999 solid; width: 145px;" value="<?php echo $operator; ?>" readonly /></td>
				</tr>		
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"><hr noshade size="1" color="#009999" /></td>
				</tr>
				<tr>
					<td colspan="2">
					
						<!-- ANFANG POSITIONEN -->

								<?php
											
											$query_positionen = "SELECT 
															a.artikel_id,
															a.anzahl,
															a.preis,
															b.bezeichnung
															FROM 
															positionen_gutschrift a,
															positionen b
															WHERE 
															a.credit_id  = ".$credit_id." AND 
															a.artikel_id = b.artikel_id";
											$result_positionen = mysql_query($query_positionen);
											$numrows_positionen = mysql_num_rows($result_positionen);
											
								?>
													
                                                    <div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Positionen:</td>
															</tr>
														</table>
													</div>
													
									                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_positionen">
										                <thead>
											                <tr style="height:30px; border-bottom: 1px #FFF solid;">
												                <th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
												                <th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												                <th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
												                <th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
												                <th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
												                <th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
											                </tr>
										                </thead>
														
														<tbody>												
												
												<?php

                                                    $position_id = 1;
												
													while($row_positionen = mysql_fetch_array($result_positionen))
														{
															
															?>
															
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;"><?php print $position_id; ?></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_positionen_x<?php print $position_id; ?>" name="row_positionen[x<?php print $position_id; ?>][desc]" value="<?php print $row_positionen['bezeichnung']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_positionen_x<?php print $position_id; ?>" name="row_positionen[x<?php print $position_id; ?>][quant]" value="<?php print number_format($row_positionen['anzahl'], 1, ',', ''); ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="preis_positionen_x<?php print $position_id; ?>" name="row_positionen[x<?php print $position_id; ?>][preis]" value="<?php print number_format($row_positionen['preis'], 2, ',', ''); ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_positionen_x<?php print $position_id; ?>" name="total_positionen[x<?php print $position_id; ?>]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_positionen_x<?php print $position_id; ?>" name="row_positionen[x<?php print $position_id; ?>][art_id]" value="<?php print $position_id['artikel_id']; ?>" /><input type="button" class="<?php if($numrows_positionen == $position_id){print"add_row_positionen";} else {print"del";} ?>" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="<?php if($numrows_positionen == $position_id){print"Hinzuf&uuml;gen";} else {print"L&ouml;schen";} ?>" /></td>
																</tr>
															
															<?php
															
															$position_id++;
														}	
											
											
										?>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE POSITIONEN -->
						
						<!-- ANFANG ZUSAMMENFASSUNG -->
							
								<div>
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid; text-align: right;">Summe der Positionen:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-top: 15px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_netto" name="total_netto" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">MwSt:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_mwst" name="total_mwst" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">Gesamtsumme:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_brutto" name="total_brutto" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
									</table>
								</div>
								
						<!-- ENDE ZUSAMMENFASSUNG -->
						
					</td>
				</tr>			
			</table>	
			
			<input type="submit" />
			
			</form>
			
		</div>