<!----------------- INCLUDES CREDIT MAIN ----------------->

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/credit.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Neue Gutschrift erstellen</h1>
			<hr noshade size="1" color="#333333">

		<script type="text/javascript">
          $(document).ready(function () {
           
                ////////////////////////////////// POSITIONEN GUTSCHRIFT ////////////////////////////////////////
            
                // FORMEL
                function kaufm(x) {

		            if (isNaN(x)) { x = 0.00; }
		            else { x = (Math.round(x * 100) / 100).toFixed(2).toString().replace('.', ','); }
		            return x;
		        }


		        // ZEILE HINZUFÜGEN
		        var id_positionen = 2;
		        $('.add_row_positionen').live('click', function () {
		            $(this).val('Löschen');
		            $(this).attr('class', 'del');
		            var appendTxt = "<tr><td align='center' style='color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold; border-bottom: 1px #FFF solid;'>" + id_positionen + "</td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; solid; height: 20px; margin: 5px 0 5px 0; width: 95%;' type='text' id='desc_positionen_" + id_positionen + "' name='row_positionen[" + id_positionen + "][desc]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input style='border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;' type='text' size='3' id='quant_positionen_" + id_positionen + "' name='row_positionen[" + id_positionen + "][quant]' /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' value='0,00' id='preis_positionen_" + id_positionen + "' name='row_positionen[" + id_positionen + "][preis]' /></td><td align='center' style='border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right:1px #FFF solid;'><input type='text' style='border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;' id='total_positionen_" + id_positionen + "' name='total_positionen[" + id_positionen + "]' readonly /></td><td align='center' style='border-bottom: 1px #FFF solid;'><input type='hidden' id='art_positionen_" + id_positionen + "' name='row_positionen[" + id_positionen + "][art_id]' value='' /><input type='button' class='add_row_positionen' style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value='Hinzuf&uuml;gen' /></td></tr>";
		            $("#table_positionen tr:last").after(appendTxt);
		            $("#table_positionen tr:last").hide().fadeIn();
		            var row_id_positionen = id_positionen;
		            $("#desc_positionen_" + id_positionen).autocomplete({
		                source: "mod_commission/art_search.php",
		                minLength: 2,
		                select: function (event, ui) {
		                    $('#art_positionen_' + row_id_positionen).val(ui.item.id);
		                }
		            });
		            id_positionen++;

		        });


		        // TABLE LISTENER
		        $("#table_positionen").on('blur', "input", refreshSumCredit);
              function refreshSumCredit() {

		            var totalsum_positionen = 0.0;

		            $("#table_positionen tbody tr").each(function () {

		                var linesum_positionen = 0.0;
		                var qty_positionen = $(this).find("[name$='[quant]']");
		                var price_positionen = $(this).find("[name$='[preis]']");

		                // KOMMA DURCH PUNKT ERSETZEN
		                var qty_val_positionen = qty_positionen.val().split(',').join('.');
		                var price_val_positionen = price_positionen.val().split(',').join('.');
		                var qty_val_positionen = parseFloat(qty_val_positionen);
		                var price_val_positionen = parseFloat(price_val_positionen);


		                if (!qty_val_positionen) {
		                    var qty_val_positionen = 0;
		                }

		                if (!price_val_positionen) {
		                    var price_val_positionen = 0;
		                }

		                if (!qty_val_positionen || price_positionen.val() == "") {
		                    lineprint_positionen = 0;
		                }

		                else {
		                    lineprint_positionen = 1;
		                }

		                if (lineprint_positionen == 1) {
		                    linesum_positionen = qty_val_positionen * price_val_positionen;
		                    totalsum_positionen = totalsum_positionen + linesum_positionen;
		                    $(this).find("[name^='total_positionen']").val(kaufm(linesum_positionen));
		                    $('#total_netto').val(kaufm(totalsum_positionen));
		                }

		                else {
		                    $(this).find("[name$='total_positionen']").text("–,–– €");
		                }

		            });

                        var total_netto = totalsum_positionen;
		                var total_brutto = total_netto * 1.19;
		                var total_mwst = total_netto * 0.19;

		                $('#total_netto').val(kaufm(total_netto));
		                $('#total_brutto').val(kaufm(total_brutto));
		                $('#total_mwst').val(kaufm(total_mwst));

                    };

              $("#TxtNamesuchen").autocomplete({
                  source: "mod_credit/contact_search.php",
                  minLength: 2,
                  select: function (event, ui) {
                      $('#CreditAdress').load('mod_credit/credit_load_contact.php?contact_id=' + ui.item.id);
                  }
              });
          
              // ERSTE ZEILE MIT AUTOCOMPLETE BESTÜCKEN
                 $("#desc_positionen_1").autocomplete({
		                    source: "mod_commission/art_search.php",
		                    minLength: 2,
		                    select: function (event, ui) {
		                        $('#art_positionen_1').val(ui.item.id);
		                    }
		                });
             });
		</script>
			
			<form action="static.php?active_module=credit&action=credit_insert" method="post" id="credit_form">
			<table border="0" cellpadding="0" cellpadding="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;">Gutschriftempf&auml;nger:</td>
					<td width="50%"></td>
				</tr>
				<tr>
					<td rowspan="4" width="50%" valign="top" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><p id="CreditAdress"><input type="text" value="Namen suchen" id="TxtNamesuchen" style="border-radius: 5px; background-color: #009999; border: 1px solid #FFF; color: #FFF; width: 145px; text-align:center;" onfocus="this.value = ''" /></p></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Gutschrift-Datum:</strong></label>&nbsp;<input type="text" name="date_credit" style="border: 1px #009999 solid; width: 145px;" value="<?php echo $date2; ?>" readonly /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Gutschrift-Nr.:</strong></label>&nbsp;<input type="text" name="credit_nr" style="border: 1px #009999 solid; width: 145px;" /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Kunden-Nr.:</strong></label>&nbsp;<input type="text" id="kdnnr" style="border: 1px #009999 solid; width: 145px;" readonly /></td>
				</tr>								
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Sachbearbeiter:</strong></label>&nbsp;<select name="operator" style="border: 1px #009999 solid; width: 145px;"><option value="Eike Lauenstein">Eike Lauenstein</option><option value="Felix Becher">Felix Becher</option></select></td>
				</tr>		
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"><hr noshade size="1" color="#009999" /></td>
				</tr>
				<tr>
					<td colspan="2">
					
						<!-- ANFANG POSITIONEN -->
						
								<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width="100%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Positionen:</td>
										</tr>
									</table>
								</div>
							
								
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_positionen">
										<thead>
											<tr style="height:30px; border-bottom: 1px #FFF solid;">
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
												<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
												<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
												<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
												<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
											</tr>
										</thead>
										<tbody>
											<tr style="border-bottom: 1px #FFF solid;">
												<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_positionen_1" name="row_positionen[1][desc]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_positionen_1" name="row_positionen[1][quant]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="preis_positionen_1" name="row_positionen[1][preis]" /></td>
												<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_positionen_1" name="total_positionen[1]" readonly /></td>
												<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_positionen_1" name="row_positionen[1][art_id]" value="" /><input type="button" class="add_row_positionen" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
											</tr>
										</tbody>
									</table>
								
								
						<!-- ENDE POSITIONEN -->

						<!-- ANFANG ZUSAMMENFASSUNG -->
							
								<div>
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid; text-align: right;">Summe der Positionen:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-top: 15px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_netto" name="total_netto" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">MwSt:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_mwst" name="total_mwst" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">Gesamtsumme:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_brutto" name="total_brutto" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
									</table>
								</div>
								
						<!-- ENDE ZUSAMMENFASSUNG -->
						
					</td>
				</tr>			
			</table>	
			
			<input type="submit" />
			
			</form>
			
		</div>