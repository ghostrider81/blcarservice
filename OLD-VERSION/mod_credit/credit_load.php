	<script type="text/javascript">
		$(function() {
			
			// GUTSCHRIFT LÖSCHEN
			$(".credit_delete").live('click', function(){
			var element = $(this);
			var del_id = element.attr("id");
			var info = 'id=' + del_id;
			if(confirm("Soll die Gutschrift wirklich gelöscht werden?"))
			{
			 $.ajax({
				   type: "POST",
				   url: "mod_credit/credit_delete.php",
				   data: info,
				   success: function(){}
			   });
			  $(this).parent().parent().animate({ backgroundColor: "#099" }, "slow")
			  .animate({ opacity: "hide" }, "fast");
			 }
			return false;
			});
		});
	</script>

			<table border="0" id="creditlist" cellpadding="0" cellspacing="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; text-align: center;">GS-Nr.:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Name:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Betrag Netto:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Betrag Brutto:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;">Sachbearbeiter:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;" width="150">Optionen:</th>
				</tr>

<?php

	include "../config/config.inc";
	include "../includes/database_connection.php";
	include "../php/math_functions.php";
	

	                    $query = "
							SELECT credit_id, contact_id, car_id, credit_nr, date_credit, operator
							FROM gutschrift 
							ORDER BY credit_id DESC";


						// Mysql Abfrage wird durchgeführt
						$result = mysql_query($query);
						$num_rows = mysql_num_rows($result);				
						
						while($row = mysql_fetch_array($result))
																
							{
							
								$credit_id = $row['credit_id'];
							
								$query2 = "SELECT name, vorname, kdnnr FROM kontakte WHERE contact_id =".$row['contact_id'];						
										$result2 = mysql_query($query2);
										$row2 = mysql_fetch_array($result2);
										
										$name = $row2['name'].", ".$row2['vorname'];
										$kdnr = $row2['kdnr'];							
								
		                        $operations = "<a href=\"mod_credit/credit_export.php?PDF=OUT&credit_id={credit_id}\" target=\"_blank\" title=\"PDF VON DIESER GUTSCHRIFT ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"mod_credit/credit_delete.php\" target=\"_blank\" title=\"DIESE GUTSCHRIFT L&Ouml;SCHEN\" class=\"credit_delete\" id=\"{credit_id}\"><img src=\"img/icon_delete.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=credit&action=credit_edit&credit_id={credit_id}\" title=\"DIESE RECHNUNG BEARBEITEN\"><img src=\"img/icon_edit.png\" width=\"16\" height=\"16\" border=\"0\" /></a>";
								
                                $summe_netto = calc_sum($row['credit_id'],"gutschrift_netto");
								$summe_brutto = calc_sum($row['credit_id'],"gutschrift_brutto");
																									    																			    	
								print "<tr>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; text-align: center;\">".$row['credit_nr']."</td>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: left;\">".$name."</td>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: right;\">".$summe_netto."</td>";
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: right;\">".$summe_brutto."</td>";				
								print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black;\">".$row['operator']."</td>";
								print str_replace("{credit_id}",$credit_id,"<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black;\">".$operations."</td>");
								print "</tr>";
															
							}

?>
			</table>