<?php

	date_default_timezone_set('Europe/Berlin');

	// Anzahl vorhandener Aufträge ermitteln
	// 1 = Entwürfe
	// 2 = Offen
	// 3 = In Bearbeitung
	// 4 = Abgeschlossen
	
	function countCommissions($status)
		{
			$a = "SELECT commission_id FROM auftrag WHERE status ='".$status."'";
		 	$b = mysql_query($a) or die(mysql_error());
		 	$c = mysql_num_rows($b);
		 	
		 	print $c;
		}

	function countBillings($status)
		{
		
			if($status == 0 or $status == 1 or $status == 3)
				{
					$a = "SELECT billing_id FROM rechnung WHERE status ='".$status."'";
				 	$b = mysql_query($a) or die(mysql_error());
				 	$c = mysql_num_rows($b);
				}

			elseif($status == 2)
				{
					$date = time();
					$a = "SELECT billing_id FROM rechnung WHERE status = 1 AND date_target <= ".$date;
				 	$b = mysql_query($a) or die(mysql_error());
				 	$c = mysql_num_rows($b);
				}
			
			elseif($status == 4)
				{
					$a = "SELECT billing_id FROM rechnung";
				 	$b = mysql_query($a) or die(mysql_error());
				 	$c = mysql_num_rows($b);
				}	
		 	
		 	print $c;
		}

	function countCredits($status)
		{
			$a = "SELECT credit_id FROM gutschrift";
		 	$b = mysql_query($a) or die(mysql_error());
		 	$c = mysql_num_rows($b);
		 	
		 	print $c;
		}
		
	// AW SETS ABRUFEN
	function getAW()
		{
			$a = "SELECT anz_aw, stundenlohn, preis_aw, beschreibung FROM aw";
			$b = mysql_query($a) or die(mysql_error());
				while($c = mysql_fetch_array($b))
					{	
						$preis_aw = number_format($c['preis_aw'], 2, ',', '.');
						print "<option value=\"".$c['preis_aw']."\">".$c['beschreibung']." / 1 Stunde / ".$c['anz_aw']." AW / ZE ".$preis_aw." €</option>";
					}
		}


	// UMWANDELN EINES UNIX TIMESTAMPS IN EUROPÄISCHES DATUM
	function make_date($unix_timestamp)
	
		{
															
			if(!empty($unix_timestamp))
																
				{
																
					$day=date("d",$unix_timestamp);
					$month=date("m",$unix_timestamp);
					$year=date("Y",$unix_timestamp);
																		
					return $date_formated = $day.'.'.$month.'.'.$year;
														
				}
																	
			elseif(empty($unix_timestamp))
																
				{
																	
					return $date_formated = "";
																		
				}
																	
		}


	// ERSTELLEN EINES UNIX TIMESTAMPS	
	function make_stamp($valid_date)
	
		{
		
			if (!empty($valid_date))
				
				{
				
					$date_cutted = explode (".", $valid_date);
				
					$tag = $date_cutted[0];
					$monat = $date_cutted[1];
					$jahr = $date_cutted[2];
					
					return $unix_stamp = mktime(0,0,0,$monat,$tag,$jahr);
	
				}
					
			elseif (empty($valid_date))
				
				{
					
					return $unix_stamp = "";
					
				}
		
		}


	// NÄCHSTE RECHNUNGSNUMMER GENERIEREN
	function get_new_billingnr()
	
		{
	
			// LETZTE VERGEBENE RECHNUNGSNUMMER UND AKTUELLES JAHR AUSLESEN
			$a = "SELECT billing_nr FROM rechnung ORDER BY billing_id DESC LIMIT 1";
			$b = mysql_query($a) or die("Es ist ein Fehler unterlaufen");
			$c = mysql_fetch_array($b);
			
			$letzte_rg = $c['billing_nr'];
			$aktuelles_jahr = date("Y");
			
			
			// LETZTE VERGEBENE RECHNUNGSNUMMER TEILEN UND ÜBERPRÜFEN
			$parts_rg = explode("/", $letzte_rg);
			
			
			// WENN AKTUELLES JAHR MIT DEM LETZTEN NR ÜBEREINSTIMMT, UM EINEN ERHÖHEN
			if($aktuelles_jahr == $parts_rg[1])
				{
					$neue_rg = $parts_rg[0]+1;
					$neue_rg = $neue_rg."/".$aktuelles_jahr;
					return $neue_rg;
				}
			
			
			// WENN NICHT, DANN AKTUELLES JAHR UND LFNDNR AUF 1 SETZEN	
			else
				{
					$neue_rg = "1/".$aktuelles_jahr;
					return $neue_rg;
				}
			
		}

	function get_new_commissionnr()
	
		{
			$a = "SELECT commission_nr FROM auftrag ORDER BY commission_id DESC LIMIT 1";
			$b = mysql_query($a) or die("Es ist ein Fehler unterlaufen");
			$c = mysql_fetch_array($b);
			
			$commission_nr = $c['commission_nr'];
			
			if($commission_nr == "" or $commission_nr == 1)
				{
					$new_commission_nr = 400;
				}
				
			else
				{
					$new_commission_nr = $commission_nr+1;
				}
				
			return $new_commission_nr;
		}
		
	function get_contact($billing_id,$attribut)
	
		{
			$a1 = "SELECT contact_id FROM rechnung WHERE billing_id =".$billing_id;
			$b1 = mysql_query($a1) or die("Es ist ein Fehler unterlaufen");
			$c1 = mysql_fetch_object($b1);
			
			$contact_id = $c1->contact_id;
		
			$a2 = "SELECT ".$attribut." FROM kontakte WHERE contact_id = ".$contact_id;
			$b2 = mysql_query($a2) or die("Es ist ein Fehler unterlaufen");
			$c2 = mysql_fetch_object($b2);
			
			return $c2->$attribut;
		}		
		
	function get_billing_detail($billing_id,$attribut,$date)
	
		{
			$a1 = "SELECT ".$attribut." FROM rechnung WHERE billing_id =".$billing_id;
			$b1 = mysql_query($a1) or die("Es ist ein Fehler unterlaufen");
			$c1 = mysql_fetch_object($b1);
			
			if($date == 1)
				{
					return make_date($c1->$attribut);
				}
				
			elseif($date == 0)
				{
					return $c1->$attribut;
				}
			
		}	

?>