<!----------------- INCLUDES CONTACTS MAIN ----------------->
<?php		
		$sql = "SELECT max(kdnnr) AS kdnnr FROM kontakte"; 
		$result = mysql_query($sql) or die("Fehler: " . mysql_error()); 
		$row = mysql_fetch_array($result);
		if($row['kdnnr'] == "")
			{
				$kundennummer = 20000;
			}
		else
			{
				$kundennummer = ($row['kdnnr']+1); 				
			}
		
?>

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/contacts.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Neuen Kontakt erstellen <span style="font-size: 10px;">Kunden-Nr.: <?php print $kundennummer; ?></span></h1>
			<hr noshade size="1" color="#333333">
			
			<form action="static.php?active_module=contact&action=contact_insert" method="post">
			<input type="hidden" name="form_kdnnr" value="<?php print $kundennummer; ?>" />
			<table border="0" cellpadding="0" cellpadding="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px;">Persönliche Daten:</td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Anrede / Titel:</td>
					<td>
					<select name="form_anrede" size="1" style="margin: 5px; border: 1px #009999 solid; width: 145px;">
													<option value="">[ Anrede ]</option>
													<option value=""></option>
													<option value="">-------------------</option>
													<option value=""></option>
													<option value="Herr">Herr</option>
													<option value="Frau">Frau</option>
													<option value="Firma">Firma</option>
													<option value="Autohaus">Autohaus</option>
					</select>&nbsp;
					<select name="form_titel" size="1" style="margin: 5px; border: 1px #009999 solid; width: 145px;">
													<option value="">[ Titel ]</option>
													<option value=""></option>
													<option value="">-------------------</option>
													<option value=""></option>
													<option value="Dipl. Ing">Dipl.-Ing.</option>
													<option value="Dipl. Ing. (FH)">Dipl.-Ing. (FH)</option>
													<option value="Dr.">Dr.</option>
													<option value="Prof.">Prof.</option>
													<option value="Prof. Dr.">Prof. Dr.</option>+
													<option value="Dr. med.">Dr. med.</option>
													<option value="Dr. med. Dent.">Dr. med. Dent.</option>
													<option value="Dr. Ing.">Dr. Ing.</option>
													<option value="Dr. Rer. Nat.">Dr. Rer. Nat.</option>
													<option value="Dr. jur.">Dr. jur.</option>
													<option value="Dipl.-Pol.">Dipl.-Pol.</option>
													<option value="Dipl. Betriebswirt">Dipl. Betriebswirt</option>
													<option value="Dipl. Betriebswirtin">Dipl. Betriebswirtin</option>
													<option value="Dipl. Finanzwirt">Dipl. Finanzwirt</option>
													<option value="Dipl. Inf.">Dipl. Inf.</option>
													<option value="Dipl. Inf. (FH)">Dipl. Inf. (FH)</option>
													<option value="Dipl. Kauffrau">Dipl. Kauffrau</option>
													<option value="Dipl. Kaufmann">Dipl. Kaufmann</option>
													<option value="Dipl. Landwirt">Dipl. Landwirt</option>
													<option value="Dipl. Physiker">Dipl. Physiker</option>
													<option value="Dipl. Psychologe">Dipl. Psychologe</option>
													<option value="Dipl. Psychologin">Dipl. Psychologin</option>
													<option value="Dipl. P&auml;dagoge">Dipl. P&auml;dagoge</option>
													<option value="Dipl. P&auml;dagogin">Dipl. P&auml;dagogin</option>
													<option value="Dipl. Soz. P&auml;d.">Dipl. Soz. P&auml;d.</option>
													<option value="Dipl. Volkswirt">Dipl. Volkswirt</option>
													<option value="Dipl. Volkswirtin">Dipl. Volkswirtin</option>
													<option value="Freifrau">Freifrau</option>
													<option value="Freigraf">Freigraf</option>
													<option value="Freiherr v.">Freiherr v.</option>
													<option value="Graf">Graf</option>
													<option value="Gr&auml;fin">Gr&auml;fin</option>
													<option value="Konsul">Konsul</option>
													<option value="Magister">Magister</option>
													<option value="Pastor">Pastor</option>
													<option value="Baron">Baron</option>
					</select>
					</td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Geburtstag:</td>
					<td><select name="form_geb_day" size="1" style="margin: 5px; border: 1px #009999 solid;">
													<option value="">Tag</option>
													<option value=""></option>
													<option value="">-----</option>
													<option value=""></option>													
													<option value="01">01</option>
													<option value="02">02</option>
													<option value="03">03</option>
													<option value="04">04</option>
													<option value="05">05</option>
													<option value="06">06</option>
													<option value="07">07</option>
													<option value="08">08</option>
													<option value="09">09</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
													<option value="13">13</option>
													<option value="14">14</option>
													<option value="15">15</option>
													<option value="16">16</option>
													<option value="17">17</option>
													<option value="18">18</option>
													<option value="19">19</option>
													<option value="20">20</option>
													<option value="21">21</option>
													<option value="22">22</option>
													<option value="23">23</option>
													<option value="24">24</option>
													<option value="25">25</option>
													<option value="26">26</option>
													<option value="27">27</option>
													<option value="28">28</option>
													<option value="29">29</option>
													<option value="30">30</option>
													<option value="31">31</option>
												</select>
												
												<select name="form_geb_month" size="1" style="margin: 5px; border: 1px #009999 solid;">
													<option value="">Monat</option>
													<option value=""></option>
													<option value="">-----</option>
													<option value=""></option>													
													<option value="01">Januar</option>
													<option value="02">Februar</option>
													<option value="03">März</option>
													<option value="04">April</option>
													<option value="05">Mai</option>
													<option value="06">Juni</option>
													<option value="07">Juli</option>
													<option value="08">August</option>
													<option value="09">September</option>
													<option value="10">Oktober</option>
													<option value="11">November</option>
													<option value="12">Dezember</option>
												</select>
												
												<select name="form_geb_year" size="1" style="margin: 5px; border: 1px #009999 solid;">
													<option value="">Jahr</option>
													<option value=""></option>
													<option value="">-----</option>
													<option value=""></option>														
													<option value="1990">1990</option>
													<option value="1989">1989</option>
													<option value="1988">1988</option>
													<option value="1987">1987</option>
													<option value="1986">1986</option>
													<option value="1985">1985</option>
													<option value="1984">1984</option>
													<option value="1983">1983</option>
													<option value="1982">1982</option>
													<option value="1981">1981</option>
													<option value="1980">1980</option>
													<option value="1979">1979</option>
													<option value="1978">1978</option>
													<option value="1977">1977</option>
													<option value="1976">1976</option>
													<option value="1975">1975</option>
													<option value="1974">1974</option>
													<option value="1973">1973</option>
													<option value="1972">1972</option>
													<option value="1971">1971</option>
													<option value="1970">1970</option>
													<option value="1969">1969</option>
													<option value="1968">1968</option>
													<option value="1967">1967</option>
													<option value="1966">1966</option>
													<option value="1965">1965</option>
													<option value="1964">1964</option>
													<option value="1963">1963</option>
													<option value="1962">1962</option>
													<option value="1961">1961</option>
													<option value="1960">1960</option>
													<option value="1959">1959</option>
													<option value="1958">1958</option>
													<option value="1957">1957</option>
													<option value="1956">1956</option>
													<option value="1955">1955</option>
													<option value="1954">1954</option>
													<option value="1953">1953</option>
													<option value="1952">1952</option>
													<option value="1951">1951</option>
													<option value="1950">1950</option>
													<option value="1949">1949</option>
													<option value="1948">1948</option>
													<option value="1947">1947</option>
													<option value="1946">1946</option>
													<option value="1945">1945</option>
													<option value="1944">1944</option>
													<option value="1943">1943</option>
													<option value="1942">1942</option>
													<option value="1941">1941</option>
													<option value="1940">1940</option>
													<option value="1939">1939</option>
													<option value="1938">1938</option>
													<option value="1937">1937</option>
													<option value="1936">1936</option>
													<option value="1935">1935</option>
													<option value="1934">1934</option>
													<option value="1933">1933</option>
													<option value="1932">1932</option>
													<option value="1931">1931</option>
													<option value="1930">1930</option>
													<option value="1929">1929</option>
													<option value="1928">1928</option>
													<option value="1927">1927</option>
													<option value="1926">1926</option>
													<option value="1925">1925</option>
													<option value="1924">1924</option>
													<option value="1923">1923</option>
													<option value="1922">1922</option>
													<option value="1921">1921</option>
													<option value="1920">1920</option>
												</select></td>
				</tr>				
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Name:</td>
					<td><input type="text" name="form_name" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Vorname:</td>
					<td><input type="text" name="form_vorname" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px;">Adresse:</td>
				</tr>				
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Straße:</td>
					<td><input type="text" name="form_strasse" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">PLZ:</td>
					<td><input type="text" name="form_plz" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Ort:</td>
					<td><input type="text" name="form_ort" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Land:</td>
					<td><input type="text" name="form_land" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px;">Kontaktdaten:</td>
				</tr>				
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Telefon 1:</td>
					<td><input type="text" name="form_telefon1" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Telefon 2:</td>
					<td><input type="text" name="form_telefon2" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Telefax:</td>
					<td><input type="text" name="form_telefax" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Mobil:</td>
					<td><input type="text" name="form_mobil" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">E-Mail 1:</td>
					<td><input type="text" name="form_email1" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">E-Mail 2:</td>
					<td><input type="text" name="form_email2" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Web:</td>
					<td><input type="text" name="form_web" style="margin: 5px; border: 1px #009999 solid; width: 300px;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Notizen:</td>
					<td><textarea name="form_bemerkungen" rows="8" cols="40" style="margin: 5px; border: 1px #009999 solid; width:785px;"></textarea></td>
				</tr>
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px;"><input type="submit" value="Speichern" style="background-color: #009999; color: #FFF; width: 100%;"></td>					
				</tr>
			</table>
			</form>
		</div>
<!----------------- END INCLUDES CONTACTS MAIN ----------------->