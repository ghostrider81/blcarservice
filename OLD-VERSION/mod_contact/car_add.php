<!----------------- INCLUDES CARS MAIN ----------------->

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/contacts.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Neues Fahrzeug erstellen</h1>
			<hr noshade size="1" color="#333333">
			
			<form action="static.php?active_module=contact&action=car_insert" method="post">
			<input type="hidden" name="form_contact_id" value="<?php print $_GET['contact_id']; ?>" />
			<table border="0" cellpadding="0" cellpadding="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px;">Fahrzeug Daten:</td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Hersteller:</td>
					<td><input type="text" name="form_hersteller" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Modell:</td>
					<td><input type="text" name="form_modell" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>				
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Kennzeichen:</td>
					<td><input type="text" name="form_kennzeichen" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Ident-Nr.:</td>
					<td><input type="text" name="form_ident" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">KBA-Nr.::</td>
					<td><input type="text" name="form_kba" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Erstzulassung:</td>
					<td><input type="text" name="form_erstzulassung" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>			
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">HU:</td>
					<td><input type="text" name="form_hu" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">AU:</td>
					<td><input type="text" name="form_au" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Leistung:</td>
					<td><input type="text" name="form_leistung" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>
				<tr>
					<td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px;">Kilometerstand:</td>
					<td><input type="text" name="form_kilometerstand" size="40" style="margin: 5px; border: 1px #009999 solid;" /></td>
				</tr>
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px;"><input type="submit" value="Speichern" style="background-color: #009999; color: #FFF; width: 100%;"></td>					
				</tr>				
			</table>
			</form>
		</div>
<!----------------- END INCLUDES CARS MAIN ----------------->