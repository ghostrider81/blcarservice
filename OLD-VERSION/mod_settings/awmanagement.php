<!----------------- BEGINN INCLUDES SETTINGS ----------------->
	<script type="text/javascript">
		$(function() {
			
			// AW RECHNER
			function aw_rechner() {
			var f_anzahl_aw = $("#anzahl_aw").val();
			var f_stundenlohn = $("#stundenlohn").val();
		
				// String in Zahlen umwandeln
				f_anzahl_aw = f_anzahl_aw.toString().replace(',', '.');
				f_stundenlohn = f_stundenlohn.toString().replace(',', '.');
		
				var f_preis_aw = f_stundenlohn / f_anzahl_aw;
		
				// Prüfen ob Ergebniss eine gültige Zahl ist, wenn nicht dann null setzen
				if(isNaN(f_preis_aw))
					{ 
						f_preis_aw = 0,00; 
					}
					
					else
					{
						f_preis_aw = (Math.round(f_preis_aw * 100) / 100).toFixed(2).toString().replace('.', ',');
					}
		
				// Ausgabe AW Preis
				$("#preis_aw").val(f_preis_aw);
					}
					
					// Bei Änderung Neuberechnung aufrufen
					$('.aw_editor').change(function () {
						aw_rechner();
					});
			
			// AW SPEICHERN	
			$("#save_aw").click( function() {
			 $.post( $("#aw_form").attr("action"),
			         $("#aw_form :input").serializeArray(),
			         function(info){ $("#result").append("<div id='aw_status'>"+info+"</div>");
			         $("#aw_status").fadeOut(2000, function(){
			        $("#aw_status").remove();
			    });
			   });
			clearInput();
			});
			 
			$("#aw_form").submit( function() {
			  return false;
			});
			
			function clearInput() {
			    $("#anzahl_aw").val(0);
			    $("#stundenlohn").val("0,00");
			    $("#preis_aw").val("0,00");
			    $("#aw_placeholder").load("mod_settings/aw_load.php");
			}
			
			// AW TABELLE LADEN
			$("#aw_placeholder").load("mod_settings/aw_load.php");
			
			// AW ZEILE LÖSCHEN
			$(".aw_delete").live('click', function(){
			var element = $(this);
			var del_id = element.attr("id");
			var info = 'id=' + del_id;
			if(confirm("Are you sure you want to delete this?"))
			{
			 $.ajax({
				   type: "POST",
				   url: "mod_settings/aw_delete.php",
				   data: info,
				   success: function(){}
			   });
			  $(this).parent().parent().animate({ backgroundColor: "#099" }, "fast")
			  .animate({ opacity: "hide" }, "fast");
			 }
			return false;
			});
		});
	</script>

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/settings.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;AW Verrechnungss&auml;tze:</h1>
			<hr noshade size="1" color="#333333">
		
			<table width="100%">
			
				<tr>
					<td width="50%" valign="top">
						
						<form method="post" action="mod_settings/aw_save.php" id="aw_form">
						
							<table border="0" cellpadding="0" cellspacing="0">
							    <tr class="aw_editor">
							        <td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold;">Anzahl AW:</td>
							        <td width="150">
							            <input type="text" name="anzahl_aw" id="anzahl_aw" value="0" style="margin: 5px; width:150px; border: 1px #009999 solid;" />
							        </td>
							    </tr>
							    <tr class="aw_editor">
							        <td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold;">Stundenlohn:</td>
							        <td width="150">
							            <input type="text" name="stundenlohn" id="stundenlohn" value="0,00" style="margin: 5px; width:150px; border: 1px #009999 solid;" />
							        </td>
							    </tr>
							    <tr class="aw_editor">
							        <td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold;">Preis AW:</td>
							        <td width="150">
							            <input type="text" name="preis_aw" id="preis_aw" value="0,00" style="margin: 5px; width:150px; border: 1px #009999 solid;" />
							        </td>
							    </tr>
							    <tr class="aw_editor">
							        <td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold;">Beschreibung:</td>
							        <td width="150">
							            <input type="text" name="beschreibung" id="beschreibung" style="margin: 5px; width:150px; border: 1px #009999 solid;" />
							        </td>
							    </tr>
							    <tr>
							    	<td width="150">
							    	<td width="150" align="center"><input type="submit" id="save_aw" value="AW speichern" style="margin: 5px; width:150px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;"></td>
							    </tr>
							    <tr>
							    	<td colspan="2" align="center"><span id="result" style="color: #099; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold;"></span></td>
							    </tr>
							</table>
							
						</form>
						
					</td>
					<td width="50%" valign="top">
					<span id="aw_placeholder">
						
					</span>
					</td>
				</tr>
			
			</table>
		
		</div>
<!----------------- END INCLUDES SETTING ----------------->