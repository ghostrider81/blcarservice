<!----------------- BEGINN INCLUDES SETTINGS ----------------->
	<script type="text/javascript">
		$(function() {
			
			// PART SPEICHERN	
			$("#save_artikel").click( function() {
			 $.post( $("#artikel_form").attr("action"),
			         $("#artikel_form :input").serializeArray(),
			          function(info){ $("#result").append("<div id='artikel_status'>"+info+"</div>");
			         $("#artikel_status").fadeOut(2000, function(){
			        $("#artikel_status").remove();
					});
			   });
			clearInput();
			});
			 
			$("#artikel_form").submit( function() {
			  return false;
			});
			
			function clearInput() {
			    $("#bezeichnung").val("");
			    $("#artikel_placeholder").load("mod_settings/part_load.php");
			}
			
			// PART TABELLE LADEN
			$("#artikel_placeholder").load("mod_settings/part_load.php");
			
			// PART ZEILE LÖSCHEN
			$(".part_delete").live('click', function(){
			var element = $(this);
			var del_id = element.attr("id");
			var info = 'id=' + del_id;
			if(confirm("Soll der Artikel wirklich gelöscht werden?"))
			{
			 $.ajax({
				   type: "POST",
				   url: "mod_settings/part_delete.php",
				   data: info,
				   success: function(){}
			   });
			  $(this).parent().parent().animate({ backgroundColor: "#099" }, "fast")
			  .animate({ opacity: "hide" }, "fast");
			 }
			return false;
			});
		});
	</script>

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/settings.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Teile / Positionen verwalten:</h1>
			<hr noshade size="1" color="#333333">
		
			<table width="100%">
			
				<tr>
					<td width="50%" valign="top">
						
						<form method="post" action="mod_settings/part_save.php" id="artikel_form">
						
							<table border="0" cellpadding="0" cellspacing="0">
							    <tr class="aw_editor">
							        <td width="150" style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold;">Artikelbezeichnung:</td>
							        <td width="150">
							            <input type="text" name="bezeichnung" id="bezeichnung" style="margin: 5px; width:150px; border: 1px #009999 solid;" />
							        </td>
							    </tr>
							    <tr>
							    	<td width="150">
							    	<td width="150" align="center"><input type="submit" id="save_artikel" value="Artikel speichern" style="margin: 5px; width:150px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;"></td>
							    </tr>
							    <tr>
							    	<td colspan="2" align="center"><span id="result" style="color: #099; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; font-weight: bold;"></span></td>
							    </tr>
							</table>
							
						</form>
						
					</td>
					<td width="50%" valign="top">
					<span id="artikel_placeholder">
						
					</span>
					</td>
				</tr>
			
			</table>
		
		</div>
<!----------------- END INCLUDES SETTING ----------------->