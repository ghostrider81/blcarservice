		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/dashboard.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Dashboard</h1>
			<hr noshade size="1" color="#333333">

            <table cellspacing="5" width="100%">
                <tr>
                    <td width="200" valign="top">
                    
                        <!-- CONTACTSEARCH WIDGET -->
			
				            <script lang="text/javascript">
					            $(document).ready(function()
					            {
							            $("#ContactSearch").autocomplete({
							                source: "mod_contact/contact_search.php",
							                minLength: 2,
							                select: function(event, ui) {
							                  window.location = ("static.php?active_module=contact&action=contact_detail&contact_id=" + ui.item.id)
							                }
							            })
							            .data("uiAutocomplete")._renderItem = function( ul, item ) {
							            return $( "<li></li>" )
							                .data( "item.autocomplete", item )
							                .append( "<a href='static.php?active_module=contact&action=contact_detail&contact_id="+ item.id + "'>"+ item.label + "</a>" )
							                .appendTo( ul );
							            };
					            });
				            </script>
				
				            <div style="background-color: #999999; border-radius: 10px; width: 180px; margin: 10px 0 0 0; padding: 10px;">
					            <h1><img src="img/contacts.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Kontaktsuche:</h1>
					            <input type="text" id="ContactSearch" name="ContactSearch" style="margin: 5px; border: 1px #009999 solid; width: 165px;" />	
				            </div>	

			            <!-- CONTACTSEARCH WIDGET -->

                    </td>
                    
                    <td valign="top">

                        <!-- FASTSTART WIDGET -->

				            <div style="background-color: #999999; border-radius: 10px; margin: 10px 0 0 0; padding: 10px;">
					            <h1><img src="img/billing.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Schnellstart:</h1>
                                <hr noshade size="1" color="#333333">
					            <ul style="margin-left: -25px;"">
                                    <li style="list-style-type: none;"><img src="img/commission.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" /><a href="static.php?active_module=commission&action=commission_add" style="vertical-align: 5px;">Neuen Auftrag erfassen</a></li>
                                    <li style="list-style-type: none;"><img src="img/billing.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" /><a href="static.php?active_module=billing&action=billing_add" style="vertical-align: 5px;">Neue Rechnung schreiben</a></li>
                                    <li style="list-style-type: none;"><img src="img/credit.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" /><a href="static.php?active_module=credit&action=credit_add" style="vertical-align: 5px;">Neue Gutschrift schreiben</a></li>
                                </ul>	
				            </div>	                

                        <!-- FASTSTART WIDGET --> 
                                          
                    </td>
                </tr>
            </table>
		</div>