<!----------------- INCLUDES COMMISSION MAIN ----------------->

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/commission.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Aufträge</h1>
			<hr noshade size="1" color="#333333">


			<?php
				// KUNDENDATEN HOLEN
				
				$commission_id = $_GET['commission_id'];
				
				$query_commission = "SELECT aw_1, aw_2, aw_3, aw_4, car_id, commission_nr, contact_id, operator, notices, date_acception, date_target FROM auftrag WHERE commission_id =".$commission_id;
				$result_commission = mysql_query($query_commission);
				$row_commission = mysql_fetch_array($result_commission);
				
				$contact_id = $row_commission['contact_id'];
				$commission_nr = $row_commission['commission_nr'];
				$operator = $row_commission['operator'];
				$notices = $row_commission['notices'];
				$date_acception = make_date($row_commission['date_acception']);
				$date_target = make_date($row_commission['date_target']);
				
				$query_kunde = "SELECT kdnnr, anrede, titel, strasse, plz, ort, name, vorname FROM kontakte WHERE contact_id =".$contact_id;
				$result_kunde = mysql_query($query_kunde);
				$row_kunde = mysql_fetch_array($result_kunde);
				
				$name = $row_kunde['name'];
				$vorname = $row_kunde['vorname'];
				$kdnnr = $row_kunde['kdnnr'];
				$anrede = $row_kunde['anrede'];
				$titel = $row_kunde['titel'];
				$strasse = $row_kunde['strasse'];
				$plz = $row_kunde['plz'];
				$ort = $row_kunde['ort'];
				
			?>
				
			<form action="static.php?active_module=commission&action=commission_save" method="post">
			<input type="hidden" name="commission_id" value="<?php print $commission_id; ?>" />
			<table border="0" cellpadding="0" cellpadding="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;">Anschrift Kunde:</td>
					<td width="50%"></td>
				</tr>
				<tr>
					<td rowspan="3" width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><?php print "<b><p>".$name.", ".$vorname."</p></b><p>".$strasse."</p><p>".$plz." ".$ort."</p>"; ?></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Kunden-Nr.:</strong></label>&nbsp;<input type="text" value="<?php print $kdnnr; ?>" style="border: 1px #009999 solid; width: 145px;" readonly /><input type="hidden" name="contact_id" value="<?php print $contact_id; ?>" /></td>
				</tr>								
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Status:</strong></label>&nbsp;<select name="status" style="border: 1px #009999 solid; width: 145px;"><option value"0">Entwurf</option><option value="1">Offen</option><option value="2">In Bearbeitung</option><option value="3">Geschlossen</option></select></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Auftrags-Nr.:</strong></label>&nbsp;<input type="text" name="commission_nr" style="border: 1px #009999 solid; width: 145px;" value="<?php print $commission_nr; ?>" readonly /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Annahme-Datum:</strong></label>&nbsp;<input type="text" name="date_acception" style="border: 1px #009999 solid; width: 145px;" value="<?php echo $date_acception; ?>" readonly /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><strong>Notizen:</strong></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Angenommen von:</strong></label>&nbsp;<input type="text" name="operator" style="border: 1px #009999 solid; width: 145px;" value="<?php print $operator; ?>" readonly /></td>
				</tr>
				<tr>
					<td rowspan="2" width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><textarea name="notices" style="width:90%; height:100%; border:1px solid #009999;"><?php print $notices; ?></textarea></td>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>Termin:</strong></label>&nbsp;<input type="text" name="date_target" style="border: 1px #009999 solid; width: 145px;" id="commission_date" value="<?php echo $date_target; ?>" /></td>
				</tr>
				<tr>
					<td width="50%" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; padding: 5px;"><label><strong>KM-Stand bei Annahme:</strong></label>&nbsp;<input type="text" name="km_acception" style="border: 1px #009999 solid; width: 145px;" /></td>
				</tr>			
				<tr>
					<td colspan="2" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"><hr noshade size="1" color="#009999" /></td>
				</tr>
				<tr>
					<td colspan="2">
					
						<!-- ANFANG PAUSCHALE -->

								<?php
											
											$query_cat0 = "SELECT 
															a.cat_id,
															a.artikel_id,
															a.anzahl,
															a.preis,
															b.bezeichnung
															FROM 
															positionen_rechnung a,
															positionen b
															WHERE 
															a.billing_id  = ".$billing_id." AND 
															a.cat_id = 0 AND 
															a.artikel_id = b.artikel_id";
											$result_cat0 = mysql_query($query_cat0);
											$numrows_cat0 = mysql_num_rows($result_cat0);
											
											if($numrows_cat0 == 0)
												{ ?>

													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Pauschale Arbeitslohn Positionen:</td>
																<td width="50%" align="right">
																	<select id="option_pauschale">
																		<option value="0">Pauschale Positionen hinzuf&uuml;gen?</option>
																		<option value="0">Inaktiv</option>
																		<option value="1">Aktiv</option>									
																	</select>&nbsp;<span id="option_pauschale_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>													

													<div id="layer_pauschale">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_pauschale">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Pauschale Arbeitslohn Positionen:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_pauschale" name="total_netto_pauschale" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_pauschale_1" name="row_pauschale[1][desc]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_pauschale_1" name="row_pauschale[1][quant]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="preis_pauschale_1" name="row_pauschale[1][preis]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_pauschale_1" name="total_pauschale[1]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_pauschale_1" name="row_pauschale[1][art_id]" value="" /><input type="button" class="add_row_pauschale" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
																</tr>
													
												<?php } 
												
												else
													
													{?>
													
													<script lang="text/javascript">
														$(document).ready(function()
														{
															$('#layer_pauschale').slideDown();
														});
													</script>	
													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Pauschale Arbeitslohn Positionen:</td>
																<td width="50%" align="right">
																	<select id="option_pauschale" disabled>
																		<option value="0">Pauschale Positionen hinzuf&uuml;gen?</option>
																		<option value="0">Inaktiv</option>
																		<option value="1" selected>Aktiv</option>									
																	</select>&nbsp;<span id="option_pauschale_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>
													
													<div id="layer_pauschale">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_pauschale">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Pauschale Arbeitslohn Positionen:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_pauschale" name="total_netto_pauschale" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>												
												
												<?php	$cat0_id = 1;
												
													while($row_cat0 = mysql_fetch_array($result_cat0))
														{
															
															?>
															
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;"><?php print $cat0_id; ?></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_pauschale_x<?php print $cat0_id; ?>" name="row_pauschale[x<?php print $cat0_id; ?>][desc]" value="<?php print $row_cat0['bezeichnung']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_pauschale_x<?php print $cat0_id; ?>" name="row_pauschale[x<?php print $cat0_id; ?>][quant]" value="<?php print $row_cat0['anzahl']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="preis_pauschale_x<?php print $cat0_id; ?>" name="row_pauschale[x<?php print $cat0_id; ?>][preis]" value="<?php print number_format($row_cat0['preis'], 2, ',', ''); ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_pauschale_x<?php print $cat0_id; ?>" name="total_pauschale[x<?php print $cat0_id; ?>]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_pauschale_x<?php print $cat0_id; ?>" name="row_pauschale[x<?php print $cat0_id; ?>][art_id]" value="<?php print $row_cat0['artikel_id']; ?>" /><input type="button" class="<?php if($numrows_cat0 == $cat0_id){print"add_row_pauschale";} else {print"del";} ?>" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="<?php if($numrows_cat0 == $cat0_id){print"Hinzuf&uuml;gen";} else {print"L&ouml;schen";} ?>" /></td>
																</tr>
															
															<?php
															
															$cat0_id++;
														}
												}	
											
											
										?>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE PAUSCHALE -->
					
						<!-- ANFANG ARBEITSLOHN MECHANIK -->

								<?php
											
											$query_cat1 = "SELECT 
															a.cat_id,
															a.artikel_id,
															a.anzahl,
															a.preis,
															b.bezeichnung
															FROM 
															positionen_auftrag a,
															positionen b
															WHERE 
															a.commission_id  = ".$commission_id." AND 
															a.cat_id = 1 AND 
															a.artikel_id = b.artikel_id";
											$result_cat1 = mysql_query($query_cat1);
											$numrows_cat1 = mysql_num_rows($result_cat1);
											
											if($numrows_cat1 == 0)
												{ ?>

													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Mechanik:</td>
																<td width="50%" align="right">
																	<select id="aw_mechanik">
																		<option value="">AW Satz ausw&auml;hlen:</option>
																		<?php getAW(); ?>
																	</select>&nbsp;<span id="aw_mechanik_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>													

													<div id="layer_mechanik">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_mechanik">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Mechanik:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_mechanik" name="total_netto_mechanik" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_mechanik_1" name="row_mechanik[1][desc]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_mechanik_1" name="row_mechanik[1][quant]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="aw_mechanik_1" name="row_mechanik[1][aw]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_mechanik_1" name="total_mechanik[1]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_mechanik_1" name="row_mechanik[1][art_id]" value="" /><input type="button" class="add_row_mechanik" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
																</tr>
													
												<?php } 
												
												else
													
													{?>
													
													<script lang="text/javascript">
														$(document).ready(function()
														{
															$('#layer_mechanik').slideDown();
														});
													</script>	
													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Mechanik:</td>
																<td width="50%" align="right">
																	<select id="aw_mechanik" name="aw_mechanik" disabled>
																		<option value="<?php print $row_commission['aw_1']; ?>"><?php print number_format($row_commission['aw_1'], 2, ',', ''); ?> €</option>
																	</select>&nbsp;<span id="aw_mechanik_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>
													
													<div id="layer_mechanik">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_mechanik">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Mechanik:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_mechanik" name="total_netto_mechanik" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>												
												
												<?php	$cat1_id = 1;
												
													while($row_cat1 = mysql_fetch_array($result_cat1))
														{
															
															?>
															
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;"><?php print $cat1_id; ?></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_mechanik_x<?php print $cat1_id; ?>" name="row_mechanik[x<?php print $cat1_id; ?>][desc]" value="<?php print $row_cat1['bezeichnung']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_mechanik_x<?php print $cat1_id; ?>" name="row_mechanik[x<?php print $cat1_id; ?>][quant]" value="<?php print $row_cat1['anzahl']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="aw_mechanik_x<?php print $cat1_id; ?>" name="row_mechanik[x<?php print $cat1_id; ?>][aw]" value="<?php print number_format($row_cat1['preis'], 2, ',', ''); ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_mechanik_x<?php print $cat1_id; ?>" name="total_mechanik[x<?php print $cat1_id; ?>]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_mechanik_x<?php print $cat1_id; ?>" name="row_mechanik[x<?php print $cat1_id; ?>][art_id]" value="<?php print $row_cat1['artikel_id']; ?>" /><input type="button" class="<?php if($numrows_cat1 == $cat1_id){print"add_row_mechanik";} else {print"del";} ?>" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="<?php if($numrows_cat1 == $cat1_id){print"Hinzuf&uuml;gen";} else {print"L&ouml;schen";} ?>" /></td>
																</tr>
															
															<?php
															
															$cat1_id++;
														}
												}	
											
											
										?>
										</tbody>
									</table>
								</div>
							
						<!-- ENDE ARBEITSLOHN MECHANIK -->
						

							
						<!-- ANFANG ARBEITSLOHN KAROSSERIE -->

								<?php
											
											$query_cat2 = "SELECT 
															a.cat_id,
															a.artikel_id,
															a.anzahl,
															a.preis,
															b.bezeichnung
															FROM 
															positionen_auftrag a,
															positionen b
															WHERE 
															a.commission_id  = ".$commission_id." AND 
															a.cat_id = 2 AND 
															a.artikel_id = b.artikel_id";
											$result_cat2 = mysql_query($query_cat2);
											$numrows_cat2 = mysql_num_rows($result_cat2);
											
											if($numrows_cat2 == 0)
												{ ?>

													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Karosserie:</td>
																<td width="50%" align="right">
																	<select id="aw_karosserie">
																		<option value="">AW Satz ausw&auml;hlen:</option>
																		<?php getAW(); ?>
																	</select>&nbsp;<span id="aw_karosserie_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>													

													<div id="layer_karosserie">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_karosserie">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Karosserie:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_karosserie" name="total_netto_karosserie" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>	
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_karosserie_1" name="row_karosserie[1][desc]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_karosserie_1" name="row_karosserie[1][quant]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="aw_karosserie_1" name="row_karosserie[1][aw]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_karosserie_1" name="total_karosserie[1]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_karosserie_1" name="row_karosserie[1][art_id]" value="" /><input type="button" class="add_row_karosserie" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
																</tr>
													
												<?php } 
												
												else
													
													{?>
													
													<script lang="text/javascript">
														$(document).ready(function()
														{
															$('#layer_karosserie').slideDown();
														});
													</script>	
													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Karosserie:</td>
																<td width="50%" align="right">
																	<select id="aw_karosserie" name="aw_karosserie" disabled>
																		<option value="<?php print $row_commission['aw_2']; ?>"><?php print number_format($row_commission['aw_2'], 2, ',', ''); ?> €</option>
																	</select>&nbsp;<span id="aw_karosserie_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>
													
													<div id="layer_karosserie">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_karosserie">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Karosserie:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_karosserie" name="total_netto_karosserie" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>												
												
												<?php	$cat2_id = 1;
												
													while($row_cat2 = mysql_fetch_array($result_cat2))
														{
															
															?>
															
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;"><?php print $cat2_id; ?></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_karosserie_x<?php print $cat2_id; ?>" name="row_karosserie[x<?php print $cat2_id; ?>][desc]" value="<?php print $row_cat2['bezeichnung']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_karosserie_x<?php print $cat2_id; ?>" name="row_karosserie[x<?php print $cat2_id; ?>][quant]" value="<?php print $row_cat2['anzahl']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="aw_karosserie_x<?php print $cat2_id; ?>" name="row_karosserie[x<?php print $cat2_id; ?>][aw]" value="<?php print number_format($row_cat2['preis'], 2, ',', ''); ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_karosserie_x<?php print $cat2_id; ?>" name="total_karosserie[x<?php print $cat2_id; ?>]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_karosserie_x<?php print $cat2_id; ?>" name="row_karosserie[x<?php print $cat2_id; ?>][art_id]" value="<?php print $row_cat2['artikel_id']; ?>" /><input type="button" class="<?php if($numrows_cat2 == $cat2_id){print"add_row_karosserie";} else {print"del";} ?>" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="<?php if($numrows_cat2 == $cat2_id){print"Hinzuf&uuml;gen";} else {print"L&ouml;schen";} ?>" /></td>
																</tr>
															
															<?php
															
															$cat2_id++;
														}
												}	
											
											
										?>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ARBEITSLOHN KAROSSERIE -->
						

							
						<!-- ANFANG ARBEITSLOHN LACK -->

								<?php
											
											$query_cat3 = "SELECT 
															a.cat_id,
															a.artikel_id,
															a.anzahl,
															a.preis,
															b.bezeichnung
															FROM 
															positionen_auftrag a,
															positionen b
															WHERE 
															a.commission_id  = ".$commission_id." AND 
															a.cat_id = 3 AND 
															a.artikel_id = b.artikel_id";
											$result_cat3 = mysql_query($query_cat3);
											$numrows_cat3 = mysql_num_rows($result_cat3);
											
											if($numrows_cat3 == 0)
												{ ?>

													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Lack:</td>
																<td width="50%" align="right">
																	<select id="aw_lack">
																		<option value="">AW Satz ausw&auml;hlen:</option>
																		<?php getAW(); ?>
																	</select>&nbsp;<span id="aw_lack_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>													

													<div id="layer_lack">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_lack">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Lack:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_lack" name="total_netto_lack" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>	
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_lack_1" name="row_lack[1][desc]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_lack_1" name="row_lack[1][quant]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="aw_lack_1" name="row_lack[1][aw]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_lack_1" name="total_lack[1]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_lack_1" name="row_lack[1][art_id]" value="" /><input type="button" class="add_row_lack" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
																</tr>
													
												<?php } 
												
												else
													
													{?>
													
													<script lang="text/javascript">
														$(document).ready(function()
														{
															$('#layer_lack').slideDown();
														});
													</script>	
													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Lack:</td>
																<td width="50%" align="right">
																	<select id="aw_lack" name="aw_lack" disabled>
																		<option value="<?php print $row_commission['aw_3']; ?>"><?php print number_format($row_commission['aw_3'], 2, ',', ''); ?> €</option>
																	</select>&nbsp;<span id="aw_lack_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>
													
													<div id="layer_lack">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_lack">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Lack:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_lack" name="total_netto_lack" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>												
												
												<?php	$cat3_id = 1;
												
													while($row_cat3 = mysql_fetch_array($result_cat3))
														{
															
															?>
															
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;"><?php print $cat3_id; ?></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_lack_x<?php print $cat3_id; ?>" name="row_lack[x<?php print $cat3_id; ?>][desc]" value="<?php print $row_cat3['bezeichnung']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_lack_x<?php print $cat3_id; ?>" name="row_lack[x<?php print $cat3_id; ?>][quant]" value="<?php print $row_cat3['anzahl']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="aw_lack_x<?php print $cat3_id; ?>" name="row_lack[x<?php print $cat3_id; ?>][aw]" value="<?php print number_format($row_cat3['preis'], 2, ',', ''); ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_lack_x<?php print $cat3_id; ?>" name="total_lack[x<?php print $cat3_id; ?>]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_lack_x<?php print $cat3_id; ?>" name="row_lack[x<?php print $cat3_id; ?>][art_id]" value="<?php print $row_cat3['artikel_id']; ?>" /><input type="button" class="<?php if($numrows_cat3 == $cat3_id){print"add_row_lack";} else {print"del";} ?>" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="<?php if($numrows_cat3 == $cat3_id){print"Hinzuf&uuml;gen";} else {print"L&ouml;schen";} ?>" /></td>
																</tr>
															
															<?php
															
															$cat3_id++;
														}
												}	
											
											
										?>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ARBEITSLOHN LACK -->
						
					
						
						<!-- ANFANG ARBEITSLOHN ELEKTRIK -->

								<?php
											
											$query_cat4 = "SELECT 
															a.cat_id,
															a.artikel_id,
															a.anzahl,
															a.preis,
															b.bezeichnung
															FROM 
															positionen_auftrag a,
															positionen b
															WHERE 
															a.commission_id  = ".$commission_id." AND 
															a.cat_id = 4 AND 
															a.artikel_id = b.artikel_id";
											$result_cat4 = mysql_query($query_cat4);
											$numrows_cat4 = mysql_num_rows($result_cat4);
											
											if($numrows_cat4 == 0)
												{ ?>

													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Elektrik:</td>
																<td width="50%" align="right">
																	<select id="aw_elektrik">
																		<option value="">AW Satz ausw&auml;hlen:</option>
																		<?php getAW(); ?>
																	</select>&nbsp;<span id="aw_elektrik_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>													

													<div id="layer_elektrik">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_elektrik">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Elektrik:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_elektrik" name="total_netto_elektrik" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_elektrik_1" name="row_elektrik[1][desc]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_elektrik_1" name="row_elektrik[1][quant]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="aw_elektrik_1" name="row_elektrik[1][aw]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_elektrik_1" name="total_elektrik[1]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_elektrik_1" name="row_elektrik[1][art_id]" value="" /><input type="button" class="add_row_elektrik" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
																</tr>
													
												<?php } 
												
												else
													
													{?>
													
													<script lang="text/javascript">
														$(document).ready(function()
														{
															$('#layer_elektrik').slideDown();
														});
													</script>	
													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Arbeitslohn Elektrik:</td>
																<td width="50%" align="right">
																	<select id="aw_elektrik" name="aw_elektrik" disabled>
																		<option value="<?php print $row_commission['aw_4']; ?>"><?php print number_format($row_commission['aw_4'], 2, ',', ''); ?> €</option>
																	</select>&nbsp;<span id="aw_elektrik_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>
													
													<div id="layer_elektrik">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_elektrik">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">E-Preis AW:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Elektrik:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_elektrik" name="total_netto_elektrik" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>												
												
												<?php	$cat4_id = 1;
												
													while($row_cat4 = mysql_fetch_array($result_cat4))
														{
															
															?>
															
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;"><?php print $cat4_id; ?></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_elektrik_x<?php print $cat4_id; ?>" name="row_elektrik[x<?php print $cat4_id; ?>][desc]" value="<?php print $row_cat4['bezeichnung']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_elektrik_x<?php print $cat4_id; ?>" name="row_elektrik[x<?php print $cat4_id; ?>][quant]" value="<?php print $row_cat4['anzahl']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="aw_elektrik_x<?php print $cat4_id; ?>" name="row_elektrik[x<?php print $cat4_id; ?>][aw]" value="<?php print number_format($row_cat4['preis'], 2, ',', ''); ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_elektrik_x<?php print $cat4_id; ?>" name="total_elektrik[x<?php print $cat4_id; ?>]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_elektrik_x<?php print $cat4_id; ?>" name="row_elektrik[x<?php print $cat4_id; ?>][art_id]" value="<?php print $row_cat4['artikel_id']; ?>" /><input type="button" class="<?php if($numrows_cat4 == $cat4_id){print"add_row_elektrik";} else {print"del";} ?>" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="<?php if($numrows_cat4 == $cat4_id){print"Hinzuf&uuml;gen";} else {print"L&ouml;schen";} ?>" /></td>
																</tr>
															
															<?php
															
															$cat4_id++;
														}
												}	
											
											
										?>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ARBEITSLOHN ELEKTRIK -->
						


						<!-- ANFANG ERSATZTEILE -->

								<?php
											
											$query_cat5 = "SELECT 
															a.cat_id,
															a.artikel_id,
															a.anzahl,
															a.preis,
															b.bezeichnung
															FROM 
															positionen_auftrag a,
															positionen b
															WHERE 
															a.commission_id  = ".$commission_id." AND 
															a.cat_id = 5 AND 
															a.artikel_id = b.artikel_id";
											$result_cat5 = mysql_query($query_cat5);
											$numrows_cat5 = mysql_num_rows($result_cat5);
											
											if($numrows_cat5 == 0)
												{ ?>

													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Material:</td>
																<td width="50%" align="right">
																	<select id="option_ersatzteile">
																		<option value="0">Material hinzuf&uuml;gen?</option>
																		<option value="0">Inaktiv</option>
																		<option value="1">Aktiv</option>									
																	</select>&nbsp;<span id="option_ersatzteile_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>													

													<div id="layer_ersatzteile">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_ersatzteile">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Material:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_ersatzteile" name="total_netto_ersatzteile" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;">1</td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_ersatzteile_1" name="row_ersatzteile[1][desc]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_ersatzteile_1" name="row_ersatzteile[1][quant]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" value="0,00" id="preis_ersatzteile_1" name="row_ersatzteile[1][preis]" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_ersatzteile_1" name="total_ersatzteile[1]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_ersatzteile_1" name="row_ersatzteile[1][art_id]" value="" /><input type="button" class="add_row_ersatzteile" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="Hinzuf&uuml;gen" /></td>
																</tr>
													
												<?php } 
												
												else
													
													{?>
													
													<script lang="text/javascript">
														$(document).ready(function()
														{
															$('#layer_ersatzteile').slideDown();
														});
													</script>	
													<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
														<table width="100%" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">Material:</td>
																<td width="50%" align="right">
																	<select id="option_ersatzteile" disabled>
																		<option value="0">Material hinzuf&uuml;gen?</option>
																		<option value="0">Inaktiv</option>
																		<option value="1" selected>Aktiv</option>									
																	</select>&nbsp;<span id="option_ersatzteile_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
																</td>
															</tr>
														</table>
													</div>
													
													<div id="layer_ersatzteile">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_ersatzteile">
															<thead>
																<tr style="height:30px; border-bottom: 1px #FFF solid;">
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Nr.:</th>
																	<th style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Bezeichnung:</th>
																	<th align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Menge:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Stk-Preis:</th>
																	<th align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Gesamtpreis:</th>
																	<th align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid;">Aktion:</th>
																</tr>
															</thead>
															<tfoot style="border-bottom: #FFF;">
																<tr>
																	<td colspan="4" align="right" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold;"><span style="margin-right: 5px;">Summe Material:</span></td>
																	<td align="center" style="border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_netto_ersatzteile" name="total_netto_ersatzteile" readonly /></td>
																	<td>&nbsp;</td>
																</tr>
															</tfoot>
															<tbody>												
												
												<?php	$cat5_id = 1;
												
													while($row_cat5 = mysql_fetch_array($result_cat5))
														{
															
															?>
															
																<tr style="border-bottom: 1px #FFF solid;">
																	<td align="center" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px #FFF solid;"><?php print $cat5_id; ?></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 95%;" id="desc_ersatzteile_x<?php print $cat5_id; ?>" name="row_ersatzteile[x<?php print $cat5_id; ?>][desc]" value="<?php print $row_cat5['bezeichnung']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 40px; text-align: center;" id="quant_ersatzteile_x<?php print $cat5_id; ?>" name="row_ersatzteile[1][quant]" value="<?php print $row_cat5['anzahl']; ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="preis_ersatzteile_x<?php print $cat5_id; ?>" name="row_ersatzteile[1][preis]" value="<?php print number_format($row_cat5['preis'], 2, ',', ''); ?>" /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 5px 0 5px 0; width: 135px; text-align: center;" id="total_ersatzteile_x<?php print $cat5_id; ?>" name="total_ersatzteile[x<?php print $cat5_id; ?>]" readonly /></td>
																	<td align="center" style="border-bottom: 1px #FFF solid;"><input type="hidden" id="art_ersatzteile_x<?php print $cat5_id; ?>" name="row_ersatzteile[x<?php print $cat5_id; ?>][art_id]" value="<?php print $row_cat5['artikel_id']; ?>" /><input type="button" class="<?php if($numrows_cat5 == $cat5_id){print"add_row_ersatzteile";} else {print"del";} ?>" style='width:100px; background-color:#009999; color:#FFFFFF; border: 1px solid #fff;' value="<?php if($numrows_cat5 == $cat5_id){print"Hinzuf&uuml;gen";} else {print"L&ouml;schen";} ?>" /></td>
																</tr>
															
															<?php
															
															$cat5_id++;
														}
												}	
											
											
										?>
										</tbody>
									</table>
								</div>
								
						<!-- ENDE ERSATZTEILE -->
						
						<!-- ANFANG TÜV -->

								<?php
									if($row_billing['tuv'] == 0)
										{ ?>
											
											<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
												<table width="100%" cellpadding="0" cellspacing="0" border="0">
													<tr>
														<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">T&Uuml;V:</td>
														<td width="50%" align="right">
															<select id="option_tuv">
																<option value="0">T&Uuml;V hinzuf&uuml;gen?</option>
																<option value="0">Inaktiv</option>
																<option value="1">Aktiv</option>									
															</select>&nbsp;<span id="option_tuv_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
														</td>
													</tr>
												</table>
											</div>
										
											<div id="layer_tuv">
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_tuv">
													<tbody>
														<tr style="height:30px; border-bottom: 1px #FFF solid;">
															<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
															<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
															<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
															<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; text-align: right;">Geb&uuml;hr f&uuml;r T&Uuml;V / AU:</td>
															<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_tuv_1" name="total_tuv_1" /></td>
															<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
														</tr>
													</tbody>
												</table>
											</div>											
											
										<?php }
										
									else
										{ ?>
											
											<script lang="text/javascript">
												$(document).ready(function()
													{
														$('#layer_tuv').slideDown();
													});
											</script>	

											<div style="background-color: #333; margin: 0; padding: 5px; border-bottom: 1px #FFF solid;">
												<table width="100%" cellpadding="0" cellspacing="0" border="0">
													<tr>
														<td width="50%" style="color: #fff; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bolder;">T&Uuml;V:</td>
														<td width="50%" align="right">
															<select id="option_tuv" disabled>
																<option value="0">T&Uuml;V hinzuf&uuml;gen?</option>
																<option value="0">Inaktiv</option>
																<option value="1" selected>Aktiv</option>									
															</select>&nbsp;<span id="option_tuv_save" style="color: #fff; font-family: Trebuchet MS; font-size:10px;"><a href="javascript:return false;" style="color: #FFF;">Speichern</a></span>
														</td>
													</tr>
												</table>
											</div>
										
											<div id="layer_tuv">
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table_tuv">
													<tbody>
														<tr style="height:30px; border-bottom: 1px #FFF solid;">
															<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
															<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
															<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
															<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; text-align: right;">Geb&uuml;hr f&uuml;r T&Uuml;V / AU:</td>
															<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_tuv_1" name="total_tuv_1" value="<?php print $row_billing['tuv']; ?>" /></td>
															<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px;"></td>
														</tr>
													</tbody>
												</table>
											</div>		
											
										<?php }
								?>
								
						<!-- ENDE TÜV -->

						<!-- ANFANG ZUSAMMENFASSUNG -->
							
								<div>
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid; text-align: right;">Summe der Positionen:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-top: 15px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_netto" name="total_netto" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-top: 15px #FFF solid;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">MwSt:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_mwst" name="total_mwst" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">T&Uuml;V / AU:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-bottom: 1px #FFF solid; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_tuv" name="total_tuv" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>										
										<tr style="height:30px; border-bottom: 1px #FFF solid;">
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="50" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
											<td align="center" width="145" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; text-align: right;">Gesamtsumme:</td>
											<td align="center" width="143" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999; border-left: 1px #FFF solid; border-right: 1px #FFF solid;"><input type="text" style="border:0; height: 20px; margin: 0; width: 135px; text-align: center;" id="total_brutto" name="total_brutto" readonly /></td>
											<td align="center" width="100" style="color: #333; font-size: 12px; font-family: Trebuchet MS; text-decoration: none; font-weight: bold; padding: 5px; background-color: #999;"></td>
										</tr>
									</table>
								</div>
								
						<!-- ENDE ZUSAMMENFASSUNG -->
						
					</td>
				</tr>			
			</table>	
			
			<input type="submit" />
			
			</form>
				
		</div>
	
<!----------------- END INCLUDES COMMISSION MAIN ----------------->