	<script type="text/javascript">
		$(function() {
			
			// PART ZEILE LÖSCHEN
			$(".commission_delete").live('click', function(){
			var element = $(this);
			var del_id = element.attr("id");
			var info = 'id=' + del_id;
			if(confirm("Soll der Auftrag wirklich gelöscht werden?"))
			{
			 $.ajax({
				   type: "POST",
				   url: "mod_commission/commission_delete.php",
				   data: info,
				   success: function(){}
			   });
			  $(this).parent().parent().animate({ backgroundColor: "#099" }, "slow")
			  .animate({ opacity: "hide" }, "fast");
			 }
			return false;
			});
		});
	</script>

			<table border="0" id="commissionlist" cellpadding="0" cellspacing="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; text-align: center;" width="50">Auftrag-Nr.:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;" width="50">Kunden-Nr.:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;" width="200">Name:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;" width="100">Netto:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;" width="100">Termin:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;" width="">Ersteller:</th>
					<th style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px; border-bottom: 1px solid black; border-left: 1px solid black; text-align: center;" width="">Optionen:</th>
				</tr>

<?php

	include "../config/config.inc";
	include "../includes/database_connection.php";
	include "../php/math_functions.php";
	
				

				$status = $_GET['status'];
				$query = "SELECT commission_id, contact_id, car_id, commission_nr, date_acception, operator, date_target FROM auftrag WHERE status =".$status." ORDER BY commission_id DESC";
													
				// Mysql Abfrage wird durchgeführt
				$result = mysql_query($query);
				$num_rows = mysql_num_rows($result);				
				
				while($row = mysql_fetch_array($result))
														
					{
					
						$query2 = "SELECT name, vorname, kdnnr FROM kontakte WHERE contact_id =".$row['contact_id'];						
						$result2 = mysql_query($query2);
						$row2 = mysql_fetch_array($result2);
						
						$name = $row2['name'];
						$vorname = $row2['vorname'];
						$kdnr = $row2['kdnr'];
						$annahmedatum = make_date($row['date_acception']);
						$termin = make_date($row['date_target']);
						
						$summe_netto = calc_sum($row['commission_id'],"auftrag");
																							    																			    	
				print "<tr>";
				print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; text-align: center;\">".$row['commission_nr']."</td>";
				print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: center;\">".$row2['kdnnr']."</td>";
				print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: left;\">".$name.", ".$vorname."</td>";
				print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: right;\">".$summe_netto."</td>";
				print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black; text-align: center;\">".$termin."</td>";
				print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black;\">".$row['operator']."</td>";
				print "<td style=\"color: #333; font-size: 12px; font-family: verdana; text-decoration: none; padding: 5px; border-top: 1px solid black; border-left: 1px solid black;\"><a href=\"mod_commission/commission_export.php?PDF=OUT&commission_id=".$row['commission_id']."\" target=\"_blank\" title=\"PDF VON DIESEM AUFTRAG ERZEUGEN\"><img src=\"img/icon_pdf.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"mod_commission/commission_delete.php\" target=\"_blank\" title=\"DIESEN AUFTRAG L&Ouml;SCHEN\" class=\"commission_delete\" id=\"".$row['commission_id']."\"><img src=\"img/icon_delete.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=commission&action=commission_edit&commission_id=".$row['commission_id']."\" title=\"DIESEN AUFTRAG BEARBEITEN\"><img src=\"img/icon_edit.png\" width=\"16\" height=\"16\" border=\"0\" /></a>&nbsp;&nbsp;&nbsp;<a href=\"static.php?active_module=billing&action=billing_convert&commission_id=".$row['commission_id']."&commission_nr=".$row['commission_nr']."\" title=\"RECHNUNG VON DIESEN AUFTRAG ERSTELLEN\"><img src=\"img/icon_convert.png\" width=\"16\" height=\"16\" border=\"0\" /></a></td>";
				print "</tr>";
													
					}

?>
			</table>