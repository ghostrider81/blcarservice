<!----------------- INCLUDES COMMISSION MAIN ----------------->

		<div style="background-color: #CCCCCC; border-radius: 10px; margin: 0; padding: 10px;">
			<h1><img src="img/commission.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />&nbsp;Neuen Auftrag erstellen</h1>
			<hr noshade size="1" color="#333333">

			<?php
			
				if (isset($_POST['action']) && $_POST['action'] == "commission_from_offer")
				
			{ ?>
			
			
			<!-- BEGINN DER AUFTRAGSERSTELLUNG MIT ANGEBOT_ID -->
			
					<?php include("includes/commission_with_id.php"); ?>
			
			<!-- ENDE DER AUFTRAGSERSTELLUNG MIT ANGEBOT_ID -->
				
			<?php } 
						
				elseif (isset($_GET['contact_id']) && $_GET['contact_id'] != "")
					
			{ ?>
			
			<!-- BEGINN DER BLANKO AUFTRAGSERSTELLUNG -->
			
					<?php include("includes/commission_without_id.php"); ?>
				
			<!-- ENDE DER BLANKO AUFTRAGSERSTELLUNG -->
					
					<?php } 
					
					else {
					
					?>
			
			<!-- BEGINN DER AUFTRAGSERSTELLUNG MIT ANGEBOTSAUSWAHL -->
			
			<script lang="text/javascript">
				$(document).ready(function()
				{
						$("#ContactSearch").autocomplete({
						    source: "mod_contact/contact_search.php",
						    minLength: 2,
						    select: function(event, ui) {
						      window.location = ("static.php?active_module=commission&action=commission_add&contact_id=" + ui.item.id)
						      //$('#ContactSearch').val(ui.item.id);
						      //return false;
						    }
						})
						.data("uiAutocomplete")._renderItem = function( ul, item ) {
						return $( "<li></li>" )
						    .data( "item.autocomplete", item )
						    .append( "<a href='static.php?active_module=commission&action=commission_add&contact_id="+ item.id + "'>"+ item.label + "</a>" )
						    .appendTo( ul );
						};
				});
			</script>		

			<table border="0" cellpadding="0" cellpadding="0" width="100%" style="border-radius: 10px; border: 1px solid black;">
				<tr>
					<td style="color: #333; font-size: 12px; font-family: verdana; text-decoration: none; font-weight: bold; padding: 5px;">
						<form action="static.php?active_module=commission&action=commission_add" method="post">
							<input type="hidden" name="action" value="commission_blank" />
							Neuen Werkstattauftrag für Kunden erstellen:&nbsp;<input type="text" name="ContactSearch" id="ContactSearch" style="margin: 5px; border: 1px #009999 solid; width: 145px;" />&nbsp;<input type="submit" value="Weiter" />
						</form>	
					</td>
				</tr>			
			</table>					
			
			
			<!-- ENDE DER AUFTRAGSERSTELLUNG MIT ANGEBOTSAUSWAHL -->
				
					<?php
						}
					?>
			
		</div>
<!----------------- END INCLUDES COMMISSION MAIN ----------------->