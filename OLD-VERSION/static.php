<?php
//error_reporting(E_ALL);

	include "auth.php";

	$active_module = $_GET['active_module'];
	$active_directory = "mod_".$active_module;
	$action = $_GET['action'];

	if (!$_GET['order'])
		{
			$order = "name";
		}
		
	else
		{
			$order = $_GET['order'];
		}

	include "config/config.inc";
	include "includes/database_connection.php";
	include "php/functions.php";
	
	$timestamp = time();
	$date1 = date("d.m.Y - H:i", $timestamp);
	$date2 = date("d.m.Y", $timestamp);										

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>B&L Carservice</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<link href="css/styles.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet">

<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
</head>

<body>

<div id="wrap">
	<div id="header">
		<img class="logo" src="img/logo.png" height="80" width="80" /> <span style="line-height: 100px; margin-left: 10px; font-family: verdana; font-weight: bold; font-size: 15px; color: #FFFFFF;">B&L Carservice</span>
		<div id="infoBox">
			<img src="img/information.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px; float: left;" />
			<span style="font-family: verdana; font-size: 11px; color: #FFFFFF; font-weight: bold;">
				<?php
					echo $date1." Uhr";
				?>
			</span><br />
			<span style="font-family: verdana; font-size: 10px; color: #FFFFFF; font-weight: bold;">Eingeloggt als: </span>
			<span style="font-family: verdana; font-size: 10px; color: #FFFFFF;"><?php print $_SESSION['username']; ?></span><br />
			
			<p style="font-family: verdana; font-size: 10px; color: #FFFFFF; line-height: 20px; height: 20px; margin-top: 15px;"><img src="img/logout.png" height="20" width="20" style="margin-right: 5px; float: left;" /><a href="logout.php">Jetzt abmelden</a></p>
		</div>
	</div>
	
	<div id="nav">
		<ul style="font-family: verdana; font-size: 12px; color: #FFFFFF;">
			<li><a href="static.php?active_module=dashboard&action=index"><img src="img/dashboard.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Dashboard</a></li>
			<li><a href="static.php?active_module=contact&action=contact_list"><img src="img/contacts.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Kontakte</a></li>
			<!--<li><a href="static.php?active_module=offers"><img src="img/offers.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Angebote</a></li>-->
			<li><a href="static.php?active_module=commission"><img src="img/commission.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Auftr&auml;ge</a></li>			
			<li><a href="static.php?active_module=billing"><img src="img/billing.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Rechnungen</a></li>
			<li><a href="static.php?active_module=credit"><img src="img/credit.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Gutschriften</a></li>
			<li><a href="static.php?active_module=tools&action=tools_overview"><img src="img/tools.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Tools</a></li>
			<li><a href="static.php?active_module=settings&action=index&cat=general"><img src="img/settings.png" height="20" width="20" style="margin-top: 5px; margin-right: 5px;" />Einstellungen</a></li>
		<ul>
	</div>
	
	<div id="main">
		<?php include $active_directory."/".$action.".php"; ?>
	</div>
	
	<div id="sidebar">
		<?php include "includes/".$active_module."_left.php"; ?>
	</div>	
	
	<div id="footer">
		<p style="font-family: verdana; color: #FFFFFF; font-size: 10px; text-align: center; line-height: 30px;">CompanyDesk 1.0 - Licensed for B&L Carservice GmbH - All rights reserved &copy; 2014</p>
	</div>
</div>

</body>
</html>
