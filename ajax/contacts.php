<?php
    /***** AJAX HANDLER FÜR DIE KATEGORIE KONTAKT *****/

    /***** JSON OBJECT PARSEN UND DATEN AUSLESEN *****/
    $json = file_get_contents('php://input');
    $jsonObj = json_decode($json);

        $action = $jsonObj->action;

        if($action != "")
        {
            include "../classes/sqlConnect.php";
            switch($action) {

                case "addContact":

                    /***** GET HIGHEST KDNR FROM KONTAKTE *****/
                    $max = $pdo->prepare("SELECT MAX(kdnnr) AS kdnnr FROM kontakte LIMIT 1");
                    $max->execute();
                    $maxKdnnr = $max->fetch(PDO::FETCH_OBJ);
                    $maxKdnnr->kdnnr = $maxKdnnr->kdnnr + 1;

                    // MySQL Query via PDO prepared Statement
                    $stmt = $pdo->prepare("INSERT INTO kontakte
                                                           (kdnnr, anrede, titel, vorname, name, strasse, plz, ort, land, telefon_1, telefon_2, telefax, mobil, email1, email2, web)
                                                     VALUES
                                                           (:kdnnr, :anrede, :titel, :vorname, :name, :strasse, :plz, :ort, :land, :telefon1, :telefon2, :telefax, :mobil, :email1, :email2, :web)");

                    $stmt->bindParam(':kdnnr', $maxKdnnr->kdnnr);
                    $stmt->bindParam(':anrede', $jsonObj->anrede);
                    $stmt->bindParam(':titel', $jsonObj->titel);
                    $stmt->bindParam(':vorname', $jsonObj->vorname);
                    $stmt->bindParam(':name', $jsonObj->name);
                    $stmt->bindParam(':strasse', $jsonObj->strasse);
                    $stmt->bindParam(':plz', $jsonObj->plz);
                    $stmt->bindParam(':ort', $jsonObj->ort);
                    $stmt->bindParam(':land', $jsonObj->land);
                    $stmt->bindParam(':telefon1', $jsonObj->telefon1);
                    $stmt->bindParam(':telefon2', $jsonObj->telefon2);
                    $stmt->bindParam(':telefax', $jsonObj->telefax);
                    $stmt->bindParam(':mobil', $jsonObj->mobil);
                    $stmt->bindParam(':email1', $jsonObj->email1);
                    $stmt->bindParam(':email2', $jsonObj->email2);
                    $stmt->bindParam(':web', $jsonObj->web);

                    // MySQL Query ausführen, bei Error DB Objekt löschen
                    if(!$stmt->execute())
                    {
                        echo PDO::errorInfo();
                        $pdo = NULL;
                        echo "Bei der Abfrage ist ein Fehler unterlaufen";
                    }

                    else
                    {
                        echo $pdo->lastInsertId();
                    }
                    break;

                case "updateContact":

                    $contactId = $jsonObj->contactid;
                    // MySQL Query via PDO prepared Statement
                        $stmt = $pdo->prepare("UPDATE kontakte SET 
                                                          anrede = :anrede, 
                                                          titel = :titel,
                                                          vorname = :vorname,
                                                          name = :name,
                                                          strasse = :ort,
                                                          plz = :plz,
                                                          ort = :ort,
                                                          land = :land,
                                                          telefon_1 = :telefon1,
                                                          telefon_2 = :telefon2,
                                                          telefax = :telefax,
                                                          mobil = :mobil,
                                                          email1 = :email1,
                                                          email2 = :email2,
                                                          web = :web                                                         
                                                         WHERE contact_id = :contactId");

                        $stmt->bindParam(':anrede', $jsonObj->anrede);
                        $stmt->bindParam(':titel', $jsonObj->titel);
                        $stmt->bindParam(':vorname', $jsonObj->vorname);
                        $stmt->bindParam(':name', $jsonObj->name);
                        $stmt->bindParam(':strasse', $jsonObj->strasse);
                        $stmt->bindParam(':plz', $jsonObj->plz);
                        $stmt->bindParam(':ort', $jsonObj->ort);
                        $stmt->bindParam(':land', $jsonObj->land);
                        $stmt->bindParam(':telefon1', $jsonObj->telefon1);
                        $stmt->bindParam(':telefon2', $jsonObj->telefon2);
                        $stmt->bindParam(':telefax', $jsonObj->telefax);
                        $stmt->bindParam(':mobil', $jsonObj->mobil);
                        $stmt->bindParam(':email1', $jsonObj->email1);
                        $stmt->bindParam(':email2', $jsonObj->email2);
                        $stmt->bindParam(':web', $jsonObj->web);
                        $stmt->bindParam(':contactId', $contactId);

                        // MySQL Query ausführen, bei Error DB Objekt löschen
                        if(!$stmt->execute())
                        {
                            echo PDO::errorInfo();
                            $pdo = NULL;
                            echo "Bei der Abfrage ist ein Fehler unterlaufen";
                        }

                        else
                        {
                            echo "Der Datensatz wurde erfolgreich geändert";
                        }
                    break;

                case "updateContactNotices":

                    $contactId = $jsonObj->contactid;
                    // MySQL Query via PDO prepared Statement
                    $stmt = $pdo->prepare("UPDATE kontakte SET 
                                                          notizen = :notizen                                                     
                                                         WHERE contact_id = :contactId");

                    $stmt->bindParam(':notizen', $jsonObj->notizen);
                    $stmt->bindParam(':contactId', $contactId);

                    // MySQL Query ausführen, bei Error DB Objekt löschen
                    if(!$stmt->execute())
                    {
                        echo PDO::errorInfo();
                        $pdo = NULL;
                        echo "Bei der Abfrage ist ein Fehler unterlaufen";
                    }

                    else
                    {
                        echo "Der Datensatz wurde erfolgreich geändert";
                    }
                    break;

                case "updateCar":

                    $carId = $jsonObj->carid;
                    // MySQL Query via PDO prepared Statement
                    $stmt = $pdo->prepare("UPDATE fahrzeuge SET 
                                                          hersteller = :hersteller                                                     
                                                         WHERE car_id = :carId");

                    $stmt->bindParam(':carId', $carId);
                    $stmt->bindParam(':hersteller', $jsonObj->hersteller);

                    // MySQL Query ausführen, bei Error DB Objekt löschen
                    if(!$stmt->execute())
                    {
                        echo PDO::errorInfo();
                        $pdo = NULL;
                        echo "Bei der Abfrage ist ein Fehler unterlaufen";
                    }

                    else
                    {
                        echo "Der Datensatz wurde erfolgreich geändert";
                    }
                    break;

                default:

                    echo "Die angeforderte Aktion wurde nicht gefunden";

            }
        }

        else {
            die("Sorry Dude... Something went wrong...");
        }










