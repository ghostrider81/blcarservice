<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

</head>

<body class="dark-gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <img class="logo-login" src="img/logo.png" />
        </div>
        <h3>Wilkommen bei Company-Desk</h3>
        <p>Bitte melden Sie sich an.</p>
        <div id="loginAlert"></div>
        <form class="m-t" role="form" action="#" id="loginForm">
            <div class="form-group">
                <input type="text" id="eingabefeldBenutzername" name="loginBenutzername" class="form-control" placeholder="Benutzername" required>
            </div>
            <div class="form-group">
                <input type="password" id="eingabefeldPasswort" name="loginPasswort" class="form-control" placeholder="Password" required>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
        </form>
    </div>
</div>

<!-- Mainly scripts -->
<script src="js/frameworks/jquery-3.3.1.min.js"></script>
<script src="js/frameworks/bootstrap.min.js"></script>
<script src="js/modules/login.js"></script>



</body>

</html>
