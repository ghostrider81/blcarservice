<?php
error_reporting(E_ALL);
include "../php/functions.php";
include "../php/functionsInvoices.php";
include "../classes/sqlConnect.php";

if(isset($_GET['status'])) {
    $status = $_GET['status'];
}
?>


<link href="css/datatables.min.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-10">
        <h2><i class="fa fa-sticky-note"></i> Rechnungen</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Alle Rechnungen anzeigen</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

            <div class="table-responsive" id="invoiceList">
                <table class="table table-striped table-bordered table-hover dataTables-invoices tablePointer" >
                    <thead>
                    <tr>
                        <th>hidden id:</th>
                        <th>Rechnungs-Nr.:</th>
                        <th>Name:</th>
                        <th>Betrag netto:</th>
                        <th>Betrag brutto:</th>
                        <th>hidden rg-date:</th>
                        <th>RG-Datum:</th>
                        <th>hidden-faellig:</th>
                        <th>Fällig am:</th>
                        <th>Zahlungsmethode:</th>
                        <th>Sachbearbeiter:</th>
                        <th>Aktionen:</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php

                    $result = invoiceGetAll($pdo, $status);

                        if($result != false) {

                            $i = 0;
                            foreach ($result AS $row) {
                                if ($result[$i]->barverkauf == 1) {
                                    $name = '<span style="font-size: 14px !important;"><i class="fa fa-money" aria-hidden="true"></i></span> <span style="font-style: italic;">Barverkauf</span>';
                                } else {
                                    $name = $result[$i]->name . ", " . $result[$i]->vorname;
                                }

                                if ($result[$i]->payment_method == 1) {
	                                $payment_method = "Überweisung";
                                }

                                elseif ($result[$i]->payment_method == 2) {
	                                $payment_method = "Barverkauf";
                                }

                                elseif ($result[$i]->payment_method == 3) {
	                                $payment_method = "Adelta Finanz";
                                }

                                else {
                                    $payment_method = "";
                                }


                                echo "<tr data-row=\"true\" id=\"" . $result[$i]->billing_id . "\">";
                                echo "<td>" . $result[$i]->billing_id . "</td>";
                                echo "<td>" . $result[$i]->billing_nr . "</td>";
                                echo "<td>" . $name . "</td>";
                                echo "<td align='right'>" . number_format($result[$i]->gesamtbetrag + $result[$i]->tuv, 2, ',', '.') . " €" . "</td>";
                                echo "<td align='right'>" . number_format(($result[$i]->gesamtbetrag * 1.19) + $result[$i]->tuv, 2, ',', '.') . " €" . "</td>";
                                echo "<td align='center'>" . $result[$i]->date_billing . "</td>";
                                echo "<td align='center'>" . makeDateFromTimestamp($result[$i]->date_billing) . "</td>";
                                echo "<td align='center'>" . $result[$i]->date_target . "</td>";
                                echo "<td align='center'>" . makeDateFromTimestamp($result[$i]->date_target) . "</td>";
                                echo "<td>" . $payment_method . "</td>";
                                echo "<td>" . $result[$i]->operator . "</td>";
                                echo "<td align='center'>";
                                echo '<div class="dropdown head-dropdown dropright list-menu-button" style="right:0;">
                                            <span class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
        
                                            </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu" style="margin-left: -150px;">
                                                <li>
                                                    <a href="modules/invoiceExport.php/?do=exportInvoice&billing_id='.$result[$i]->billing_id.'" target="_blank" title="Rechnung PDF generieren">
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF generieren</a>
                                                </li>
                                            </ul>
                                        </div>';
                                echo "</td>";
                                echo "</tr>";

                                $i++;
                            }
                        }
                    ?>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>hidden id:</th>
                        <th>Rechnungs-Nr.:</th>
                        <th>Name:</th>
                        <th>Betrag netto:</th>
                        <th>Betrag brutto:</th>
                        <th>hidden rg-date:</th>
                        <th>RG-Datum:</th>
                        <th>hidden-faellig:</th>
                        <th>Fällig am:</th>
                        <th>Zahlungsmethode:</th>
                        <th>Sachbearbeiter:</th>
                        <th>Aktionen:</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

        $('.dataTables-invoices').DataTable({
            "order": [[ 0, "desc" ]],
            bAutoWidth: false ,
            aoColumns : [
                { visible: false },
                { sWidth: '9%', orderData: [0] },
                { sWidth: '25%' },
                { sWidth: '9%' },
                { sWidth: '9%' },
                { visible: false },
                { sWidth: '9%', orderData: [5] },
                { visible: false },
                { sWidth: '9%', orderData: [7] },
                { sWidth: '12%' },
                { sWidth: '12%' },
                { sWidth: '6%' }
            ],
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'Rechnungen'},
                {extend: 'pdf', title: 'Rechnungen'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });

</script>