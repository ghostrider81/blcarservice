<?php
    include "../classes/sqlConnect.php";
    include "../php/functions.php";

    $aw_sets = getAwSetsForSettings($pdo);
?>

<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <h2><i class="fa fa-cogs"></i> AW Verwaltung</h2>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <button class="btn btn-sm btn-primary pull-right sub-button" type="submit" id="awSave"><i class="fa fa-floppy-o"></i> <strong>AW Sets speichern</strong></button>
    </div>
</div>

<div class="wrapper wrapper-content">

    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-info-circle"></i> AW Sätze festlegen</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row form-row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">Stundenlohn:</div>
                    <div class="col-md-3">Anzahl AW:</div>
                    <div class="col-md-3">Preis AW:</div>
                </div>
                <div class="row form-row" data-aw-settings>
                    <div class="col-md-3">Arbeitslohn Mechanik:</div>
                    <div class="col-md-3"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control" type="text" value="<?=number_format($aw_sets[0]->stundenlohn_m, "2", ",", ".")?>" data-aw-settings-field="stundenlohn-m"></div></div>
                    <div class="col-md-3"><div><input class="form-control" type="text" value="<?=$aw_sets[0]->anzahl_aw_m?>" data-aw-settings-field="anzahl-m"></div></div>
                    <div class="col-md-3"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control" type="text" value="<?=number_format($aw_sets[0]->preis_aw_m, "2", ",", ".")?>" data-aw-settings-field="preis-m" disabled></div></div>
                </div>
                <div class="row form-row" data-aw-settings>
                    <div class="col-md-3">Arbeitslohn Karosserie:</div>
                    <div class="col-md-3"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control" type="text" value="<?=number_format($aw_sets[0]->stundenlohn_k, "2", ",", ".")?>" data-aw-settings-field="stundenlohn-k"></div></div>
                    <div class="col-md-3"><div><input class="form-control" type="text" value="<?=$aw_sets[0]->anzahl_aw_k?>" data-aw-settings-field="anzahl-k"></div></div>
                    <div class="col-md-3"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control" type="text" value="<?=number_format($aw_sets[0]->preis_aw_k, "2", ",", ".")?>" data-aw-settings-field="preis-k" disabled></div></div>
                </div>
                <div class="row form-row" data-aw-settings>
                    <div class="col-md-3">Arbeitslohn Lack:</div>
                    <div class="col-md-3"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control" type="text" value="<?=number_format($aw_sets[0]->stundenlohn_l, "2", ",", ".")?>" data-aw-settings-field="stundenlohn-l"></div></div>
                    <div class="col-md-3"><div><input class="form-control" type="text" value="<?=$aw_sets[0]->anzahl_aw_l?>" data-aw-settings-field="anzahl-l"></div></div>
                    <div class="col-md-3"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control" type="text" value="<?=number_format($aw_sets[0]->preis_aw_l, "2", ",", ".")?>" data-aw-settings-field="preis-l" disabled></div></div>
                </div>
                <div class="row form-row" data-aw-settings>
                    <div class="col-md-3">Arbeitslohn Elektrik:</div>
                    <div class="col-md-3"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control" type="text" value="<?=number_format($aw_sets[0]->stundenlohn_e, "2", ",", ".")?>" data-aw-settings-field="stundenlohn-e"></div></div>
                    <div class="col-md-3"><div><input class="form-control" type="text" value="<?=$aw_sets[0]->anzahl_aw_e?>" data-aw-settings-field="anzahl-e"></div></div>
                    <div class="col-md-3"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control" type="text" value="<?=number_format($aw_sets[0]->preis_aw_e, "2", ",", ".")?>" data-aw-settings-field="preis-e" disabled></div></div>
                </div>
            </div>
        </div>
    </div>

</div>