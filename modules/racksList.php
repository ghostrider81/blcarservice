<?php
error_reporting(0);
include "../classes/sqlConnect.php";
include "../php/functions.php";
include "../php/functionsWheels.php";
?>

<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2><i class="fa fa-archive"></i> Reifenlager</h2>
    </div>
</div>

<div class="wrapper wrapper-content">

    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-info-circle"></i> Regale einsehen</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row form-row">
                    <div class="col-md-12"><?php wheelsBuildRacks($pdo); ?></div>
                </div>
            </div>
        </div>
    </div>


    <div id="wheelDataWindow">
        <ul>
            <li><span>Entry-ID:</span><span id="dataWindowEntryID"></span></li>
            <li><span>Regal-Nr.:</span><span id="dataWindowRackID"></span></li>
            <li><span>Fach-Nr.:</span><span id="dataWindowSubjectID"></span></li>
            <li><span>Kennzeichen:</span><span id="dataWindowKennzeichen"></span></li>
            <li><span>Hersteller:</span><span id="dataWindowManufacturer"></span></li>
            <li><span>Reifentyp:</span><span id="dataWindowModel"></span></li>
            <li><span>Reifenart:</span><span id="dataWindowType"></span></li>
            <li><span>Radschrauben?</span><span id="dataWindowStuds"></span></li>
            <li><span>Radkappen?</span><span id="dataWindowWheelcover"></span></li>
            <li><span>Profil VL:</span><span id="dataWindowProfileVl"></span></li>
            <li><span>Profil VR:</span><span id="dataWindowProfileVr"></span></li>
            <li><span>Profil HL:</span><span id="dataWindowProfileHl"></span></li>
            <li><span>Profil HR:</span><span id="dataWindowProfileHr"></span></li>
            <li><span>Reifen in Ordnung?</span><span id="dataWindowWheelOk"></span></li>
            <li><span>Kontakt informieren?</span><span id="dataWindowInformContact"></span></li>
            <li><span>Notizen:</span><span id="dataWindowInformNotes"></span></li>
        </ul>
    </div>


    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div id="wheelRackForm" class="form-horizontal">
                <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-archive" aria-hidden="true"></i> In Regal <span id="placeholderRegal">{regal}</span> - Fach <span id="placeholderFach">{fach}</span> eintragen:</h4>
                    </div>
                    <div class="modal-body">

                            <div class="form-group">
                                <label class="control-label col-sm-4 text-left" for="contact">Kunde:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="contact" class="form-control" />
                                    <input type="hidden" id="contact_id" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="mark">Kennzeichen:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="mark">
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4 text-left" for="manufacturer">Hersteller:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="manufacturer" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4 text-left" for="model">Reifentyp:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="model" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4 text-left" for="type">Reifenart:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="type">
                                        <option value="0">Sommerreifen</option>
                                        <option value="1">Winterreifen</option>
                                        <option value="2">Allwetterreifen</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4 text-left" for="hubcap">Radkappen:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="hubcap">
                                        <option value="1">Ja</option>
                                        <option value="0">Nein</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4 text-left" for="studs">Radschrauben:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="studs">
                                        <option value="1">Ja</option>
                                        <option value="0">Nein</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-12 col-md-4 col-lg-4">Profiltiefe in mm:</label>
                                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                                    <input type="text" id="profileVL" class="form-control" placeholder="vl" />
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                                    <input type="text" id="profileVR" class="form-control" placeholder="vr" />
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                                    <input type="text" id="profileHL" class="form-control" placeholder="hl" />
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                                    <input type="text" id="profileHR" class="form-control" placeholder="hr" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="wheel_ok">Reifen in Ordnung?</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="wheel_ok">
                                        <option value="1" selected>OK</option>
                                        <option value="0">Nicht OK</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="kunde_info">Kunden informieren?</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="inform_contact">
                                        <option value="1">Ja</option>
                                        <option value="0" selected>Nein</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="notizen">Notizen:</label>
                                <div class="col-sm-8">
                                    <textarea id="notes" class="form-control" />
                                </div>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" id="pushList">
                            <strong>
                                <i class="fa fa-save"></i> Speichern
                            </strong>
                        </button>
                    </div>
                </div>
                <!-- /.Modal content -->
            </div>
        </div>
    </div>

</div>

