<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-10">
        <h2>Kontakte</h2>
    </div>
    <div class="col-lg-2">
        <button class="btn btn-sm btn-primary pull-right sub-button" type="submit" id="contactAdd"><i class="fa fa-floppy-o"></i> <strong>Kunden speichern</strong></button>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-info-circle"></i> Neuen Kontakt anlegen</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" id="contactContainer">

                <div class="row text-height">
                    <div class="col-md-12 text-bold"><i class="fa fa-user"></i> Persönliche Daten:</div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Kunden-Typ:</div>
                    <div class="col-md-8"></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Titel:</div>
                    <div class="col-md-8" id="titel"><input type="text" id="titel" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Anrede:</div>
                    <div class="col-md-8" id="anrede"><input type="text" id="anrede" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Vorname:</div>
                    <div class="col-md-8" id="vorname"><input type="text" id="vorname" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Name:</div>
                    <div class="col-md-8"><input type="text" id="name" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>

                <hr class="divider"/>

                <div class="row text-height">
                    <div class="col-md-12 text-bold"><i class="fa fa-home"></i> Adresse:</div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Strasse:</div>
                    <div class="col-md-8"><input type="text" id="strasse" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">PLZ:</div>
                    <div class="col-md-8"><input type="text" id="plz" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Ort:</div>
                    <div class="col-md-8"><input type="text" id="ort" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Land:</div>
                    <div class="col-md-8"><input type="text" id="land" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>

                <hr class="divider"/>

                <div class="row text-height">
                    <div class="col-md-12 text-bold"><i class="fa fa-fax"></i> Kommunikation:</div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Telefon:</div>
                    <div class="col-md-8"><input type="text" id="telefon1" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Telefon:</div>
                    <div class="col-md-8"><input type="text" id="telefon2" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Mobil:</div>
                    <div class="col-md-8"><input type="text" id="mobil" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Telefax:</div>
                    <div class="col-md-8"><input type="text" id="telefax" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">E-Mail:</div>
                    <div class="col-md-8"><input type="text" id="email1" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">E-Mail:</div>
                    <div class="col-md-8"><input type="text" id="email2" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Web:</div>
                    <div class="col-md-8"><input type="text" id="web" data-input="true" class="form-control margin-bottom-formfield" /></div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-4">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-pencil" aria-hidden="true"></i> Kundennotizen</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" id="noticesContainer">
                <div class="row">
                    <div class="col-md-12" data-notices="true">
                        <textarea id="notizen" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row"></div>

</div>