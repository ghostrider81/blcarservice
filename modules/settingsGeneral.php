<?php
    include "../classes/sqlConnect.php";
?>

<div class="row wrapper border-bottom white-bg page-heading row-fix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2><i class="fa fa-cogs"></i> Allgemeine Einstellungen</h2>
	</div>
</div>

<div class="wrapper wrapper-content">

    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-info-circle"></i> Nummernkreisläufe</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row form-row">
                    <div class="col-md-2">Nächste Auftrags-Nr.:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value="" readonly></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Nächste Rechnungs-Nr.:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value="" readonly></div>
                </div>
            </div>
        </div>
    </div>

</div>