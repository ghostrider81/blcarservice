<?php
include "../php/functions.php";
include "../php/functionsAssignments.php";
include "../classes/sqlConnect.php";
?>


<link href="css/datatables.min.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-10">
        <h2><i class="fa fa-sticky-note"></i> Auftragsliste</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Alle Aufträge anzeigen</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

            <div class="table-responsive" id="assignmentList">
                <table class="table table-striped table-bordered table-hover datatable-assignment tablePointer" >
                    <thead>
                    <tr>
                        <th>hidden</th>
                        <th>Auftrags-Nr.:</th>
                        <th>Kunden-Nr.:</th>
                        <th>Name:</th>
                        <th>Angenommen am:</th>
                        <th>Termin:</th>
                        <th>Gesamtbetrag netto:</th>
                        <th>Sachbearbeiter:</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php

                        $result = assignmentGetAll($pdo);

                        if($result != false) {
                            $i = 0;
                            foreach ($result AS $row) {

                                echo "<tr data-row=\"true\" id=\"" . $result[$i]->commission_id . "\">";
                                echo "<td>" . $result[$i]->commission_id . "</td>";
                                echo "<td data-commission >" . $result[$i]->commission_nr . "</td>";
                                echo "<td>" . $result[$i]->kdnnr . "</td>";
                                echo "<td>" . $result[$i]->name . ", " . $result[$i]->vorname . "</td>";
                                echo "<td align='center'>" . makeDateFromTimestamp($result[$i]->date_acception) . "</td>";
                                echo "<td align='center'>" . makeDateFromTimestamp($result[$i]->date_target) . "</td>";
                                echo "<td align='right'>" . number_format($result[$i]->gesamtbetrag, 2, ',', '.') . " €" . "</td>";
                                echo "<td>" . $result[$i]->operator . "</td>";
                                echo "<td align='center'>";
                                echo '<div class="dropdown head-dropdown list-menu-button">
                                            <span class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
        
                                            </span>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu" style="margin-left: -150px;">
                                                <li>
                                                    <a href="modules/assignmentExport.php/?do=exportAssignment&commission_id='.$result[$i]->commission_id.'" target="_blank" title="Auftrag PDF generieren">
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF generieren</a>
                                                </li>
                                                <li>
                                                    <a id="assignmentToInvoice" href=\'#nogo\' title=\'Auftrag in Rechnung umwandeln\'>
                                                    <i class="fa fa fa-exchange" aria-hidden="true"></i> In Rechnung umwandeln</a>
                                                </li>
                                            </ul>
                                        </div>';
                                echo "</td>";
                                echo "</tr>";
                                $i++;
                            }
                        }
                    ?>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>hidden</th>
                        <th>Auftrags-Nr.:</th>
                        <th>Kunden-Nr.:</th>
                        <th>Name:</th>
                        <th>Angenommen am:</th>
                        <th>Termin:</th>
                        <th>Gesamtbetrag netto:</th>
                        <th>Sachbearbeiter:</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="setInvoiceCat" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 900px">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Rechnung umwandeln</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                          <h3>Kundendaten</h3>
                          <table class="table table-condensed table-bordered">
                              <thead>
                              <tr class="hidden">
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                              </tr>
                              </thead>
                              <tbody id="kundenTabelle">

                              </tbody>
                          </table>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 3px">
                          <h3>Zuweisen <i style="font-size: 16px" class="fa fa-hand-o-up"></i></h3>
                          <table id="dropTable" class="table table-condensed table-bordered">
                              <tbody>
                                  <tr>
                                      <td id="tdlength" class="draggableItem col-md-4" style="border-bottom: 5px solid #c6ecd8">Arbeitslohn</td>
                                      <td style="border-bottom: 5px solid #0f8896" class="draggableItem col-md-4">Karosserie</td>
                                      <td style="border-bottom: 5px solid #1b374d" class="draggableItem col-md-4">Lack</td>
                                  </tr>
                                    <tr>
                                        <td style="border-bottom: 5px solid #fca720" class="draggableItem col-md-4">Material</td>
                                        <td style="border-bottom: 5px solid #ee502f" class="draggableItem col-md-4">Elektrik</td>
                                        <td style="border-bottom: 5px solid #2ac3cb" class="draggableItem col-md-4">Mechanik</td>
                                    </tr>
                              </tbody>
                          </table>
                      </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <h3>Auftragsliste</h3>
                        <table class="table table-condensed table-bordered">
                            <thead>
                            <tr>
                                <th class="col-md-1">Pos</th>
                                <th class="col-md-7">Bezeichnung</th>
                                <th class="col-md-2">Gesamtpreis</th>
                                <th class="col-md-2">Kategorie</th>
                            </tr>
                            </thead>
                            <tbody id="auftragsListe">
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                        <h3>Fahrzeugdaten</h3>
                        <table class="table table-condensed table-bordered">
                            <thead>
                            <tr>
                                <th>Hersteller</th>
                                <th>Typ</th>
                                <th>Kennzeichen</th>
                            </tr>
                            </thead>
                            <tbody id="vehicleList">
                            <tr>
                                <td>A</td>
                                <td>B</td>
                                <td >C</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary pull-right sub-button" type="submit" id="assignmentToInvoiceSubmit"><i class="fa fa-floppy-o"></i> <strong>Rechnung speichern</strong></button>
            </div>
        </div>

    </div>
</div>


<script>
    $( document ).ready(function() {
           $('.datatable-assignment').DataTable({
               "order": [[ 0, "desc" ]],
               bAutoWidth: false ,
               aoColumns : [
                   { visible: false },
                   { sWidth: '8%', orderData: [0] },
                   { sWidth: '8%' },
                   { sWidth: '30%' },
                   { sWidth: '12%' },
                   { sWidth: '12%' },
                   { sWidth: '12%' },
                   { sWidth: '10%' },
                   { sWidth: '8%' }
               ],
               pageLength: 25,
               responsive: true,
               dom: '<"html5buttons"B>lTfgitp',
               buttons: [
                   {extend: 'copy'},
                   {extend: 'csv'},
                   {extend: 'excel', title: 'Aufträge'},
                   {extend: 'pdf', title: 'Aufträge'},

                   {extend: 'print',
                       customize: function (win){
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');

                           $(win.document.body).find('table')
                               .addClass('compact')
                               .css('font-size', 'inherit');
                       }
                   }
               ]

           });
    });

        // TODO: - see console for error desc



</script>