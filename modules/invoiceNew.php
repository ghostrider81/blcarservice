<?php
error_reporting(E_ALL);

include "../php/functions.php";
include "../classes/sqlConnect.php";
include "../php/functionsInvoices.php";


?>


<div class="row wrapper border-bottom white-bg page-heading row-fix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2><i class="fa fa-sticky-note"></i> Rechnungen</h2>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="col-md-6" style="padding-left: 0; padding-right: 0;">
		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Kundendaten</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">
                        <div class="btn-group" id="toggleCustomerSearch">
                            <button type="button" class="btn btn-sm custmBTN01 activeToggle"><i class="fa fa-search"></i> Suche</button>
                            <button type="button" class="btn btn-sm custmBTN01 "><i class="fa fa-money"></i> Barverkauf</button>
                        </div>
                    <input type="hidden" name="contactId" id="contactId" />
					<div class="row" id="contactContainer">
                        <div class="col-md-12">
                            <div class="inner-addon right-addon">
                                <i class="glyphicon glyphicon-search"></i>
                                <input type="text" data-key="contactName" id="findAdress" class="form-control" placeholder="Namen suchen" />
                            </div>
                        </div>
					</div>
                    <table class="table table-condensed table-bordered">
                        <thead>
                        <tr class="hidden">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody id="kundenTabelle">

                        </tbody>
                    </table>

                </div>
			</div>
			<div class="ibox float-e-margins" id="vehwrapper" style="display: none">
				<div class="ibox-title">
					<h5><i class="fa fa-automobile"></i> Fahrzeugdaten</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content vehicleList">
					<p class="bold">Auswahlliste</p>
                    <select data-carselect style="width: 100%;"></select>
                </div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-pencil"></i> Notizen</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">

					<textarea data-key="notes" class="form-control"></textarea>

				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6" style="padding-left: 0; padding-right: 0;">
		<div class="col-md-12">
			<div class="ibox float-e-margins">

				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i><i class="fas fa-info-square"></i> Rechnungsdaten</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>

				<div class="ibox-content" id="contractData">

					<div class="row form-row">
						<div class="col-md-4">Rechnungs-Datum:</div>
						<div class="col-md-8"><input class="form-control" data-key="invoiceDate" type="text" value="<?=getCurrentDate()?>" readonly></div>
					</div>

					<div class="row form-row">
						<div class="col-md-4">Rechnungs-Nr.:</div>
						<div class="col-md-8"><input class="form-control" data-key="invoiceNumber" type="text" value="<?=getNextInvoiceNumber($pdo)?>" readonly></div>
					</div>

					<div class="row form-row">
						<div class="col-md-4">Kunden-Nr.:</div>
						<div class="col-md-8"><input class="form-control" data-key="kdnnr" type="text" readonly></div>
					</div>

					<div class="row form-row">
						<div class="col-md-4">Status:</div>
						<div class="col-md-8">
							<select data-key="status" name="invoiceStatus" class="form-control">
								<option value="0">Rechnungsentwurf</option>
                                <option value="1">Offen</option>
                                <option value="2">Fällig</option>
                                <option value="3">Bezahlt</option>
							</select>
						</div>
					</div>

                    <div class="row form-row">
                        <div class="col-md-4">Sachbearbeiter:</div>
                        <div class="col-md-8">
                            <select data-key="operator" name="sachbearbeiter" class="form-control">
                                <option value="0">Sachbearbeiter wählen:</option>
                                <option value="Eike Lauenstein">Eike Lauenstein</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-4">KM-Stand:</div>
                        <div class="col-md-8"><input data-key="kmStand" class="form-control" type="text"></div>
                    </div>

					<div class="row form-row">
						<div class="col-md-4">Zahlbar bis:</div>
						<div class="col-md-8"><input class="form-control" data-date type="text" data-key="payableDate" value=""></div>
					</div>

					<div class="row form-row">
						<div class="col-md-4">Zahlungsmethode:</div>
						<div class="col-md-8">
							<select data-key="paymentMethod" name="paymentMethod" class="form-control">
                                <option value="0" selected>Zahlungsmethode wählen:</option>
                                <option value="1">Überweisung</option>
                                <option value="2">Barverkauf</option>
                                <option value="3">Adelta Finanz</option>
							</select>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<form id="formArbeitslohn">

		<!-- / ARBEITSLOHN BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Pauschale Arbeitslohn Positionen</h5>
					<div class="ibox-tools">
						<select data-key="workRewarding" id="selectArbeitslohn">
							<option value="">Pauschale Positionen hinzufügen:</option>
							<option value="true">Aktiv</option>
							<option value="false">Inaktiv</option>
						</select>
						<a class="collapse-link right" id="selectArbeitslohnSave" style="display: none;">
							<i class="fa fa-floppy-o" id="selectArbeitslohnSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerArbeitslohn" style="display: none;">

					<!-- Position Row -->
					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-1">Position:</div>
						<div class="col-md-4">Bezeichnung:</div>
						<div class="col-md-1">Menge:</div>
						<div class="col-md-2">E-Preis AW:</div>
						<div class="col-md-2">Gesamtpreis:</div>
						<div class="col-md-2">Aktion:</div>
					</div>

					<div class="row" id="rowArbeitslohn1" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-1"><input class="form-control lfn" data-key="lfn_1" type="text" style="border: 1px solid #999;" value="1" readonly></div>
						<div class="col-md-4"><input class="form-control" data-key="bezeichnung_1" type="text" style="border: 1px solid #999;"></div>
						<div class="col-md-1"><input class="form-control qty" data-key="anzahl_1" onkeypress="validateNumber(event)" type="text" name="menge" style="border: 1px solid #999;"></div>
						<div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input class="form-control amount" data-key="preis_1" onkeypress="validateNumber(event)" type="text" style="border: 1px solid #999;"></div></div>
						<div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input class="form-control amount" data-totalprice type="text" style="background: #eee;border: 1px solid #999;" value="0"></div></div>
						<div class="col-md-2"><input class="form-control addRowArbeitslohn" type="submit" value="Reihe hinzufügen"></div>
					</div>

					<hr class="divider-full" id="dividerArbeitslohn" />
					<div class="row">
						<div class="col-md-8 text-right">Summe Pauschale Arbeitslohn Positionen:</div>
						<div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-areatotalprice type="text" style="background: #eee;border: 1px solid #999;"></div></div>
						<div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>
		</div>

		<!-- / ARBEITSLOHN ENDE -->

		<!-- / MECHANIK BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Arbeitslohn Mechanik</h5>
					<div class="ibox-tools">
						<select id="awMechanik">
							<option value="">AW Satz auswählen:</option>
							<?php getAwSets($pdo); ?>
						</select>
						<a class="collapse-link right" id="awMechanikSave" style="display: none;">
							<i class="fa fa-floppy-o" id="awMechanikSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerMechanik" style="display: none;">

					<!-- Position Row -->
					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-1">Position:</div>
						<div class="col-md-4">Bezeichnung:</div>
						<div class="col-md-1">Menge:</div>
						<div class="col-md-2">E-Preis AW:</div>
						<div class="col-md-2">Gesamtpreis:</div>
						<div class="col-md-2">Aktion:</div>
					</div>

					<div class="row" id="rowMechanik" style="padding-top: 5px; padding-bottom: 5px;" data-mechanik="true">
						<div class="col-md-1"><input class="form-control lfn" data-key="lfn_1" type="text" name="lfn" style="border: 1px solid #999;" value="1" readonly></div>
						<div class="col-md-4"><input class="form-control" data-key="bezeichnung_1" type="text" name="bezeichnung" style="border: 1px solid #999;"></div>
						<div class="col-md-1"><input class="form-control qty" data-key="anzahl_1" onkeypress="validateNumber(event)" type="text" min="1" name="menge" style="border: 1px solid #999;"></div>
						<div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount aw" data-key="preis_1" onkeypress="validateNumber(event)"  value="" readonly type="text" style="background: #eee;border: 1px solid #999;"></div></div>
						<div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-totalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><input class="form-control addRowMechanik" type="button" value="Reihe hinzufügen"></div>
					</div>


					<hr class="divider-full" id="dividerMechanik" />
					<div class="row">
						<div class="col-md-8 text-right">Summe Arbeitslohn Mechanik:</div>
						<div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-areatotalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>
		</div>

		<!-- / MECHANIK ENDE -->

		<!-- / KAROSSERIE BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Arbeitslohn Karosserie</h5>
					<div class="ibox-tools">
						<select id="awKarosserie">
							<option value="">AW Satz auswählen:</option>
							<?php getAwSets($pdo); ?>
						</select>
						<a class="collapse-link right" id="awKarosserieSave" style="display: none;">
							<i class="fa fa-floppy-o" id="awKarosserieSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerKarosserie" style="display: none;">

					<!-- Position Row -->
					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-1">Position:</div>
						<div class="col-md-4">Bezeichnung:</div>
						<div class="col-md-1">Menge:</div>
						<div class="col-md-2">E-Preis AW:</div>
						<div class="col-md-2">Gesamtpreis:</div>
						<div class="col-md-2">Aktion:</div>
					</div>

					<div class="row" id="rowKarosserie" style="padding-top: 5px; padding-bottom: 5px;" data-karosserie="true">
						<div class="col-md-1"><input class="form-control lfn" data-key="lfn_1" type="text" name="lfn" style="border: 1px solid #999;" value="1" readonly></div>
						<div class="col-md-4"><input class="form-control" data-key="bezeichnung_1" type="text" name="bezeichnung" style="border: 1px solid #999;"></div>
						<div class="col-md-1"><input class="form-control qty" data-key="anzahl_1" onkeypress="validateNumber(event)" type="text" min="1" name="menge" style="border: 1px solid #999;"></div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount aw" data-key="preis_1" onkeypress="validateNumber(event)" type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-totalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><input class="form-control addRowKarosserie" type="button" value="Reihe hinzufügen"></div>
					</div>

					<hr class="divider-full" id="dividerKarosserie" />
					<div class="row">
						<div class="col-md-8 text-right">Summe Arbeitslohn Karosserie:</div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-areatotalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>
		</div>

		<!-- / KAROSSERIE ENDE -->

		<!-- / LACK BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Arbeitslohn Lack</h5>
					<div class="ibox-tools">
						<select id="awLack">
							<option value="">AW Satz auswählen:</option>
							<?php getAwSets($pdo); ?>
						</select>
						<a class="collapse-link right" id="awLackSave" style="display: none;">
							<i class="fa fa-floppy-o" id="awLackSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerLack" style="display: none;">

					<!-- Position Row -->
					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-1">Position:</div>
						<div class="col-md-4">Bezeichnung:</div>
						<div class="col-md-1">Menge:</div>
						<div class="col-md-2">E-Preis AW:</div>
						<div class="col-md-2">Gesamtpreis:</div>
						<div class="col-md-2">Aktion:</div>
					</div>

					<div class="row" id="rowLack" style="padding-top: 5px; padding-bottom: 5px;" data-lack="true">
						<div class="col-md-1"><input class="form-control lfn" data-key="lfn_1" type="text" name="lfn" style="border: 1px solid #999;" value="1" readonly></div>
						<div class="col-md-4"><input class="form-control" data-key="bezeichnung_1" type="text" name="bezeichnung" style="border: 1px solid #999;"></div>
						<div class="col-md-1"><input class="form-control qty" data-key="anzahl_1" onkeypress="validateNumber(event)" type="text" min="1" name="menge" style="border: 1px solid #999;"></div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount aw" data-key="preis_1" onkeypress="validateNumber(event)" type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-totalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><input class="form-control addRowLack" type="button" value="Reihe hinzufügen"></div>
					</div>

					<hr class="divider-full" id="dividerLack" />
					<div class="row">
						<div class="col-md-8 text-right">Summe Arbeitslohn Lack:</div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-areatotalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>
		</div>

		<!-- / LACK ENDE -->

		<!-- / ELEKTRIK BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Arbeitslohn Elektrik</h5>
					<div class="ibox-tools">
						<select id="awElektrik">
							<option value="">AW Satz auswählen:</option>
							<?php getAwSets($pdo); ?>
						</select>
						<a class="collapse-link right" id="awElektrikSave" style="display: none;">
							<i class="fa fa-floppy-o" id="awElektrikSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerElektrik" style="display: none;">

					<!-- Position Row -->
					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-1">Position:</div>
						<div class="col-md-4">Bezeichnung:</div>
						<div class="col-md-1">Menge:</div>
						<div class="col-md-2">E-Preis AW:</div>
						<div class="col-md-2">Gesamtpreis:</div>
						<div class="col-md-2">Aktion:</div>
					</div>

					<div class="row" id="rowElektrik" style="padding-top: 5px; padding-bottom: 5px;" data-elektrik="true">
						<div class="col-md-1"><input class="form-control lfn" data-key="lfn_1" type="text" name="lfn" style="border: 1px solid #999;" value="1" readonly></div>
						<div class="col-md-4"><input class="form-control" data-key="bezeichnung_1" type="text" name="bezeichnung" style="border: 1px solid #999;"></div>
						<div class="col-md-1"><input class="form-control qty" data-key="anzahl_1" onkeypress="validateNumber(event)" type="text" min="1" name="menge" style="border: 1px solid #999;"></div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount aw" data-key="preis_1" onkeypress="validateNumber(event)" type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-totalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><input class="form-control addRowElektrik" type="button" value="Reihe hinzufügen"></div>
					</div>

					<hr class="divider-full" id="dividerElektrik" />
					<div class="row">
						<div class="col-md-8 text-right">Summe Arbeitslohn Elektrik:</div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-areatotalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>
		</div>

		<!-- / ELEKTRIK ENDE -->

		<!-- / MATERIAL BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Material</h5>
					<div class="ibox-tools">
						<select id="selectMaterial">
							<option value="">Material hinzufügen?</option>
							<option value="true">Aktiv</option>
							<option value="false">Inaktiv</option>
						</select>
						<a class="collapse-link right" id="selectMaterialSave" style="display: none;">
							<i class="fa fa-floppy-o" id="selectMaterialSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerMaterial" style="display: none;">

					<!-- Position Row -->
					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-1">Position:</div>
						<div class="col-md-4">Bezeichnung:</div>
						<div class="col-md-1">Menge:</div>
						<div class="col-md-2">E-Preis AW:</div>
						<div class="col-md-2">Gesamtpreis:</div>
						<div class="col-md-2">Aktion:</div>
					</div>

					<div class="row" id="rowMaterial" style="padding-top: 5px; padding-bottom: 5px;" data-material="true">
						<div class="col-md-1"><input class="form-control lfn" data-key="lfn_1" type="text" name="lfn" style="border: 1px solid #999;" value="1" readonly></div>
						<div class="col-md-4"><input class="form-control" data-key="bezeichnung_1" type="text" name="bezeichnung" style="border: 1px solid #999;"></div>
						<div class="col-md-1"><input class="form-control qty" data-key="anzahl_1" onkeypress="validateNumber(event)" type="text" min="1" name="menge" style="border: 1px solid #999;"></div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-key="preis_1" onkeypress="validateNumber(event)" type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-totalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"><input class="form-control addRowMaterial" type="button" value="Reihe hinzufügen"></div>
					</div>

					<hr class="divider-full" id="dividerMaterial" />
					<div class="row">
						<div class="col-md-8 text-right">Summe Material:</div>
						<div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-areatotalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
						<div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>
		</div>

		<!-- / MATERIAL ENDE -->

		<!-- / ATT BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Altteilepfand</h5>
					<div class="ibox-tools">
						<select id="selectAtt">
							<option value="">Altteilepfand hinzufügen?</option>
							<option value="true">Aktiv</option>
							<option value="false">Inaktiv</option>
						</select>
						<a class="collapse-link right" id="selectAttSave" style="display: none;">
							<i class="fa fa-floppy-o" id="selectAttSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerAtt" style="display: none;">

					<!-- Position Row -->
                    <div class="row" style="padding-top: 5px; padding-bottom: 5px;">
                        <div class="col-md-5">Bezeichnung:</div>
                        <div class="col-md-1">Satz:</div>
                        <div class="col-md-2">Stück-Preis:</div>
                        <div class="col-md-2">Gesamtpreis:</div>
                        <div class="col-md-2"></div>
                    </div>

                    <div class="row" id="rowAtt" style="padding-top: 5px; padding-bottom: 5px;" data-att="true">
                        <div class="col-md-5"><input class="form-control" data-key="bezeichnung" type="text" name="bezeichnung" style="border: 1px solid #999;"></div>
                        <div class="col-md-1"><input class="form-control qty" data-key="satz_1" onkeypress="validateNumber(event)" type="text" min="1" name="menge" style="border: 1px solid #999;"></div>
                        <div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input class="form-control amount" data-key="preis_1" onkeypress="validateNumber(event)" type="text" style="border: 1px solid #999;"></div></div>
                        <div class="col-md-2"><div class="inner-addon right-addon"><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-totalprice type="text" style="background: #eee;border: 1px solid #999;" readonly></div></div>
                        <div class="col-md-2"></div>
                    </div>

					<hr class="divider-full" id="dividerAtt" />
					<div class="row">
						<div class="col-md-8 text-right">Summe Altteilepfand:</div>
						<div class="col-md-2"><input id="att_totalprice" data-key="attResult" data-areatotalprice disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
						<div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>
		</div>

		<!-- / ATT ENDE -->

		<!-- / TUEV BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> TÜV</h5>
					<div class="ibox-tools">
						<select id="selectTuev">
							<option value="">TÜV hinzufügen?</option>
							<option value="true">Aktiv</option>
							<option value="false">Inaktiv</option>
						</select>
						<a class="collapse-link right" id="selectTuevSave" style="display: none;">
							<i class="fa fa-floppy-o" id="selectTuevSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerTuev" style="display: none;">

					<!-- Position Row -->
					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-8"></div>
						<div class="col-md-2">Gesamtpreis:</div>
						<div class="col-md-2"></div>
					</div>

					<div class="row" id="rowTuev" style="padding-top: 5px; padding-bottom: 5px;" data-tuev="true">
						<div class="col-md-8"></div>
						<div class="col-md-2">
							<input class="hidden quantity" data-key="anzahl_1" onkeypress="validateNumber(event)" type="text" min="1" value="1" name="menge" style="border: 1px solid #999;">
							<input class="form-control" type="text" onkeypress="validateNumber(event)" data-key="preis_1" value="0" id="carInspectionFeeInput" name="tuevGesamtpreis" style="border: 1px solid #999;"></div>
						<div class="col-md-2"></div>
					</div>

					<hr class="divider-full" id="dividerTuev" />
					<div class="row">
						<div class="col-md-8 text-right">Summe TÜV:</div>
						<div class="col-md-2"><input id="tuv_totalprice" data-key="carInspectionResult" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
						<div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>
		</div>

		<!-- / TUEV ENDE -->

		<!-- / RABATT BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Rabatt</h5>
					<div class="ibox-tools">
						<select id="selectRabatt">
							<option value="">Rabatt hinzufügen?</option>
							<option value="true">Aktiv</option>
							<option value="false">Inaktiv</option>
						</select>
						<a class="collapse-link right" id="selectRabattSave" style="display: none;">
							<i class="fa fa-floppy-o" id="selectRabattSaveBtn" style="font-size: 17px; color: #666;"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" id="containerRabatt" style="display: none;">

					<!-- Position Row -->

					<div class="row" id="rowRabatt" style="padding-top: 5px; padding-bottom: 5px;" data-rabatt="true">
						<div class="col-md-8 text-right">Rabatt auf AW Positionen gewähren:</div>
						<div class="col-md-2"><input class="form-control" type="text" onkeypress="validateNumber(event)" data-key="rabatt" value="0" id="RabattInput" name="rabattGesamtpreis" style="border: 1px solid #999;"></div>
                        <div class="col-md-2"></div>
					</div>

					<!-- /Position Row -->

				</div>
			</div>



		</div>

		<!-- / RABATT ENDE -->

		<!-- / SUMMARY BEGINN -->

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-info-circle"></i> Zusammenfassung:</h5>
				</div>
				<div class="ibox-content" id="containerSummary">

					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-8 text-right">Gesamt netto:</div>
						<div class="col-md-2"><input id="resultRaw" data-key="resultRaw" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
						<div class="col-md-2"></div>
					</div>

					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-8 text-right">MwSt:</div>
						<div class="col-md-2"><input id="taxFromResult" data-key="taxFromResult" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
						<div class="col-md-2"></div>
					</div>

					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-8 text-right">Rabatt:</div>
						<div class="col-md-2">
                            <input id="rabattResult" data-key="rabatt" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
						<div class="col-md-2"></div>
					</div>

					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-8 text-right">TÜV:</div>
						<div class="col-md-2">
                            <input id="carInspectionFee" data-key="preis_1" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
						<div class="col-md-2"></div>
					</div>

					<div class="row" style="padding-top: 5px; padding-bottom: 5px;">
						<div class="col-md-8 text-right">Gesamt brutto:</div>
						<div class="col-md-2"><input id="resultWithTax" data-key="resultWithTax" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
						<div class="col-md-2"></div>
					</div>

				</div>
			</div>
		</div>

		<!-- / SUMMARY ENDE -->

        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        <button class="btn btn-sm btn-primary pull-right sub-button" type="submit" id="addInvoice" style="margin-top: 15px;"><i class="fa fa-floppy-o"></i> <strong>Rechnung speichern</strong></button>
                    </div>
                </div>
            </div>
        </div>

	</form>

</div>
