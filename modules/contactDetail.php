<?php
include "../php/functionsContacts.php";
include "../php/functions.php";
include "../classes/sqlConnect.php";

$contactId = $_GET['contactId'];

$result = contactsGetDetail($pdo, $contactId);

$counter = count($result);

$kdnnr = $result[0]->kdnnr;
$anrede = $result[0]->anrede;
$titel = $result[0]->titel;
$geburtstag = $result[0]->geburtstag;
$name = $result[0]->name;
$vorname = $result[0]->vorname;
$strasse = $result[0]->strasse;
$plz = $result[0]->plz;
$ort = $result[0]->ort;
$land = $result[0]->land;
$telefon1 = $result[0]->telefon_1;
$telefon2 = $result[0]->telefon_2;
$mobil = $result[0]->mobil;
$telefax = $result[0]->telefax;
$email1 = $result[0]->email1;
$email2 = $result[0]->email2;
$web = $result[0]->web;
$notizen = $result[0]->notizen;

$spacer = '<span class="light-color"><i class="fa fa-bolt"></i></span>';
?>

<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-10">
        <h2><i class="fa fa-user"></i> Kontakte</h2>
    </div>
    <div class="col-lg-2">
        <div class="dropdown head-dropdown pull-right sub-button">
            <button class="btn btn-sm btn-primary dropdown-toggle " type="button" id="contactOptions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <strong>
                    <i class="fa fa-user" aria-hidden="true"></i> Optionen <span class="caret"></span>
                </strong>
            </button>
            <ul class="dropdown-menu" aria-labelledby="contactOptions">
                <li><a href="#" data-toggle="modal" data-target="#myModal" class="dropdown-item"><i class="fa fa-plus-circle" aria-hidden="true"></i> Neues Fahrzeug hinzufügen</a></li>
                <li><a href="#" class="dropdown-item"><i class="fa fa-trash-o" aria-hidden="true"></i> Diesen Kunden löschen</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-info-circle"></i> Kontakt-Details</h5>
                <div class="ibox-tools">
                    <a id="contactEdit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a id="contactSave" style="display: none;">
                        <i class="fa fa-save"></i>
                    </a>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" id="contactContainer">

                <div class="row text-height">
                    <div class="col-md-12 text-bold"><i class="fa fa-user"></i> Persönliche Daten:</div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Kunden-Typ:</div>
                    <div class="col-md-8">

                        <?php
                            if($anrede == "Firma") {
                                echo '<i class="fa fa-industry" aria-hidden="true"></i> Firma';
                            }

                            elseif($anrede == "Autohaus") {
                                echo '<i class="fa fa-car" aria-hidden="true"></i> Autohaus';
                            }

                            else {
                                echo '<i class="fa fa-user" aria-hidden="true"></i> Privat';
                            }
                        ?>

                    </div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Kunden-Nr.:<input type="hidden" id="contactId" value="<?php echo $contactId; ?>" /></div>
                    <div class="col-md-8"><?php echo $kdnnr; ?></div>
                </div>
                        <?php
                            if($anrede == "Autohaus" or $anrede == "Firma") { ?>

                                <div class="row text-height text-highlight">
                                    <div class="col-md-4 text-bold">Firma:</div>
                                    <div class="col-md-8" id="name" data-editable="true"><span><?php echo checkVarContent($spacer, $name); ?></span></div>
                                </div>
                                <div class="row text-height text-highlight">
                                    <div class="col-md-4 text-bold">Firma 2:</div>
                                    <div class="col-md-8" id="vorname" data-editable="true"><span><?php echo checkVarContent($spacer, $vorname); ?></span></div>
                                </div>
                        <?php
                            }

                            else { ?>

                                <div class="row text-height text-highlight">
                                    <div class="col-md-4 text-bold">Titel:</div>
                                    <div class="col-md-8" id="titel" data-editable="true"><span><?php echo checkVarContent($spacer, $titel); ?></span></div>
                                </div>
                                <div class="row text-height text-highlight">
                                    <div class="col-md-4 text-bold">Anrede:</div>
                                    <div class="col-md-8" id="anrede" data-editable="true"><span><?php echo checkVarContent($spacer, $anrede); ?></span></div>
                                </div>
                                <div class="row text-height text-highlight">
                                    <div class="col-md-4 text-bold">Vorname:</div>
                                    <div class="col-md-8" id="vorname" data-editable="true"><span><?php echo checkVarContent($spacer, $vorname); ?></span></div>
                                </div>
                                <div class="row text-height text-highlight">
                                    <div class="col-md-4 text-bold">Name:</div>
                                    <div class="col-md-8" id="name" data-editable="true"><span><?php echo checkVarContent($spacer, $name); ?></span></div>
                                </div>
                        <?php
                            }
                            ?>


                    <hr class="divider"/>

                <div class="row text-height">
                    <div class="col-md-12 text-bold"><i class="fa fa-home"></i> Adresse:</div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Strasse:</div>
                    <div class="col-md-8" id="strasse" data-editable="true"><span><?php echo checkVarContent($spacer, $strasse); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">PLZ:</div>
                    <div class="col-md-8" id="plz" data-editable="true"><span><?php echo checkVarContent($spacer, $plz); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Ort:</div>
                    <div class="col-md-8" id="ort" data-editable="true"><span><?php echo checkVarContent($spacer, $ort); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Land:</div>
                    <div class="col-md-8" id="land" data-editable="true"><span><?php echo checkVarContent($spacer, $land); ?></span></div>
                </div>

                    <hr class="divider"/>

                <div class="row text-height">
                    <div class="col-md-12 text-bold"><i class="fa fa-fax"></i> Kommunikation:</div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Telefon:</div>
                    <div class="col-md-8" id="telefon1" data-editable="true"><span><?php echo checkVarContent($spacer, $telefon1); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Telefon:</div>
                    <div class="col-md-8" id="telefon2" data-editable="true"><span><?php echo checkVarContent($spacer, $telefon2); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Mobil:</div>
                    <div class="col-md-8" id="mobil" data-editable="true"><span><?php echo checkVarContent($spacer, $mobil); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Telefax:</div>
                    <div class="col-md-8" id="telefax" data-editable="true"><span><?php echo checkVarContent($spacer, $telefax); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">E-Mail:</div>
                    <div class="col-md-8" id="email1" data-editable="true"><span><?php echo checkVarContent($spacer, $email1); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">E-Mail:</div>
                    <div class="col-md-8" id="email2" data-editable="true"><span><?php echo checkVarContent($spacer, $email2); ?></span></div>
                </div>
                <div class="row text-height text-highlight">
                    <div class="col-md-4 text-bold">Web:</div>
                    <div class="col-md-8" id="web" data-editable="true"><span><?php echo checkVarContent($spacer, $web); ?></span></div>
                </div>

            </div>
        </div>



                <?php

                if($result[0]->car_id == NULL)
                {
                    echo '<div class="ibox float-e-margins">';
                    echo '<div class="ibox-title">';
                    echo '<h5><i class="fa fa-car" aria-hidden="true"></i></h5>';
                    echo '<div class="ibox-tools">';
                    echo '<a class="collapse-link">';
                    echo '<i class="fa fa-chevron-up"></i>';
                    echo '</a>';
                    echo '</div>';
                    echo '</div>';
                    echo '<div class="ibox-content">';
                    echo "Keine Fahrzeuge vorhanden";
                    echo '</div>';
                    echo '</div>';
                }

                else
                {
                    foreach ($result AS $row) {

                        echo '<div class="ibox float-e-margins">';
                        echo '<div class="ibox-title">';
                        echo '<h5><i class="fa fa-car" aria-hidden="true"></i> ' . checkVarContent($spacer, $row->hersteller) . ' ' . checkVarContent($spacer, $row->modell) . '</h5>';
                        echo '<div class="ibox-tools">';
                        echo '<a id="carEdit-'.$row->car_id.'">';
                        echo '<i class="fa fa-pencil"></i>';
                        echo '</a>';
                        echo '<a id="carSave-'.$row->car_id.'" style="display: none;">';
                        echo '<i class="fa fa-save"></i>';
                        echo '</a>';
                        echo '<a class="collapse-link">';
                        echo '<i class="fa fa-chevron-up"></i>';
                        echo '</a>';
                        echo '</div>';
                        echo '</div>';
                        echo '<div class="ibox-content" id="carContainer-'.$row->car_id.'">';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">Hersteller:</div>';
                        echo '<div class="col-md-8" id="hersteller-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, $row->hersteller) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">Modell:</div>';
                        echo '<div class="col-md-8" id="modell-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, $row->modell) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">Kennzeichen:</div>';
                        echo '<div class="col-md-8" id="kennzeichen-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, $row->kennzeichen) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">Erstzulassung:</div>';
                        echo '<div class="col-md-8" id="erstzulassung-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, makeDateFromTimestamp($row->erstzulassung)) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">Ident-Nr.:</div>';
                        echo '<div class="col-md-8" id="identnr-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, $row->identnr) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">KBA-Nr.:</div>';
                        echo '<div class="col-md-8" id="kbanr-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, $row->kbanr) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">Leistung:</div>';
                        echo '<div class="col-md-8" id="leistung-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, $row->leistung) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">Kilometerstand:</div>';
                        echo '<div class="col-md-8" id="km-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, $row->km) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">HU:</div>';
                        echo '<div class="col-md-8" id="hu-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, makeDateFromTimestamp($row->hu)) . '</div>';
                        echo '</div>';
                        echo '<div class="row text-height text-highlight">';
                        echo '<div class="col-md-4 text-bold">AU:</div>';
                        echo '<div class="col-md-8" id="au-'.$row->car_id.'" data-car="'.$row->car_id.'">' . checkVarContent($spacer, makeDateFromTimestamp($row->au)) . '</div>';
                        echo '</div>';
                        echo '</div>';
                        echo '</div>';

                    }
                }
                ?>



    </div>

    <div class="col-md-4">


        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-pencil" aria-hidden="true"></i> Kundennotizen</h5>
                <div class="ibox-tools">
                    <a id="noticesEdit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a id="noticesSave" style="display: none;">
                        <i class="fa fa-save"></i>
                    </a>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" id="noticesContainer">
                <div class="row">
                    <div class="col-md-12" data-notices="true">
                        <?php echo checkVarContent($spacer, $notizen); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-envelope" aria-hidden="true"></i> E-Mail senden</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <form role="form">
                            <div class="form-group"><label>Betreff:</label> <input placeholder="Betreff" class="form-control autoenter" type="text" required></div>
                            <div class="form-group"><label>Nachricht:</label> <textarea placeholder="Ihre Nachricht" class="form-control autoenter" required></textarea></div>
                            <div>
                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                                    <strong><i class="fa fa-paper-plane" aria-hidden="true"></i> E-Mail senden</strong>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row"></div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-car" aria-hidden="true"></i> Ein neues Fahrzeug anlegen</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Hersteller">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Modell">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Kennzeichen">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Erstzulassung">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Ident-Nr.">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="KBA-Nr.">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Leistung">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="Kilometerstand">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="HU">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="AU">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                        <strong>
                            <i class="fa fa-save"></i> Speichern
                        </strong>
                    </button>
                </div>
            </div>

        </div>
    </div>

</div>