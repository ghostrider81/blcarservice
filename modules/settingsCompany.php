<?php
include "../classes/sqlConnect.php";
include "../php/functions.php";
?>

<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <h2><i class="fa fa-cogs"></i> Firmendaten</h2>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <button class="btn btn-sm btn-primary pull-right sub-button" type="submit" id="assignmentAdd"><i class="fa fa-floppy-o"></i> <strong>Änderungen speichern</strong></button>
    </div>
</div>

<div class="wrapper wrapper-content">

    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-info-circle"></i> Stammdaten</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row form-row">
                    <div class="col-md-2">Firmenbezeichnung:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Geschäftsführer:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Strasse:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">PLZ:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Ort:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Telefon:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Telefax:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">E-Mail:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Web:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Sitz der Gesellschaft:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Amtsgericht:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Handelsregister:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Gerichtsstand:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Steuernummer:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">UStId:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Bank:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">BLZ:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">Konto-Nr.:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">IBAN:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
                <div class="row form-row">
                    <div class="col-md-2">BIC:</div>
                    <div class="col-md-10"><input class="form-control" type="text" value=""></div>
                </div>
            </div>
        </div>
    </div>

</div>