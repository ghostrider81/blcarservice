<?php
    include "../classes/sqlConnect.php";

    if($_GET['term'] != "")
    {
        $searchQuery = $_GET['term'];


        $stmt = $pdo->prepare("
                                          SELECT 
                                              k.contact_id, 
                                              k.kdnnr, 
                                              k.name, 
                                              k.vorname,
                                          GROUP_CONCAT(f.kennzeichen) AS kennzeichen
                                          FROM 
                                              kontakte k
                                          LEFT JOIN
                                              fahrzeuge f
                                          ON
                                              k.contact_id = f.contact_id
                                          WHERE 
                                              k.name 
                                          LIKE 
                                              '%".$searchQuery."%' 
                                          OR 
                                              k.vorname 
                                          LIKE 
                                                '%".$searchQuery."%'
                                          GROUP BY k.contact_id
                                          ");

        if(!$stmt->execute())
        {
            echo PDO::errorInfo();
            $pdo = NULL;
            echo "Bei der Abfrage ist ein Fehler unterlaufen";
        }

        $searchResult = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($searchResult AS $row) {
            $searchReturn[] = array(
                "value" => $row->name.', '.$row->vorname.' - (Kunden-Nr. '.$row->kdnnr.')',
                "id" => $row->contact_id,
                "vehicleData" => $row->kennzeichen
            );

        }

        echo json_encode($searchReturn);
    }