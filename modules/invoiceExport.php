<?php
error_reporting(E_ALL);

if ( isset($_GET['do']) ) {
    if ($_GET['do'] == 'exportInvoice') {

        include "../classes/sqlConnect.php";
        include "../php/functions.php";
        include "../php/functionsAssignments.php";
        include_once('../pdf/fpdf.php');
        include_once('../pdf/fpdi.php');


        //DATEN AUS DB HOLEN

        $query_billing = $pdo->prepare("SELECT contact_id, car_id, billing_nr, commission_nr, barverkauf, date_billing, operator, notices, tuv, payment_method, att_percent, att_price, att_desc FROM rechnung WHERE billing_id =".$_GET['billing_id']);
        $query_billing->execute();
        $result_billing = $query_billing->fetchAll(PDO::FETCH_OBJ);

        if($result_billing[0]->barverkauf != 1)
        {
            $query_contact = $pdo->prepare("SELECT anrede, name, vorname, strasse, plz, ort, telefon_1, kdnnr FROM kontakte WHERE contact_id =".$result_billing[0]->contact_id);
            $query_contact->execute();
            $result_contact = $query_contact->fetchAll(PDO::FETCH_OBJ);

            $anrede = utf8_decode($result_contact[0]->anrede);
            $name = utf8_decode($result_contact[0]->name);
            $vorname = utf8_decode($result_contact[0]->vorname);
            $strasse = utf8_decode($result_contact[0]->strasse);
            $plz = $result_contact[0]->plz;
            $telefon = $result_contact[0]->telefon_1;
            $kunden_nr = $result_contact[0]->kdnnr;
            $ort = utf8_decode($result_contact[0]->ort);

            if($vorname == "" && $name != "")
            {
                $receiverName = $name;
            }

            elseif($vorname != "" && $name == "")
            {
                $receiverName = $vorname;
            }

            else
            {
                $receiverName = $name.", ".$vorname;
            }

            $query_car = $pdo->prepare("SELECT hersteller, modell, kennzeichen, identnr, erstzulassung,kbanr, hu, au, km FROM fahrzeuge WHERE car_id =".$result_billing[0]->car_id);
            $query_car->execute();
            $result_car = $query_car->fetchAll(PDO::FETCH_OBJ);

        }

        $notices = utf8_decode($result_billing[0]->notices);
        $payment_method = utf8_decode($result_billing[0]->payment_method);


        $pdf = new FPDI();

        if($payment_method == 3) {
            $pageCount = $pdf->setSourceFile("../pdf/template/billing_adelta_blanko.pdf");
        }

        else {
            $pageCount = $pdf->setSourceFile("../pdf/template/billing_blanko.pdf");
        }

        $tplIdx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage();
        $pdf->useTemplate($tplIdx, 0, 0, 210);


        //SEITENEIGENSCHAFTEN

        define('EURO',chr(128));


        //ADRESSFELD
        if($result_billing[0]->barverkauf != 1)
        {
            $pdf->SetFont('Helvetica', '', 9);
            $pdf->SetXY(19, 57);
            $pdf->MultiCell(100,  5, $anrede, 0, 'L');

            $pdf->SetFont('Helvetica', '', 9);
            $pdf->SetXY(19, 62);
            $pdf->MultiCell(100,  5, $receiverName, 0, 'L');

            $pdf->SetFont('Helvetica', '', 9);
            $pdf->SetXY(19, 67);
            $pdf->MultiCell(100,  5, $strasse, 0, 'L');

            $pdf->SetFont('Helvetica', '', 9);
            $pdf->SetXY(19, 72);
            $pdf->MultiCell(100,  5, $plz.' '.$ort, 0, 'L');
        }

        else
        {
            $pdf->SetFont('Helvetica', '', 9);
            $pdf->SetXY(19, 62);
            $pdf->MultiCell(100,  5, "BARVERKAUF", 0, 'L');
        }


        //DATUM KUNDENNUMMER

        $pdf->SetFont('Helvetica', '', 9);
        $pdf->SetXY(31.5, 105.8);
        $pdf->MultiCell(50,  5, makeDateFromTimestamp($result_billing[0]->date_billing), 0, 'L');

        $pdf->SetFont('Helvetica', '', 9);
        $pdf->SetXY(48, 110.4);
        $pdf->MultiCell(50,  5, utf8_decode($result_billing[0]->commission_nr), 0, 'L');

        $pdf->SetFont('Helvetica', '', 9);
        $pdf->SetXY(51, 115);
        $pdf->MultiCell(50,  5, utf8_decode($result_billing[0]->billing_nr), 0, 'L');

        if($result_billing[0]->barverkauf != 1)
        {
            $pdf->SetFont('Helvetica', '', 9);
            $pdf->SetXY(46, 119.6);
            $pdf->MultiCell(50,  5, utf8_decode($result_contact[0]->kdnnr), 0, 'L');


            //KFZ DATEN
        if(!empty($result_car[0])) {
            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(114, 104.1);
            $pdf->MultiCell(50,  5, utf8_decode($result_car[0]->hersteller), 0, 'L');

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(114, 107.4);
            $pdf->MultiCell(50,  5, utf8_decode($result_car[0]->modell), 0, 'L');

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(114, 110.6);
            $pdf->MultiCell(50,  5, utf8_decode($result_car[0]->kennzeichen), 0, 'L');

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(114, 113.7);
            $pdf->MultiCell(50,  5, utf8_decode($result_car[0]->identnr), 0, 'L');

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(114, 116.9);
            $pdf->MultiCell(50,  5, makeDateFromTimestamp($result_car[0]->erstzulassung), 0, 'L');

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(114, 120.1);
            $pdf->MultiCell(50,  5, utf8_decode($result_car[0]->km), 0, 'L');
        }

        }

        //POSITIONEN

        $pdf->SetDrawColor(102, 102, 102);
        $pdf->SetLineWidth(1);
        $pdf->Line(20, 130, 157.5, 130);

        $pdf->SetFillColor(153, 153, 153);
        $pdf->SetFont('Helvetica', '', 7);
        $pdf->SetXY(19.5, 130.5);
        $pdf->MultiCell(15,  5, 'Pos.Nr.', 0, 'C', 1);

        $pdf->SetFont('Helvetica', '', 7);
        $pdf->SetXY(34.5, 130.5);
        $pdf->MultiCell(72.5,  5, 'Bezeichnung', 0, 'L', 1);

        $pdf->SetFont('Helvetica', '', 7);
        $pdf->SetXY(107, 130.5);
        $pdf->MultiCell(17,  5, 'Menge/AW', 0, 'C', 1);

        $pdf->SetFont('Helvetica', '', 7);
        $pdf->SetXY(124, 130.5);
        $pdf->MultiCell(17,  5, 'E-Preis/AW', 0, 'C', 1);

        $pdf->SetFont('Helvetica', '', 7);
        $pdf->SetXY(141, 130.5);
        $pdf->MultiCell(17,  5, 'Gesamtpreis', 0, 'C', 1);

        $pdf->SetLineWidth(0.3);
        $pdf->Line(19.6, 135.5, 157.9, 135.5);

        $pdf->Line(34.5, 130.5, 34.5, 135.5);
        $pdf->Line(107, 130.5, 107, 135.5);
        $pdf->Line(124, 130.5, 124, 135.5);
        $pdf->Line(141, 130.5, 141, 135.5);

        $pdf->SetLineWidth(0.1);

        $y_achse = 135.6;
        $lfnd_nr = 1;

        // POSITIONEN CAT 0

        $query_positions0 = $pdo->prepare("SELECT artikel_id, cat_id, anzahl, preis FROM positionen_rechnung WHERE cat_id = 0 AND billing_id =".$_GET['billing_id']);
        $query_positions0->execute();
        $result_positions0 = $query_positions0->fetchAll(PDO::FETCH_OBJ);

        if($query_positions0->rowCount() != 0)
        {
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Pauschale Arbeitslohn Positionen', 0, 'L', 0);
            $y_achse = $y_achse+5;

            $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

            // DYNAMISCHE AUSGABE POSITIONEN

            $cat0_gesamtpreis = 0;
            $index0 = 0;

            foreach($result_positions0  AS $row_positions0)
            {
                $query_positions_desc0 = $pdo->prepare("SELECT bezeichnung FROM positionen WHERE artikel_id =".$result_positions0[$index0]->artikel_id);
                $query_positions_desc0->execute();
                $result_positions_desc0 = $query_positions_desc0->fetchAll(PDO::FETCH_OBJ);

                $row_gesamtpreis0 = $result_positions0[$index0]->anzahl * $result_positions0[$index0]->preis;
                $row_gesamtpreis_f0 = number_format($result_positions0[$index0]->anzahl * $result_positions0[$index0]->preis, 2, ',', '');
                $cat0_gesamtpreis = $cat0_gesamtpreis + $row_gesamtpreis0;

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(34, $y_achse);
                $pdf->MultiCell(72.5,  5, utf8_decode($result_positions_desc0[0]->bezeichnung), 0, 'L');
                $current_y = $pdf->GetY();
                $cellHeight = $current_y - $y_achse;

                $pdf->Line(107, $y_achse, 107, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(19.5, $y_achse);
                $pdf->MultiCell(15,  5, $lfnd_nr, 0, 'C');
                $pdf->Line(34.5, $y_achse, 34.5, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(107, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions0[$index0]->anzahl, 1, ',', ''), 0, 'C');
                $pdf->Line(124, $y_achse, 124, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(124, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions0[$index0]->preis, 2, ',', '').' '.EURO, 0, 'R');
                $pdf->Line(141, $y_achse, 141, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(141, $y_achse);
                $pdf->MultiCell(17,  5, $row_gesamtpreis_f0.' '.EURO, 0, 'R');

                $y_achse = $y_achse + $cellHeight;

                $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

                $lfnd_nr++;
                if($y_achse >= 235)
                {
                    $pdf->AddPage();
                    $y_achse = 25;
                }

                $index0++;
            }

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Summe Pauschale Arbeitslohn Positionen:', 0, 'L', 0);

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(133, $y_achse);
            $pdf->MultiCell(25,  5, number_format($cat0_gesamtpreis, 2, ',', '').' '.EURO, 0, 'R');

            $pdf->Line(141, $y_achse, 141, $y_achse+5);
            $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

            $y_achse = $y_achse+5;
        }

        else
        {
            $cat0_gesamtpreis = 0;
        }

        // POSITIONEN CAT 1

        $query_positions1 = $pdo->prepare("SELECT artikel_id, cat_id, anzahl, preis FROM positionen_rechnung WHERE cat_id = 1 AND billing_id =".$_GET['billing_id']);
        $query_positions1->execute();
        $result_positions1 = $query_positions1->fetchAll(PDO::FETCH_OBJ);

        if($query_positions1->rowCount() != 0)
        {
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Positionen Arbeitslohn Mechanik', 0, 'L', 0);
            $y_achse = $y_achse+5;

            $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

            // DYNAMISCHE AUSGABE POSITIONEN

            $cat1_gesamtpreis = 0;
            $index1 = 0;

            foreach($result_positions1 AS $row_positions1)
            {

                $query_positions_desc1 = $pdo->prepare("SELECT bezeichnung FROM positionen WHERE artikel_id =".$result_positions1[$index1]->artikel_id);
                $query_positions_desc1->execute();
                $result_positions_desc1 = $query_positions_desc1->fetchAll(PDO::FETCH_OBJ);

                $row_gesamtpreis1 = $result_positions1[$index1]->anzahl * $result_positions1[$index1]->preis;
                $row_gesamtpreis_f1 = number_format($result_positions1[$index1]->anzahl * $result_positions1[$index1]->preis, 2, ',', '');
                $cat1_gesamtpreis = $cat1_gesamtpreis + $row_gesamtpreis1;

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(34, $y_achse);
                $pdf->MultiCell(72.5,  5, utf8_decode($result_positions_desc1[0]->bezeichnung), 0, 'L');
                $current_y = $pdf->GetY();
                $cellHeight = $current_y - $y_achse;

                $pdf->Line(107, $y_achse, 107, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(19.5, $y_achse);
                $pdf->MultiCell(15,  5, $lfnd_nr, 0, 'C');
                $pdf->Line(34.5, $y_achse, 34.5, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(107, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions1[$index1]->anzahl, 1, ',', ''), 0, 'C');
                $pdf->Line(124, $y_achse, 124, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(124, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions1[$index1]->preis, 2, ',', '').' '.EURO, 0, 'R');
                $pdf->Line(141, $y_achse, 141, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(141, $y_achse);
                $pdf->MultiCell(17,  5, $row_gesamtpreis_f1.' '.EURO, 0, 'R');

                $y_achse = $y_achse + $cellHeight;

                $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

                $lfnd_nr++;
                if($y_achse >= 235)
                {
                    $pdf->AddPage();
                    $y_achse = 25;
                }

                $index1++;
            }

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Summe Mechanik:', 0, 'L', 0);

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(133, $y_achse);
            $pdf->MultiCell(25,  5, number_format($cat1_gesamtpreis, 2, ',', '').' '.EURO, 0, 'R');

            $pdf->Line(141, $y_achse, 141, $y_achse+5);
            $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

            $y_achse = $y_achse+5;
        }

        else
        {
            $cat1_gesamtpreis = 0;
        }




        // POSITIONEN CAT 2

        $query_positions2 = $pdo->prepare("SELECT artikel_id, cat_id, anzahl, preis FROM positionen_rechnung WHERE cat_id = 2 AND billing_id =".$_GET['billing_id']);
        $query_positions2->execute();
        $result_positions2 = $query_positions2->fetchAll(PDO::FETCH_OBJ);

        if($query_positions2->rowCount() != 0)
        {
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Positionen Arbeitslohn Karosserie', 0, 'L', 0);
            $y_achse = $y_achse+5;

            $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

            // DYNAMISCHE AUSGABE POSITIONEN

            $cat2_gesamtpreis = 0;
            $index2 = 0;

            foreach($result_positions2 AS $row_positions2)
            {

                $query_positions_desc2 = $pdo->prepare("SELECT bezeichnung FROM positionen WHERE artikel_id =".$result_positions2[$index2]->artikel_id);
                $query_positions_desc2->execute();
                $result_positions_desc2 = $query_positions_desc2->fetchAll(PDO::FETCH_OBJ);

                $row_gesamtpreis2 = $result_positions2[$index2]->anzahl * $result_positions2[$index2]->preis;
                $row_gesamtpreis_f2 = number_format($result_positions2[$index2]->anzahl * $result_positions2[$index2]->preis, 2, ',', '');
                $cat2_gesamtpreis = $cat2_gesamtpreis + $row_gesamtpreis2;

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(34, $y_achse);
                $pdf->MultiCell(72.5,  5, utf8_decode($result_positions_desc2[0]->bezeichnung), 0, 'L');
                $current_y = $pdf->GetY();
                $cellHeight = $current_y - $y_achse;

                $pdf->Line(107, $y_achse, 107, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(19.5, $y_achse);
                $pdf->MultiCell(15,  5, $lfnd_nr, 0, 'C');
                $pdf->Line(34.5, $y_achse, 34.5, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(107, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions2[$index2]->anzahl, 1, ',', ''), 0, 'C');
                $pdf->Line(124, $y_achse, 124, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(124, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions2[$index2]->preis, 2, ',', '').' '.EURO, 0, 'R');
                $pdf->Line(141, $y_achse, 141, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(141, $y_achse);
                $pdf->MultiCell(17,  5, $row_gesamtpreis_f2.' '.EURO, 0, 'R');

                $y_achse = $y_achse + $cellHeight;

                $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

                $lfnd_nr++;
                if($y_achse >= 235)
                {
                    $pdf->AddPage();
                    $y_achse = 25;
                }

                $index2++;
            }

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Summe Karosserie:', 0, 'L', 0);

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(133, $y_achse);
            $pdf->MultiCell(25,  5, number_format($cat2_gesamtpreis, 2, ',', '').' '.EURO, 0, 'R');

            $pdf->Line(141, $y_achse, 141, $y_achse+5);
            $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

            $y_achse = $y_achse+5;
        }

        else
        {
            $cat2_gesamtpreis = 0;
        }


        // POSITIONEN CAT 3

        $query_positions3 = $pdo->prepare("SELECT artikel_id, cat_id, anzahl, preis FROM positionen_rechnung WHERE cat_id = 3 AND billing_id =".$_GET['billing_id']);
        $query_positions3->execute();
        $result_positions3 = $query_positions3->fetchAll(PDO::FETCH_OBJ);

        if($query_positions3->rowCount() != 0)
        {

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Positionen Arbeitslohn Lack', 0, 'L', 0);
            $y_achse = $y_achse+5;

            $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

            // DYNAMISCHE AUSGABE POSITIONEN

            $cat3_gesamtpreis = 0;
            $index3 = 0;

            foreach($result_positions3 AS $row_positions3)
            {

                $query_positions_desc3 = $pdo->prepare("SELECT bezeichnung FROM positionen WHERE artikel_id =".$result_positions3[$index3]->artikel_id);
                $query_positions_desc3->execute();
                $result_positions_desc3 = $query_positions_desc3->fetchAll(PDO::FETCH_OBJ);

                $row_gesamtpreis3 = $result_positions3[$index3]->anzahl * $result_positions3[$index3]->preis;
                $row_gesamtpreis_f3 = number_format($result_positions3[$index3]->anzahl * $result_positions3[$index3]->preis, 2, ',', '');
                $cat3_gesamtpreis = $cat3_gesamtpreis + $row_gesamtpreis3;

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(34, $y_achse);
                $pdf->MultiCell(72.5,  5, utf8_decode($result_positions_desc3[0]->bezeichnung), 0, 'L');
                $current_y = $pdf->GetY();
                $cellHeight = $current_y - $y_achse;

                $pdf->Line(107, $y_achse, 107, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(19.5, $y_achse);
                $pdf->MultiCell(15,  5, $lfnd_nr, 0, 'C');
                $pdf->Line(34.5, $y_achse, 34.5, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(107, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions3[$index3]->anzahl, 1, ',', ''), 0, 'C');
                $pdf->Line(124, $y_achse, 124, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(124, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions3[$index3]->preis, 2, ',', '').' '.EURO, 0, 'R');
                $pdf->Line(141, $y_achse, 141, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(141, $y_achse);
                $pdf->MultiCell(17,  5, $row_gesamtpreis_f3.' '.EURO, 0, 'R');

                $y_achse = $y_achse + $cellHeight;

                $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

                $lfnd_nr++;
                if($y_achse >= 235)
                {
                    $pdf->AddPage();
                    $y_achse = 25;
                }

                $index3++;
            }
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Summe Lack:', 0, 'L', 0);

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(133, $y_achse);
            $pdf->MultiCell(25,  5, number_format($cat3_gesamtpreis, 2, ',', '').' '.EURO, 0, 'R');

            $pdf->Line(141, $y_achse, 141, $y_achse+5);
            $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

            $y_achse = $y_achse+5;
        }

        else
        {
            $cat3_gesamtpreis = 0;
        }

        // POSITIONEN CAT 4

        $query_positions4 = $pdo->prepare("SELECT artikel_id, cat_id, anzahl, preis FROM positionen_rechnung WHERE cat_id = 4 AND billing_id =".$_GET['billing_id']);
        $query_positions4->execute();
        $result_positions4 = $query_positions4->fetchAll(PDO::FETCH_OBJ);

        if($query_positions4->rowCount() != 0)
        {
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Positionen Arbeitslohn Elektrik', 0, 'L', 0);
            $y_achse = $y_achse+5;

            $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

            // DYNAMISCHE AUSGABE POSITIONEN

            $cat4_gesamtpreis = 0;
            $index4 = 0;

            foreach($result_positions4 AS $row_positions4)
            {

                $query_positions_desc4 = $pdo->prepare("SELECT bezeichnung FROM positionen WHERE artikel_id =".$result_positions4[$index4]->artikel_id);
                $query_positions_desc4->execute();
                $result_positions_desc4 = $query_positions_desc4->fetchAll(PDO::FETCH_OBJ);

                $row_gesamtpreis4 = $result_positions4[$index4]->anzahl * $result_positions4[$index4]->preis;
                $row_gesamtpreis_f4 = number_format($result_positions4[$index4]->anzahl * $result_positions4[$index4]->preis, 2, ',', '');
                $cat4_gesamtpreis = $cat4_gesamtpreis + $row_gesamtpreis4;

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(34, $y_achse);
                $pdf->MultiCell(72.5,  5, utf8_decode($result_positions_desc4[0]->bezeichnung), 0, 'L');
                $current_y = $pdf->GetY();
                $cellHeight = $current_y - $y_achse;

                $pdf->Line(107, $y_achse, 107, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(19.5, $y_achse);
                $pdf->MultiCell(15,  5, $lfnd_nr, 0, 'C');
                $pdf->Line(34.5, $y_achse, 34.5, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(107, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions4[$index4]->anzahl, 1, ',', ''), 0, 'C');
                $pdf->Line(124, $y_achse, 124, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(124, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions4[$index4]->preis, 2, ',', '').' '.EURO, 0, 'R');
                $pdf->Line(141, $y_achse, 141, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(141, $y_achse);
                $pdf->MultiCell(17,  5, $row_gesamtpreis_f4.' '.EURO, 0, 'R');

                $y_achse = $y_achse + $cellHeight;

                $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

                $lfnd_nr++;
                if($y_achse >= 235)
                {
                    $pdf->AddPage();
                    $y_achse = 25;
                }

                $index4++;
            }

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Summe Elektrik:', 0, 'L', 0);

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(133, $y_achse);
            $pdf->MultiCell(25,  5, number_format($cat4_gesamtpreis, 2, ',', '').' '.EURO, 0, 'R');

            $pdf->Line(141, $y_achse, 141, $y_achse+5);
            $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

            $y_achse = $y_achse+5;
        }

        else
        {
            $cat4_gesamtpreis = 0;
        }

        // POSITIONEN CAT 5

        $query_positions5 = $pdo->prepare("SELECT artikel_id, cat_id, anzahl, preis FROM positionen_rechnung WHERE cat_id = 5 AND billing_id =".$_GET['billing_id']);
        $query_positions5->execute();
        $result_positions5 = $query_positions5->fetchAll(PDO::FETCH_OBJ);

        if($query_positions5->rowCount() != 0)
        {
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Positionen Ersatzteile', 0, 'L', 0);
            $y_achse = $y_achse+5;

            $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

            // DYNAMISCHE AUSGABE POSITIONEN

            $cat5_gesamtpreis = 0;
            $index5 = 0;

            foreach($result_positions5 AS $row_positions5)
            {

                $query_positions_desc5 = $pdo->prepare("SELECT bezeichnung FROM positionen WHERE artikel_id =".$result_positions5[$index5]->artikel_id);
                $query_positions_desc5->execute();
                $result_positions_desc5 = $query_positions_desc5->fetchAll(PDO::FETCH_OBJ);

                $row_gesamtpreis5 = $result_positions5[$index5]->anzahl * $result_positions5[$index5]->preis;
                $row_gesamtpreis_f5 = number_format($result_positions5[$index5]->anzahl * $result_positions5[$index5]->preis, 2, ',', '');
                $cat5_gesamtpreis = $cat5_gesamtpreis + $row_gesamtpreis5;

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(34, $y_achse);
                $pdf->MultiCell(72.5,  5, utf8_decode($result_positions_desc5[0]->bezeichnung), 0, 'L');
                $current_y = $pdf->GetY();
                $cellHeight = $current_y - $y_achse;

                $pdf->Line(107, $y_achse, 107, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(19.5, $y_achse);
                $pdf->MultiCell(15,  5, $lfnd_nr, 0, 'C');
                $pdf->Line(34.5, $y_achse, 34.5, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(107, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions5[$index5]->anzahl, 1, ',', ''), 0, 'C');
                $pdf->Line(124, $y_achse, 124, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(124, $y_achse);
                $pdf->MultiCell(17,  5, number_format($result_positions5[$index5]->preis, 2, ',', '').' '.EURO, 0, 'R');
                $pdf->Line(141, $y_achse, 141, $y_achse+$cellHeight);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY(141, $y_achse);
                $pdf->MultiCell(17,  5, $row_gesamtpreis_f5.' '.EURO, 0, 'R');

                $y_achse = $y_achse + $cellHeight;

                $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

                $lfnd_nr++;
                if($y_achse >= 235)
                {
                    $pdf->AddPage();
                    $y_achse = 25;
                }

                $index5++;
            }

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Summe Ersatzteile:', 0, 'L', 0);

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(133, $y_achse);
            $pdf->MultiCell(25,  5, number_format($cat5_gesamtpreis, 2, ',', '').' '.EURO, 0, 'R');

            $pdf->Line(141, $y_achse, 141, $y_achse+5);
            $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

            $y_achse = $y_achse+5;
        }

        else
        {
            $cat5_gesamtpreis = 0;
        }

        if($result_billing[0]->att_price != 0)
        {
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'Altteile Pfand', 0, 'L', 0);
            $y_achse = $y_achse+5;

            $pdf->Line(19.6, $y_achse, 157.9, $y_achse);

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(34, $y_achse);
            $pdf->MultiCell(72.5,  5, utf8_decode($result_billing[0]->att_desc), 0, 'L');
            $current_y = $pdf->GetY();
            $cellHeight = $current_y - $y_achse;

            $pdf->Line(107, $y_achse, 107, $y_achse+$cellHeight);

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(15,  5, $lfnd_nr, 0, 'C');
            $pdf->Line(34.5, $y_achse, 34.5, $y_achse+$cellHeight);

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(107, $y_achse);
            $pdf->MultiCell(17,  5, number_format($result_billing[0]->att_percent, 1, ',', '').' %', 0, 'C');
            $pdf->Line(124, $y_achse, 124, $y_achse+$cellHeight);

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(124, $y_achse);
            $pdf->MultiCell(17,  5, number_format($result_billing[0]->att_price, 2, ',', '').' '.EURO, 0, 'R');
            $pdf->Line(141, $y_achse, 141, $y_achse+$cellHeight);

            $att_total = $result_billing->att_price / 100 * $result_billing->att_percent;
            $att_total = $att_total / 100 * 19;

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(141, $y_achse);
            $pdf->MultiCell(17,  5, number_format($att_total, 2, ',', '').' '.EURO, 0, 'R');

            $y_achse = $y_achse + $cellHeight;

            $pdf->Line(19.6, $y_achse, 157.9, $y_achse);
        }

        else {
            $att_total = 0;
        }
        //SUMMARY

        $gesamtsumme_netto = $cat0_gesamtpreis + $cat1_gesamtpreis + $cat2_gesamtpreis + $cat3_gesamtpreis + $cat4_gesamtpreis + $cat5_gesamtpreis;
        $gesamtsumme_brutto = $gesamtsumme_netto * 1.19;
        $mwst = $gesamtsumme_netto * 0.19;

        $pdf->SetFont('Helvetica', 'B', 7);
        $pdf->SetXY(19.5, $y_achse);
        $pdf->MultiCell(100,  5, 'Gesamtbetrag (netto):', 0, 'L', 0);

        $pdf->SetFont('Helvetica', 'B', 7);
        $pdf->SetXY(133, $y_achse);
        $pdf->MultiCell(25,  5, number_format($gesamtsumme_netto, 2, ',', '').' '.EURO, 0, 'R');

        $pdf->Line(141, $y_achse, 141, $y_achse+5);
        $pdf->SetLineWidth(0.3);
        $pdf->Line(19.6, $y_achse, 157.9, $y_achse);
        $pdf->SetLineWidth(0.1);
        $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

        $y_achse = $y_achse+5;

        $pdf->SetFont('Helvetica', 'B', 7);
        $pdf->SetXY(19.5, $y_achse);
        $pdf->MultiCell(100,  5, 'USt. (19,00 %):', 0, 'L', 0);

        $pdf->SetFont('Helvetica', 'B', 7);
        $pdf->SetXY(133, $y_achse);
        $pdf->MultiCell(25,  5, number_format($mwst, 2, ',', '').' '.EURO, 0, 'R');

        $pdf->Line(141, $y_achse, 141, $y_achse+5);
        $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

        $y_achse = $y_achse+5;

        if($result_billing[0]->att_price != 0)
        {
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, 'USt. Altteile Pfand:', 0, 'L', 0);

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(133, $y_achse);
            $pdf->MultiCell(25,  5, number_format($att_total, 2, ',', '').' '.EURO, 0, 'R');

            $pdf->Line(141, $y_achse, 141, $y_achse+5);
            $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

            $y_achse = $y_achse+5;
        }

        if($result_billing[0]->tuv != 0)
        {
            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(19.5, $y_achse);
            $pdf->MultiCell(100,  5, utf8_decode('TÜV / AU (brutto):'), 0, 'L', 0);

            $pdf->SetFont('Helvetica', 'B', 7);
            $pdf->SetXY(133, $y_achse);
            $pdf->MultiCell(25,  5, number_format($result_billing[0]->tuv, 2, ',', '').' '.EURO, 0, 'R');

            $pdf->Line(141, $y_achse, 141, $y_achse+5);
            $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

            $y_achse = $y_achse+5;
        }


        $pdf->SetFont('Helvetica', 'B', 7);
        $pdf->SetXY(19.5, $y_achse);
        $pdf->MultiCell(100,  5, 'Gesamtbetrag:', 0, 'L', 0);

        $pdf->SetFont('Helvetica', 'B', 7);
        $pdf->SetXY(133, $y_achse);
        $pdf->MultiCell(25,  5, number_format($gesamtsumme_brutto + $result_billing[0]->tuv + $att_total, 2, ',', '').' '.EURO, 0, 'R');

        $pdf->Line(141, $y_achse, 141, $y_achse+5);
        $pdf->Line(19.6, $y_achse+5, 157.9, $y_achse+5);

        if($notices != "")
        {
            $y_achse = $y_achse+10;

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(19, $y_achse);
            $pdf->MultiCell(150,  5, $notices, 0, 'L', 0);

            $y_achse = $y_achse-5;
        }

        $y_achse = $y_achse+10;

        // ADELTA FINANZ
        if($payment_method == 3) {
            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(19, $y_achse);
            $pdf->MultiCell(140,  5, utf8_decode('Um uns besser auf unsere Dienstleistung konzentrieren zu können, haben wir die Organisation des Rechnungsmanagements an unseren Partner abgegeben. Bitte beachten Sie, dass wir die Forderungen an den Geschäftsbereich Mobilitäts-Markt der Firma ADELTA.FINANZ AG, Marc-Chagall-Straße 2, 40477 Düsseldorf abgetreten haben. Bitte überweisen Sie den obigen Betrag unter Angabe der Rechnungsnummer innerhalb von 30 Tagen mit schuldbefreiender Wirkung auf das unten genannte Konto.'), 0, 'L', 0);

            $y_achse = $y_achse+22;
        }

        // BARVERKAUF
        elseif($payment_method == 2) {
            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(19, $y_achse);
            $pdf->MultiCell(150,  5, utf8_decode('Zahlbar bei Abholung.'), 0, 'L', 0);
        }

        else {
            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetXY(19, $y_achse);
            $pdf->MultiCell(150,  5, utf8_decode('Die Rechnung ist sofort ohne Abzug zahlbar.'), 0, 'L', 0);
        }

        $y_achse = $y_achse+5;

        $pdf->SetFont('Helvetica', '', 7);
        $pdf->SetXY(19, $y_achse);
        $pdf->MultiCell(150,  5, utf8_decode('Ihr B&L Carservice Team bedankt sich für Ihren Auftrag und wünscht allzeit gute Fahrt.'), 0, 'L', 0);

        $y_achse = $y_achse+5;

        $pdf->SetFont('Helvetica', '', 7);
        $pdf->SetXY(19, $y_achse);
        $pdf->MultiCell(150,  5, utf8_decode('Hinweis: Bitte Räder unbedingt nach 20 - 100 KM nachziehen lassen!'), 0, 'L', 0);

        $pdf->Output();

    }

}

else {
    ?>

    <a target="_blank" href="?PDF=OUT">PDF erzeugen</a>

    <?
}
?>

