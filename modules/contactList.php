<?php
    include "../php/functionsContacts.php";
    include "../classes/sqlConnect.php";
?>


<link href="css/datatables.min.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-10">
        <h2><i class="fa fa-user"></i> Kontakte</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Alle Kontakte anzeigen</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-contacts tablePointer" >
                    <thead>
                    <tr>
                        <th>Kunden-Nr.:</th>
                        <th>Name:</th>
                        <th>Straße:</th>
                        <th>PLZ:</th>
                        <th>Ort:</th>
                        <th>Telefon:</th>
                    </tr>
                    </thead>
                    <tbody>

<?php

    $result = contactsGetAll($pdo);
    foreach($result AS $row)
        {
            echo "<tr data-row-contact=\"true\" id=\"".$row->contact_id."\">";
            echo "<td>".$row->kdnnr."</td>";
            echo "<td>".$row->name.", ".$row->vorname."</td>";
            echo "<td>".$row->strasse."</td>";
            echo "<td>".$row->plz."</td>";
            echo "<td>".$row->ort."</td>";
            echo "<td>".$row->telefon_1."</td>";
            echo "</tr>";
        }

?>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Kunden-Nr.:</th>
                        <th>Name:</th>
                        <th>Straße:</th>
                        <th>PLZ:</th>
                        <th>Ort:</th>
                        <th>Telefon:</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $('.dataTables-contacts').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'Kontaktliste'},
                {extend: 'pdf', title: 'Kontaktliste'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });
</script>