<?php
/**
 * Created by PhpStorm.
 * User: KNM-Kurtisevic
 * Date: 17.10.2018
 * Time: 10:26
 */
?>
<div class="hidden">
    <div data-commission-id=""></div>
</div>
<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2><i class="fa fa-sticky-note"></i> Auftrag bearbeiten</h2>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="col-md-6" style="padding-left: 0; padding-right: 0;">
      <div class="col-md-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5><i class="fa fa-info-circle"></i> Kundendaten</h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <div class="row" id="contactContainer">
                      <div class="col-md-12">
                          <div class="inner-addon right-addon">
                              <i class="glyphicon glyphicon-search"></i>
                              <input type="text" data-key="contactName" id="findAdress" class="form-control" placeholder="Namen suchen" />
                          </div>
                      </div>
                  </div>
                  <table class="table table-condensed table-bordered">
                      <thead>
                      <tr class="hidden">
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                      </tr>
                      </thead>
                      <tbody id="kundenTabelle">

                      </tbody>
                  </table>

              </div>
          </div>
          <div class="ibox float-e-margins" id="vehwrapper" style="display: none">
              <div class="ibox-title">
                  <h5><i class="fa fa-automobile"></i> Fahrzeugdaten</h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content vehicleList">
                  <p class="bold">Auswahlliste</p>
                  <select data-carselect style="width: 100%;"></select>
              </div>
          </div>
      </div>
      <div class="col-md-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5><i class="fa fa-pencil"></i> Notizen</h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">

                  <textarea data-key="notices" class="form-control"></textarea>

              </div>
          </div>
      </div>
  </div>
  <div class="col-md-6" style="padding-left: 0; padding-right: 0;">
      <div class="col-md-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5><i class="fa fa-info-circle"></i><i class="fas fa-info-square"></i> Auftragsdaten</h5>
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content" id="contractData">
                  <div class="row form-row">
                      <div class="col-md-4">Kunden-Nr.:</div>
                      <div class="col-md-8"><input class="form-control" data-key="contact_id_commission" type="text" disabled></div>
                  </div>
                  <div class="row form-row">
                      <div class="col-md-4">Auftrags-Nr.:</div>
                      <div class="col-md-8"><input class="form-control" data-key="commission_nr" type="text" disabled></div>
                  </div>
                  <div class="row form-row">
                      <div class="col-md-4">Annahmedatum:</div>
                      <div class="col-md-8"><input class="form-control" data-date type="text" data-key="date_acception" disabled></div>
                  </div>
                  <div class="row form-row">
                      <div class="col-md-4">Angenommen von:</div>
                      <div class="col-md-8">
                          <select data-key="operator" name="sachbearbeiter" class="form-control">
                              <option value="0">Sachbearbeiter wählen:</option>
                              <option value="Eike Lauenstein">Eike Lauenstein</option>
                          </select>
                      </div>
                  </div>
                  <div class="row form-row">
                      <div class="col-md-4">Auftrag Mitarbeiter zuweisen:</div>
                      <div class="col-md-8">
                          <select data-key="selectedEmployee" name="mitarbeiter" class="form-control">
                              <option value="">Mitarbeiter wählen</option>
                          </select>
                      </div>
                  </div>
                  <div class="row form-row">
                      <div class="col-md-4">Termin:</div>
                      <div class="col-md-8"><input data-key="date_target" data-date class="form-control" type="text"></div>
                  </div>
                  <div class="row form-row">
                      <div class="col-md-4">KM-Stand bei Annahme:</div>
                      <div class="col-md-8"><input data-key="km_acception" class="form-control" type="text"></div>
                  </div>


              </div>
          </div>
      </div>
  </div>


<form id="formArbeitslohn">

    <!-- / ARBEITSLOHN BEGINN -->

    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-info-circle"></i> Pauschale Arbeitslohn Positionen</h5>
                <div class="ibox-tools">
                    <select data-key="workRewarding" id="selectArbeitslohn">
                        <option value="">Pauschale Positionen hinzufügen:</option>
                        <option value="true">Aktiv</option>
                        <option value="false">Inaktiv</option>
                    </select>
                    <a class="collapse-link right" id="selectArbeitslohnSave" style="display: none;">
                        <i class="fa fa-floppy-o" id="selectArbeitslohnSaveBtn" style="font-size: 17px; color: #666;"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" id="containerArbeitslohn" style="display: none;">

                <!-- Position Row -->
                    <div class="row" style="padding-top: 5px; padding-bottom: 5px;">
                        <div class="col-md-1">Position:</div>
                        <div class="col-md-4">Bezeichnung:</div>
                        <div class="col-md-1">Menge:</div>
                        <div class="col-md-2">E-Preis AW:</div>
                        <div class="col-md-2">Gesamtpreis:</div>
                        <div class="col-md-2">Aktion:</div>
                    </div>

                    <div class="row" id="rowArbeitslohn1" style="padding-top: 5px; padding-bottom: 5px;">
                        <div class="col-md-1"><input class="form-control lfn" data-key="lfn_1" type="text" style="border: 1px solid #999;" value="1" readonly></div>
                        <div class="col-md-4"><input class="form-control" data-key="bezeichnung_1" type="text" style="border: 1px solid #999;"></div>
                        <div class="col-md-1"><input class="form-control qty" data-key="anzahl_1" onkeypress="validateNumber(event)" type="text" name="menge" style="border: 1px solid #999;"></div>
                        <div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input class="form-control amount" data-key="preis_1" onkeypress="validateNumber(event)" type="text" style="border: 1px solid #999;"></div></div>
                        <div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input class="form-control amount" data-totalprice type="text" disabled style="background: #eee;border: 1px solid #999;" value="0"></div></div>
                        <div class="col-md-2"><input class="form-control addRowArbeitslohn" type="submit" value="Reihe hinzufügen"></div>
                    </div>

                <hr class="divider-full" id="dividerArbeitslohn" />
                    <div class="row">
                        <div class="col-md-8 text-right">Summe Pauschale Arbeitslohn Positionen:</div>
                        <div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-areatotalprice type="text" style="background: #eee;border: 1px solid #999;"></div></div>
                        <div class="col-md-2"></div>
                    </div>

                <!-- /Position Row -->

            </div>
        </div>
    </div>

    <!-- / ARBEITSLOHN ENDE -->




<!-- / SUMMARY BEGINN -->

<div class="col-md-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5><i class="fa fa-info-circle"></i> Zusammenfassung:</h5>
        </div>
        <div class="ibox-content" id="containerSummary">

            <div class="row" style="padding-top: 5px; padding-bottom: 5px;">
                <div class="col-md-8 text-right">Gesamt netto:</div>
                <div class="col-md-2"><input id="resultRaw" data-key="resultRaw" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
                <div class="col-md-2"></div>
            </div>

            <div class="row" style="padding-top: 5px; padding-bottom: 5px;">
                <div class="col-md-8 text-right">MwSt:</div>
                <div class="col-md-2"><input id="taxFromResult" data-key="taxFromResult" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
                <div class="col-md-2"></div>
            </div>

            <div class="row" style="padding-top: 5px; padding-bottom: 5px;">
                <div class="col-md-8 text-right">Gesamt brutto:</div>
                <div class="col-md-2"><input id="resultWithTax" data-key="resultWithTax" disabled class="form-control" type="text" style="border: 1px solid #999;" readonly></div>
                <div class="col-md-2"></div>
            </div>

        </div>
    </div>
</div>

<!-- / SUMMARY ENDE -->

<div class="col-md-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <button class="btn btn-sm btn-primary pull-right sub-button update" type="submit" id="assignmentAdd" style="margin-top: 15px;"><i class="fa fa-floppy-o"></i> <strong>Auftrag speichern</strong></button>
            </div>
        </div>
    </div>
</div>

</form>

</div>
