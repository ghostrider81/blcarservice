<?php

if ( isset($_GET['do']) ) {
    if ($_GET['do'] == 'exportAssignment') {

        include "../classes/sqlConnect.php";
        include "../php/functions.php";
        include "../php/functionsAssignments.php";
        include_once('../pdf/fpdf.php');


        class PDF extends FPDF

        {

            //HEADER ERSTELLEN

            function Header()

            {

                // Linienfarbe auf Schwarz einstellen
                $this->SetDrawColor(0, 0, 0);

                // Linienbreite einstellen, 0.5 mm
                $this->SetLineWidth(0.3);

                //Select Arial bold 15
                $this->SetFont('Arial','B',15);

                //Move to the right
                $this->Cell(80);

                //Framed title
                $this->Cell(30,10,'Werkstatt Auftrag',0,0,'C');

                //Line break
                $this->Ln(20);

            }

            // FOOTER ERSTELLEN

            function Footer()

            {

                // Über dem unteren Seitenrand positionieren
                $this->SetY(-22);
                // Schriftart festlegen
                $this->SetFont('Helvetica','',9);
                // Zentrierte Ausgabe der Seitenzahl
                $this->Cell(0,10,'Seite '.$this->PageNo(),0,0,'C');
                // Fusslinie
                $this->Line(20, 257, 190, 257);
                // Fusszeile setzen
                $this->SetXY(20, 260);
                $this->Multicell(63,4,"B&L Carservice GmbH\nAuf dem Sand 30c\n40721 Hilden", 0, 'L');
                $this->SetXY(83, 260);
                $this->Multicell(63,4,"\nTel.: 0 21 03 - 45 88 7\nFax: 0 21 03 - 25 82 44", 0, 'L');
                $this->SetXY(146, 260);
                $this->Multicell(63,4,"\nMail: info@bl-carservice.de\nInternet: www.bl-carservice.de", 0, 'L');

            }

        }



        //SEITENEIGENSCHAFTEN

        define('EURO',chr(128));

        $pdf = new PDF('P', 'mm', 'A4');

        $pdf->SetFont('Helvetica', '', 10);

        $pdf->SetDrawColor(200);


        //DATEN AUS DB HOLEN

        $query_commission = $pdo->prepare("SELECT contact_id, car_id, commission_nr, date_acception, date_target, operator, notices, signature FROM auftrag WHERE commission_id =".$_GET['commission_id']);
        $query_commission->execute();
        $result_commission = $query_commission->fetchAll(PDO::FETCH_OBJ);

        $query_contact = $pdo->prepare("SELECT anrede, name, vorname, strasse, plz, ort, telefon_1, kdnnr FROM kontakte WHERE contact_id =".$result_commission[0]->contact_id);
        $query_contact->execute();
        $result_contact = $query_contact->fetchAll(PDO::FETCH_OBJ);

        $query_car = $pdo->prepare("SELECT hersteller, modell, kennzeichen, identnr, erstzulassung,kbanr, hu, au, km FROM fahrzeuge WHERE car_id =".$result_commission[0]->car_id);
        $query_car->execute();
        $result_car = $query_car->fetchAll(PDO::FETCH_OBJ);



        // Daten aufbereiten

        $anrede = utf8_decode($result_contact[0]->anrede);
        $name = utf8_decode($result_contact[0]->name);
        $vorname = utf8_decode($result_contact[0]->vorname);
        $strasse = utf8_decode($result_contact[0]->strasse);
        $plz = $result_contact[0]->plz;
        $ort = utf8_decode($result_contact[0]->ort);
        $telefon = $result_contact[0]->telefon_1;
        $kunden_nr = $result_contact[0]->kdnnr;

        $auftrag_nr = $result_commission[0]->commission_nr;
        $annahme_date = makeDateFromTimestamp($result_commission[0]->date_acception);
        $termin_date = makeDateFromTimestamp($result_commission[0]->date_target);
        $sachbearbeiter = utf8_decode($result_commission[0]->operator);


        $uebertrag = utf8_decode('Übertrag');



        $rg_written = "date_written";
        $rg_nummer = "rg_nummer";
        $arbeitsbeschreibung = "arbeitsbeschreibung";
        $auftraggeber = "auftraggeber";



        $pdf->AddPage();

        $y_achse = 25;

        // ANSCHRIFT KUNDE LINKS

        $pdf->SetFont('Helvetica', 'B', 11);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(80,  5, 'Anschrift Kunde', 0, 'L');

        // Linienfarbe auf Schwarz einstellen
        $pdf->SetDrawColor(0, 0, 0);

        // Linienbreite einstellen, 0.5 mm
        $pdf->SetLineWidth(0.2);

        $y_achse = 30;
        $current_y = 30;

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(50, $y_achse);
        $pdf->MultiCell(50,  5, $name.", ".$vorname, 1, 'L');

        $y_achse = $pdf->GetY();
        $cell_height = $current_y - $y_achse;

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(30,  $cell_height, 'Name:', 1, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(30,  5, 'Strasse:', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(50, $y_achse);
        $pdf->MultiCell(50,  5, $strasse, 1, 'L');

        $y_achse = $y_achse + 5;

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(30,  5, 'PLZ / Wohnort:', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(50, $y_achse);
        $pdf->MultiCell(50,  5, $plz.' '.$ort, 1, 'L');

        $y_achse = $y_achse + 5;

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(30,  5, 'Telefon:', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(50, $y_achse);
        $pdf->MultiCell(50,  5, $telefon, 1, 'L');


        // DATEN RECHTS

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(110, 30);
        $pdf->MultiCell(30,  5, 'Auftrags-Nr.:', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(140, 30);
        $pdf->MultiCell(50,  5, $auftrag_nr, 1, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(110, 35);
        $pdf->MultiCell(30,  5, 'Kunden-Nr.:', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(140, 35);
        $pdf->MultiCell(50,  5, $kunden_nr, 1, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(110, 40);
        $pdf->MultiCell(30,  5, 'Annahmedatum:', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(140, 40);
        $pdf->MultiCell(50,  5, $annahme_date, 1, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(110, 45);
        $pdf->MultiCell(30,  5, 'Sachbearbeiter:', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(140, 45);
        $pdf->MultiCell(50,  5, $sachbearbeiter, 1, 'L');

        $current_y = $pdf->GetY();

        if($current_y = 50)
        {
            $y_achse = $y_achse + 10;
        }

        elseif($current_y = 45)
        {
            $y_achse = $y_achse + 5;
        }

        // FAHRZEUG DATEN REIHE 1

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(42,  5, 'Hersteller / Modell:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(62, $y_achse);
        $pdf->MultiCell(43,  5, 'Kennzeichen:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(105, $y_achse);
        $pdf->MultiCell(43,  5, 'Ident-Nr.:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(148, $y_achse);
        $pdf->MultiCell(42,  5, 'Kilometerstand:', 0, 'L');

        $y_achse = $y_achse + 5;

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(42,  5, utf8_decode($result_car[0]->hersteller)." ".utf8_decode($result_car[0]->modell), 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(62, $y_achse);
        $pdf->MultiCell(43,  5, utf8_decode($result_car[0]->kennzeichen), 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(105, $y_achse);
        $pdf->MultiCell(43,  5, $result_car[0]->identnr, 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(148, $y_achse);
        $pdf->MultiCell(42,  5, $result_car[0]->km, 1, 'L');

        $y_achse = $y_achse + 10;

        // FAHRZEUG DATEN REIHE 2

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(42,  5, 'Erstzulassung:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(62, $y_achse);
        $pdf->MultiCell(43,  5, 'KBA-Nr.:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(105, $y_achse);
        $pdf->MultiCell(43,  5, 'HU:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(148, $y_achse);
        $pdf->MultiCell(42,  5, 'AU:', 0, 'L');

        $y_achse = $y_achse + 5;

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(42,  5, makeDateFromTimestamp($result_car[0]->erstzulassung), 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(62, $y_achse);
        $pdf->MultiCell(43,  5, $result_car[0]->kbanr, 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(105, $y_achse);
        $pdf->MultiCell(43,  5, makeDateFromTimestamp($result_car[0]->hu), 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(148, $y_achse);
        $pdf->MultiCell(42,  5, makeDateFromTimestamp($result_car[0]->au), 1, 'L');

        $y_achse = $y_achse + 10;

        // POSITIONEN ÜBERSCHRIFTEN

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(14,  5, 'Nr.:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(34, $y_achse);
        $pdf->MultiCell(14,  5, 'Kat.:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(48, $y_achse);
        $pdf->MultiCell(74,  5, 'Arbeitstext:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(122, $y_achse);
        $pdf->MultiCell(68,  5, 'Info:', 0, 'L');

        // DYNAMISCHE AUSGABE POSITIONEN

        $y_achse = $y_achse + 5;
        $lfnd_nr = 1;

        $query_positions = $pdo->prepare("SELECT artikel_id, cat_id FROM positionen_auftrag WHERE commission_id =".$_GET['commission_id']);
        $query_positions->execute();
        $result_positions = $query_positions->fetchAll(PDO::FETCH_OBJ);


        $i = 0;
        foreach($result_positions AS $row_positions)
        {

            if($row_positions->cat_id != 5)


            {
                if($row_positions->cat_id == 0)
                {
                    $cat = "AL";
                }

                elseif($row_positions->cat_id == 1)
                {
                    $cat = "MEC";
                }

                elseif($row_positions->cat_id == 2)
                {
                    $cat = "KAR";
                }

                elseif($row_positions->cat_id == 3)
                {
                    $cat = "LAC";
                }

                elseif($row_positions->cat_id == 4)
                {
                    $cat = "ELE";
                }

                elseif($row_positions->cat_id == 5)
                {
                    $cat = "MAT";
                }


                $query_positions_desc = $pdo->prepare("SELECT bezeichnung FROM positionen WHERE artikel_id =".$result_positions[$i]->artikel_id);
                $query_positions_desc->execute();
                $result_positions_desc = $query_positions_desc->fetchAll(PDO::FETCH_OBJ);


                $pdf->SetFont('Helvetica', '', 10);
                $pdf->SetXY(48, $y_achse);
                $pdf->MultiCell(74,  5, utf8_decode($result_positions_desc[0]->bezeichnung), 1, 'L');
                $current_y = $pdf->GetY();
                $cellHeight = $current_y - $y_achse;

                $pdf->SetFont('Helvetica', '', 10);
                $pdf->SetXY(20, $y_achse);
                $pdf->MultiCell(14,  $cellHeight, $lfnd_nr, 1, 'L');

                $pdf->SetFont('Helvetica', '', 10);
                $pdf->SetXY(34, $y_achse);
                $pdf->MultiCell(14,  $cellHeight, $cat, 1, 'L');

                $pdf->SetFont('Helvetica', '', 10);
                $pdf->SetXY(122, $y_achse);
                $pdf->MultiCell(68,  $cellHeight, '', 1, 'L');

                $y_achse = $y_achse + $cellHeight;
                $lfnd_nr++;
                if($y_achse >= 235)
                {
                    $pdf->AddPage();
                    $y_achse = 25;
                }
            }

            $i++;
        }

        if($y_achse >= 130)
        {
            $pdf->AddPage();
            $y_achse = 25;
        }

        $y_achse = $y_achse + 5;
        $pdf->Line(20, $y_achse, 190, $y_achse);

        $y_achse = $y_achse + 5;
        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(50,  5, 'Termin:  '.makeDateFromTimestamp($result_commission[0]->date_target), 0, 'L');

        $y_achse = $y_achse + 5;
        $pdf->Line(35, $y_achse, 55, $y_achse);

        $y_achse = $y_achse + 5;
        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(50,  5, 'Auftragserweiterung am:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(90, $y_achse);
        $pdf->MultiCell(50,  5, 'durch:', 0, 'L');

        $y_achse = $y_achse + 5;
        $pdf->Line(64, $y_achse, 90, $y_achse);
        $pdf->Line(104, $y_achse, 190, $y_achse);

        $y_achse = $y_achse + 5;

        // ERWEITERUNGS POSITIONEN ÜBERSCHRIFTEN

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(14,  5, 'Nr.:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(34, $y_achse);
        $pdf->MultiCell(14,  5, 'Kat.:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(48, $y_achse);
        $pdf->MultiCell(74,  5, 'Arbeitstext:', 0, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(122, $y_achse);
        $pdf->MultiCell(68,  5, 'Info:', 0, 'L');

        // TABELLE POSITIONEN ERWEITERUNG

        $y_achse = $y_achse + 5;

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(14,  5, '', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(34, $y_achse);
        $pdf->MultiCell(14,  5, '', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(48, $y_achse);
        $pdf->MultiCell(74,  5, '', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(122, $y_achse);
        $pdf->MultiCell(68,  5, '', 1, 'L');

        $y_achse = $y_achse + 5;

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(14,  5, '', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(34, $y_achse);
        $pdf->MultiCell(14,  5, '', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(48, $y_achse);
        $pdf->MultiCell(74,  5, '', 1, 'L');

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(122, $y_achse);
        $pdf->MultiCell(68,  5, '', 1, 'L');

        // DISCLAIMER

        $y_achse = $y_achse + 10;


        $pdf->SetFont('Helvetica', '', 8);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(170, 4, utf8_decode("Mit Ihrer Unterschrift erkennen Sie die im Servicebereich aushängenden KFZ-Reparaturbedingungen an.\n\nDatenbestimmungen:\nMit meiner Unterschrift willige ich den gesetzlich geltenden Datenschutzbestimmungen ein. Die B&L Carservice GmbH darf meine Daten für interne Zwecke speichern und für Werbemaßnahmen wie z.B. Infopost nutzen. Der Widerruf bedarf der Schriftform."), 0, 'L');

        $y_achse = $y_achse + 30;

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(25,  5, 'Notizen:', 0, 'L');

        $y_achse = $y_achse + 5;

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(170,  5, utf8_decode($result_commission[0]->notices), 1, 'L');

        $y_achse = $y_achse + 20;

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(40,  5, 'Endabnahme durch:', 0, 'L');

        $y_achse = $y_achse + 5;

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(20, $y_achse);
        $pdf->MultiCell(60,  5, '', 1, 'L');

        $y_achse = $y_achse + 5;



        $pdf->SetFont('Helvetica', '', 8);
        $pdf->SetXY(120, $y_achse);

        if ($result_commission[0]->signature != "" && file_exists("../signaturepad/".$result_commission[0]->signature)) {
            $signaturepad = "../signaturepad/".$result_commission[0]->signature;
            $pdf->Image($signaturepad, 122, $y_achse-18, 22);
        }

        $pdf->Line(120, $y_achse, 190, $y_achse);
        $pdf->MultiCell(70, 4, utf8_decode("Unterschrift Kunde"), 0, 'M');


        $pdf->Output('kunde.pdf', 'I');


    }

}

else {
    ?>

    <a target="_blank" href="assignmentExport.php?do=exportAssignment">PDF erzeugen</a>

    <?
}
?>

