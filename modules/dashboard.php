<div class="row wrapper border-bottom white-bg page-heading row-fix">
    <div class="col-lg-10">
        <h2>Dashboard</h2>
    </div>
    <div class="col-lg-2">
        <button class="btn btn-sm btn-primary pull-right settings-btn" type="submit"><i class="fa fa-cogs"></i> <strong>Dashboard bearbeiten</strong></button>
    </div>
</div>

