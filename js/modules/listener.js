var GlobalListener = function() {
    console.log('GlobalListener loaded');
    this.GlobalBoxes = [
        {   // Arbeitslohn
            'ID' : 'Arbeitslohn',
            'inputID' : "#containerArbeitslohn input[data-key^='anzahl'], input[data-key^='preis']",
            'boxID' : '#containerArbeitslohn',

            'rowDividerID' : '#dividerArbeitslohn',
            'rowID' : "div[id^='rowArbeitslohn']",
            'addRowID' : '.addRowArbeitslohn',
            'deleteRowID' : '.deleteRowArbeitslohn',

            'selectCatID' : '#selectArbeitslohn',
            'selectSaveCatID' : '#selectArbeitslohnSave'
        },
        {   // Mechanik
            'ID' : 'Mechanik',
            'inputID' : "#containerMechanik input[data-key^='anzahl'], input[data-key^='preis']",
            'boxID' : '#containerMechanik',

            'rowDividerID' : '#dividerMechanik',
            'rowID' : "div[id^='rowMechanik']",
            'addRowID' : '.addRowMechanik',
            'deleteRowID' : '.deleteRowMechanik',

            'selectCatID' : '#awMechanik',
            'selectSaveCatID' : '#awMechanikSave'
        },
        {   // Karosserie
            'ID' : 'Karosserie',
            'inputID' : "#containerKarosserie input[data-key^='anzahl'], input[data-key^='preis']",
            'boxID' : '#containerKarosserie',

            'rowDividerID' : '#dividerKarosserie',
            'rowID' : "div[id^='rowKarosserie']",
            'addRowID' : '.addRowKarosserie',
            'deleteRowID' : '.deleteRowKarosserie',

            'selectCatID' : '#awKarosserie',
            'selectSaveCatID' : '#awKarosserieSave'
        },
        {   // Lack
            'ID' : 'Lack',
            'inputID' : "#containerLack input[data-key^='anzahl'], input[data-key^='preis']",
            'boxID' : '#containerLack',

            'rowDividerID' : '#dividerLack',
            'rowID' : "div[id^='rowLack']",
            'addRowID' : '.addRowLack',
            'deleteRowID' : '.deleteRowLack',

            'selectCatID' : '#awLack',
            'selectSaveCatID' : '#awLackSave'
        },
        {   // Elektrik
            'ID' : 'Elektrik',
            'inputID' : "#containerElektrik input[data-key^='anzahl'], input[data-key^='preis']",
            'boxID' : '#containerElektrik',

            'rowDividerID' : '#dividerElektrik',
            'rowID' : "div[id^='rowElektrik']",
            'addRowID' : '.addRowElektrik',
            'deleteRowID' : '.deleteRowElektrik',

            'selectCatID' : '#awElektrik',
            'selectSaveCatID' : '#awElektrikSave'
        },
        {   // Material
            'ID' : 'Material',
            'inputID' : "#containerMaterial input[data-key^='anzahl'], input[data-key^='preis']",
            'boxID' : '#containerMaterial',

            'rowDividerID' : '#dividerMaterial',
            'rowID' : "div[id^='rowMaterial']",
            'addRowID' : '.addRowMaterial',
            'deleteRowID' : '.deleteRowMaterial',

            'selectCatID' : '#selectMaterial',
            'selectSaveCatID' : '#selectMaterialSave'
        },
        {   // Tuev
            'ID' : 'Tuev',
            'inputID' : "#containerTuev input[data-key^='anzahl'], input[data-key^='preis']",
            'boxID' : '#containerTuev',
            'rowID' : "div[id^='rowTuev']",
            'deleteRowID' : '.deleteRowTuev',
            'selectCatID' : '#selectTuev',
            'selectSaveCatID' : '#selectTuevSave'
        },

        {   // Att
            'ID' : 'Att',
            'inputID' : "#containerAtt input[data-key^='satz'], input[data-key^='preis']",
            'boxID' : '#containerAtt',

            'rowDividerID' : '#dividerMaterial',
            'rowID' : "div[id^='rowAtt']",
            'addRowID' : '.addRowAtt',
            'deleteRowID' : '.deleteRowAtt',

            'selectCatID' : '#selectAtt',
            'selectSaveCatID' : '#selectAttSave'
        },
        {   // Rabatt
            'ID' : 'Att',
            'inputID' : "#containerRabatt input[data-key='rabatt']",
            'boxID' : '#containerRabatt',

            'rowDividerID' : '#dividerRabatt',
            'rowID' : "div[id^='rowRabatt']",
            'addRowID' : '.addRowAtt',
            'deleteRowID' : '.deleteRowAtt',

            'selectCatID' : '#selectRabatt',
            'selectSaveCatID' : '#selectRabattSave'
        },
    ];

    this.searchDescID =  'input[data-key^="bezeichnung"]';
    this.datePickerID = '[data-date]';
    this.findVehicleID = '#findAdress';
    this.toggleCustomerSearchContainerID = '#toggleCustomerSearch button';
    this.inputArray = ['input[data-key^="anzahl"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'];
    this.init();

};
GlobalListener.prototype = {
    init: function () {
        this.setupHandler();
        this.initGlobalBoxes();
        this.searchDescriptionInput();
    },
    setupHandler: function () {
          $('body').on('click', this.datePickerID, this.setDatePicker.bind(this))
          $('body').on('click', this.findVehicleID, this.getVehicleInfo.bind(this));
          $('body').on('click', this.toggleCustomerSearchContainerID, this.toggleCustomerSearch.bind(this))
    },
    // Main Function
    initGlobalBoxes: function () {
        var that = this;
        var GB = this.GlobalBoxes;
        GB.forEach(function (object, index) {
            // update Calculate
            $('body').on('keyup', object.inputID, function () {
                that.updateCalculateBoxes(object.boxID, object.rowID);
            });
            // add row
            if(object.boxID.indexOf('#containerTuev') == -1)
            {
                $('body').on('click', object.addRowID, function (e) {
                    that.addRow(e, object.rowDividerID, object.boxID, object.rowID, object.ID);
                });
            }
            // delte row
            $('body').on('click', object.deleteRowID, function (e) {
                that.deleteRow(e, object.boxID, object.rowID, object.ID);
            })

            // change select
            $('body').on('change', object.selectCatID, function (e) {
                console.log('change select is started', $(e.currentTarget).val())
                $(e.currentTarget).val() != "" ?
                    $(object.selectSaveCatID).css({'display':'inline'})
                    :$(object.selectSaveCatID).css({'display':'none'});
            })
            $('body').on('click', object.selectSaveCatID+'Btn', function (e) {
                $(object.selectCatID).prop('disabled', 'disabled');
                $(object.selectSaveCatID).css({'display':'none'});
                if(object.selectCatID.indexOf('Lack') > -1) {
                    $(object.boxID+' .aw').val(setCurrencyEUR($(object.selectCatID).val()));
                }
                else if (object.selectCatID.indexOf('Elektrik') > -1) {
                    $(object.boxID+' .aw').val(setCurrencyEUR($(object.selectCatID).val()));
                }
                else if (object.selectCatID.indexOf('Karosserie') > -1) {
                    $(object.boxID+' .aw').val(setCurrencyEUR($(object.selectCatID).val()));
                }
                else if (object.selectCatID.indexOf('Mechanik') > -1) {
                    $(object.boxID+' .aw').val(setCurrencyEUR($(object.selectCatID).val()));
                }
            })
        })







    },
     updateCalculateBoxes: function (parentContainerSelector, rowSelector) {
        // loop each rowContainer with USERINPUT
         var rabatt = 0 ;
        $(""+parentContainerSelector+" "+rowSelector).each(function (index, object) {
            if(parentContainerSelector.indexOf('containerAtt') > -1) {
                var tmp_Menge = $(object).find("input[data-key^='satz']").val().replace(',', '.') || 0.00;
                var tmp_Preis = $(object).find("input[data-key^='preis']").val().replace(',', '.') || 0.00;
                var resultRaw = (tmp_Menge * tmp_Preis) / 100;
                console.log('satz !! ');

            } else if(parentContainerSelector.indexOf('containerRabatt') > -1) {
                    rabatt = $(object).find("input[data-key^='rabatt']").val().replace(',', '.') || 0.00;
                    console.log('rabatt !? ',  $(object).find("input[data-key='rabatt']").val());
                    resultRaw = 0.00;
            } else {
                var tmp_Menge = $(object).find("input[data-key^='anzahl']").val().replace(',', '.') || 0.00;
                var tmp_Preis = $(object).find("input[data-key^='preis']").val().replace(',', '.') || 0.00;
                var resultRaw = tmp_Menge * tmp_Preis;

            }
                var result = (Math.round(resultRaw * 100) / 100).toFixed(2).toString().replace('.', ',');
                //result = parseFloat(result);
                // each row Currency result
                //console.log('result ', result)
                $(object).find('input[data-totalprice]').val(result);

        });



        // set areatotalprice
        var areaTotalPrice = 0.00;
        $(''+parentContainerSelector+'').find("input[data-totalprice]").each(function (index, object) {
            var totalRowPrice = $(object).val().replace(',', '.') || 0.00;
            if(totalRowPrice > 0.00) {
                areaTotalPrice = parseFloat(totalRowPrice) + parseFloat(areaTotalPrice);
            }


        });


        var areaTotalPriceView = (Math.round(areaTotalPrice * 100) / 100).toFixed(2).toString().replace('.', ',');
        $(''+parentContainerSelector+'').find("input[data-areatotalprice]").val(areaTotalPriceView);

        // set totalAreaPrice, tax, Result
        var totalAreaPrice = 0.00;
        var totalArr = [];
        //var totalDezi = new Decimal();
        //console.log('Dezi before ', totalDezi);
        $('input[data-areatotalprice]').each(function (index, object) {

            var areaPrice = $(object).val().replace(',', '.') || 0.00;
            //totalDezi += new Decimal(areaPrice);
            if(areaPrice > 0.00)
            {
                totalAreaPrice = parseFloat(areaPrice) + parseFloat(totalAreaPrice);


            }
            totalArr.push(areaPrice);
            //console.log('area ', areaPrice, ' type :: ', typeof areaPrice);
           // console.log('total AREA PRICE ', totalAreaPrice);
        });
         console.log('se  lisssz :: ', seperatedTotalPriceForRabat);
        //console.log('Dezi spezi ', totalDezi);
        //console.clear();
        //console.log('AREA PRICE :: ', totalAreaPrice,totalArr);
        //console.log('area price ',totalAreaPrice);
        console.log('totalArr ', totalAreaPrice);
        //totalAreaPrice = parseFloat(totalAreaPrice).toFixed(2);


        // Result Summary
        var tax = parseFloat(totalAreaPrice) * 0.19;
        //tax = parseFloat(Math.round(tax *100)/100).toFixed(2);
        var inspectionFee = 0.00;
        try {
            inspectionFee = $('#carInspectionFeeInput').val().replace(',', '.') || 0.00;
        } catch (e) { }
        //console.log('myTOTAL PRICE ', inspectionFee);
         var sepList = ['containerMechanik',
             'containerElektrik','containerKarosserie',
             'containerLack']
        var seperatedTotalPriceForRabat = 0.00;
         sepList.forEach(function (item, indi) {
             $('#'+item).find('input[data-areatotalprice]').each(function (indix, objix) {
                 var value = $(objix).val().replace(',', '.') || 0.00;
                 if(value > 0) {
                     seperatedTotalPriceForRabat = parseFloat(seperatedTotalPriceForRabat) + parseFloat(value);
                 }
             })
         });

         console.log('.. ', seperatedTotalPriceForRabat);


        console.log('rabatz', rabatt);
         var resultRabatt = (parseFloat(seperatedTotalPriceForRabat) * parseFloat(rabatt)) / 100 || 0;
        console.log('result Rabat:: ', resultRabatt);

        //inspectionFee = parseFloat(Math.round(inspectionFee *100)/100);

        //console.log('inspect fee ', inspectionFee);
         console.log('  se se se ', seperatedTotalPriceForRabat);
         var resultWithRabatt = parseFloat(totalAreaPrice) - parseFloat(resultRabatt);

        var resultWithTax = parseFloat(resultWithRabatt) + parseFloat(tax) + parseFloat(inspectionFee);

        //console.log([inspectionFee], [resultWithTax], [totalAreaPrice], [tax]);

        //resultWithTax = parseFloat(resultWithTax);

        var inspectionFeeView = (Math.round(parseFloat(inspectionFee) * 100) / 100).toFixed(2).toString().replace('.', ',');
        var resultRabattView = (Math.round(parseFloat(resultRabatt) * 100) / 100).toFixed(2).toString().replace('.', ',');
        var totalAreaPriceView = (Math.round(parseFloat(totalAreaPrice) * 100) / 100).toFixed(2).toString().replace('.', ',');
        var taxView = (Math.round(parseFloat(tax) * 100) / 100).toFixed(2).toString().replace('.', ',');
       // var inspectionFeeView = (Math.round(inspectionFee * 100) / 100).toFixed(2).toString().replace('.', ',')
        var resultWithTaxView = (Math.round(parseFloat(resultWithTax) * 100) / 100).toFixed(2).toString().replace('.', ',');

        //console.log('result with tax innner ',resultWithTax);
        $('#tuv_totalprice').val(inspectionFeeView);
        $('#rabattResult').val(resultRabattView);
        $('#resultRaw').val(totalAreaPriceView);
        $('#taxFromResult').val(taxView);
        $('#carInspectionFee').val(inspectionFeeView);
        $('#resultWithTax').val(resultWithTaxView);
    },
    searchDescriptionInput: function() {
        // description
        $('body').on('keyup', this.searchDescID, function (e) {

            $($(this)).autocomplete({
                source: function (request, response) {
                    //$.post("modules/contactSearch.php", request, response);
                    var dataPaket = {
                        'state': 'searchForPositions',
                        'searchKey': request.term

                    };

                    $.ajax({
                        type: "POST",
                        url:"php/functionsAssignments.php",
                        data: dataPaket,
                        success: response,
                        dataType: 'json'
                    });
                },
                minLength: 3,
                messages: {
                    noResults: '',
                    results: function() {}
                },
                select: function(event, ui) {
                    console.log('Bezeich')
                    //$('#eee').val(ui.item.id);
                    //getVehicleInformation(ui.item);
                }
            });
        });


    },
    deleteRow: function(e, parentContainerSelector, rowID, ID) {
        e.preventDefault();
        var that = this;
        $(e.currentTarget).parent().parent().toggle(function() {
            $(this).remove();
            that.numberMyRows(parentContainerSelector, rowID, ID);
            that.updateCalculateBoxes(parentContainerSelector, rowID);

        });



    },
    addRow: function (e, rowDividerID, boxID, rowID, ID) {
        e.preventDefault();

        var caption = rowDividerID.replace('#divider', '');
        var cap = caption + '_0';

        $(e.currentTarget).val('Löschen');
        $(e.currentTarget).attr('class', 'form-control deleteRow'+ID);


        var awprice = $(boxID).find('.aw').val() || '';
        if(awprice != '') {
            awprice = setCurrencyEUR(awprice.replace(/,/g, '.'));
        }

        $(rowDividerID).before('<div class="row" class="row'+cap+'" id="row'+caption+'" style="padding-top: 5px; padding-bottom: 5px;">' +
            '<div class="col-md-1"><input class="form-control lfn" data-key="lfn_0" type="text" style="border: 1px solid #999;" value="0" readonly></div>\n' +
            '<div class="col-md-4"><input class="form-control" data-key="bezeichnung_0" type="text" style="border: 1px solid #999;"></div>\n' +
            '<div class="col-md-1"><input class="form-control qty" min="1" data-key="anzahl_0" type="text" onkeypress="validateNumber(event)" name="menge" style="border: 1px solid #999;"></div>\n' +
            '<div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input class="form-control amount" onkeypress="validateNumber(event)" data-key="preis_0" type="text" value="'+awprice+'" style="border: 1px solid #999;"></div></div>\n' +
            '<div class="col-md-2"><div class="inner-addon right-addon" ><i class="glyphicon glyphicon-euro"></i><input disabled class="form-control amount" data-totalprice type="text" style="background: #eee;border: 1px solid #999;"></div></div>\n' +
            '<div class="col-md-2"><input class="form-control addRow'+caption+'" type="submit" value="Reihe hinzufügen"></div>\n' +
            '</div>');

        if(awprice != '') {
            $(rowID).find('[data-key="preis_0"]').attr('disabled', 'disabled');
        }


        this.numberMyRows(boxID, rowID, ID);


    },

    numberMyRows: function(boxID, rowID, ID) {
        var i = 1;
        var that = this;
        $(boxID).find('.lfn').each(function() {
            $(this).val(i);
            i++;
        });
        $(rowID).each(function (index, object) {
            var newIndex = (index+1)

            var cuttedRowID = 'row'+ID+'_';
            cuttedRowID += newIndex;

            $(object).attr('id', cuttedRowID);
            that.inputArray.forEach(function (obj, ind) {
                var dataKey = $(object).find(obj).data('key');
                var cuttedKey = dataKey.substring(0, dataKey.indexOf('_'));
                cuttedKey = cuttedKey+'_'+newIndex;
                $(object).find(obj).attr('data-key', cuttedKey);
            });


        });

    },
    toggleCustomerSearch: function (e) {
        $(this.toggleCustomerSearchContainerID).removeClass('activeToggle');
        $(e.currentTarget).addClass('activeToggle');
        switch (true) {
            case $(e.currentTarget).text().indexOf('Suche')>-1:
                $('select[data-key="paymentMethod"]').val('Überweisung');
                $('#findAdress').parent().css('display', 'block');
                $('#findAdress').val('');
                $('#kundenTabelle').empty();
                break
            case $(e.currentTarget).text().indexOf('Barverkauf')>-1:
                $('select[data-key="paymentMethod"]').val('Barverkauf');
                $('#findAdress').parent().css('display', 'none');
                $('#kundenTabelle').empty();
                $('#kundenTabelle').append(
                    '<tr data-contact-id="barverkauf">' +
                    '<td style="background: #eee;vertical-align: middle"><span style="font-weight: bold;">Kunde:</span></td>'+
                    '<td><input style="border:0;padding:0" data-key="bar_Anrede" placeholder="Anrede" class="form-control" type="text"></td>' +
                    '<td><input style="border:0px;padding:0" data-key="bar_Vorname" placeholder="Vorname" class="form-control" type="text"></td>' +
                    '<td><input style="border:0px;padding:0" data-key="bar_Nachname" placeholder="Nachname" class="form-control" type="text"></td>' +
                    '</tr>');
                $('#kundenTabelle').append(
                    '<tr>' +
                    '<td  style="background: #eee;vertical-align: middle"><span style="font-weight: bold;">Anschrift:</span></td>'+
                    '<td><input style="border:0;padding:0" data-key="bar_Ort" placeholder="Ort" class="form-control" type="text"></td>' +
                    '<td><input style="border:0;padding:0" data-key="bar_PLZ" placeholder="PLZ" class="form-control" type="text"></td>' +
                    '<td><input style="border:0;padding:0" data-key="bar_Strasse" placeholder="Strasse" class="form-control" type="text"></td>' +
                    '</tr>');
                break

        }
    },

    setDatePicker: function (e) {
        $(e.currentTarget).datepicker().datepicker("show");
    },

    setContractAndVehicle: function (object) {
        console.log('setContractAndVehicle: - object !! ', object)     ;
    },

    getVehicleInfo: function (e) {
        console.log('from listener.js : ',$(e.currentTarget).val());
        $(e.currentTarget).autocomplete({
            source: "modules/contactSearch.php",
            minLength: 3,
            messages: {
                noResults: '',
                results: function() {}
            },
            select: function(event, ui) {
                $('#eee').val(ui.item.id);
                var selectedCustomer = ui.item.value;
                var customerArray = selectedCustomer.split('-');

                var customerFullName = customerArray[0];
                var customerFullNameSplitted = customerFullName.split(',');
                var nachname = customerFullNameSplitted[0];
                var vorname = customerFullNameSplitted[1].trim();

                var kundenNr = customerArray[2].replace('Nr. ','').replace(')', '');

                var dataPaket = {
                    'state':'getVehicleInformationByFullNameKDNR',
                    'vorname':vorname,
                    'nachname':nachname,
                    'kundenNr':kundenNr
                };

                getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {
                    //console.log('get verhicle dbb', response);

                    $('#vehicleList').empty();
                    // each JSONObject from Response DB
                    JSON.parse(response).forEach(function (object, index) {
                        //console.log('response --> ', object)
                        if(object.hersteller != undefined)
                        {
                            $('#vehwrapper').css('display', 'block');
                            var option = '<input type="radio" name="vehicle">';
                            if(index == 0) {
                                $('select[data-carselect]').append('<option data-car_id="'+object.car_id+'" selected>'+object.kennzeichen+'  :  '+object.modell+'</option>');

                            } else {
                                console.log('read index' , index);
                                $('select[data-carselect]').append('<option data-car_id="'+object.car_id+'">'+object.kennzeichen+'  :  '+object.modell+'</option>');
                            }

                            /*var vehicleRow = '<tr data-car_id="'+object.car_id+'">' +
                                '<td>'+object.hersteller+'</td>'+
                                '<td>'+object.modell+'</td>'+
                                '<td>'+object.kennzeichen+'</td>'+
                                '<td style="text-align: center">'+option+'</td>'+
                                '</tr>';*/





                            //$('#vehicleList').append(vehicleRow);

                        }


                    });
                    var object = JSON.parse(response);
                    //$('#contactContainer').css('display', 'none');
                    $('#kundenTabelle').removeClass('hidden');

                    $('#kundenTabelle').empty();
                    $('#kundenTabelle').append(
                        '<tr data-contact-id="'+object[0].contact_id+'">' +
                        '<td style="background: #eee"><span style="font-weight: bold">Kunde:</span></td>'+
                        '<td>'+object[0].anrede+'</td>' +
                        '<td>'+object[0].vorname+'</td>' +
                        '<td>'+object[0].name+'</td>' +
                        '</tr>');
                    $('#kundenTabelle').append(
                        '<tr>' +
                        '<td style="background: #eee"><span style="font-weight: bold">Anschrift:</span></td>'+
                        '<td>'+object[0].ort+'</td>' +
                        '<td>'+object[0].plz+'</td>' +
                        '<td>'+object[0].strasse+'</td>' +
                        '</tr>');

                    $('input[data-key="kdnnr"]').val(object[0].kdnnr)



                });
            }
        });
    }

}



var globalListener = new GlobalListener();








