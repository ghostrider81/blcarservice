$(document).ready(function(){

    $('#loginForm').on('submit', function(e) {

        e.preventDefault();

        $.ajax({
            url: "php/login.php",
            data: $("#loginForm").serialize(),
            type: "POST",

            success: function(msg){

                if(msg.indexOf('true')>-1){

                    $(location).attr('href', 'static.php');

                }

                else {
                    $('#loginAlert').html('<div class="alert alert-danger"><strong>Hinweis: </strong>'+ msg +'</div>')
                }
            }
        });
    })

})