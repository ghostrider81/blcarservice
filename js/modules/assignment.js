/************************
****** ESSENTIALS *******
************************/
console.log('assignment loaded')
/***** Funktion die alle Positions Rows in einem einem DIV neu nummeriert *****/

$('body').on('click', '#assignmentList tr[data-row] td:not(:last-child)', function (e) {
    getAssignmentEdit($(e.currentTarget).parent('tr').attr('id'));
});

/** ASSIGNMENT NEW - MAIN FUNCTION (onClick li a = 'Neuen Auftrag anlegen') **/
$('body').on('click', '#assignmentNew', function (e) {

    // TODO :  operators...
    var dataPaket =  {
        'state':'getOperators',
        'data': 'MaybeDataInsideHer'
    };
    getDBdata(URL+'blcarservice/php/functionsInvoices.php', dataPaket, function (response) {
        JSON.parse(response).forEach(function (object, index) {
            $('select[data-key="operator"]').append(
                '<option value="'+object.operator+'">'+object.operator+'</option>'
            );
        });
    })
});



$('body').on('click', '#assignmentToInvoiceSubmit', function (e) {
   console.clear();
    var assignmentList = getDataFromHTMLToJSON('#auftragsListe', 'td')
    var contactid = $('[data-contact-id]').data('contact-id');
    var km = $('[data-km-acception]').data('km-acception');
    var carid = $('td[data-key="car_id"]').text();

    var assignmentListObj = {};
    $('#auftragsListe').find('td[data-key]').each(function (index, object) {
        console.log('jo ', object)
        //console.log($(object).find('select'));
        var cat = 'cat_des'+index;
        if($(object).find('select').length > 0) {
            console.log('selected ', $(object).find('option:selected').val());
            var catObj = $(object).text().substr(0, 10);
            var catDes = false;
            switch (true) {
                case catObj.indexOf('Karosserie') > -1:
                    catDes = 'Karosserie - ';
                    break;
                case catObj.indexOf('Mechanik') > -1:
                    catDes = 'Mechanik - ';
                    break;
                case catObj.indexOf('Elektrik') > -1:
                    catDes = 'Elektrik - ';
                    break;
                case catObj.indexOf('Lack') > -1:
                    catDes = 'Lack - ';
                    break;
            }

            assignmentListObj[cat] = $(object).data('artikel-id') +' : '+ $(object).find('option:selected').val();

        }
        else {
            assignmentListObj[cat] = $(object).data('artikel-id') +' : '+ $(object).text();
            console.log('not found');
        }
    });

    console.log(' !!! lololo ', assignmentListObj)

    var dataPaket =  {
        'state':'getAssignmentToInvoiceSubmit',
        'data': {
            'assignmentList' : assignmentListObj,
            'contact_id' : contactid,
            'car_id' : carid,
            'km_acception' : km
        }
    };
    console.log('assignmentList', assignmentList);

    getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {

    });

});

$('body').on('click', '#assignmentToInvoice', function (e) {
    e.preventDefault();
    var tableRow = $(e.currentTarget).parent().parent().parent().parent().parent();
    var commissionNr = $(tableRow).attr('id');


    var dataPaket =  {
        'state':'getAssignmentToInvoiceINFO',
        'data': {
            'commissionID' : commissionNr
        }
    };

    getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {
        $('#vehicleList').empty();
        $('#auftragsListe').empty();
        var maxLength = JSON.parse(response).length;

        function buildView(callback) {
            JSON.parse(response).forEach(function (object, index) {
                if(index == 0) {

                    if(object.hersteller != undefined)
                    {
                        $('#vehwrapper').css('display', 'block');
                        var option = '<input type="radio" name="vehicle">';
                        if(index == 0) option = '<input type="radio" name="vehicle" checked>';

                        var vehicleRow = '<tr data-km="'+object.km+'" data-car_id="'+object.car_id+'">' +
                            '<td data-key="hersteller">'+object.hersteller+'</td>'+
                            '<td class="hidden" data-key="car_id">'+object.car_id+'</td>'+
                            '<td data-key="modell">'+object.modell+'</td>'+
                            '<td data-key="kennzeichen">'+object.kennzeichen+'</td>'+
                            '</tr>';

                        $('#vehicleList').append(vehicleRow);
                    }
                    //$('#contactContainer').css('display', 'none');
                    $('#kundenTabelle').removeClass('hidden');
                    $('#kundenTabelle').empty();
                    $('#kundenTabelle').append(
                        '<tr  data-contact-id="'+object.contact_id+'">' +
                        '<td style="background: #F5F5F6"><span style="font-weight: bold">Kunde:</span></td>'+
                        '<td>'+object.anrede+'</td>' +
                        '<td>'+object.vorname+'</td>' +
                        '<td>'+object.name+'</td>' +
                        '</tr>');
                    $('#kundenTabelle').append(
                        '<tr data-km-acception="'+object.km_acception+'">' +
                        '<td style="background: #F5F5F6"><span style="font-weight: bold">Anschrift:</span></td>'+
                        '<td>'+object.ort+'</td>' +
                        '<td>'+object.plz+'</td>' +
                        '<td>'+object.strasse+'</td>' +
                        '</tr>');

                    $('input[data-key="kdnnr"]').val(object.kdnnr)
                }

                console.log('object  ----  :: ---- :: --', object);
                var catDesc = false;
                switch (object.cat_id) {
                    case '0':
                        catDesc = 'Arbeitslohn';
                        break;
                    case '1':
                        catDesc = 'Mechanik';
                        break;
                    case '2':
                        catDesc = 'Karosserie';
                        break;
                    case '3':
                        catDesc = 'Lack';
                        break;
                    case '4':
                        catDesc = 'Elektrik';
                        break;
                    case '5':
                        catDesc = 'Material';
                        break;
                }
                var anzahl = object.anzahl;
                var preis = object.preis;
                var gesamtpreis = anzahl * preis;
                catDesc = catDesc != false ? catDesc : '-';
                gesamtpreis = parseFloat(Math.round(gesamtpreis *100)/100);

                var dataPaket =  {
                    'state':'getArtikelDesc',
                    'artikel_id' : object.artikel_id
                };

                getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {

                    $('#auftragsListe').append(
                        '<tr>'+
                        '<td>'+index+'</td>'+
                        '<td>'+JSON.stringify(JSON.parse(response)).replace(/"/g, '')+'</td>'+
                        '<td>'+gesamtpreis+'</td>'+
                        '<td data-artikel-id="'+object.artikel_id+'" data-key="cat_id" class="dropArea">'+catDesc+'</td>'+
                        '</tr>'
                    );
                    if(index == maxLength-1)
                    {

                        callback();

                    }

                });
            })
        }

        buildView(function () {
            startDragNDrop();
        })



    });
    /*
    $( ".draggable" ).draggable({
        helper: "clone",
        revert: "invalid"
    });*/
    function startDragNDrop() {
        $( ".draggableItem" ).draggable({
            revert: true,
            helper: "clone",
            start: function (ev, ui) {
                $(ui.helper).addClass('dragHoverActiv');
            },
            revertDuration: 0
        });
        $(".dropArea").droppable({
            accept: ".draggableItem",
            activeClass: 'dropAreaActive',
            tolerance: "pointer",
            over: function (event, ui) {
                console.log('hover over drop area');
                //console.log('ui? ',$('#'+ui.draggable.id))
                $(this).addClass('dragHoverActive');
                //$(this).removeClass("tech_field");
                //$(this).addClass("ui-state-highlight");
                $(ui.draggable[0]).css('z-index', '1');
                console.log('over ! ', $(ui.draggable[0]));
            },
            out: function (event, ui) {
                $(this).removeClass('dragHoverActive');
                // $(this).addClass("tech_field");
                // $(this).removeClass("ui-state-highlight");
            },
            drop: function (event, ui) {
                console.log(' __ ', event)
                console.log(' __ ', ui)
                console.log('item :: ', ui.draggable[0].innerHTML);
                var that = this;
                var selectedAw = ['Mechanik', 'Karosserie', 'Lack', 'Elektrik']
                if(
                    ui.draggable[0].innerHTML.indexOf(selectedAw[0]) > -1 ||
                    ui.draggable[0].innerHTML.indexOf(selectedAw[1]) > -1 ||
                    ui.draggable[0].innerHTML.indexOf(selectedAw[2]) > -1 ||
                    ui.draggable[0].innerHTML.indexOf(selectedAw[3]) > -1

                ){
                    var dataPaket =  {
                        'state':'getAwSets',
                    };

                    getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {
                        console.log('GET AW SETS ! ', response);
                        $(that).empty();
                        $(that).append(ui.draggable[0].innerHTML);
                        $(that).append('<select class="awSets"></select>')
                        $('.awSets').append(response);
                    });
                } else {
                    $(this).html(ui.draggable[0].innerHTML);
                }

                //$(this).html(ui.draggable[0].innerHTML);
                console.log('..',$(ui.draggable).val());
                $(this).removeClass('dragHoverActive');

                $(ui.draggable[0]).css('z-index', '1');
                //$(this).addClass("tech_field");
                //$(this).removeClass("ui-state-highlight");
                //alert($('#'+ui.draggable[0].id).children()[0].textContent);
                //dialog.dialog( "open" );
            }

        });
    }

    $('#setInvoiceCat').modal('show');
});

console.clear();
/****                  assignmentAdd func Main                  contact-id , car id    *****/
$('body').on('click', '#assignmentAdd',function () {
    var signatureInterval = setInterval(isSignatureChecked, 5000);

    function isSignatureChecked() {

        var signature = $('[data-key="signature"]').is(":checked") ?  1 : 0;
        if(signature){

        var dataPaket =  {
            'state':'checkSignature',
            'data': 'MaybeDataInsideHer'
        };
        console.log('check in php interval');

            getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {

                if(response == "true")
                {
                    clearInterval(signatureInterval);
                    signatureInterval = false;

                    $("#signatureModal").modal();
                    $("#signatureModal").on("hidden.bs.modal", function () {
                        $('#dynamicContent').load('modules/assignmentList.php');
                    });

                }
            });
        }

    }

    $('#assignmentAdd').attr('disabled','true');

    var contractData = getDataFromHTMLToJSON('#contractData', 'input');
    var notes = getDataFromHTMLToJSON('.ibox-content', 'textarea');
    var workRewarding = getDataFromHTMLToJSON('#containerArbeitslohn', 'input');
    var workRewardingMechanik = getDataFromHTMLToJSON('#containerMechanik', 'input');
    var workRewardingKarosserie = getDataFromHTMLToJSON('#containerKarosserie', 'input');
    var workRewardingLack = getDataFromHTMLToJSON('#containerLack', 'input');
    var workRewardingElektrik = getDataFromHTMLToJSON('#containerElektrik', 'input');
    var material = getDataFromHTMLToJSON('#containerMaterial', 'input');
    var carInspection = getDataFromHTMLToJSON('#containerTuev', 'input.form-control');
    var operator = $('select[data-key="operator"] option:selected').val();
    var selectedEmployee = $('select[data-key="selectedEmployee"] option:selected').val();
    var signature;
    // COMMISSION ID !!
    $('[data-key="signature"]').is(":checked") ?  signature = 1 : signature = 0;

    var mechanik_aw =  $('select[id="awMechanik"]').children("option:selected").val();
    var karosserie_aw =  $('select[id="awKarosserie"]').children("option:selected").val();
    var lack_aw =  $('select[id="awLack"]').children("option:selected").val();
    var elektrik_aw =  $('select[id="awElektrik"]').children("option:selected").val();

    var carid1 = $($('.vehicleList input[type="radio"]:checked').parent().parent()).data('car_id');
    var carid2 = $('option[data-car_id]:selected').data('car_id');

    var carid = carid1 || carid2;


    contractData['operator'] = operator;
    contractData['selectedEmployee'] = selectedEmployee;
    var contactid = $('[data-contact-id]').data('contact-id');
    console.log('operator --> ', operator, '   selectedEmployee ', selectedEmployee);

    var otherList = {
        'contact_id':contactid,
        'car_id':carid,
        'signature':signature,
        'mechanik_aw' : mechanik_aw,
        'karosserie_aw' : karosserie_aw,
        'lack_aw' : lack_aw,
        'elektrik_aw' : elektrik_aw
    };
    otherList['commission_id'] = $('div[data-commission-id]').data('commission-id');

    //console.log(' h h h h h h ', $('.vehicleList input[type="radio"]:checked').parent().parent()[0].childNodes[0].innerHTML);

    console.log('contractData ', contractData);
    console.log('notes ', notes);
    console.log('workRewarding ', workRewarding);
    console.log('workRewardingMechanik ', workRewardingMechanik);
    console.log('workRewardingKarosserie ', workRewardingKarosserie);
    console.log('workRewardingLack ', workRewardingLack);
    console.log('workRewardingElektrik ', workRewardingElektrik);
    console.log('material ', material);
    console.log('signature ', signature);

    console.log('other list', otherList);
    console.log(carInspection);

    var state = 'setNewAssignment';
    if($(this).hasClass('update'))
    {
        state = 'updateAssignment';
    }

    var dataPaket =  {
        'state':state,
        'data': [contractData, signature, otherList, carInspection, notes, workRewarding, workRewardingMechanik, workRewardingKarosserie, workRewardingLack, workRewardingElektrik, material]
    };

    getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {
        console.log('fired to php')


    });

    alert("Der Datensatz wurde gespeichert");
    $('#dynamicContent').load('modules/assignmentList.php');
});



function getAssignmentEdit(commissionID) {
    var id = commissionID;
    var dataPacket = {
        'state':'getAssignmentEdit',
        'commissionID':commissionID
    };

    getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPacket, function (response) {
        var data = JSON.parse(response);
        var contractData = {};
        if(response)
        {
            $('#dynamicContent').load('modules/assignmentEdit.php', function(status, xhr) {
                // cat_id 0, 1, 2, 3, 4
                var dataPaket = [];
                var arbeitslohn = [];
                var mechanik = [];
                var karosserie = [];
                var lack = [];
                var elektrik = [];
                var material = [];
                // vehicle
                $('#vehicleList').empty();
                $('#vehwrapper').css('display', 'block');
                JSON.parse(response).forEach(function (object, index) {
                    if (index == 0) {
                        dataPaket.push(object)
                    }


                        arbeitslohn.push(object);

                });

                console.log('VEH INFO ::: !!!');
                getVehInfoBYFullName(dataPaket[0].vorname, dataPaket[0].name, function (response) {
                    console.log('... ', response);

                    JSON.parse(response).forEach(function (object, index) {
                        if (index == 0) {
                            $('select[data-carselect]').append('<option data-car_id="' + object.car_id + '" selected>' + object.kennzeichen + '  :  ' + object.modell + '</option>');

                        }else {
                            $('select[data-carselect]').append('<option data-car_id="' + object.car_id + '">' + object.kennzeichen + '  :  ' + object.modell + '</option>');
                        }
                    })
                });


                // commission box
                $('input[data-key="contact_id_commission"]').val(dataPaket[0].kdnnr).attr('data-contact-id', dataPaket[0].contact_id);;
                $('input[data-key="commission_nr"]').val(dataPaket[0].commission_nr);
                //console.log('timestampe : ', dataPaket[0].date_acception);
                var date = dataPaket[0].date_acception;
                var date2 = dataPaket[0].date_target;
                $('input[data-key="date_acception"]').val(timeConverter(date));
                $('input[data-key="date_target"]').val(timeConverter(date2));
                $('input[data-key="km_acception"]').val(dataPaket[0].km_acception);
                $('select[data-key="operator"]').val(dataPaket[0].operator);
                $('select[data-key="selectedEmployee"]').val(dataPaket[0].employee);
                //$('input[data-key="contact_id"]').css()


                $('div[data-commission-id]').attr('data-commission-id', commissionID);

                //globalListener.setContractAndVehicle(dataPaket[0]);
                console.log('NOTES :: ', dataPaket[0].notices);
                // notes
                $('textarea[data-key="notices"]').text(dataPaket[0].notices);

                $('#kundenTabelle').append(
                    '<tr>' +
                    '<td style="background: #eee"><span style="font-weight: bold">Kunde:</span></td>'+
                    '<td>'+dataPaket[0].anrede+'</td>' +
                    '<td>'+dataPaket[0].vorname+'</td>' +
                    '<td>'+dataPaket[0].name+'</td>' +
                    '</tr>');
                $('#kundenTabelle').append(
                    '<tr>' +
                    '<td style="background: #eee"><span style="font-weight: bold">Anschrift:</span></td>'+
                    '<td>'+dataPaket[0].ort+'</td>' +
                    '<td>'+dataPaket[0].plz+'</td>' +
                    '<td>'+dataPaket[0].strasse+'</td>' +
                    '</tr>');

                var copyElements = [
                    [
                        '#rowArbeitslohn1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Arbeitslohn'
                    ],
                    [
                        '#rowMechanik1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Mechanik'
                    ],
                    [
                        '#rowKarosserie1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Karosserie'
                    ],
                    [
                        '#rowLack1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Lack'
                    ],
                    [
                        '#rowElektrik1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Elektrik'
                    ],
                    [
                        '#rowMaterial1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Material'
                    ],

                ];
                // copy and create for each box
                [arbeitslohn, mechanik, karosserie, lack, elektrik, material].forEach(function (object, index) {
                    //console.log('eached ', index);
                    if(object.length > 0) {
                        copyAndReplaceHTML(object,copyElements[index][0], copyElements[index][1], copyElements[index][2]);
                    }
                    else {
                        // set any other box aw sets
                        var dataPaket = {
                            'state':'getAwSets'
                        }
                        getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {
                            //    console.log('getAwSets----', copyElements[index][2], response);
                            $('#aw'+copyElements[index][2]).append(response);
                            //console.log('AW SETS _______________________', response);
                        });
                    }
                });

            });
        }
    });
}




//
function copyAndReplaceHTML(dataPaket, parenContainerSelector, childSelector, changeActionRowSelector) {
    var p = $(parenContainerSelector);

    dataPaket.forEach(function (obj, ind) {
        //clone and append
        var parentObj = $(p).clone(true,true);
        $(parenContainerSelector).before(parentObj);
        // loop ea child
        childSelector.forEach(function (array, index) {
            var isNotSet = true;




        //console.log('Parent:: ', parentObj)
            //$(parentObj).find(''+childSelector[index]+'')
            //console.log('..find...',  $(parentObj).find(''+childSelector[index]+''));
            if(childSelector[index].indexOf('bezeichnung') > -1) {
                var dataPaketIN = {
                    'state':'getArtikelDesc',
                    'artikel_id':dataPaket[ind].artikel_id
                };
                getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaketIN, function (response) {
                    $(parentObj).find(''+childSelector[index]+'').val(JSON.stringify(JSON.parse(response)).replace(/"/g, ''));
                });
            }
            else if(childSelector[index].indexOf('preis') > -1) {
                $(parentObj).find(childSelector[index]).val(setCurrencyEUR(dataPaket[ind].preis));
                $(parentObj).find(childSelector[index]).trigger('keyup')
            }
            else if(childSelector[index].indexOf('anzahl') > -1 ){
                $(parentObj).find(childSelector[index]).val(dataPaket[ind].anzahl);
                $(parentObj).find(childSelector[index]).trigger('keyup')
                if(dataPaket[ind].cat_id != 0 && isNotSet)
                {
                    isNotSet = false;
                    //console.log('aw-->',$(parentObj).find('#aw'+changeActionRowSelector));
                    $('#aw'+changeActionRowSelector).empty()
                    .append('<option>'+setCurrencyEUR(dataPaket[ind].preis)+' €</option>')
                    .attr('disabled', 'disabled');
                }
            }
            if (dataPaket.length - 1 == ind && childSelector.length - 1 == index)
            {
                //console.log('parent sel:  ', parenContainerSelector);
                //$(parenContainerSelector).remove();
            }

        });

    });

    $("div[id^='row"+changeActionRowSelector+"']").each(function (index, object) {
        var last = $("div[id^='row"+changeActionRowSelector+"']").length-2 == index ? true : false;
        if(!last) {
            $(object).find('.addRow' + changeActionRowSelector)
                .removeClass('addRow' + changeActionRowSelector)
                .addClass('deleteRow' + changeActionRowSelector)
                .val('Löschen');
        }
    });


    //numberRows(changeActionRowSelector);

    // display box with value
    $('#container'+changeActionRowSelector).css('display', 'block');

    var lastIndex = false;
    // index row and each data-key
    $("div[id^='row"+changeActionRowSelector+"']").each(function (index, object) {
        var cuttedID = $(object).attr('id').substr(0, $(object).attr('id').indexOf('1'));
        $(object).attr('id', cuttedID+'_'+(index+1));
        lastIndex = cuttedID+'_'+(index+1);
        childSelector.forEach(function (obj, ind) {
            var dataKey = $(object).find(obj).data('key');
            //console.log('data key ', dataKey);
            if(obj.indexOf('lfn') > -1){
                $(object).find(obj).val((index+1));
            }
            var cuttedKey = dataKey.substring(0, dataKey.indexOf('_'));
            cuttedKey = cuttedKey+'_'+(index+1);
            $(object).find(obj).attr('data-key', cuttedKey);
        });
    });

    //remove last row
    $('#'+lastIndex).remove();

}






/***  create JSON From html-attr: data-key &  User Input ***/
function getDataFromHTMLToJSON(parentContainerSelector, childItemSelector) {
    // HTML target childItem must have data-key attribute to set key : value pairs
    var JSONDataPacket = {};

    $(''+parentContainerSelector+'').find(''+childItemSelector+'[data-key]').each(function (index, object) {
            var key = $(object).data('key').slice(0, $(object).data('key').length-1);

            try {
                JSONDataPacket[''+key+''+index] = $(object).val();
                if($(object).val().length > 0) {
                    JSONDataPacket[''+key+''+index] = $(object).val();
                } else {
                    JSONDataPacket[''+key+''+index] = $(object).text();
                }
            }
            catch (e){

            }

    });
    return JSONDataPacket;
}






















