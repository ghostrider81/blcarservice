/** INVOICE EDIT FUNC **/
$('body').on('click', '#invoiceList tr[data-row] td:not(:last-child)', function (e) {
    getInvoiceEdit($(e.currentTarget).parent('tr').attr('id'))
});
/** INVOICE NEW FUNC **/
$('body').on('click', '#invoiceNew', function (e) {
    var dataPaket = {
        'state': 'getOperators',
        'data': 'MaybeDataInsideHer'
    };
    getDBdata(URL + 'blcarservice/php/functionsInvoices.php', dataPaket, function (response) {
        console.log('operator', response);
        JSON.parse(response).forEach(function (object, index) {

            $('select[data-key="operator"]').append(
                '<option value="' + object.operator + '">' + object.operator + '</option>'
            );
        });
    })
});


/** ADD INVOICE FUNC **/
// newInvoice ->
$('body').on('click', '#addInvoice', function () {

    $('#addInvoice').attr('disabled', 'true');

    var contractData = getDataFromHTMLToJSON('#contractData', 'input');
    var notes = getDataFromHTMLToJSON('.ibox-content', 'textarea');
    var workRewarding = getDataFromHTMLToJSON('#containerArbeitslohn', 'input');
    var workRewardingMechanik = getDataFromHTMLToJSON('#containerMechanik', 'input');
    var workRewardingKarosserie = getDataFromHTMLToJSON('#containerKarosserie', 'input');
    var workRewardingLack = getDataFromHTMLToJSON('#containerLack', 'input');
    var workRewardingElektrik = getDataFromHTMLToJSON('#containerElektrik', 'input');
    var material = getDataFromHTMLToJSON('#containerMaterial', 'input');
    var carInspection = getDataFromHTMLToJSON('#containerTuev', 'input.form-control');

    var att = getDataFromHTMLToJSON('#containerAtt', 'input');


    //console.clear();
    // vehicleInformation
    //var hersteller = $('.vehicleList input[type="radio"]:checked').parent().parent()[0].childNodes[0].innerHTML;
    //var car_id = $('.vehicleList input[type="radio"]:checked').parent().parent()[0].childNodes[1].innerHTML;
    //var modell = $('.vehicleList input[type="radio"]:checked').parent().parent()[0].childNodes[1].innerHTML;

    var carid1 = $($('.vehicleList input[type="radio"]:checked').parent().parent()).data('car_id');
    var carid2 = $('option[data-car_id]:selected').data('car_id');

    var carid = carid1 || carid2;

    console.log('CAR ID ----', carid, carid2);


    console.log('contractData ', contractData);
    console.log('notes ', notes);
    console.log('workRewarding ', workRewarding);
    console.log('workRewardingMechanik ', workRewardingMechanik);
    console.log('workRewardingKarosserie ', workRewardingKarosserie);
    console.log('workRewardingLack ', workRewardingLack);
    console.log('workRewardingElektrik ', workRewardingElektrik);
    console.log('material ', material);
    var contactid = $('[data-contact-id]').data('contact-id');
    var status = $('select[data-key="status"]').children("option:selected").val();
    var operator = $('select[data-key="operator"]').children("option:selected").val();
    var paymentMethod = $('select[data-key="paymentMethod"]').children("option:selected").val();
    var payableDate = $('input[data-key="payableDate"]').val();
    var mechanik_aw = $('select[id="awMechanik"]').children("option:selected").val();
    var karosserie_aw = $('select[id="awKarosserie"]').children("option:selected").val();
    var lack_aw = $('select[id="awLack"]').children("option:selected").val();
    var elektrik_aw = $('select[id="awElektrik"]').children("option:selected").val();

    var rabattinput = $('#RabattInput').val();
    var rabattResult = $('#rabattResult').val();


    var otherList = {
        'contact_id': contactid,
        'car_id': carid,
        'status': status,
        'sachbearbeiter': operator,
        'paymentMethod': paymentMethod,
        'payableDate': payableDate,
        'mechanik_aw': mechanik_aw,
        'karosserie_aw': karosserie_aw,
        'lack_aw': lack_aw,
        'elektrik_aw': elektrik_aw,
        'rabatt_input':rabattinput,
        'rabatt_result':rabattResult,
        'att':att
    };
    otherList['billing_id'] = $('div[data-billing-id]').data('billing-id');
    console.log(' other LIST ', otherList);
    if (contactid == 'barverkauf') {
        console.clear();
        var kundenDaten = getDataFromHTMLToJSON('#kundenTabelle', 'input');
        otherList['barverkauf'] = kundenDaten;
        console.log('in contact id - bar verkauf ', contactid);
        console.log(kundenDaten);
    }


    // action----
    var state = 'setNewInvoice';
    if ($(this).hasClass('update')) {
        state = 'updateInvoice';
    }
    var dataPaket = {
        'state': state,
        'data': [contractData, otherList, carInspection, notes, workRewarding, workRewardingMechanik, workRewardingKarosserie, workRewardingLack, workRewardingElektrik, material]
    };

    getDBdata(URL + 'blcarservice/php/functionsInvoices.php', dataPaket, function (response) {
        console.log('fired to php')
    });

});


/** INVOICE EDIT FUNC **/
function getInvoiceEdit(id) {
    var dataPaket = {
        'state': 'getInvoiceEdit',
        'billingID': id
    }
    getDBdata(URL + 'blcarservice/php/functionsInvoices.php', dataPaket, function (response) {
        //console.log(response);

        var data = JSON.parse(response);
        var contractData = {};
        if (response) {
            $('#dynamicContent').load('modules/invoiceEdit.php', function (status, xhr) {
                // cat_id 0, 1, 2, 3, 4
                var dataPaket = [];
                var arbeitslohn = [];
                var mechanik = [];
                var karosserie = [];
                var lack = [];
                var elektrik = [];
                var material = [];
                // vehicle
                $('#vehicleList').empty();
                $('#vehwrapper').css('display', 'block');
                JSON.parse(response).forEach(function (object, index) {


                    if (index == 0) {
                        dataPaket.push(object)
                    }
                    if (object.cat_id == '0') {
                        arbeitslohn.push(object);
                    }
                    else if (object.cat_id == '1') {
                        mechanik.push(object);
                    }
                    else if (object.cat_id == '2') {
                        karosserie.push(object);
                    }
                    else if (object.cat_id == '3') {
                        lack.push(object);
                    }
                    else if (object.cat_id == '4') {
                        elektrik.push(object);
                    }
                    else if (object.cat_id == '5') {
                        material.push(object);
                    }
                });

                console.log('VEH INFO ::: !!!');
                getVehInfoBYFullName(dataPaket[0].vorname, dataPaket[0].name, function (response) {
                    //console.log('... ', response);

                    JSON.parse(response).forEach(function (object, index) {
                        if (index == 0) {
                            $('select[data-carselect]').append('<option data-car_id="' + object.car_id + '" selected>' + object.kennzeichen + '  :  ' + object.modell + '</option>');

                        }else {
                            $('select[data-carselect]').append('<option data-car_id="' + object.car_id + '">' + object.kennzeichen + '  :  ' + object.modell + '</option>');
                        }
                    })
                });

                // commission box
                $('input[data-key="kdnnr"]').val(dataPaket[0].kdnnr).attr('data-contact-id', dataPaket[0].contact_id);
                $('input[data-key="invoiceNumber"]').val(dataPaket[0].billing_nr);
                //console.log('timestampe : ', dataPaket[0].date_acception);
                var date = dataPaket[0].date_billing;
                var date2 = dataPaket[0].date_target;
                $('input[data-key="invoiceDate"]').val(timeConverter(date));
                $('input[data-key="payableDate"]').val(timeConverter(date2));
                $('input[data-key="date_target"]').val(dataPaket[0].date_target);
                $('select[data-key="operator"]').val(dataPaket[0].operator);
                //$('input[data-key="contact_id"]').css()
                $('input[data-key="kmStand"]').val(dataPaket[0].kilometerstand);
                console.log('dataPacket  BILLING ID ', dataPaket[0].billing_id);
                $('div[data-billing-id]').attr('data-billing-id', id);
                // status
                $('select[data-key="status"]').val(dataPaket[0].status);


                // payment Method
                $('select[data-key="paymentMethod"]').val(dataPaket[0].payment_method);


                $('textarea[data-key="notices"]').text(dataPaket[0].notices);

                $('#kundenTabelle').removeClass('hidden');
                //$('input[data-key="contactName"]').css('display', 'none');
                $('#kundenTabelle').append(
                    '<tr>' +
                    '<td style="background: #eee"><span style="font-weight: bold">Kunde:</span></td>' +
                    '<td>' + dataPaket[0].anrede + '</td>' +
                    '<td>' + dataPaket[0].vorname + '</td>' +
                    '<td>' + dataPaket[0].name + '</td>' +
                    '</tr>');
                $('#kundenTabelle').append(
                    '<tr>' +
                    '<td style="background: #eee"><span style="font-weight: bold">Anschrift:</span></td>' +
                    '<td>' + dataPaket[0].ort + '</td>' +
                    '<td>' + dataPaket[0].plz + '</td>' +
                    '<td>' + dataPaket[0].strasse + '</td>' +
                    '</tr>');

                var copyElements = [
                    [
                        '#rowArbeitslohn1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Arbeitslohn'
                    ],
                    [
                        '#rowMechanik1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Mechanik'
                    ],
                    [
                        '#rowKarosserie1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Karosserie'
                    ],
                    [
                        '#rowLack1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Lack'
                    ],
                    [
                        '#rowElektrik1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Elektrik'
                    ],
                    [
                        '#rowMaterial1',
                        ['input[data-key^="anzahl"]', 'input[data-key^="lfn"]', 'input[data-key^="preis"]', 'input[data-key^="bezeichnung"]'],
                        'Material'
                    ]

                ];
                $('#carInspectionFeeInput').val(dataPaket[0].tuv);
                $('#selectTuev').css('display', 'none');
                // copy and create for each box
                [arbeitslohn, mechanik, karosserie, lack, elektrik, material].forEach(function (object, index) {
                    //console.log('eached ', index);
                    if (object.length > 0) {
                        copyAndReplaceHTML(object, copyElements[index][0], copyElements[index][1], copyElements[index][2]);

                    }
                    else {
                        // set any other box aw sets
                        var dataPaket = {
                            'state': 'getAwSets'
                        };
                        getDBdata(URL + 'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {
                            //    console.log('getAwSets----', copyElements[index][2], response);
                            $('#aw' + copyElements[index][2]).append(response);
                        });
                    }
                });

            });
        }

    });
}
