$(document).ready(function() {

    $('#dynamicContent').on('mouseenter', 'td[data-subject-id]', function() {

            var fach_id = $(this).data('subject-id');
            var regal_id = $(this).parent().parent().parent().data('rack-id');

            var entryDataPackage = new Object();
            entryDataPackage.action = "getEntryData";
            entryDataPackage.rack_id = regal_id;
            entryDataPackage.subject_id = fach_id;

            console.log(entryDataPackage);

            $.ajax({
                type: "POST",
                url:"php/functionsWheels.php",
                data: JSON.stringify(entryDataPackage),
                success: function(e) {

                    if($.trim(e)) {
                        $('#wheelDataWindow').show();
                        $('#dataWindowEntryID').html(e[0].entry_id);
                        $('#dataWindowRackID').html(e[0].rack_id);
                        $('#dataWindowSubjectID').html(e[0].subject_id);
                        $('#dataWindowKennzeichen').html(e[0].mark);
                        $('#dataWindowProfileVl').html(e[0].profile_vl);
                        $('#dataWindowProfileVr').html(e[0].profile_vr);
                        $('#dataWindowProfileHr').html(e[0].profile_hr);
                        $('#dataWindowProfileHl').html(e[0].profile_hl);
                        $('#dataWindowWheelOk').html(e[0].wheel_ok);
                        $('#dataWindowInformContact').html(e[0].inform_contact);
                        $('#dataWindowNotes').html(e[0].notes);
                    }

                },
                error: function(response, status, xhr) {
                    console.log(response);
                    console.log(status);
                    console.log(xhr);
                }
            });

        $('#dynamicContent').on('mouseleave', 'td[data-subject-id]', function() {
            $('#wheelDataWindow').hide();
        });
    });
});



/*************************************************
****** SUBMIT RACK_ID AN SUBJECT_ID TO MODAL *****
*************************************************/

$('body').on('click', "td[data-subject-id]", function() {
    var fach_id = ($(this).data('subject-id'));
    var regal_id = ($(this).parent().parent().parent().data('rack-id'));
    $('#myModal').modal('show');

    $('#placeholderRegal').text(regal_id);
    $('#placeholderFach').text(fach_id);
});



/***************************************************
 ****** AUTOCOMPLETE FOR CONTACT AND GET MARKS *****
 ***************************************************/

$('body').on('keyup', '#contact', function () {
    $($(this)).autocomplete({
        source: "modules/contactSearch.php",
        minLength: 3,
        messages: {
            noResults: '',
            results: function() {}
        },
        select: function(event, ui) {
            $('#mark').empty();
            $('#contact_id').val(ui.item.id);
            var kennzeichenAll = ui.item.vehicleData;
            var kennzeichen = kennzeichenAll.split(',');
            $.each( kennzeichen, function( index, value ) {
                $('#mark').append("<option value='" + value + "'>" + value + "</option>");
            });
        }
    }).autocomplete( "widget" ).addClass( "ui-autocomplete-wheelracks" );
});



/**************************************************
 ****** BUILD DATAPACKAGE FOR MYSQL OPERATION *****
 **************************************************/

$('body').on('click', '#pushList', function() {
    var wheelDataPackage = new Object();
        wheelDataPackage.action = "newOccupancy";
        wheelDataPackage.rack_id = $('#placeholderRegal').text();
        wheelDataPackage.subject_id = $('#placeholderFach').text();
        wheelDataPackage.contact_id = $('#contact_id').val();
        wheelDataPackage.mark = $('#mark').val();
        wheelDataPackage.profile_vl = $('#profileVL').val();
        wheelDataPackage.profile_vr = $('#profileVR').val();
        wheelDataPackage.profile_hl = $('#profileHL').val();
        wheelDataPackage.profile_hr = $('#profileHR').val();
        wheelDataPackage.wheel_ok = $('#wheel_ok').val();
        wheelDataPackage.inform_contact = $('#inform_contact').val();
        wheelDataPackage.notes = $('#notes').val();
        console.log(wheelDataPackage);


        $.ajax({
            type: "POST",
            url:"php/functionsWheels.php",
            data: JSON.stringify(wheelDataPackage),
            success: function(e) {
               $('#myModal').modal('hide');
               $('body').removeClass('modal-open');
               $('.modal-backdrop').remove();
                setTimeout(function () {
                    $('#dynamicContent').load('modules/racksList.php');
                }, 1000)
            },
            dataType: 'json'
        });




});