/***** AJAX CLICK ROW HANDLER *****/
$("#dynamicContent").on('click',"tr[data-row-contact='true']", function() {
    var rowId = $(this).attr('id');
    $('#dynamicContent').load('modules/contactDetail.php?contactId='+rowId, function(response, status, xhr) {
        if ( status == "error" ) {
            var msg = "Das angeforderte Modul <strong>" + dest + "</strong> steht im Moment leider nicht zur Verfügung.<br />Fehlercode: ";
            $("#dynamicContent").html('<div class="alert alert-danger error-margin"><strong>Achtung: </strong>'+ msg + '<strong>' + xhr.status + " " + xhr.statusText + '</strong></div>'); }
    });
});

$('input').keydown(function (e) {
    if (e.which === 13) {
        var index = $('input').index(this) + 1;
        $('input').eq(index).focus();
    }
});

/***** MAKE CONTACTS EDITABLE *****/
$('#contactEdit').on('click', function() {
    $('#contactEdit').hide();
    $('#contactSave').show();
    $('#contactContainer [data-editable="true"] span').each(function() {
        var content = $(this).text();
        var input = '<input type="text" value="'+content+'" class="margin-bottom-formfield form-control autoenter" />';
        $(this).html(input);
    });
});

$('#contactSave').on('click', function() {
    var contactId = $('#contactId').val();
    var contactInput = new Object();
        contactInput.action = "updateContact";
        contactInput.contactid = contactId.toString();

    $('#contactEdit').show();
    $('#contactSave').hide();
    $('#contactContainer [data-editable="true"]').each(function() {

        var key = $(this)[0].id;
        var content = $(this).find('input').val();

        if(content == "")
            {
                content = "<i class=\"fa fa-bolt light-color\"></i>"
                contactInput[key] = "";
            }

            else
            {
                contactInput[key] = content;
            }


        var spanContent = '<span>'+content+'</span>';
        $(this).html(spanContent);
    });

    var stringify = JSON.stringify(contactInput);

    $.ajax({
        type: "POST",
        url: "ajax/contacts.php",
        data: stringify,
        success: function(msg) {
            alert(msg);
        }
    });
});


/***** MAKE CONTACT NOTICES EDITABLE *****/
$('#noticesEdit').on('click', function() {
    $('#noticesEdit').hide();
    $('#noticesSave').show();
    var content = $('#noticesContainer [data-notices="true"]').text().trim();
    var input = '<textarea class="margin-bottom-formfield form-control">'+content+'</textarea>';
    $('#noticesContainer [data-notices="true"]').html(input);
});

$('#noticesSave').on('click', function () {
    var contactId = $('#contactId').val();
    var noticesInput = new Object();
    noticesInput.action = "updateContactNotices";
    noticesInput.contactid = contactId;
    $('#noticesEdit').show();
    $('#noticesSave').hide();
    var content = $('#noticesContainer [data-notices="true"]').find('textarea').val();
    if(content == "")
    {
        content = "<i class=\"fa fa-bolt light-color\"></i>"
        noticesInput.notizen = "";
    }

    else
    {
        noticesInput.notizen = content;
    }

    $('#noticesContainer [data-notices="true"]').html(content);

    var stringify = JSON.stringify(noticesInput);

    $.ajax({
        type: "POST",
        url: "ajax/contacts.php",
        data: stringify,
        success: function(msg) {
            alert(msg);
        }
    });
});


/***** MAKE EACH CAR EDITABLE *****/
$('[id^=carEdit-]').on('click', function() {
    var carId = $(this).attr('id').match(/[0-9]+/)[0];
    $('#carEdit-'+carId).hide();
    $('#carSave-'+carId).show();

    $('#carContainer-'+carId+' [data-car="'+carId+'"]').each(function() {
        var content = $(this).text();
        var input = '<input type="text" value="'+content+'" class="margin-bottom-formfield form-control" />';
        $(this).html(input);
    });
});

$('[id^=carSave-]').on('click', function() {
    var carId = $(this).attr('id').match(/[0-9]+/)[0];

    var carInput = new Object();
    carInput.action = "updateCar";
    carInput.carid = carId;

    $('#carEdit-'+carId).show();
    $('#carSave-'+carId).hide();

    $('#carContainer-'+carId+' [data-car="'+carId+'"]').each(function() {

        var key = $(this)[0].id.split('-');
            key = key[0];
        var content = $(this).find('input').val();

        if(content == "")
        {
            content = "<i class=\"fa fa-bolt light-color\"></i>"
            carInput[key] = "";
        }

        else
        {
            carInput[key] = content;
        }


        var spanContent = '<span>'+content+'</span>';
        $(this).html(content);

    });

    var stringify = JSON.stringify(carInput);

    $.ajax({
        type: "POST",
        url: "ajax/contacts.php",
        data: stringify,
        success: function(msg) {
            alert(msg);
        }
    });
});


/***** SAVE NEW CONTACT AND REFER TO DETAIL PAGE *****/
$('#contactAdd').on('click', function () {
    var contactInput = new Object();
    contactInput.action = "addContact";
    $('#contactAdd').prop('disabled',true);

    $('#contactContainer [data-input="true"]').each(function() {

        var key = $(this)[0].id;
        var content = $(this).val();
        contactInput[key] = content;

    });

    var notizen = $('#notizen').val();
    contactInput.notizen = notizen;

    console.log(contactInput);
    var stringify = JSON.stringify(contactInput);

    $.ajax({
        type: "POST",
        url: "ajax/contacts.php",
        data: stringify,
        success: function(msg) {
            $('#dynamicContent').load('modules/contactDetail.php?contactId='+msg);
            alert("Der Kontakt wurde erfolgreich angelegt");
        }
    });

});