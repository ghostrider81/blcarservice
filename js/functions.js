/***** AJAX NAVIGATION HANDLER *****/
$("a[data-navcomponent='true']").on('click', function() {

    if($(this).attr('id') == "invoiceListDraft") {
        var destPath = "invoiceList.php?status=0";
    }
    else if($(this).attr('id') == "invoiceListOpen") {
        var destPath = "invoiceList.php?status=1";
    }
    else if($(this).attr('id') == "invoiceListDue") {
        var destPath = "invoiceList.php?status=2";
    }
    else if($(this).attr('id') == "invoiceListPaid") {
        var destPath = "invoiceList.php?status=3";
    }
    else if($(this).attr('id') == "invoiceListAll") {
        var destPath = "invoiceList.php?status=all";
    }
    else {
        var destPath = $(this).attr('id')+'.php';
    }

    $('#dynamicContent').load('modules/'+destPath, function(response, status, xhr) {
        rowArbeitslohnId = 1;
        rowMechanikId = 1;
        rowKarosserieId = 1;
        rowLackId = 1;
        rowElektrikId = 1;
        rowMaterialId = 1;

        if ( status == "error" ) {
            var msg = "Das angeforderte Modul <strong>" + destPath + "</strong> steht im Moment leider nicht zur Verfügung.<br />Fehlercode: ";
            $("#dynamicContent").html('<div class="alert alert-danger error-margin" style="margin-left: 15px; margin-right: 15px;"><strong>Achtung: </strong>'+ msg + '<strong>' + xhr.status + " " + xhr.statusText + '</strong></div>'); }
    });
});




/*** MENUSTAT LISTENER ***/
$('#side-menu').on('click', function() {
    countInvoiceBadges();
});




/*** AW SETTINGS LISTENER & CALCULATOR ***/
$('#dynamicContent').on('keyup', 'input[data-aw-settings-field]', function() {
        calcAwSets();
});


/*** AW SETS CALC ***/
function calcAwSets() {
    $("div[data-aw-settings]").each( function() {
        var anzahl = ($(this).find('input[data-aw-settings-field*="anzahl"]').val())*1;
        var stundenlohn = $(this).find('input[data-aw-settings-field*="stundenlohn"]').val();
            stundenlohn = stundenlohn.replace(',','.');
            stundenlohn = parseFloat(stundenlohn);
        var preis = roundCommercial(stundenlohn / anzahl);

        $(this).find('input[data-aw-settings-field*="preis"]').val(preis);
    })
}


/*** SAVE AW SETS ***/
$('#dynamicContent').on('click', '#awSave', function () {

    $(this).attr("disbled","true");

    var aw_sets = new Object();
    aw_sets.state = "saveAwSets";
    aw_sets.anzahl_m = $('input[data-aw-settings-field="anzahl-m"]').val();
    aw_sets.stundenlohn_m = $('input[data-aw-settings-field="stundenlohn-m"]').val();
    aw_sets.preis_m = $('input[data-aw-settings-field="preis-m"]').val();
    aw_sets.anzahl_k = $('input[data-aw-settings-field="anzahl-k"]').val();
    aw_sets.stundenlohn_k = $('input[data-aw-settings-field="stundenlohn-k"]').val();
    aw_sets.preis_k = $('input[data-aw-settings-field="preis-k"]').val();
    aw_sets.anzahl_l = $('input[data-aw-settings-field="anzahl-l"]').val();
    aw_sets.stundenlohn_l = $('input[data-aw-settings-field="stundenlohn-l"]').val();
    aw_sets.preis_l = $('input[data-aw-settings-field="preis-l"]').val();
    aw_sets.anzahl_e = $('input[data-aw-settings-field="anzahl-e"]').val();
    aw_sets.stundenlohn_e = $('input[data-aw-settings-field="stundenlohn-e"]').val();
    aw_sets.preis_e = $('input[data-aw-settings-field="preis-e"]').val();


    getDBdata(URL+'blcarservice/php/functions.php', aw_sets, function (response) {
        $('#dynamicContent').load(URL+'blcarservice/modules/settingsAw.php');
    })

});


/*** ROUND COMMERCIAL WITH KOMMA REPLACE ***/
function roundCommercial(x) {
    if (isNaN(x)) { x = 0.00; }
    else { x = (Math.round(x * 100) / 100).toFixed(2).toString().replace('.', ','); }
    return x;
}


$(document).ready( function(){

    //INIT BADGE VALUES
    countInvoiceBadges();
    interval = setInterval(countInvoiceBadges,900000);

});


function getVehInfoBYFullName(vorname, nachname, callback) {
    var dataPaket =  {
        'state':'getVehicleInformationByFullNameKDNR',
        'vorname':vorname,
        'nachname':nachname
    };
    getDBdata(URL+'blcarservice/php/functionsAssignments.php', dataPaket, function (response) {
        callback(response);
    })


}


/** FILL INVOICE QTY BADGES **/
function countInvoiceBadges() {

    var dataPaket =  {
        'state':'getStatusCount'
    };
    getDBdata(URL+'blcarservice/php/functionsInvoices.php', dataPaket, function (response) {
        var data = JSON.parse(response);
        $('[data-state="0"]').html(data[0].state_0);
        $('[data-state="1"]').html(data[0].state_1);
        $('[data-state="2"]').html(data[0].state_2);
        $('[data-state="3"]').html(data[0].state_3);
    })
}


function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    date<10 ?   date = "0"+date : date = date;
    var converted_time = date + '.' + month + '.' + year;
    return converted_time;
}


function getDBdata(url, dataPaket, callback) {
    $.ajax({
        url:url,
        type:"POST",
        crossDomain:true,
        global:true,
        async:true,
        data: dataPaket,
        success:function(response) {

            if(response === "")

                return null;
            callback(response)

        }
    });
}

function getFormattedDate(date) {
    var MyDate = date;
    var MyDateString;

    MyDate.setDate(MyDate.getDate() + 20);

    MyDateString = ('0' + MyDate.getDate()).slice(-2) + '/'
        + ('0' + (MyDate.getMonth()+1)).slice(-2) + '/'
        + MyDate.getFullYear();

    return MyDate;
}



function setCurrencyEUR(value){
    return new Intl.NumberFormat('de-DE', {
        minimumFractionDigits : 2
    }).format([value]);
}



//
function validateNumber(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /^[0-9.,]+$/;
    if( !regex.test(key) ) {
        theEvent.returnValue = 0;
        doNotPushInput();
    }

    // data-key: menge - prevent ',' and '.'

    //if(evt.srcElement.dataset.key.match(/menge*/)){
       // evt.srcElement.value.replace(/,/, '');
       // evt.srcElement.value.replace(/./, '');
    //}

    function doNotPushInput() {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
