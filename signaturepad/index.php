<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Signatur</title>
  <meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="framework/bootstrap.min.css">

  <!-- jQuery library -->
  <script src="framework/jquery-3.3.1.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="framework/bootstrap.js"></script>

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <link rel="stylesheet" href="css/signature-pad.css">

  <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="css/ie9.css">
  <![endif]-->

  <style>
    .btn-primary {
      background-color: #009999;
      border-color: #009999;
      color: #FFFFFF;
    }
    .footer {
      position: fixed;
      bottom: 0;
      width: 100%;
      height: 30px;
      line-height: 30px;
      background-color: #f5f5f5;
    }
    .activeNavigationElement, .activeNavigationElement:focus, .activeNavigationElement:active, .activeNavigationElement:hover {
      background: #027a7a;
    }
    [data-navigate],[data-navigate]:hover, [data-navigate]:focus, [data-navigate]:active {
      background: #027a7a;
      border:1px solid #027a7a;
      border-radius: 0 !important;
    }

    .signature-pad {

    }
    .customToggleButton {
      position: absolute;
      top:0;
      left: 0;

    }
  </style>
  <script type="text/javascript">



  </script>
</head>
<body onselectstart="return false">


  <div class="customToggleButton">
      <button class="btn btn-primary" style="display: none" data-navigate="Zu">Zurück</button>
  </div>

  <div id="invoiceInfo" style="padding-bottom: 35px" class="row">

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h3>Kundendaten</h3>
          <table class="table table-condensed table-bordered">
              <thead>
              <tr class="hidden">
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
              </thead>
              <tbody id="kundenTabelle">
              <tr>
                  <td class="col-md-3 col-sm-3 col-xs-3" style="background: #eee">Kunde</td>
                  <td class="col-md-3 col-sm-3 col-xs-3" data-key="anrede"></td>
                  <td class="col-md-3 col-sm-3 col-xs-3" data-key="vorname"></td>
                  <td class="col-md-3 col-sm-3 col-xs-3" data-key="name"></td>
              </tr>
              <tr>
                  <td style="background: #eee">Anschrift</td>
                  <td data-key="strasse"></td>
                  <td data-key="plz"></td>
                  <td data-key="ort"></td>
              </tr>
              </tbody>
          </table>
      </div>



      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <h3>Fahrzeugdaten</h3>
        <table class="table table-condensed table-bordered">
            <thead>
            <tr>
                <th>Hersteller</th>
                <th>Typ</th>
                <th>Kennzeichen</th>
            </tr>
            </thead>
            <tbody id="vehicleList">
            <tr>
                <td data-key="hersteller"></td>
                <td data-key="typ"></td>
                <td data-key="kennzeichen"></td>
            </tr>
            </tbody>
        </table>

          <div class="">
              <div class="btn-group" style="width: 100%;">
                  <a href="#" class="btn btn-primary" style="margin-top: 35px; width: 100%;" data-key="save" data-navigate>Unterschrift setzen</a>
              </div>
          </div>

      </div>

  </div>

</div>





  <div id="signature-pad" style="display: none" class="signature-pad">
    <div class="signature-pad--body">
      <canvas></canvas>
    </div>
    <div class="signature-pad--footer">
      <div class="description">Unterschrift</div>

      <div class="signature-pad--actions">
        <div>
          <button type="button" class="btn btn-primary button clear" data-navigate="clear">Löschen</button>
        </div>
        <div>
          <button type="button" class="btn btn-primary button save" data-navigate>Unterschrift Speichern</button>
        </div>
      </div>
    </div>
  </div>

  <script src="js/signature_pad.umd.js"></script>
  <script src="js/app.js"></script>
  <script src="js/Signature.js"></script>
  <script src="js/functions.js"></script>
</body>
</html>
