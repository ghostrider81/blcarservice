<?php

include "../classes/sqlConnect.php";

function get_pending_assignment($pdo) {
    $sqlQuery = "SELECT assignment_id FROM assignment_pending WHERE status = 1";
    $stmt = $pdo->prepare($sqlQuery);
    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $assignment_id = $result[0]->assignment_id;

        get_assignment_details($pdo, $assignment_id);
    }

    else {
        echo "null";
    }
}

function get_pending_assignment_id($pdo) {
    $sqlQuery = "SELECT assignment_id FROM assignment_pending WHERE status = 1";
    $stmt = $pdo->prepare($sqlQuery);
    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    else {
	    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
	    return $result[0]->assignment_id;
    }
}



function get_assignment_details($pdo, $assignment_id) {
    $commissionID = $assignment_id;

    $sqlQuery = "SELECT * FROM auftrag LEFT JOIN positionen_auftrag ON auftrag.commission_id = positionen_auftrag.commission_id LEFT JOIN fahrzeuge ON auftrag.car_id = fahrzeuge.car_id LEFT JOIN kontakte ON auftrag.contact_id = kontakte.contact_id WHERE auftrag.commission_id = ".$commissionID;
    $stmt = $pdo->prepare($sqlQuery);
    // MySQL Query ausführen, bei Error DB Objekt löschen
    if(!$stmt->execute())
    {
        $pdo = NULL;
        echo "Bei der Abfrage ist ein Fehler unterlaufen";
    }

    // MySQL Result prüfen ob leer
    if($stmt->rowCount() > 0)
    {
        // Result ist nicht leer
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
    }

    else
    {
        // Result ist leer
        $pdo = NULL;
        echo " Assignment Details failed";
    }
}


function save_signature_filename($pdo, $filename) {
	$assignment_id = get_pending_assignment_id($pdo);
	$status_finish = 2;

    $sqlQuery = "UPDATE assignment_pending SET status = :status_finish WHERE assignment_id = :assignment_id ";
    $stmt = $pdo->prepare($sqlQuery);

    $stmt->bindParam(':assignment_id', $assignment_id);
    $stmt->bindParam(':status_finish', $status_finish);

    $stmt->execute();


    $sqlQuery2 = "UPDATE auftrag SET signature = :filename WHERE commission_id = :assignment_id ";
    $stmt2 = $pdo->prepare($sqlQuery2);

    $stmt2->bindParam(':filename', $filename);
    $stmt2->bindParam(':assignment_id', $assignment_id);

    $stmt2->execute();
}
