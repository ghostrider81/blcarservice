var state =  JSON.stringify({ 'state' : 'get_pending_assignments' });
var interval = null;


$(document).ready( function(){
    interval = setInterval(checkAssignments,5000);
});


function checkAssignments(){
    $.ajax({
        url: 'api.php?state=get_pending_assignments',
        type: 'POST',
        crossDomain:true,
        global:true,
        async:true,

        success: function(data){
            if(data != "null"){
                clearInterval(interval);

                var data = JSON.parse(data);

                $('td[data-key="anrede"]').text(data[0].anrede);
                $('td[data-key="vorname"]').text(data[0].vorname);
                $('td[data-key="name"]').text(data[0].name);
                $('td[data-key="strasse"]').text(data[0].strasse);
                $('td[data-key="plz"]').text(data[0].plz);
                $('td[data-key="ort"]').text(data[0].ort);
                $('td[data-key="hersteller"]').text(data[0].hersteller);
                $('td[data-key="typ"]').text(data[0].typ);
                $('td[data-key="kennzeichen"]').text(data[0].kennzeichen);

                console.log(data);
            }
        }
    });
}