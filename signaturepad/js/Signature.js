var Signature = function () {
    this.NAVIGATE_ID = '[data-navigate]';
    this.INVOICE_VIEW_ID = '#invoiceInfo'; 
    this.SIGNATURE_VIEW_ID = '#signature-pad';

    this.CONTACT_TABLE_ID = '#kundenTabelle';
    this.ASSIGNMENT_TABLE_ID = '#auftragsListe';
    this.VEHICLE_TABLE_ID = '#vehicleList';

    var canvas = document.querySelector("canvas");

    this.signaturePad = new SignaturePad(canvas);
    this.init();
}

Signature.prototype = {
    init: function () {
        this.setupHandler();
    },
    setupHandler: function () {
        $('body').on('click', this.NAVIGATE_ID, this.navigate.bind(this));
    },
    navigate: function (e) {
        e.preventDefault();
        var selected = $(e.currentTarget).text();

        $(this.NAVIGATE_ID).removeClass('activeNavigationElement');
        $(e.currentTarget).addClass('activeNavigationElement');

        var VIEWID = [
            $(this.INVOICE_VIEW_ID),
            $(this.SIGNATURE_VIEW_ID)
        ]
        console.log('navigate', $(e.currentTarget).text());
        switch (selected) {
            case 'Zurück' :
                VIEWID[0].css('display', 'block');
                VIEWID[1].css('display', 'none');
                $(e.currentTarget).css('display', 'none');
                break;
            case 'Unterschrift setzen' :
                VIEWID[0].css('display', 'none');
                VIEWID[1].css('display', '');
                $('button[data-navigate="Zu"]').css('display', 'block');
                resizeCanvas();
                break;
            case 'Daten anfordern' :
                var that = this;

                var dataPaket = {
                    'state': 'getInvoiceData',
                    'somevalue': 'sv'
                }
                this.connectDB(dataPaket, function (response) {
                    console.log('fired to php');

                    /*   contact info
                    $(that.CONTACT_TABLE_ID).empty();
                    $(that.CONTACT_TABLE_ID).append(
                        '<tr>' +
                        '<td style="background: #eee">Kunde</td>' +
                        '<td>Mr.</td>' +
                        '<td>Tob</td>' +
                        '<td>Quer</td>' +
                        '</tr>'
                    ).append(
                        '<tr>' +
                        '<td style="background: #eee">Anschrift</td>' +
                        '<td>Ort</td>' +
                        '<td>PLZ</td>' +
                        '<td>STR</td>' +
                        '</tr>'
                    );*/


                    // assignment List each
                    that.clearAndAppendTableRows(that.ASSIGNMENT_TABLE_ID, [
                        [{'Pos': '1'}, {'Bez': 'Reper.'}, {'Preis': '9955 €'}, {'CATID': 'Arbeitslohn'}],
                        [{'Pos': '2'}, {'Bez': 'Lackiert'}, {'Preis': '455 €'}, {'CATID': 'Lack'}],
                        [{'Pos': '3'}, {'Bez': 'Auspuff ausgetauscht'}, {'Preis': '777 €'}, {'CATID': 'Material'}]

                    ]);
                    //Vehicle List
                    that.clearAndAppendTableRows(that.VEHICLE_TABLE_ID, [
                        [{'Hersteller': 'VW'}, {'Typ': 'Opel'}, {'Kennzeichen': 'HS-23941'}],
                        [{'Hersteller': 'Toyota'}, {'Typ': 'Auris'}, {'Kennzeichen': 'HS-23941'}],
                        [{'Hersteller': 'BMW'}, {'Typ': '4er'}, {'Kennzeichen': 'HS-23941'}]

                    ]);


                });
                break;

                /** SIGNATURE CANVAS CTRL ELEMENT **/
            case 'Unterschrift Speichern' :
                $('[data-key="save"]').on('click', function() {
                   $(this).attr('disabled',true);
                });
                var image = this.signaturePad.toDataURL();
                console.log('img: ', image);
                var dataPaket = {
                    'state': 'setSignatureFile',
                    'signatureIMG': image
                }
                this.connectDB(dataPaket, function (response) {
                    console.log('fired to php ', response);
                });

                break;
            case 'Löschen' :
                this.signaturePad.clear();
                break;
        }
    },

    clearAndAppendTableRows: function (id, dataPaket) {
        $(id).empty();

        // each array
        dataPaket.forEach(function (object, index) {
            //console.log(Object.keys(object).length)
            var tr = '<tr>'

            // each json object
            object.forEach(function (data, ind) {
                var key = Object.keys(data);
                tr += '<td>' + data[key] + '</td>'
            });

            tr += '</tr>';

            $(id).append(tr);
        });

    },

    connectDB: function (dataPaket, callback) {
        $.ajax({
            url: URL + 'blcarservice/signaturepad/api.php',
            type: "POST",
            crossDomain: true,
            global: true,
            async: true,
            data: dataPaket,
            success: function (response) {
                return callback(response) || null;
            },
            error: function (e) {
                console.log('connectDB ERROR : ' + e);
            }
        });
    }
}
var signature = new Signature();