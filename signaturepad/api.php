<?php
/**
 * Created by PhpStorm.
 * User: TylerRodney
 * Date: 23.11.2018
 * Time: 20:00
 */
include "php/functionsSignature.php";

$state = $_REQUEST['state'];

switch ($state)
{
    case 'setSignatureFile' :
        echo 'setSignatureFile in php has been started';
        // requires php5
        define('UPLOAD_DIR', 'images/');
        $img = $_REQUEST['signatureIMG'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . uniqid() . '.png';
        $success = file_put_contents($file, $data);

        save_signature_filename($pdo, $file);

        echo get_pending_assignment_id($pdo);

        break;


    case 'get_pending_assignments' :

        get_pending_assignment($pdo);

        break;

}